# JaxBson

JaxBson is a Java component to convert Java objects to BSON and back again using JAXB annotations.

##### Author/Copyright

Vincenzo Zocca

Qnixyz GmbH

Switzerland

qnixyz@qnixyz.org

## 1 TL;DR

You have a Java class with JAXB annotations:

```
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ClassWithJAXB {

  Date date;
  
  String foo;

  @JaxBsonName(name = "_id") // NOTE: This is a JaxBson annotation to overwrite the JAXB name
  @XmlAttribute
  String id;
}
 ```
 
When you marshal a Java object to XML:

```
    final JAXBContext jaxbContext = JAXBContext.newInstance(ClassWithJAXB.class);
    final Marshaller m = jaxbContext.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    final ClassWithJAXB o = new ClassWithJAXB();
    o.id = "my-id";
    o.date = new Date(0L);
    o.foo = "my-foo";
    m.marshal(o, System.out);
```

You get:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<classWithJAXB id="my-id">
    <date>1970-01-01T01:00:00+01:00</date>
    <foo>my-foo</foo>
</classWithJAXB>
```

With JaxBson you can convert the Java object directly to BSON:

```
    final JaxBsonContext jaxBsonContext = JaxBsonContext.newInstance(ClassWithJAXB.class);
    final Document bson = jaxBsonContext.toBson(o);
    System.out.println(bson.toJson());
```

And then you get a BSON object which you can store in MongoDB:

```
{"type": "classWithJAXB", "_id": "my-id", "date": {"$date": "1970-01-01T00:00:00Z"}, "foo": "my-foo"}
```

Of course you can convert the BSON object back to the Java object:

```
    final ClassWithJAXB oBack = (ClassWithJAXB) jaxBsonContext.toObject(bson);
    m.marshal(oBack, System.out);
```

And you get back:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<classWithJAXB id="my-id">
    <date>1970-01-01T01:00:00+01:00</date>
    <foo>my-foo</foo>
</classWithJAXB>
```

You would be tempted to use Eclipselink MOXy and generate JSON and store that in MongoDB:

```
    final JAXBContext jaxbContext = JAXBContext.newInstance(ClassWithJAXB.class);
    final Marshaller m = jaxbContext.createMarshaller();
    m.setProperty(JAXBContextProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
    m.setProperty(JAXBContextProperties.JSON_INCLUDE_ROOT, false);
...
    m.marshal(o, System.out);
```

Which would output this JSON:

```
{"id":"my-id","date":"1970-01-01T01:00:00+01:00","foo":"my-foo"}
````

That's pretty good but there are a few drawbacks.

<ul>
<li>You loose the direct information that JSON field `date` is of type date</li>
<li>Also, you'd like to rename the JSON field `id` into MongoDB's well known `_id` field</li>
</ul>

JaxBson can take care of these two and perhaps even a few other issues.

## 2 Restrictions

### 2.1 Namespaces

Namespaces are not supported like they are in XML. Mostly this isn't an issue and there are practical solutions to working with them.

#### 2.1.1 Resolve Clashing Name Issues Using `@JaxBsonName`

Both attributes and elements are mapped to BSON field names.

Following field definitions are OK for generating XML:

```
  @XmlAttribute(name = "bsonClash")
  private String bsonClashAttribute1;

  @XmlAttribute(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashAttribute2;

  @XmlElement(name = "bsonClash")
  private String bsonClashElement1;

  @XmlElement(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashElement2;
```

And would generate XML like:

```
 ... xmlns:ns2="http://www.qnixyz.org/ns2" ... bsonClash="bsonClashAttribute1-value" ns2:bsonClash="bsonClashAttribute2-value" ...
  <bsonClash>bsonClashElement1-value</bsonClash>
  <ns2:bsonClash>bsonClashElement2-value</ns2:bsonClash>
```

But in BSON all Java fields would resolve in the same field name and cause clashes.
The `@JaxBsonName` annotation can provide a solution:

```
  @JaxBsonName(name = "bsonClashAttribute1")
  @XmlAttribute(name = "bsonClash")
  private String bsonClashAttribute1;

  @JaxBsonName(name = "bsonClashAttribute2")
  @XmlAttribute(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashAttribute2;

  @JaxBsonName(name = "bsonClashElement1")
  @XmlElement(name = "bsonClash")
  private String bsonClashElement1;

  @JaxBsonName(name = "bsonClashElement2")
  @XmlElement(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashElement2;
```

#### 2.1.2 Resolve Clashing Name Issues in `@XmlAnyAttribute` fields

The `@JaxBsonName` annotation isn't suited for `@XmlAnyAttribute` fields. Instead the `@JaxBsonXmlAnyAttributeMappings` and `@JaxBsonXmlAnyAttributeMapping` annotations should be used to map namespace-URIs to prefixes for BSON field names.

```
  @XmlAnyAttribute
  @JaxBsonXmlAnyAttributeMappings(mappings = { //
      @JaxBsonXmlAnyAttributeMapping(bsonPrefix = "", namespaceURI = "urn:other"), //
      @JaxBsonXmlAnyAttributeMapping(bsonPrefix = "o2:", namespaceURI = "urn:other2"), //
  })
  private Map<QName, String> anyAttribute;
```

Setting `anyAttribute`:

```
    anyAttribute = new HashMap<>();
    anyAttribute.put(new QName("urn:other", "anyAttribute1"), "anyAttributeV1");
    anyAttribute.put(new QName("urn:other2", "anyAttribute2"), "anyAttributeV2");

```

Results in BSON fields:

```
 "anyAttribute1": "anyAttributeV1",
 "o2:anyAttribute2": "anyAttributeV2"
```
 
### 2.2 XmlAccessorType

Until version 2.0.0, all classes must have XmlAccessorType set to field: `@XmlAccessorType(XmlAccessType.FIELD)`. Starting from version 2.0.0 this restriction is dropped.

### 2.3 JAXB method annotations

JAXB method annotations are NOT supported.

### 2.4 Supported JAXB annotations

<ul>
<li>XmlAnyAttribute</li>
<li>XmlAttribute</li>
<li>XmlElement</li>
<li>XmlElementRef</li>
<li>XmlElementRefs</li>
<li>XmlElementWrapper</li>
<li>XmlElements</li>
<li>XmlRootElement</li>
<li>XmlTransient</li>
<li>XmlValue</li>
<li>XmlJavaTypeAdapter: Only on field</li>
</ul>

### 2.5 Not (yet) Supported JAXB annotations

<ul>
<li>XmlAnyElement</li>
<li>XmlJavaTypeAdapter: On package</li>
</ul>

### 2.6 Not (yet) Supported JAXB types

<ul>
<li>JAXBElement</li>
</ul>

## 3 Main Use Case

The main use case is if you JAXB annotated Java classes and you want to persist objects in a MongoDB collection.

You can also persist objects of classes without JAXB annotations by having class and field properties in a configuration, without needing to alter existing classes.

### 3.1 Approach Using JaxBson

JaxBson allows you to convert directly from Java objects to BSON. There are annotations available to make this process as smooth as possible.

You might want to allow for the ID field in your model to be named `_id` in the MongoDB collection without changing the JAXB behavior. You can use the `JaxBsonName` annotation for that.

The advantages with this approach are:

1. Type information is kept. MongoDB's recommended types are used.

2. Type mismatches are taken car of where possible.

3. You can customize the conversion from Java object to BSON and back

4. Performance should be better and the process of conversion should be quicker

### 3.2 Approach Using Json as Intermediate Format

You can think of marshaling objects to Json using Eclipselink MOXy and then use the resulting Json to feed into MongoDB.

The drawbacks with this approach are:

1. You tend to loose type information.
<ul>
<li>Dates/times may be written out as ISO 8601 string and importing them as such goes hand in hand with guessing.</li>
<li>Same for numbers. Which type do you want?</li>
</ul>

2. The process requires more performance and takes longer as you have two phases

## 4 How Java Object Fields Are Mapped to BSON Fields

Essentially the java fields from classes -and their superclasses- are mapped to simple BSON fields, BSON Objects, List of simple BSON fields and lists of BSON objects.

### 4.1 Mapping of Simple Fields

Complex fields are fields that resolve to a simple type. (Theoretically you can map the value of a complex field to a simple type through an `XmlAdapter`)

#### 4.1.1 Boolean Fields

Boolean fields -object and primitive- are mapped to BSON Boolean. Arrays and collection of boolean values are mapped to BSON list of Boolean.

#### 4.1.2 Byte Fields

Byte fields -object and primitive- are mapped to BSON Binary.

Arrays and collection of byte values are also mapped to BSON Binary, as a Binary is essentially a container for bytes.

#### 4.1.3 Character Fields

Character fields -object and primitive- are mapped to BSON String.

Arrays and collection of character values are also mapped to BSON String, as a string is essentially a collection or an array of characters.

#### 4.1.4 Date Based Fields

Date based fields are mapped to BSON Dates. Arrays and collection of date based values are mapped to BSON list of Date.

Supported date based types:

1. Calendar
2. Date
3. GregorianCalendar
4. XMLGregorianCalendar

#### 4.1.5 Number Based Fields

Number based fields are mapped to BSON numbers. Arrays and collection of number based values are mapped to BSON list of number.

Supported number based types:

1. BigDecimal ==> Mapped to Decimal128
2. BigInteger ==> Mapped to Decimal128
3. Decimal128 ==> Mapped to Decimal128
4. Double object and primitive ==> Mapped to Double
5. Float object and primitive ==> Mapped to Double
6. Integer object and primitive ==> Mapped to Integer
7. Long object and primitive ==> Mapped to Long
8. Short object and primitive ==> Mapped to Integer

#### 4.1.6 String Based Fields

String based fields are mapped to BSON strings. Arrays and collection of string based values are mapped to BSON list of string.

Supported string based types:

1. File
2. String
3. URI
4. URL

### 4.2 Mapping of Complex Fields

Complex fields are fields that resolve to a complex type. (Theoretically you can map a simple type to a complex type through an `XmlAdapter`)

Complex type are mapped to BSON Documents. Arrays and collection of complex values are mapped to BSON list of Document.

### 4.3 How BSON Field Names Are Resolved

The name of fields with a value of one type is determined as follows (first success):

1. Through JaxBsonName annotation
2. Through XmlElementWrapper annotation
3. Through XmlElement annotation
4. Through XmlElementRef annotation
5. Through XmlAttribute annotation
6. Through XmlValue annotation. In which case the name is `_`.
7. Through field name. In which case the name is the field name itself.
8. Note that `XmlAnyAttribute` and `XmlAnyElement` allow for different names. These annotations are not yet supported.

### 4.4 How BSON Document Type Names Are Resolved

The name of the BSON Document types is determined as follows (first success):

1. Through JaxBsonName annotation
2. Through XmlType annotation
3. Through type/class name. In which case the name is the simple type name itself with a lowercase first character.

### 4.5 The `type` BSON Field in BSON Document

To know the type of a BSON Document a field named `type` is added. You can change the name of the field in the context configuration in `typeFieldName`.

You can also switch on the smart field option `allowSmartTypeField`. Consider that doing so may hinder you in the future if you want to have multiple types of Documents inside the same MongoDB collection or inside the BSON Document.

### 4.6 The `_` BSON Field in BSON Document

A BSON field named `_` is used to persist the value of `XmlValue` fields. You can change the name of the field in the context configuration in `xmlValueFieldName`.

## 5 JaxBson Annotations

The behavior of JaxBson can be manipulated through annotations. JaxBson annotations can either be added to the Java code but they can also be specified in the JaxBson configuration. By defining annotations in the JaxBson configuration the Java classes in context need no changes for JaxBson. Annotations in the JaxBson configuration have precedence over annotations in the Java classes.

### 5.1 JaxBsonIgnoreTransient

Annotation `JaxBsonIgnoreTransient` is to ignore transient properties of fields.

### 5.2 JaxBsonName

Annotation `JaxBsonName` to force a name on fields or on types. If JAXB `JaxBsonConfiguration` this annotation can be set for fields by using `JaxBsonNameFieldMap` and for types by using `JaxBsonNameTypeMap`. Only during 

### 5.3 JaxBsonNumberHint

Annotation `JaxBsonNumberHint` to control the conversion of numbers to BSON. This annotation is to tell JaxBson the precision of the number, the rounding mode and whether a number is to be converted to string.

Typically `JaxBsonNumberHint` is used to control the behavior of `BigDecimal`. By default `BigDecimal` is converted to `Decimal128` which has a maximal precision of 34 and an exception will be thrown if a `BigDecimal` with a higher precision is supplied. You may want to reduce the precision or convert to string. You should make a trade off whether numeric operations are favored over precision.

### 5.4 JaxBsonToBsonPost

Annotation `JaxBsonToBsonPost` to let a method be called after the object was converted to BSON. Zero or one parameter of type `JaxBsonContext` is allowed.

### 5.5 JaxBsonToBsonPre

Annotation `JaxBsonToBsonPre` to let a method be called before the object will be converted to BSON. Zero or one parameter of type `JaxBsonContext` is allowed.

### 5.6 JaxBsonToObjectPost

Annotation `JaxBsonToObjectPost` to let a method be called after the BSON object was converted to Java object. Zero or one parameter of type `JaxBsonContext` is allowed.

### 5.7 JaxBsonToObjectPre

Annotation `JaxBsonToObjectPre` to let a method be called before the BSON object will be converted to Java object. Zero or one parameter of type `JaxBsonContext` is allowed.

### 5.8 JaxBsonTransient

Annotation `JaxBsonTransient` to force fields to be transient.

### 5.9 JaxBsonXmlAnyAttributeMapping

Annotation `JaxBsonXmlAnyAttributeMapping` map XML namespace-URIs to prefixes for BSON attributes.

### 5.10 JaxBsonXmlAnyAttributeMappings

Annotation `JaxBsonXmlAnyAttributeMappings` contains an array of `JaxBsonXmlAnyAttributeMapping`.

## 6 A Few Words On `@JaxBsonToBsonPost`, `@JaxBsonToBsonPre`, `@JaxBsonToObjectPost` and `@JaxBsonToObjectPre`

You can use these annotations to generate prettier BSON without affecting the XML.

Consider a simple `String` to `String` map in JAXB. It is usually implemented using a `Map` and a `@XmlJavaTypeAdapter`:

```
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JaxbMapExample {

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapElement {

    @XmlAttribute
    public String key;

    @XmlAttribute
    public String value;

    public MapElement() {}

    public MapElement(final String key, final String value) {
      this.key = key;
      this.value = value;
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapList {
    public List<MapElement> element = new ArrayList<>();
  }

  public static class MapXmlAdapter extends XmlAdapter<MapList, Map<String, String>> {

    @Override
    public MapList marshal(final Map<String, String> v) throws Exception {
      if (v == null) {
        return null;
      }
      final MapList ret = new MapList();
      v.forEach((key, value) -> ret.element.add(new MapElement(key, value)));
      return ret;
    }

    @Override
    public Map<String, String> unmarshal(final MapList v) throws Exception {
      if (v == null || v.element == null) {
        return null;
      }
      final Map<String, String> ret = new HashMap<>();
      v.element.forEach(e -> ret.put(e.key, e.value));
      return ret;
    }
  }

  @XmlJavaTypeAdapter(MapXmlAdapter.class)
  public Map<String, String> map = new HashMap<>();
}
```

The XML output you get from this actually is human readable. There's a `map` that contains `element`s with `key`s and `value`s:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<jaxbMapExample>
    <map>
        <element key="key1" value="value1"/>
        <element key="key2" value="value2"/>
    </map>
</jaxbMapExample>
```

However, the BSON output using standard JaxBson is not as easy to read as the XML version:

```
{
  "type": "jaxbMapExample",
  "map": {
    "type": "mapList",
    "element": [
      {
        "type": "mapElement",
        "key": "key1",
        "value": "value1"
      },
      {
        "type": "mapElement",
        "key": "key2",
        "value": "value2"
      }
    ]
  }
}
```

To get nicer BSON, you can let JaxBson perform actions on your object before and after the Java object is converted to BSON and vice versa.

Let the `map` field be `@JaxBsonTransient` so that JaxBson ignores it. Create a field `mapList` that is `@XmlTransient` so that JAXB ignores it. But make it `@JaxBsonIgnoreTransient` so that JaxBson will consider it nevertheless. And perhaps use `@JaxBsonName` to give it a readable name.
Implement `@JaxBsonToBsonPost`, `@JaxBsonToBsonPre`, `@JaxBsonToObjectPost` and `@JaxBsonToObjectPre` to fill `mapList` before generating BSON and to fill `map` after generating the Java object.

```
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JaxbMapPrePostExample {

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapElement {

    @XmlAttribute
    public String key;

    @XmlAttribute
    public String value;

    public MapElement() {}

    public MapElement(final String key, final String value) {
      this.key = key;
      this.value = value;
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  public static class MapList {
    public List<MapElement> element = new ArrayList<>();
  }

  public static class MapXmlAdapter extends XmlAdapter<MapList, Map<String, String>> {

    @Override
    public MapList marshal(final Map<String, String> v) throws Exception {
      if (v == null) {
        return null;
      }
      final MapList ret = new MapList();
      v.forEach((key, value) -> ret.element.add(new MapElement(key, value)));
      return ret;
    }

    @Override
    public Map<String, String> unmarshal(final MapList v) throws Exception {
      if (v == null || v.element == null) {
        return null;
      }
      final Map<String, String> ret = new HashMap<>();
      v.element.forEach(e -> ret.put(e.key, e.value));
      return ret;
    }
  }

  @JaxBsonTransient
  @XmlJavaTypeAdapter(MapXmlAdapter.class)
  public Map<String, String> map = new HashMap<>();

  @JaxBsonName(name = "map")
  @JaxBsonIgnoreTransient
  @XmlTransient
  private List<MapElement> mapList = new ArrayList<>();

  @JaxBsonToBsonPost
  private void myJaxBsonToBsonPost() {
    mapList = null;
  }

  @JaxBsonToBsonPre
  private void myJaxBsonToBsonPre() {
    if (map != null) {
      mapList = new ArrayList<>();
      map.forEach((key, value) -> mapList.add(new MapElement(key, value)));
    }
  }

  @JaxBsonToObjectPost
  private void myJaxBsonToObjectPost() {
    if (mapList != null) {
      map = new HashMap<>();
      mapList.forEach(e -> map.put(e.key, e.value));
    }
  }

  @JaxBsonToObjectPre
  private void myJaxBsonToObjectPre() {
    mapList = null;
  }
}
```

Now the XML output remains as it was before:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<jaxbMapPrePostExample>
    <map>
        <element key="key1" value="value1"/>
        <element key="key2" value="value2"/>
    </map>
</jaxbMapPrePostExample>
```

But the BSON looks a bit better as `map` now is a list of map elements:

```
{
  "type": "jaxbMapPrePostExample",
  "map": [
    {
      "type": "mapElement",
      "key": "key1",
      "value": "value1"
    },
    {
      "type": "mapElement",
      "key": "key2",
      "value": "value2"
    }
  ]
}
```

## 7 JaxBson Configuration

The behavior of JaxBson can be controlled through the JaxBson configuration (`JaxBsonConfiguration`). All configuration properties have sensible default values and can be changed at will.

Note that the JaxBson context uses configuration properties during instantiation and hence it is best to first create a specific JaxBson configuration before creating the JaxBson context.

### 7.1 JaxBson Annotations

All JaxBson annotations can both be defined in the Java classes and in the JaxBson Configuration. Classic annotations in Java classes may have the typical advantage of having all definitions near to one another. Annotations outside of the Java classes may be useful in cases whereby the Java code may not be changed or in cases where annotations in Java classes are undesired.

### 7.2 Conversions to/from Bson

All conversions from Java to Bson and vice versa are implemented through Functional Interfaces and Lambda Expressions. Conversions can be replaced at will but they should be considered an advanced topic.

#### 7.2.1 Date Based Conversions

Date based values like `java.util.Calendar`, `java.util.Date` and `javax.xml.datatype.XMLGregorianCalendar` are converted to `java.util.Calendar` in Bson.

#### 7.2.2 Number Based Conversions

Number based values like all Java primitive number and their related object types are converted to the best fitting Bson number type, including `org.bson.types.Decimal128`. The big math types `java.math.BigDecimal` and `java.math.BigInteger` are supported.

#### 7.2.3 String Based Conversions

Well supported string based values are `java.lang.String`, `java.io.File`, `java.net.URI` and `java.net.URL` and need no XML adapter. When converting from Bson back to Java object, basically any value can be converted to `String` is the field is defined as such.

### 7.3 Marshalling/Unmarshalling to/from XML

The JaxBson Configuration can be marshalled/unmarshalled to/from XML.

Note that Lambdas are __NOT__ marshalled/unmarshalled.

Following code:

```
    final Marshaller m = JaxBsonConfiguration.getJaxbContext().createMarshaller();
    m.setSchema(JaxBsonConfiguration.getXmlSchema());
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    m.setEventHandler(new ValidationEventHandler() {

      @Override
      public boolean handleEvent(final ValidationEvent event) {
        if (event.getSeverity() >= ValidationEvent.ERROR) {
          return false;
        }
        return true;
      }
    });

    final JaxBsonConfiguration cfg = new JaxBsonConfiguration();
    m.marshal(cfg, System.out);
  }
```

results in:

```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<jaxBsonConfiguration allowSmartTypeField="false" id="5ef48f92-24a8-4e56-a2bc-643ce2ac7f1c" typeFieldName="type" valueBooleanPrimBooleanNull="false" valueByteBooleanFalse="0" valueByteBooleanTrue="1" valueBytePrimBooleanNull="0" valueBytePrimNull="0" valueCharacterBooleanFalse="45" valueCharacterBooleanTrue="43" valueCharacterPrimBooleanNull="0" valueCharacterPrimNull="32" valueNumberBooleanFalse="0" valueNumberBooleanTrue="1" valueNumberPrimBooleanNull="0" valueNumberPrimNull="0" valueStringBooleanFalse="false" valueStringBooleanTrue="true" xmlValueFieldName="_"/>
```

### 7.4 Converting to/from Bson

The JaxBson Configuration can be converted to/from Bson and hence can be written/read to/from MongoDB.

Notes:

- Lambdas are __NOT__ converted
- If applicable, consider the implications of using custom values for `allowSmartTypeField` and `typeFieldName`

Following code:

```
    final JaxBsonContext ctx = JaxBsonContext.newInstance(JaxBsonConfiguration.class);
    final JaxBsonConfiguration cfg = new JaxBsonConfiguration();
    System.out.println(ctx.toBson(cfg).toJson());
```

results in:

```
{"type": "jaxBsonConfiguration", "_id": "f6b0179d-b775-49cf-896f-a3a5cf3240ae", "allowSmartTypeField": false, "typeFieldName": "type", "valueBooleanPrimBooleanNull": false, "valueByteBooleanFalse": {"$binary": {"base64": "AA==", "subType": "00"}}, "valueByteBooleanTrue": {"$binary": {"base64": "AQ==", "subType": "00"}}, "valueBytePrimBooleanNull": {"$binary": {"base64": "AA==", "subType": "00"}}, "valueBytePrimNull": {"$binary": {"base64": "AA==", "subType": "00"}}, "valueCharacterBooleanFalse": "-", "valueCharacterBooleanTrue": "+", "valueCharacterPrimBooleanNull": "\u0000", "valueCharacterPrimNull": " ", "valueNumberBooleanFalse": "0", "valueNumberBooleanTrue": "1", "valueNumberPrimBooleanNull": "0", "valueNumberPrimNull": "0", "valueStringBooleanFalse": "false", "valueStringBooleanTrue": "true", "xmlValueFieldName": "_"}
```

## 8 Performance

Method `org.qnixyz.jbson.JaxBsonContextPerformanceTest.test()` compares JAXB and JaxBson. See the source in project directory `src/test/java`.

The result show that both are in the same ballpark. More important than these results is the fact that by using JaxBson you may well skip one conversion step.

| Description                                   | Value    |
| --------------------------------------------- | --------:|
| jaxbMarshalDuration for 1000000 documents     |  61680ms |
| jaxbUnmarshalDuration for 1000000 documents   | 293570ms |
| jaxBsonToBsonDuration for 1000000 documents   |  36860ms |
| jaxBsonToObjectDuration for 1000000 documents | 205940ms |
| jaxbMarshalDuration/jaxBsonToBsonDuration     |     1,67 |
| jaxBsonToBsonDuration/jaxbMarshalDuration     |     0,60 |
| jaxbUnmarshalDuration/jaxBsonToObjectDuration |     1,43 |
| jaxBsonToObjectDuration/jaxbUnmarshalDuration |     0,70 |

