package org.qnixyz.jbson.impl;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import static org.qnixyz.jbson.impl.Utils.lcFirst;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.xml.namespace.QName;
import org.bson.Document;
import org.qnixyz.jbson.JaxBsonException;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.helpers.JaxBsonJavaTypeAdapterHelper;
import org.qnixyz.jbson.helpers.JaxBsonNameHelper;
import org.qnixyz.jbson.helpers.JaxBsonSeeAlsoHelper;
import org.qnixyz.jbson.helpers.XmlAnyAttributeHelper;
import org.qnixyz.jbson.helpers.XmlAnyElementHelper;
import org.qnixyz.jbson.helpers.XmlElementHelper;
import org.qnixyz.jbson.helpers.XmlElementRefHelper;
import org.qnixyz.jbson.helpers.XmlElementRefsHelper;
import org.qnixyz.jbson.helpers.XmlElementsHelper;
import org.qnixyz.jbson.helpers.XmlRootElementHelper;
import org.qnixyz.jbson.helpers.XmlTransientHelper;
import org.qnixyz.jbson.helpers.XmlValueHelper;

/**
 * Field descriptor.
 *
 * @author Vincenzo Zocca
 */
class FieldDescriptor {

  private final SortedMap<String, FieldSubDescriptor> bsonFieldNameMap = new TreeMap<>();

  private final Map<Class<?>, FieldSubDescriptor> bsonFieldTypeMap = new HashMap<>();

  private final Constructor<Collection<Object>> collectionConstructor;

  private final JaxBsonContextImpl ctx;

  private final Field field;

  private final JaxBsonFieldContextImpl fieldCtx;

  private boolean ignore;

  private final Class<?> mainType;

  private boolean multiBsonFieldNames;

  private NamespaceMap namespaceMap;

  private boolean oneMainType;

  private final Set<Class<?>> referredTypes = new HashSet<>();

  private ValueContainerType valueContainerType;

  protected FieldDescriptor(final JaxBsonContextImpl ctx, final Field field) {
    this.ctx = Objects.requireNonNull(ctx, "Supplied parameter 'ctx' is null");
    this.field = Objects.requireNonNull(field, "Supplied parameter 'field' is null");
    this.field.setAccessible(true);
    this.fieldCtx = createJaxBsonFieldContext();
    checkAnnotations();
    setIgnore();
    if (this.ignore) {
      this.mainType = null;
      this.collectionConstructor = null;
      return;
    }
    checkXmlAnnotations();
    setValueContainerType();
    setOneMainType();
    this.mainType = makeMainType();
    setMultiBsonFieldNames();
    if (this.fieldCtx.getXmlAnyAttribute() != null) {
      checkXmlAnyAttributeField();
      analyzeXmlAnyAttribute();
    } else if (this.multiBsonFieldNames) {
      analyzeMultiBsonFieldNames();
    } else {
      analyzeSingleBsonFieldName();
    }
    this.collectionConstructor = makeCollectionConstructor();
    addReferredTypes();
  }

  private void addFieldSubDescriptor(final FieldSubDescriptor fds) {
    checkDuplicateNames(fds);
    this.bsonFieldNameMap.put(fds.getName(), fds);
    this.bsonFieldTypeMap.put(fds.getMainType(), fds);
    this.referredTypes.addAll(fds.getReferredTypes());
  }

  private void addReferredTypes() {
    addReferredTypes(this.fieldCtx.getJaxBsonSeeAlso());
  }

  private void addReferredTypes(final JaxBsonSeeAlso seeAlso) {
    if ((seeAlso == null) || (seeAlso.value() == null) || (seeAlso.value().length == 0)) {
      return;
    }
    for (final Class<?> cls : seeAlso.value()) {
      this.referredTypes.add(cls);
    }
  }

  protected void addXmlAnyAttributes(final Document bson, final Object o) {
    if (this.fieldCtx.getXmlAnyAttribute() == null) {
      throw new IllegalStateException("This is a bug. Field has no XmlAnyAttribute annotation.");
    }
    try {
      @SuppressWarnings("unchecked")
      final Map<QName, Object> map = (Map<QName, Object>) this.field.get(o);
      if (map == null) {
        return;
      }
      map.forEach((qName, value) -> {
        final String bsonFieldName = this.namespaceMap.getBsonFieldName(qName);
        if (bson.containsKey(bsonFieldName)) {
          throw new JaxBsonException.JaxBsonNameClashException(
              "Json field name '" + bsonFieldName + "' results multiple time through field " + this
                  + ". Bson so far: " + bson.toJson());
        }
        bson.put(bsonFieldName, value.toString());
      });
    } catch (IllegalArgumentException | IllegalAccessException e) {
      throw new IllegalStateException("Unexpected exception", e);
    }
  }

  protected void addXmlAnyAttributes(final SortedMap<String, String> xmlAttributeMap,
      final Object o) {
    final Map<QName, Object> map = new HashMap<>();
    xmlAttributeMap.forEach((bsonFieldName, bsonValue) -> {
      final QName qName = this.namespaceMap.getQName(bsonFieldName);
      map.put(qName, bsonValue);
    });
    try {
      this.field.set(o, map);
    } catch (IllegalArgumentException | IllegalAccessException e) {
      throw new IllegalStateException("Unexpected exception", e);
    }
  }

  private void analyzeMultiBsonFieldNames() {
    if (this.fieldCtx.getXmlElements() != null) {
      analyzeMultiBsonFieldNamesXmlElements();
      return;
    }
    if (this.fieldCtx.getXmlElementRefs() != null) {
      analyzeMultiBsonFieldNamesXmlElementRefs();
      return;
    }
    throw new UnsupportedOperationException("This is a bug");
  }

  private void analyzeMultiBsonFieldNamesXmlElementRefs() {
    for (final XmlElementRefHelper xer : this.fieldCtx.getXmlElementRefs().value()) {
      final String name = makeName(xer);
      addFieldSubDescriptor(
          new FieldSubDescriptor(this.ctx, this, name, this.valueContainerType, xer.type(), null));
    }
  }

  private void analyzeMultiBsonFieldNamesXmlElements() {
    for (final XmlElementHelper xe : this.fieldCtx.getXmlElements().value()) {
      final String name = makeName(xe);
      addFieldSubDescriptor(
          new FieldSubDescriptor(this.ctx, this, name, this.valueContainerType, xe.type(), null));
    }
  }

  private void analyzeSingleBsonFieldName() {
    final String name = makeSingleName();
    addFieldSubDescriptor(new FieldSubDescriptor(this.ctx, this, name, this.valueContainerType,
        this.mainType, this.fieldCtx.getJaxBsonXmlAdapter()));
  }

  private void analyzeXmlAnyAttribute() {
    if (this.fieldCtx.getJaxBsonXmlAnyAttributeMappingCfg() != null) {
      this.namespaceMap =
          new NamespaceMap(this, this.fieldCtx.getJaxBsonXmlAnyAttributeMappingCfg());
    } else if (this.fieldCtx.getJaxBsonXmlAnyAttributeMapping() != null) {
      this.namespaceMap = new NamespaceMap(this, this.fieldCtx.getJaxBsonXmlAnyAttributeMapping());
    } else if (this.fieldCtx.getJaxBsonXmlAnyAttributeMappingsCfg() != null) {
      this.namespaceMap =
          new NamespaceMap(this, this.fieldCtx.getJaxBsonXmlAnyAttributeMappingsCfg());
    } else if (this.fieldCtx.getJaxBsonXmlAnyAttributeMappings() != null) {
      this.namespaceMap = new NamespaceMap(this, this.fieldCtx.getJaxBsonXmlAnyAttributeMappings());
    } else {
      this.namespaceMap = new NamespaceMap(this);
    }
  }

  private void checkAnnotations() {
    if (((this.fieldCtx.getJaxBsonTransient() != null)
        || (this.fieldCtx.getJaxBsonTransientCfg() != null))
        && ((this.fieldCtx.getJaxBsonIgnoreTransient() != null)
            || (this.fieldCtx.getJaxBsonIgnoreTransientCfg() != null))) {
      throw new IllegalStateException(
          "Both annotations " + JaxBsonTransient.class.getName() + " and "
              + JaxBsonIgnoreTransient.class.getName() + " specified for field " + this.field);
    }
  }

  private void checkDuplicateNames(final FieldSubDescriptor fds) {
    final FieldSubDescriptor dup = this.bsonFieldNameMap.get(fds.getName());
    if (dup != null) {
      throw new IllegalStateException("Multiple fields of name '" + fds.getName() + "' in class "
          + this.field.getDeclaringClass().getName() + ". E.g. '" + dup + "' and '" + fds + "'");
    }
  }

  private void checkXmlAnnotations() {

    if (this.fieldCtx.getXmlAnyAttribute() == null) {
      if ((this.fieldCtx.getJaxBsonXmlAnyAttributeMappingCfg() != null)
          || (this.fieldCtx.getJaxBsonXmlAnyAttributeMapping() != null)) {
        throw new IllegalStateException("JaxBsonXmlAnyAttributeMapping annotation found in field "
            + this.field + " which hasn't got a XmlAnyAttribute annotation");
      }
      if ((this.fieldCtx.getJaxBsonXmlAnyAttributeMappingsCfg() != null)
          || (this.fieldCtx.getJaxBsonXmlAnyAttributeMappings() != null)) {
        throw new IllegalStateException("JaxBsonXmlAnyAttributeMappings annotation found in field "
            + this.field + " which hasn't got a XmlAnyAttribute annotation");
      }
    } else if ((this.fieldCtx.getJaxBsonXmlAnyAttributeMappingCfg() != null)
        && (this.fieldCtx.getJaxBsonXmlAnyAttributeMapping() != null)
        && (this.fieldCtx.getJaxBsonXmlAnyAttributeMappingsCfg() != null)
        && (this.fieldCtx.getJaxBsonXmlAnyAttributeMappings() != null)) {
      throw new IllegalStateException(
          "Both JaxBsonXmlAnyAttributeMappings and JaxBsonXmlAnyAttributeMappings annotations found in field "
              + this.field);
    }

    if (this.fieldCtx.getXmlAnyElement() != null) {
      throw new IllegalStateException(
          "XmlAnyElement annotation not supported. Found in field " + this.field);
    }
  }

  private void checkXmlAnyAttributeField() {
    if (this.field.getType().isArray()) {
      throw new IllegalStateException("XmlAnyAttribute field " + this + " is an array");
    }
    if (!Map.class.isAssignableFrom(this.field.getType())) {
      throw new IllegalStateException("XmlAnyAttribute field " + this + " is not of interface type "
          + Map.class.getName() + " but of type " + this.field.getType().getName());
    }
    if (!this.field.getType().isInterface()
        && !HashMap.class.isAssignableFrom(this.field.getType())) {
      throw new IllegalStateException("XmlAnyAttribute field " + this + " is neither of type "
          + Map.class.getName() + " nor of type " + HashMap.class.getName() + " but of type "
          + this.field.getType().getName());
    }
    final Class<?> genericTypeKey = getGenericType(this.field.getGenericType(), 0);
    if (genericTypeKey == null) {
      throw new IllegalStateException("XmlAnyAttribute field " + this + " has no key-generic type");
    }
    if (!QName.class.isAssignableFrom(genericTypeKey)) {
      throw new IllegalStateException("XmlAnyAttribute field " + this + " has key-generic type of "
          + genericTypeKey.getName() + " instead of " + QName.class.getName());
    }
    final Class<?> genericTypeValue = getGenericType(this.field.getGenericType(), 1);
    if (genericTypeValue == null) {
      throw new IllegalStateException(
          "XmlAnyAttribute field " + this + " has no value-generic type");
    }
    if (!String.class.isAssignableFrom(genericTypeValue)
        && !genericTypeValue.equals(Object.class)) {
      throw new IllegalStateException("XmlAnyAttribute field " + this
          + " has value-generic type of " + genericTypeValue.getName() + " instead of "
          + String.class.getName() + " or " + Object.class.getName());
    }
  }

  protected Collection<Object> collectionInstance() {
    if (!this.valueContainerType.isCollection()) {
      throw new IllegalStateException("This is a bug. Field is not a collection.");
    }
    if (this.collectionConstructor == null) {
      return this.valueContainerType.collectionInstance();
    }
    try {
      return this.collectionConstructor.newInstance();
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("Failed to instantiate object of collection class '"
          + this.field.getType().getName() + "'.", e);
    }
  }

  private JaxBsonFieldContextImpl createJaxBsonFieldContext() {
    return new JaxBsonFieldContextImpl( //
        this.ctx, //
        this.field, //
        this.field.getAnnotation(JaxBsonIgnoreTransient.class), //
        this.ctx.getConfiguration().getJaxBsonIgnoreTransient(this.field), //
        JaxBsonNameHelper.instance(this.field), //
        this.ctx.getConfiguration().getJaxBsonName(this.field), //
        JaxBsonSeeAlsoHelper.instance(this.field), //
        this.field.getAnnotation(JaxBsonNumberHint.class), //
        this.ctx.getConfiguration().getJaxBsonNumberHint(this.field), //
        this.field.getAnnotation(JaxBsonTransient.class), //
        this.ctx.getConfiguration().getJaxBsonTransient(this.field), //
        XmlAnyAttributeHelper.instance(this.field), //
        this.field.getAnnotation(JaxBsonXmlAnyAttributeMapping.class), //
        this.ctx.getConfiguration().getJaxBsonXmlAnyAttributeMapping(this.field), //
        this.field.getAnnotation(JaxBsonXmlAnyAttributeMappings.class), //
        this.ctx.getConfiguration().getJaxBsonXmlAnyAttributeMappings(this.field), //
        XmlAnyElementHelper.instance(this.field), //
        XmlElementRefHelper.instance(this.field), //
        XmlElementRefsHelper.instance(this.field), //
        XmlElementsHelper.instance(this.field), //
        JaxBsonJavaTypeAdapterHelper.jaxBsonJavaTypeAdapter(this.field), //
        XmlTransientHelper.instance(this.field), //
        XmlValueHelper.instance(this.field) //
    );
  }

  public Document debugInfo() {
    final Document ret = new Document();
    ret.append("field", this.field.getName());
    ret.append("ignore", this.ignore);
    ret.append("mainType", this.mainType == null ? this.mainType : this.mainType.getName());
    ret.append("multiBsonFieldNames", this.multiBsonFieldNames);
    ret.append("oneMainType", this.oneMainType);
    final List<Document> fsds = new ArrayList<>();
    ret.append("fieldSubDescriptors", fsds);
    for (final FieldSubDescriptor fds : this.bsonFieldNameMap.values()) {
      fsds.add(fds.debugInfo());
    }
    return ret;
  }

  protected Field getField() {
    return this.field;
  }

  protected JaxBsonFieldContextImpl getFieldCtx() {
    return this.fieldCtx;
  }

  private Class<?> getFieldMainType() {
    final ValueContainerType vct = ValueContainerType.fromField(this.field);
    final Class<?> type = this.field.getType();
    if (vct.isArray()) {
      return type.getComponentType();
    }
    if (vct.isCollection()) {
      return getGenericType(this.field.getGenericType(), 0);
    }
    return type;
  }

  protected FieldSubDescriptor getFieldSubDescriptor() {
    if (this.bsonFieldNameMap.size() != 1) {
      throw new IllegalStateException("Not exactly one sub-field (" + this.bsonFieldNameMap.size()
          + ") in field descriptor " + this);
    }
    return this.bsonFieldNameMap.get(this.bsonFieldNameMap.firstKey());
  }

  protected FieldSubDescriptor getFieldSubDescriptor(final Class<?> type) {
    return getFieldSubDescriptor(type, true);
  }

  protected FieldSubDescriptor getFieldSubDescriptor(final Class<?> type, final boolean checkRet) {
    final FieldSubDescriptor ret = this.bsonFieldTypeMap.get(type);
    if (checkRet && (ret == null)) {
      throw new IllegalStateException(
          "No sub-field for type '" + type.getName() + "' in field descriptor " + this);
    }
    return ret;
  }

  protected FieldSubDescriptor getFieldSubDescriptor(final String name) {
    return getFieldSubDescriptor(name, true);
  }

  protected FieldSubDescriptor getFieldSubDescriptor(final String name, final boolean checkRet) {
    final FieldSubDescriptor ret = this.bsonFieldNameMap.get(name);
    if (checkRet && (ret == null)) {
      throw new IllegalStateException(
          "No sub-field for name '" + name + "' in field descriptor " + this);
    }
    return ret;
  }

  protected Collection<FieldSubDescriptor> getFieldSubDescriptors() {
    return this.bsonFieldNameMap.values();
  }

  private Class<?> getGenericType(final Type genericFieldType, final int i) {
    if (genericFieldType instanceof ParameterizedType) {
      final ParameterizedType aType = (ParameterizedType) genericFieldType;
      final Type[] fieldArgTypes = aType.getActualTypeArguments();
      if (fieldArgTypes.length > i) {
        return (Class<?>) fieldArgTypes[i];
      }
    }
    return null;
  }

  protected Collection<Class<?>> getReferredTypes() {
    return this.referredTypes;
  }

  protected boolean isIgnore() {
    return this.ignore;
  }

  protected boolean isOneMainType() {
    return this.oneMainType;
  }

  protected boolean isXmlAnyAttribute() {
    return this.fieldCtx.getXmlAnyAttribute() != null;
  }

  private Constructor<Collection<Object>> makeCollectionConstructor() {
    if (!this.valueContainerType.isCollection()) {
      return null;
    }
    if (this.field.getType().isInterface()) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Constructor<Collection<Object>> ret =
          (Constructor<Collection<Object>>) this.field.getType().getDeclaredConstructor();
      ret.setAccessible(true);
      return ret;
    } catch (NoSuchMethodException | SecurityException e) {
      throw new IllegalStateException(
          "No no-argument constructor for collection type " + this.field.getType().getName());
    }
  }

  private Class<?> makeMainType() {
    if (this.fieldCtx.getXmlAnyAttribute() != null) {
      return null;
    }
    if (this.fieldCtx.getJaxBsonXmlAdapter() == null) {
      return getFieldMainType();
    }
    return this.fieldCtx.getJaxBsonXmlAdapter().valueType(this.field);
  }

  private String makeName(final XmlElementHelper ann) {
    if (!isBlank(ann.name()) && !ann.name().equalsIgnoreCase("##default")) {
      return ann.name();
    }
    final Class<?> type = ann.type();
    final XmlRootElementHelper xre = XmlRootElementHelper.instance(type);
    return makeName(xre, type);
  }

  private String makeName(final XmlElementRefHelper ann) {
    if (ann.isDefaultType()) {
      return null;
    }
    final Class<?> type = ann.type();
    final XmlRootElementHelper xre = XmlRootElementHelper.instance(type);
    return makeName(xre, type);
  }

  private String makeName(final XmlRootElementHelper ann, final Class<?> type) {
    if ((ann == null) || isBlank(ann.name()) || ann.name().equalsIgnoreCase("##default")) {
      return lcFirst(type.getSimpleName());
    }
    return ann.name();
  }

  private String makeSingleName() {
    if ((this.fieldCtx.getJaxBsonNameCfg() != null)
        && !isBlank(this.fieldCtx.getJaxBsonNameCfg().name())
        && !this.fieldCtx.getJaxBsonNameCfg().name().equalsIgnoreCase("##default")) {
      return this.fieldCtx.getJaxBsonNameCfg().name();
    }
    if ((this.fieldCtx.getJaxBsonName() != null) && !isBlank(this.fieldCtx.getJaxBsonName().name())
        && !this.fieldCtx.getJaxBsonName().name().equalsIgnoreCase("##default")) {
      return this.fieldCtx.getJaxBsonName().name();
    }
    if (this.fieldCtx.getXmlValue() != null) {
      return this.ctx.getConfiguration().getXmlValueFieldName();
    }
    return this.field.getName();
  }

  private void setIgnore() {
    if (Modifier.isStatic(this.field.getModifiers())) {
      this.ignore = true;
      return;
    }
    if (this.fieldCtx.getJaxBsonTransientCfg() != null) {
      this.ignore = true;
      return;
    }
    if (this.fieldCtx.getJaxBsonTransient() != null) {
      this.ignore = true;
      return;
    }
    if (this.fieldCtx.getJaxBsonIgnoreTransientCfg() != null) {
      return;
    }
    if (this.fieldCtx.getJaxBsonIgnoreTransient() != null) {
      return;
    }
    if (this.fieldCtx.getXmlTransient() != null) {
      this.ignore = true;
      return;
    }
    if (Modifier.isTransient(this.field.getModifiers())) {
      this.ignore = true;
      return;
    }
  }

  private void setMultiBsonFieldNames() {
    this.multiBsonFieldNames = //
        !this.valueContainerType.isCollection() //
            && !this.valueContainerType.isArray() //
            && ((this.fieldCtx.getXmlElementRefs() != null //
            ) || (this.fieldCtx.getXmlElements() != null //
            ));
  }

  private void setOneMainType() {
    this.oneMainType =
        (this.fieldCtx.getXmlElementRefs() == null) && (this.fieldCtx.getXmlElements() == null);
  }

  private void setValueContainerType() {
    if (this.fieldCtx.getXmlAnyAttribute() != null) {
      this.valueContainerType = ValueContainerType.ANY_ATTRIBUTE;
    } else if (this.fieldCtx.getJaxBsonXmlAdapter() == null) {
      this.valueContainerType = ValueContainerType.fromField(this.field);
    } else {
      this.valueContainerType = ValueContainerType.NONE;
    }
  }

  protected void toBson(final Document bson, final Object o) {
    toBson(bson, o, false);
  }

  protected void toBson(final Document bson, final Object o, boolean omitType) {
    try {
      final Object value = this.field.get(o);
      if (value == null) {
        return;
      }
      if (this.multiBsonFieldNames) {
        final FieldSubDescriptor fds = getFieldSubDescriptor(value.getClass());
        fds.toBson(bson, value, false);
      } else {
        omitType = omitType && this.oneMainType;
        final FieldSubDescriptor fds = getFieldSubDescriptor();
        fds.toBson(bson, value, omitType);
      }
    } catch (IllegalArgumentException | IllegalAccessException e) {
      throw new IllegalStateException("Unexpected exception", e);
    }
  }

  protected void toObject(final Object o, final String bsonFieldName, final Object bsonValue) {
    final FieldSubDescriptor fds = this.bsonFieldNameMap.get(bsonFieldName);
    if (fds == null) {
      throw new IllegalStateException("This may be a bug. Failed to process Bson field name '"
          + bsonFieldName + "' in " + this);
    }
    fds.toObject(o, bsonValue);
  }
}
