package org.qnixyz.jbson.impl;

import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class NumberXmlAdapter extends JaxBsonAdapter<String, Number> {

  public NumberXmlAdapter() {}

  @Override
  public String marshal(final Number v) throws Exception {
    if (v == null) {
      return null;
    }
    return v.toString();
  }

  @Override
  public Number unmarshal(final String v) throws Exception {
    if (v == null) {
      return null;
    }
    try {
      return Integer.parseInt(v);
    } catch (final NumberFormatException e) {
    }
    try {
      return Long.parseLong(v);
    } catch (final NumberFormatException e) {
    }
    try {
      return Float.parseFloat(v);
    } catch (final NumberFormatException e) {
    }
    return Double.parseDouble(v);
  }
}
