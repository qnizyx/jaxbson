package org.qnixyz.jbson.impl;

import java.util.Objects;
import java.util.TreeMap;
import javax.xml.namespace.QName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;

class NamespaceMap {

  private final FieldDescriptor fd;

  private final TreeMap<String, String> toBsonMap = new TreeMap<>();

  private final TreeMap<String, String> toObjectMap = new TreeMap<>();

  protected NamespaceMap(final FieldDescriptor fd) {
    this.fd = Objects.requireNonNull(fd, "Supplied parameter 'fd' is null");
    add("", "");
  }

  protected NamespaceMap(final FieldDescriptor fd, final JaxBsonXmlAnyAttributeMapping mapping) {
    this.fd = Objects.requireNonNull(fd, "Supplied parameter 'fd' is null");
    Objects.requireNonNull(mapping, "Supplied parameter 'mapping' is null");
    add(mapping);
  }

  protected NamespaceMap(final FieldDescriptor fd, final JaxBsonXmlAnyAttributeMappings mappings) {
    this.fd = Objects.requireNonNull(fd, "Supplied parameter 'fd' is null");
    Objects.requireNonNull(mappings, "Supplied parameter 'mappings' is null");
    if (mappings.mappings() != null) {
      for (final JaxBsonXmlAnyAttributeMapping mapping : mappings.mappings()) {
        add(mapping);
      }
    }
  }

  private void add(final JaxBsonXmlAnyAttributeMapping mapping) {
    add(mapping.namespaceURI(), mapping.bsonPrefix());
  }

  private void add(final String namespaceURI, final String bsonPrefix) {
    Objects.requireNonNull(namespaceURI, "Supplied parameter 'namespaceURI' is null");
    Objects.requireNonNull(bsonPrefix, "Supplied parameter 'bsonPrefix' is null");
    if (toBsonMap.containsKey(namespaceURI)) {
      throw new IllegalStateException(
          "Multiple definitions for namespaceURI '" + namespaceURI + "' in field " + fd);
    }
    if (toObjectMap.containsKey(bsonPrefix)) {
      throw new IllegalStateException(
          "Multiple definitions for bsonPrefix '" + bsonPrefix + "' in field " + fd);
    }
    toBsonMap.put(namespaceURI, bsonPrefix);
    toObjectMap.put(bsonPrefix, namespaceURI);
  }

  protected String getBsonFieldName(final QName qName) {
    Objects.requireNonNull(qName, "Supplied parameter 'qName' is null");
    final String bsonPrefix = toBsonMap.get(qName.getNamespaceURI());
    if (bsonPrefix == null) {
      throw new IllegalStateException("Failed to map namespaceURI '" + qName.getNamespaceURI()
          + "' to Bson field prefix for field " + fd);
    }
    return bsonPrefix + qName.getLocalPart();
  }

  protected QName getQName(final String bsonFieldName) {
    Objects.requireNonNull(fd, "Supplied parameter 'fd' is null");
    Objects.requireNonNull(bsonFieldName, "Supplied parameter 'bsonFieldName' is null");
    for (final String bsonPrefix : toObjectMap.descendingKeySet()) {
      if (bsonFieldName.startsWith(bsonPrefix)) {
        final String namespaceURI = toObjectMap.get(bsonPrefix);
        final String localPart = bsonFieldName.substring(bsonPrefix.length());
        return new QName(namespaceURI, localPart);
      }
    }
    throw new IllegalStateException("Failed to map BSIN field name '" + bsonFieldName
        + "' to namespaceURI+localPart for field " + fd);
  }
}
