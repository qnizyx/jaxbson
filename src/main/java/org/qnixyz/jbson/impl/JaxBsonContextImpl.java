package org.qnixyz.jbson.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.bson.Document;
import org.qnixyz.jbson.JaxBsonConfiguration;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonContextImpl implements JaxBsonContext {

  private final JaxBsonConfiguration configuration;

  private final SimpleTypes simpleTypes;

  private final Map<Class<?>, ClassDescriptor> typeMap = new HashMap<>();

  private final SortedMap<String, ClassDescriptor> typeNameMap = new TreeMap<>();

  private final Class<?>[] types;

  private final Map<Class<?>, JaxBsonAdapter<Object, Object>> xmlAdapterMap = new HashMap<>();

  public JaxBsonContextImpl(final Class<?>... types) {
    this(JaxBsonConfiguration.DEFAULT, types);
  }

  public JaxBsonContextImpl(final JaxBsonConfiguration configuration, final Class<?>... types) {
    this.configuration =
        Objects.requireNonNull(configuration, "Supplied parameter 'configuration' is null");
    this.types = Objects.requireNonNull(types, "Supplied parameter 'types' is null");
    if (types.length < 1) {
      throw new IllegalArgumentException("Supplied parameter 'types' is empty");
    }
    this.simpleTypes = new SimpleTypes();
    index();
  }

  @Override
  public Document debugInfo() {
    final Document ret = new Document();
    ret.append("NOTE",
        "THIS CONTENT IS FOR DEBUGGING PURPOSES. IT'S FORMAT MAY CHANGE AT ANY TIME. EVEN THE METHOD TO GENERATE IT MAY AT SOME TIME BE DEPRECATED.");
    ret.append("configurationId", this.configuration.getId());
    final List<Document> cds = new ArrayList<>();
    ret.append("classDescriptors", cds);
    for (final ClassDescriptor cd : this.typeNameMap.values()) {
      cds.add(cd.debugInfo());
    }
    return ret;
  }

  @Override
  public JaxBsonConfiguration getConfiguration() {
    return this.configuration;
  }

  protected SimpleTypes getSimpleTypes() {
    return this.simpleTypes;
  }

  @SuppressWarnings("unchecked")
  protected JaxBsonAdapter<Object, Object> getXmlAdapterInstance(final Class<?> xmlAdapterClass) {
    JaxBsonAdapter<Object, Object> ret = this.xmlAdapterMap.get(xmlAdapterClass);
    if (ret == null) {
      try {
        final Constructor<?> constructor = xmlAdapterClass.getDeclaredConstructor();
        constructor.setAccessible(true);
        ret = (JaxBsonAdapter<Object, Object>) constructor.newInstance();
        this.xmlAdapterMap.put(xmlAdapterClass, ret);
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
          | InvocationTargetException | NoSuchMethodException | SecurityException e) {
        throw new IllegalStateException(
            "Unexpected exception while getting the no-argument constructor for XmlAdapter class "
                + xmlAdapterClass.getName(),
            e);
      }
    }
    return ret;
  }

  private void index() {
    Set<Class<?>> types = new HashSet<>(Arrays.asList(this.types));
    while (!types.isEmpty()) {
      types = index(types);
    }
  }

  private Set<Class<?>> index(final Class<?> type) {
    final Set<Class<?>> ret = new HashSet<>();
    if (this.typeMap.containsKey(type)) {
      return ret;
    }
    final ClassDescriptor cd = new ClassDescriptor(this, type);
    if (this.typeNameMap.containsKey(cd.getTypeName())) {
      throw new IllegalStateException("Both types " + type.getName() + " and "
          + this.typeNameMap.get(cd.getTypeName()).getType() + " have the same type name '"
          + cd.getTypeName() + "'");
    }
    this.typeMap.put(type, cd);
    this.typeNameMap.put(cd.getTypeName(), cd);
    ret.addAll(cd.getReferredTypes());
    return ret;
  }

  private Set<Class<?>> index(final Set<Class<?>> types) {
    final Set<Class<?>> ret = new HashSet<>();
    types.forEach(e -> ret.addAll(index(e)));
    ret.removeAll(this.typeMap.keySet());
    return ret;
  }

  @Override
  public Document toBson(final Object o) {
    Objects.requireNonNull(o, "Supplied parameter 'o' is null");
    return toBson(o, false);
  }

  @Override
  public Document toBson(final Object o, final boolean omitType) {
    Objects.requireNonNull(o, "Supplied parameter 'o' is null");
    final Class<?> type = o.getClass();
    final ClassDescriptor cd = this.typeMap.get(type);
    if (cd == null) {
      throw new IllegalStateException("Type '" + o.getClass().getName() + "' not in context");
    }
    return cd.toBson(o, omitType);
  }

  @Override
  public Object toObject(final Document bson) {
    Objects.requireNonNull(bson, "Supplied parameter 'bson' is null");
    final String typeName = bson.getString(this.configuration.getTypeFieldName());
    if (typeName == null) {
      throw new IllegalStateException("No type field in Bson object: " + bson);
    }
    return toObject(bson, typeName);
  }

  @Override
  public Object toObject(final Document bson, final Class<?> type) {
    Objects.requireNonNull(bson, "Supplied parameter 'bson' is null");
    Objects.requireNonNull(type, "Supplied parameter 'type' is null");
    final ClassDescriptor cd = this.typeMap.get(type);
    if (cd == null) {
      throw new IllegalStateException("Type '" + type.getSimpleName() + "' not in context");
    }
    return cd.toObject(bson);
  }

  @Override
  public Object toObject(final Document bson, final String typeName) {
    Objects.requireNonNull(bson, "Supplied parameter 'bson' is null");
    Objects.requireNonNull(typeName, "Supplied parameter 'typeName' is null");
    final ClassDescriptor cd = this.typeNameMap.get(typeName);
    if (cd == null) {
      throw new IllegalStateException("Type name '" + typeName
          + "' not in context. Type names in context: " + this.typeNameMap.keySet());
    }
    return cd.toObject(bson);
  }
}
