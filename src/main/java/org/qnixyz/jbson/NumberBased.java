package org.qnixyz.jbson;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.bson.types.Decimal128;

public class NumberBased {

  public static class BsonNumber {

    private final Object value;

    public BsonNumber(final Decimal128 value) {
      this.value = value;
    }

    public BsonNumber(final Double value) {
      this.value = value;
    }

    public BsonNumber(final Integer value) {
      this.value = value;
    }

    public BsonNumber(final Long value) {
      this.value = value;
    }

    public BsonNumber(final String value) {
      this.value = value;
    }

    public Object getValue() {
      return this.value;
    }
  }

  @FunctionalInterface
  public interface Checker {
    boolean check(Class<?> type);
  }

  private static class FuncMap {

    private final Map<Class<?>, ToBson> toBsonMap = new HashMap<>();

    private final Map<Class<?>, ToObject> toObjectMap = new HashMap<>();

    private FuncMap() {
      putToObjectMap();
      putToBsonMap();
    }

    private ToBson getToBson(final Class<?> type) {
      return this.toBsonMap.get(type);
    }

    private ToObject getToObject(final Class<?> fieldType) {
      return this.toObjectMap.get(fieldType);
    }

    private void putToBsonMap() {
      this.toBsonMap.put(BigDecimal.class, (fieldCtx, value) -> {
        BigDecimal cast = (BigDecimal) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
          cast = cast.round(new MathContext(fieldCtx.getJaxBsonNumberHintCfg().precision(),
              fieldCtx.getJaxBsonNumberHintCfg().roundingMode()));
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
          cast = cast.round(new MathContext(fieldCtx.getJaxBsonNumberHint().precision(),
              fieldCtx.getJaxBsonNumberHint().roundingMode()));
        }
        return new BsonNumber(new Decimal128(cast));
      });
      this.toBsonMap.put(BigInteger.class, (fieldCtx, value) -> {
        final BigInteger cast = (BigInteger) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(new Decimal128(new BigDecimal(cast)));
      });
      this.toBsonMap.put(Decimal128.class, (fieldCtx, value) -> {
        final Decimal128 cast = (Decimal128) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Double.class, (fieldCtx, value) -> {
        final Double cast = (Double) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Double.TYPE, (fieldCtx, value) -> {
        final Double cast = (Double) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Float.class, (fieldCtx, value) -> {
        final Float cast = (Float) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast.doubleValue());
      });
      this.toBsonMap.put(Float.TYPE, (fieldCtx, value) -> {
        final Float cast = (Float) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast.doubleValue());
      });
      this.toBsonMap.put(Integer.class, (fieldCtx, value) -> {
        final Integer cast = (Integer) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber((Integer) value);
      });
      this.toBsonMap.put(Integer.TYPE, (fieldCtx, value) -> {
        final Integer cast = (Integer) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Long.class, (fieldCtx, value) -> {
        final Long cast = (Long) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Long.TYPE, (fieldCtx, value) -> {
        final Long cast = (Long) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast);
      });
      this.toBsonMap.put(Short.class, (fieldCtx, value) -> {
        final Short cast = (Short) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast.intValue());
      });
      this.toBsonMap.put(Short.TYPE, (fieldCtx, value) -> {
        final Short cast = (Short) value;
        if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
          if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
            return new BsonNumber(cast.toString());
          }
        } else if (fieldCtx.getJaxBsonNumberHint() != null) {
          if (fieldCtx.getJaxBsonNumberHint().asString()) {
            return new BsonNumber(cast.toString());
          }
        }
        return new BsonNumber(cast.intValue());
      });
    }

    private void putToObjectMap() {

      this.toObjectMap.put(BigDecimal.class, (__, value) -> {
        final Class<?> valueType = value.getClass();
        if (Decimal128.class.equals(valueType)) {
          return ((Decimal128) value).bigDecimalValue();
        }
        return new BigDecimal(value.doubleValue());
      });

      this.toObjectMap.put(BigInteger.class, (__, value) -> {
        final Class<?> valueType = value.getClass();
        if (Decimal128.class.equals(valueType)) {
          return ((Decimal128) value).bigDecimalValue().toBigInteger();
        }
        return BigInteger.valueOf(value.longValue());
      });

      this.toObjectMap.put(Decimal128.class, (__, value) -> {
        return new Decimal128(value.longValue());
      });

      this.toObjectMap.put(Double.class, (__, value) -> value.doubleValue());
      this.toObjectMap.put(Double.TYPE, (__, value) -> value.doubleValue());
      this.toObjectMap.put(Float.class, (__, value) -> value.floatValue());
      this.toObjectMap.put(Float.TYPE, (__, value) -> value.floatValue());
      this.toObjectMap.put(Integer.class, (__, value) -> value.intValue());
      this.toObjectMap.put(Integer.TYPE, (__, value) -> value.intValue());
      this.toObjectMap.put(Long.class, (__, value) -> value.longValue());
      this.toObjectMap.put(Long.TYPE, (__, value) -> value.longValue());
      this.toObjectMap.put(Short.class, (__, value) -> value.shortValue());
      this.toObjectMap.put(Short.TYPE, (__, value) -> value.shortValue());
    }
  }

  @FunctionalInterface
  public interface ToBson {
    BsonNumber convert(JaxBsonFieldContext fieldCtx, Object value);
  }

  @FunctionalInterface
  public interface ToObject {
    Object convert(Class<?> type, Number value);
  }

  private static final FuncMap FUNC_MAP = new FuncMap();

  private static Class<?>[] PRIMITIVE_TYPES = { //
      Double.TYPE, //
      Float.TYPE, //
      Integer.TYPE, //
      Long.TYPE, //
      Short.TYPE, //
  };

  /**
   * Set containing all primitive types supported by this class.
   */
  public static final Set<Class<?>> PRIMITIVE_TYPES_SET =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(PRIMITIVE_TYPES)));

  private static Class<?>[] REFERENCE_TYPES = { //
      BigDecimal.class, //
      BigInteger.class, //
      Decimal128.class, //
      Double.class, //
      Float.class, //
      Integer.class, //
      Long.class, //
      Short.class, //
  };

  /**
   * Set containing all reference types supported by this class.
   */
  public static final Set<Class<?>> REFERENCE_TYPES_SET =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(REFERENCE_TYPES)));

  private static Class<?>[] TYPES = { //
      BigDecimal.class, //
      BigInteger.class, //
      Decimal128.class, //
      Double.class, //
      Double.TYPE, //
      Float.class, //
      Float.TYPE, //
      Integer.class, //
      Integer.TYPE, //
      Long.class, //
      Long.TYPE, //
      Short.class, //
      Short.TYPE, //
  };

  /**
   * Set containing all types supported by this class.
   */
  public static final Set<Class<?>> TYPES_SET =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(TYPES)));

  private final Set<Class<?>> _checkerFalse = new HashSet<>();

  private final Set<Class<?>> _checkerTrue = new HashSet<>();

  private final Checker checker = (type) -> {
    if (this._checkerTrue.contains(type)) {
      return true;
    }
    if (this._checkerFalse.contains(type)) {
      return false;
    }
    for (final Class<?> e : TYPES_SET) {
      if (e.isAssignableFrom(type)) {
        this._checkerTrue.add(type);
        return true;
      }
    }
    this._checkerFalse.add(type);
    return false;
  };

  private final ToBson toBson = (fieldCtx, value) -> {
    if (value == null) {
      return null;
    }
    final Class<?> type = value.getClass();
    final ToBson func = FUNC_MAP.getToBson(type);
    if (func != null) {
      return func.convert(fieldCtx, value);
    }
    if (BigDecimal.class.isAssignableFrom(type)) {
      BigDecimal cast = (BigDecimal) value;
      if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
        cast = cast.round(new MathContext(fieldCtx.getJaxBsonNumberHint().precision(),
            fieldCtx.getJaxBsonNumberHint().roundingMode()));
      }
      return new BsonNumber(new Decimal128(cast));
    }
    if (BigInteger.class.isAssignableFrom(type)) {
      final BigInteger cast = (BigInteger) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(new Decimal128(new BigDecimal(cast)));
    }
    if (Decimal128.class.isAssignableFrom(type)) {
      final Decimal128 cast = (Decimal128) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Double.class.isAssignableFrom(type)) {
      final Double cast = (Double) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Double.TYPE.isAssignableFrom(type)) {
      final Double cast = (Double) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Float.class.isAssignableFrom(type)) {
      final Float cast = (Float) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast.doubleValue());
    }
    if (Float.TYPE.isAssignableFrom(type)) {
      final Float cast = (Float) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast.doubleValue());
    }
    if (Integer.class.isAssignableFrom(type)) {
      return new BsonNumber((Integer) value);
    }
    if (Integer.TYPE.isAssignableFrom(type)) {
      final Integer cast = (Integer) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Long.class.isAssignableFrom(type)) {
      final Long cast = (Long) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Long.TYPE.isAssignableFrom(type)) {
      final Long cast = (Long) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast);
    }
    if (Short.class.isAssignableFrom(type)) {
      final Short cast = (Short) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast.intValue());
    }
    if (Short.TYPE.isAssignableFrom(type)) {
      final Short cast = (Short) value;
      if (fieldCtx.getJaxBsonNumberHintCfg() != null) {
        if (fieldCtx.getJaxBsonNumberHintCfg().asString()) {
          return new BsonNumber(cast.toString());
        }
      } else if (fieldCtx.getJaxBsonNumberHint() != null) {
        if (fieldCtx.getJaxBsonNumberHint().asString()) {
          return new BsonNumber(cast.toString());
        }
      }
      return new BsonNumber(cast.intValue());
    }
    throw new IllegalStateException("Failed to convert value to Bson: " + value);
  };

  private final ToObject toObject = (type, value) -> {
    if (value == null) {
      return null;
    }
    final Class<?> valueType = value.getClass();
    if (type.equals(valueType)) {
      return value;
    }
    final ToObject func = FUNC_MAP.getToObject(type);
    if (func != null) {
      return func.convert(type, value);
    }
    throw new IllegalStateException("Failed to convert value to Java object: " + value);
  };

  public Checker getChecker() {
    return this.checker;
  }

  public ToBson getToBson() {
    return this.toBson;
  }

  public ToObject getToObject() {
    return this.toObject;
  }
}
