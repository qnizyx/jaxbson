package org.qnixyz.jbson;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.cfg.JaxBsonIgnoreTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameTypeMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToBsonPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPostMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreMethodMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonTransientFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsFieldMap;
import org.qnixyz.jbson.impl.NumberXmlAdapter;

public class JaxBsonConfiguration {

  public static class Builder {
    private boolean allowSmartTypeField = DEFAULT_ALLOW_SMART_TYPE_FIELD;
    private DateBased dateBased = new DateBased();
    private String id = UUID.randomUUID().toString();
    private JaxBsonIgnoreTransientFieldMap jaxBsonIgnoreTransientFieldMap;
    private JaxBsonNameFieldMap jaxBsonNameFieldMap;
    private JaxBsonNameTypeMap jaxBsonNameTypeMap;
    private JaxBsonNumberHintFieldMap jaxBsonNumberHintFieldMap;
    private JaxBsonToBsonPostMethodMap jaxBsonToBsonPostMethodMap;
    private JaxBsonToBsonPreMethodMap jaxBsonToBsonPreMethodMap;
    private JaxBsonToObjectPostMethodMap jaxBsonToObjectPostMethodMap;
    private JaxBsonToObjectPreMethodMap jaxBsonToObjectPreMethodMap;
    private JaxBsonTransientFieldMap jaxBsonTransientFieldMap;
    private JaxBsonXmlAnyAttributeMappingFieldMap jaxBsonXmlAnyAttributeMappingFieldMap;
    private JaxBsonXmlAnyAttributeMappingsFieldMap jaxBsonXmlAnyAttributeMappingsFieldMap;
    private NumberBased numberBased = new NumberBased();
    private StringBased stringBased = new StringBased();
    private JaxBsonToBson toBson = JaxBsonToBson.DEFAULT;
    private JaxBsonToObject toObject = JaxBsonToObject.DEFAULT;
    private String typeFieldName = DEFAULT_TYPE_FIELD_NAME;
    private boolean valueBooleanPrimBooleanNull = DEFAULT_BOOLEAN_PRIM_BOOLEAN_NULL;
    private byte valueByteBooleanFalse = DEFAULT_BYTE_PRIM_BOOLEAN_FALSE;
    private Byte valueByteBooleanNull = DEFAULT_BYTE_BOOLEAN_BOOLEAN_NULL;
    private byte valueByteBooleanTrue = DEFAULT_BYTE_PRIM_BOOLEAN_TRUE;
    private byte valueBytePrimBooleanNull = DEFAULT_BYTE_PRIM_BOOLEAN_NULL;
    private byte valueBytePrimNull = DEFAULT_BYTE_PRIM_NULL;
    private char valueCharacterBooleanFalse = DEFAULT_CHAR_PRIM_BOOLEAN_FALSE;
    private Character valueCharacterBooleanNull = DEFAULT_CHARACTER_BOOLEAN_NULL;
    private char valueCharacterBooleanTrue = DEFAULT_CHAR_PRIM_BOOLEAN_TRUE;
    private char valueCharacterPrimBooleanNull = DEFAULT_CHARACTER_PRIM_BOOLEAN_NULL;
    private char valueCharacterPrimNull = DEFAULT_CHAR_PRIM_NULL;
    private Number valueNumberBooleanFalse = DEFAULT_NUMBER_BOOLEAN_FALSE;
    private Number valueNumberBooleanNull = DEFAULT_NUMBER_BOOLEAN_NULL;
    private Number valueNumberBooleanTrue = DEFAULT_NUMBER_BOOLEAN_TRUE;
    private Number valueNumberPrimBooleanNull = DEFAULT_NUMBER_PRIM_BOOLEAN_NULL;
    private Number valueNumberPrimNull = DEFAULT_NUMBER_PRIM_NULL;
    private String valueStringBooleanFalse = DEFAULT_STRING_BOOLEAN_FALSE;
    private String valueStringBooleanNull = DEFAULT_STRING_BOOLEAN_NULL;
    private String valueStringBooleanTrue = DEFAULT_STRING_BOOLEAN_TRUE;
    private String xmlValueFieldName = DEFAULT_XML_VALUE_FIELD_NAME;

    public Builder allowSmartTypeField(final boolean allowSmartTypeField) {
      this.allowSmartTypeField = allowSmartTypeField;
      return this;
    }

    public JaxBsonConfiguration build() {
      return new JaxBsonConfiguration(this);
    }

    public Builder dateBased(final DateBased dateBased) {
      this.dateBased = Objects.requireNonNull(dateBased, "Field 'dateBased'");
      return this;
    }

    public Builder id(final String id) {
      this.id = Objects.requireNonNull(id, "Field 'id'");
      return this;
    }

    public Builder jaxBsonIgnoreTransientFieldMap(
        final JaxBsonIgnoreTransientFieldMap jaxBsonIgnoreTransientFieldMap) {
      this.jaxBsonIgnoreTransientFieldMap = jaxBsonIgnoreTransientFieldMap;
      return this;
    }

    public Builder jaxBsonNameFieldMap(final JaxBsonNameFieldMap jaxBsonNameFieldMap) {
      this.jaxBsonNameFieldMap = jaxBsonNameFieldMap;
      return this;
    }

    public Builder jaxBsonNameTypeMap(final JaxBsonNameTypeMap jaxBsonNameTypeMap) {
      this.jaxBsonNameTypeMap = jaxBsonNameTypeMap;
      return this;
    }

    public Builder jaxBsonNumberHintFieldMap(
        final JaxBsonNumberHintFieldMap jaxBsonNumberHintFieldMap) {
      this.jaxBsonNumberHintFieldMap = jaxBsonNumberHintFieldMap;
      return this;
    }

    public Builder jaxBsonToBsonPostMethodMap(
        final JaxBsonToBsonPostMethodMap jaxBsonToBsonPostMethodMap) {
      this.jaxBsonToBsonPostMethodMap = jaxBsonToBsonPostMethodMap;
      return this;
    }

    public Builder jaxBsonToBsonPreMethodMap(
        final JaxBsonToBsonPreMethodMap jaxBsonToBsonPreMethodMap) {
      this.jaxBsonToBsonPreMethodMap = jaxBsonToBsonPreMethodMap;
      return this;
    }

    public Builder jaxBsonToObjectPostMethodMap(
        final JaxBsonToObjectPostMethodMap jaxBsonToObjectPostMethodMap) {
      this.jaxBsonToObjectPostMethodMap = jaxBsonToObjectPostMethodMap;
      return this;
    }

    public Builder jaxBsonToObjectPreMethodMap(
        final JaxBsonToObjectPreMethodMap jaxBsonToObjectPreMethodMap) {
      this.jaxBsonToObjectPreMethodMap = jaxBsonToObjectPreMethodMap;
      return this;
    }

    public Builder jaxBsonTransientFieldMap(
        final JaxBsonTransientFieldMap jaxBsonTransientFieldMap) {
      this.jaxBsonTransientFieldMap = jaxBsonTransientFieldMap;
      return this;
    }

    public Builder jaxBsonXmlAnyAttributeMappingFieldMap(
        final JaxBsonXmlAnyAttributeMappingFieldMap jaxBsonXmlAnyAttributeMappingFieldMap) {
      this.jaxBsonXmlAnyAttributeMappingFieldMap = jaxBsonXmlAnyAttributeMappingFieldMap;
      return this;
    }

    public Builder jaxBsonXmlAnyAttributeMappingsFieldMap(
        final JaxBsonXmlAnyAttributeMappingsFieldMap jaxBsonXmlAnyAttributeMappingsFieldMap) {
      this.jaxBsonXmlAnyAttributeMappingsFieldMap = jaxBsonXmlAnyAttributeMappingsFieldMap;
      return this;
    }

    public Builder numberBased(final NumberBased numberBased) {
      this.numberBased = Objects.requireNonNull(numberBased, "Field 'numberBased'");
      return this;
    }

    public Builder stringBased(final StringBased stringBased) {
      this.stringBased = Objects.requireNonNull(stringBased, "Field 'stringBased'");
      return this;
    }

    public Builder toBson(final JaxBsonToBson toBson) {
      this.toBson = Objects.requireNonNull(toBson, "Field 'toBson'");
      return this;
    }

    public Builder toObject(final JaxBsonToObject toObject) {
      this.toObject = Objects.requireNonNull(toObject, "Field 'toObject'");
      return this;
    }

    public Builder typeFieldName(final String typeFieldName) {
      this.typeFieldName = Objects.requireNonNull(typeFieldName, "Field 'typeFieldName'");
      return this;
    }

    public Builder valueBooleanPrimBooleanNull(final boolean valueBooleanPrimBooleanNull) {
      this.valueBooleanPrimBooleanNull = valueBooleanPrimBooleanNull;
      return this;
    }

    public Builder valueByteBooleanFalse(final byte valueByteBooleanFalse) {
      this.valueByteBooleanFalse = valueByteBooleanFalse;
      return this;
    }

    public Builder valueByteBooleanNull(final Byte valueByteBooleanNull) {
      this.valueByteBooleanNull = valueByteBooleanNull;
      return this;
    }

    public Builder valueByteBooleanTrue(final byte valueByteBooleanTrue) {
      this.valueByteBooleanTrue = valueByteBooleanTrue;
      return this;
    }

    public Builder valueBytePrimBooleanNull(final byte valueBytePrimBooleanNull) {
      this.valueBytePrimBooleanNull = valueBytePrimBooleanNull;
      return this;
    }

    public Builder valueBytePrimNull(final byte valueBytePrimNull) {
      this.valueBytePrimNull = valueBytePrimNull;
      return this;
    }

    public Builder valueCharacterBooleanFalse(final char valueCharacterBooleanFalse) {
      this.valueCharacterBooleanFalse = valueCharacterBooleanFalse;
      return this;
    }

    public Builder valueCharacterBooleanNull(final Character valueCharacterBooleanNull) {
      this.valueCharacterBooleanNull = valueCharacterBooleanNull;
      return this;
    }

    public Builder valueCharacterBooleanTrue(final char valueCharacterBooleanTrue) {
      this.valueCharacterBooleanTrue = valueCharacterBooleanTrue;
      return this;
    }

    public Builder valueCharacterPrimBooleanNull(final char valueCharacterPrimBooleanNull) {
      this.valueCharacterPrimBooleanNull = valueCharacterPrimBooleanNull;
      return this;
    }

    public Builder valueCharacterPrimNull(final char valueCharacterPrimNull) {
      this.valueCharacterPrimNull = valueCharacterPrimNull;
      return this;
    }

    public Builder valueNumberBooleanFalse(final Number valueNumberBooleanFalse) {
      this.valueNumberBooleanFalse =
          Objects.requireNonNull(valueNumberBooleanFalse, "Field 'valueNumberBooleanFalse'");
      return this;
    }

    public Builder valueNumberBooleanNull(final Number valueNumberBooleanNull) {
      this.valueNumberBooleanNull = valueNumberBooleanNull;
      return this;
    }

    public Builder valueNumberBooleanTrue(final Number valueNumberBooleanTrue) {
      this.valueNumberBooleanTrue =
          Objects.requireNonNull(valueNumberBooleanTrue, "Field 'valueNumberBooleanTrue'");
      return this;
    }

    public Builder valueNumberPrimBooleanNull(final Number valueNumberPrimBooleanNull) {
      this.valueNumberPrimBooleanNull =
          Objects.requireNonNull(valueNumberPrimBooleanNull, "Field 'valueNumberPrimBooleanNull'");
      return this;
    }

    public Builder valueNumberPrimNull(final Number valueNumberPrimNull) {
      this.valueNumberPrimNull =
          Objects.requireNonNull(valueNumberPrimNull, "Field 'valueNumberPrimNull'");
      return this;
    }

    public Builder valueStringBooleanFalse(final String valueStringBooleanFalse) {
      this.valueStringBooleanFalse =
          Objects.requireNonNull(valueStringBooleanFalse, "Field 'valueStringBooleanFalse'");
      return this;
    }

    public Builder valueStringBooleanNull(final String valueStringBooleanNull) {
      this.valueStringBooleanNull = valueStringBooleanNull;
      return this;
    }

    public Builder valueStringBooleanTrue(final String valueStringBooleanTrue) {
      this.valueStringBooleanTrue =
          Objects.requireNonNull(valueStringBooleanTrue, "Field 'valueStringBooleanTrue'");
      return this;
    }

    public Builder xmlValueFieldName(final String xmlValueFieldName) {
      this.xmlValueFieldName =
          Objects.requireNonNull(xmlValueFieldName, "Field 'xmlValueFieldName'");
      return this;
    }
  }

  public static final JaxBsonConfiguration DEFAULT;

  public static final boolean DEFAULT_ALLOW_SMART_TYPE_FIELD = false;
  public static final boolean DEFAULT_BOOLEAN_PRIM_BOOLEAN_NULL = false;
  public static final Byte DEFAULT_BYTE_BOOLEAN_BOOLEAN_NULL = null;
  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_FALSE = 0x0;
  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_NULL = 0x0;
  public static final byte DEFAULT_BYTE_PRIM_BOOLEAN_TRUE = 0x1;
  public static final byte DEFAULT_BYTE_PRIM_NULL = 0x0;
  public static final char DEFAULT_CHAR_PRIM_BOOLEAN_FALSE = '-';
  public static final char DEFAULT_CHAR_PRIM_BOOLEAN_TRUE = '+';
  public static final char DEFAULT_CHAR_PRIM_NULL = ' ';
  public static final Character DEFAULT_CHARACTER_BOOLEAN_NULL = null;
  public static final char DEFAULT_CHARACTER_PRIM_BOOLEAN_NULL = 0x0;
  public static final String DEFAULT_INSTANCE_ID = "default";
  public static final Number DEFAULT_NUMBER_BOOLEAN_FALSE = 0;
  public static final Number DEFAULT_NUMBER_BOOLEAN_NULL = null;
  public static final Number DEFAULT_NUMBER_BOOLEAN_TRUE = 1;
  public static final Number DEFAULT_NUMBER_PRIM_BOOLEAN_NULL = 0;
  public static final Number DEFAULT_NUMBER_PRIM_NULL = 0;
  public static final String DEFAULT_STRING_BOOLEAN_FALSE = Boolean.FALSE.toString();
  public static final String DEFAULT_STRING_BOOLEAN_NULL = null;
  public static final String DEFAULT_STRING_BOOLEAN_TRUE = Boolean.TRUE.toString();
  public static final String DEFAULT_TYPE_FIELD_NAME = "type";
  public static final String DEFAULT_XML_VALUE_FIELD_NAME = "_";

  static {
    DEFAULT = new JaxBsonConfiguration.Builder().build();
  }

  private final boolean allowSmartTypeField;

  @JaxBsonTransient
  private final DateBased dateBased;

  @JaxBsonName(name = "_id")
  private final String id;

  @JaxBsonTransient
  private final Map<Class<?>, IndexedEnum> indexedEnumMap = new HashMap<>();

  @JaxBsonJavaTypeAdapter(JaxBsonIgnoreTransientFieldMap.XmlAdapter.class)
  private final JaxBsonIgnoreTransientFieldMap jaxBsonIgnoreTransientFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNameFieldMap.XmlAdapter.class)
  private final JaxBsonNameFieldMap jaxBsonNameFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNameTypeMap.XmlAdapter.class)
  private final JaxBsonNameTypeMap jaxBsonNameTypeMap;

  @JaxBsonJavaTypeAdapter(JaxBsonNumberHintFieldMap.XmlAdapter.class)
  private final JaxBsonNumberHintFieldMap jaxBsonNumberHintFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToBsonPostMethodMap.XmlAdapter.class)
  private final JaxBsonToBsonPostMethodMap jaxBsonToBsonPostMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToBsonPreMethodMap.XmlAdapter.class)
  private final JaxBsonToBsonPreMethodMap jaxBsonToBsonPreMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToObjectPostMethodMap.XmlAdapter.class)
  private final JaxBsonToObjectPostMethodMap jaxBsonToObjectPostMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonToObjectPreMethodMap.XmlAdapter.class)
  private final JaxBsonToObjectPreMethodMap jaxBsonToObjectPreMethodMap;

  @JaxBsonJavaTypeAdapter(JaxBsonTransientFieldMap.XmlAdapter.class)
  private final JaxBsonTransientFieldMap jaxBsonTransientFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonXmlAnyAttributeMappingFieldMap.XmlAdapter.class)
  private final JaxBsonXmlAnyAttributeMappingFieldMap jaxBsonXmlAnyAttributeMappingFieldMap;

  @JaxBsonJavaTypeAdapter(JaxBsonXmlAnyAttributeMappingsFieldMap.XmlAdapter.class)
  private final JaxBsonXmlAnyAttributeMappingsFieldMap jaxBsonXmlAnyAttributeMappingsFieldMap;

  @JaxBsonTransient
  private final NumberBased numberBased;

  @JaxBsonTransient
  private final StringBased stringBased;

  @JaxBsonTransient
  private final JaxBsonToBson toBson;

  @JaxBsonTransient
  private final JaxBsonToObject toObject;

  private final String typeFieldName;

  private final boolean valueBooleanPrimBooleanNull;

  private final byte valueByteBooleanFalse;

  private final Byte valueByteBooleanNull;

  private final byte valueByteBooleanTrue;

  private final byte valueBytePrimBooleanNull;

  private final byte valueBytePrimNull;

  private final char valueCharacterBooleanFalse;

  private final Character valueCharacterBooleanNull;

  private final char valueCharacterBooleanTrue;

  private final char valueCharacterPrimBooleanNull;

  private final char valueCharacterPrimNull;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private final Number valueNumberBooleanFalse;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private final Number valueNumberBooleanNull;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private final Number valueNumberBooleanTrue;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private final Number valueNumberPrimBooleanNull;

  @JaxBsonJavaTypeAdapter(NumberXmlAdapter.class)
  private final Number valueNumberPrimNull;

  private final String valueStringBooleanFalse;

  private final String valueStringBooleanNull;

  private final String valueStringBooleanTrue;

  private final String xmlValueFieldName;

  private JaxBsonConfiguration() {
    this.allowSmartTypeField = false;
    this.dateBased = null;
    this.id = null;
    this.jaxBsonIgnoreTransientFieldMap = null;
    this.jaxBsonNameFieldMap = null;
    this.jaxBsonNameTypeMap = null;
    this.jaxBsonNumberHintFieldMap = null;
    this.jaxBsonToBsonPostMethodMap = null;
    this.jaxBsonToBsonPreMethodMap = null;
    this.jaxBsonToObjectPostMethodMap = null;
    this.jaxBsonToObjectPreMethodMap = null;
    this.jaxBsonTransientFieldMap = null;
    this.jaxBsonXmlAnyAttributeMappingFieldMap = null;
    this.jaxBsonXmlAnyAttributeMappingsFieldMap = null;
    this.numberBased = null;
    this.stringBased = null;
    this.toBson = null;
    this.toObject = null;
    this.typeFieldName = null;
    this.valueBooleanPrimBooleanNull = false;
    this.valueByteBooleanFalse = 0;
    this.valueByteBooleanNull = null;
    this.valueByteBooleanTrue = 0;
    this.valueBytePrimBooleanNull = 0;
    this.valueBytePrimNull = 0;
    this.valueCharacterBooleanFalse = ' ';
    this.valueCharacterBooleanNull = null;
    this.valueCharacterBooleanTrue = ' ';
    this.valueCharacterPrimBooleanNull = ' ';
    this.valueCharacterPrimNull = ' ';
    this.valueNumberBooleanFalse = null;
    this.valueNumberBooleanNull = null;
    this.valueNumberBooleanTrue = null;
    this.valueNumberPrimBooleanNull = null;
    this.valueNumberPrimNull = null;
    this.valueStringBooleanFalse = null;
    this.valueStringBooleanNull = null;
    this.valueStringBooleanTrue = null;
    this.xmlValueFieldName = null;
  }

  private JaxBsonConfiguration(final Builder b) {
    this.allowSmartTypeField = b.allowSmartTypeField;
    this.dateBased = Objects.requireNonNull(b.dateBased, "Field 'dateBased'");
    this.id = Objects.requireNonNull(b.id, "Field 'id'");
    this.jaxBsonIgnoreTransientFieldMap = b.jaxBsonIgnoreTransientFieldMap;
    this.jaxBsonNameFieldMap = b.jaxBsonNameFieldMap;
    this.jaxBsonNameTypeMap = b.jaxBsonNameTypeMap;
    this.jaxBsonNumberHintFieldMap = b.jaxBsonNumberHintFieldMap;
    this.jaxBsonToBsonPostMethodMap = b.jaxBsonToBsonPostMethodMap;
    this.jaxBsonToBsonPreMethodMap = b.jaxBsonToBsonPreMethodMap;
    this.jaxBsonToObjectPostMethodMap = b.jaxBsonToObjectPostMethodMap;
    this.jaxBsonToObjectPreMethodMap = b.jaxBsonToObjectPreMethodMap;
    this.jaxBsonTransientFieldMap = b.jaxBsonTransientFieldMap;
    this.jaxBsonXmlAnyAttributeMappingFieldMap = b.jaxBsonXmlAnyAttributeMappingFieldMap;
    this.jaxBsonXmlAnyAttributeMappingsFieldMap = b.jaxBsonXmlAnyAttributeMappingsFieldMap;
    this.numberBased = Objects.requireNonNull(b.numberBased, "Field 'numberBased'");
    this.stringBased = Objects.requireNonNull(b.stringBased, "Field 'stringBased'");
    this.toBson = Objects.requireNonNull(b.toBson, "Field 'toBson'");
    this.toObject = Objects.requireNonNull(b.toObject, "Field 'toObject'");
    this.typeFieldName = Objects.requireNonNull(b.typeFieldName, "Field 'typeFieldName'");
    this.valueBooleanPrimBooleanNull = b.valueBooleanPrimBooleanNull;
    this.valueByteBooleanFalse = b.valueByteBooleanFalse;
    this.valueByteBooleanNull = b.valueByteBooleanNull;
    this.valueByteBooleanTrue = b.valueByteBooleanTrue;
    this.valueBytePrimBooleanNull = b.valueBytePrimBooleanNull;
    this.valueBytePrimNull = b.valueBytePrimNull;
    this.valueCharacterBooleanFalse = b.valueCharacterBooleanFalse;
    this.valueCharacterBooleanNull = b.valueCharacterBooleanNull;
    this.valueCharacterBooleanTrue = b.valueCharacterBooleanTrue;
    this.valueCharacterPrimBooleanNull = b.valueCharacterPrimBooleanNull;
    this.valueCharacterPrimNull = b.valueCharacterPrimNull;
    this.valueNumberBooleanFalse =
        Objects.requireNonNull(b.valueNumberBooleanFalse, "Field 'valueNumberBooleanFalse'");
    this.valueNumberBooleanNull = b.valueNumberBooleanNull;
    this.valueNumberBooleanTrue =
        Objects.requireNonNull(b.valueNumberBooleanTrue, "Field 'valueNumberBooleanTrue'");
    this.valueNumberPrimBooleanNull =
        Objects.requireNonNull(b.valueNumberPrimBooleanNull, "Field 'valueNumberPrimBooleanNull'");
    this.valueNumberPrimNull =
        Objects.requireNonNull(b.valueNumberPrimNull, "Field 'valueNumberPrimNull'");
    this.valueStringBooleanFalse =
        Objects.requireNonNull(b.valueStringBooleanFalse, "Field 'valueStringBooleanFalse'");
    this.valueStringBooleanNull = b.valueStringBooleanNull;
    this.valueStringBooleanTrue =
        Objects.requireNonNull(b.valueStringBooleanTrue, "Field 'valueStringBooleanTrue'");
    this.xmlValueFieldName =
        Objects.requireNonNull(b.xmlValueFieldName, "Field 'xmlValueFieldName'");
  }

  public DateBased getDateBased() {
    return this.dateBased;
  }

  public String getId() {
    return this.id;
  }

  public IndexedEnum getIndexedEnum(final Class<?> type) {
    IndexedEnum ret = this.indexedEnumMap.get(type);
    if (!this.indexedEnumMap.containsKey(type)) {
      ret = new IndexedEnum(type);
      this.indexedEnumMap.put(type, ret);
    }
    return ret;
  }

  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransient(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonIgnoreTransientFieldMap == null ? null
        : this.jaxBsonIgnoreTransientFieldMap.get(field);
  }

  public JaxBsonIgnoreTransientFieldMap getJaxBsonIgnoreTransientFieldMap() {
    return this.jaxBsonIgnoreTransientFieldMap;
  }

  public JaxBsonName getJaxBsonName(final Class<?> type) {
    Objects.requireNonNull(type);
    return this.jaxBsonNameTypeMap == null ? null : this.jaxBsonNameTypeMap.get(type);
  }

  public JaxBsonName getJaxBsonName(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonNameFieldMap == null ? null : this.jaxBsonNameFieldMap.get(field);
  }

  public JaxBsonNameFieldMap getJaxBsonNameFieldMap() {
    return this.jaxBsonNameFieldMap;
  }

  public JaxBsonNameTypeMap getJaxBsonNameTypeMap() {
    return this.jaxBsonNameTypeMap;
  }

  public JaxBsonNumberHint getJaxBsonNumberHint(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonNumberHintFieldMap == null ? null
        : this.jaxBsonNumberHintFieldMap.get(field);
  }

  public JaxBsonNumberHintFieldMap getJaxBsonNumberHintFieldMap() {
    return this.jaxBsonNumberHintFieldMap;
  }

  public JaxBsonToBsonPost getJaxBsonToBsonPost(final Method method) {
    Objects.requireNonNull(method);
    return this.jaxBsonToBsonPostMethodMap == null ? null
        : this.jaxBsonToBsonPostMethodMap.get(method);
  }

  public JaxBsonToBsonPostMethodMap getJaxBsonToBsonPostMethodMap() {
    return this.jaxBsonToBsonPostMethodMap;
  }

  public JaxBsonToBsonPre getJaxBsonToBsonPre(final Method method) {
    Objects.requireNonNull(method);
    return this.jaxBsonToBsonPreMethodMap == null ? null
        : this.jaxBsonToBsonPreMethodMap.get(method);
  }

  public JaxBsonToBsonPreMethodMap getJaxBsonToBsonPreMethodMap() {
    return this.jaxBsonToBsonPreMethodMap;
  }

  public JaxBsonToObjectPost getJaxBsonToObjectPost(final Method method) {
    Objects.requireNonNull(method);
    return this.jaxBsonToObjectPostMethodMap == null ? null
        : this.jaxBsonToObjectPostMethodMap.get(method);
  }

  public JaxBsonToObjectPostMethodMap getJaxBsonToObjectPostMethodMap() {
    return this.jaxBsonToObjectPostMethodMap;
  }

  public JaxBsonToObjectPre getJaxBsonToObjectPre(final Method method) {
    Objects.requireNonNull(method);
    return this.jaxBsonToObjectPreMethodMap == null ? null
        : this.jaxBsonToObjectPreMethodMap.get(method);
  }

  public JaxBsonToObjectPreMethodMap getJaxBsonToObjectPreMethodMap() {
    return this.jaxBsonToObjectPreMethodMap;
  }

  public JaxBsonTransient getJaxBsonTransient(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonTransientFieldMap == null ? null : this.jaxBsonTransientFieldMap.get(field);
  }

  public JaxBsonTransientFieldMap getJaxBsonTransientFieldMap() {
    return this.jaxBsonTransientFieldMap;
  }

  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMapping(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonXmlAnyAttributeMappingFieldMap == null ? null
        : this.jaxBsonXmlAnyAttributeMappingFieldMap.get(field);
  }

  public JaxBsonXmlAnyAttributeMappingFieldMap getJaxBsonXmlAnyAttributeMappingFieldMap() {
    return this.jaxBsonXmlAnyAttributeMappingFieldMap;
  }

  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappings(final Field field) {
    Objects.requireNonNull(field);
    return this.jaxBsonXmlAnyAttributeMappingsFieldMap == null ? null
        : this.jaxBsonXmlAnyAttributeMappingsFieldMap.get(field);
  }

  public JaxBsonXmlAnyAttributeMappingsFieldMap getJaxBsonXmlAnyAttributeMappingsFieldMap() {
    return this.jaxBsonXmlAnyAttributeMappingsFieldMap;
  }

  public NumberBased getNumberBased() {
    return this.numberBased;
  }

  public StringBased getStringBased() {
    return this.stringBased;
  }

  public JaxBsonToBson getToBson() {
    return this.toBson;
  }

  public JaxBsonToObject getToObject() {
    return this.toObject;
  }

  public String getTypeFieldName() {
    return this.typeFieldName;
  }

  public byte getValueByteBooleanFalse() {
    return this.valueByteBooleanFalse;
  }

  public Byte getValueByteBooleanNull() {
    return this.valueByteBooleanNull;
  }

  public byte getValueByteBooleanTrue() {
    return this.valueByteBooleanTrue;
  }

  public byte getValueBytePrimBooleanNull() {
    return this.valueBytePrimBooleanNull;
  }

  public byte getValueBytePrimNull() {
    return this.valueBytePrimNull;
  }

  public char getValueCharacterBooleanFalse() {
    return this.valueCharacterBooleanFalse;
  }

  public Character getValueCharacterBooleanNull() {
    return this.valueCharacterBooleanNull;
  }

  public char getValueCharacterBooleanTrue() {
    return this.valueCharacterBooleanTrue;
  }

  public char getValueCharacterPrimBooleanNull() {
    return this.valueCharacterPrimBooleanNull;
  }

  public char getValueCharacterPrimNull() {
    return this.valueCharacterPrimNull;
  }

  public Number getValueNumberBooleanFalse() {
    return this.valueNumberBooleanFalse;
  }

  public Number getValueNumberBooleanNull() {
    return this.valueNumberBooleanNull;
  }

  public Number getValueNumberBooleanTrue() {
    return this.valueNumberBooleanTrue;
  }

  public Number getValueNumberPrimBooleanNull() {
    return this.valueNumberPrimBooleanNull;
  }

  public Number getValueNumberPrimNull() {
    return this.valueNumberPrimNull;
  }

  public String getValueStringBooleanFalse() {
    return this.valueStringBooleanFalse;
  }

  public String getValueStringBooleanNull() {
    return this.valueStringBooleanNull;
  }

  public String getValueStringBooleanTrue() {
    return this.valueStringBooleanTrue;
  }

  public String getXmlValueFieldName() {
    return this.xmlValueFieldName;
  }

  public boolean isAllowSmartTypeField() {
    return this.allowSmartTypeField;
  }

  public boolean isValueBooleanPrimBooleanNull() {
    return this.valueBooleanPrimBooleanNull;
  }
}
