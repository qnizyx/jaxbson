package org.qnixyz.jbson.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.qnixyz.jbson.JaxBsonContext;

/**
 * Annotation to let a method be called after the Bson object was converted to Java object. Zero or
 * one parameter of type {@link JaxBsonContext} is allowed.
 *
 * @author Vincenzo Zocca
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface JaxBsonToObjectPost {
}
