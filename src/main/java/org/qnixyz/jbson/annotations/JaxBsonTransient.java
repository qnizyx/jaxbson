package org.qnixyz.jbson.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation to force fields to be transient.
 *
 * @author Vincenzo Zocca
 */
@Retention(RUNTIME)
@Target({FIELD})
public @interface JaxBsonTransient {
}
