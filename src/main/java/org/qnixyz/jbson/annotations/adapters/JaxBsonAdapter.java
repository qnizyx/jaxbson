package org.qnixyz.jbson.annotations.adapters;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public abstract class JaxBsonAdapter<ValueType, BoundType> {

  protected JaxBsonAdapter() {}

  public Class<?> boundType(final Class<?> valueType, final String descr) {
    try {
      final Method method = this.getClass().getDeclaredMethod("unmarshal", valueType);
      return method.getReturnType();
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException(
          "JaxBson XML java type adapter class '" + this.getClass().getName()
              + "' doesn't have a 'unmarshal' method to convert '" + valueType + "', for " + descr,
          e);
    } catch (final SecurityException e) {
      throw new IllegalStateException(
          "Cannot access method 'unmarshal' through reflection for class '"
              + this.getClass().getName() + "' and value type '" + valueType + "' for " + descr,
          e);
    }
  }

  public abstract ValueType marshal(BoundType v) throws Exception;

  public abstract BoundType unmarshal(ValueType v) throws Exception;

  public Class<?> valueType(final Class<?> boundType, final String descr) {
    try {
      final Method method = this.getClass().getDeclaredMethod("marshal", boundType);
      return method.getReturnType();
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException(
          "JaxBson XML java type adapter class '" + this.getClass().getName()
              + "' doesn't have a 'marshal' method to convert '" + boundType + "', for " + descr,
          e);
    } catch (final SecurityException e) {
      throw new IllegalStateException(
          "Cannot access method 'marshal' through reflection for class '"
              + this.getClass().getName() + "' and value type '" + boundType + "' for " + descr,
          e);
    }
  }

  public Class<?> valueType(final Field field) {
    return valueType(field.getType(), "field " + field.getName());
  }
}
