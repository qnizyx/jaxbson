package org.qnixyz.jbson.annotations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.math.RoundingMode;

/**
 * Annotation for number operation hints.
 *
 * @author Vincenzo Zocca
 */
@Retention(RUNTIME)
@Target({FIELD})
public @interface JaxBsonNumberHint {

  public static final boolean AS_STRING_DEFAULT = false;

  public static final int PRECISION_DEFAULT = 34;

  public static final RoundingMode ROUNDING_MODE_DEFAULT = RoundingMode.HALF_UP;

  boolean asString() default AS_STRING_DEFAULT;

  int precision() default PRECISION_DEFAULT;

  RoundingMode roundingMode() default RoundingMode.HALF_UP;
}
