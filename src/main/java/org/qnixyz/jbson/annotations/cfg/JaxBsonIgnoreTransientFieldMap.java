package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonIgnoreTransientFieldMap {

  public static class Builder {

    private Map<Field, JaxBsonIgnoreTransient> map;

    public JaxBsonIgnoreTransientFieldMap build() {
      return new JaxBsonIgnoreTransientFieldMap(this);
    }

    public boolean containsKey(final Field key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonIgnoreTransient get(final Field key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Field> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Field field, final JaxBsonIgnoreTransient jaxBsonIgnoreTransient) {
      Objects.requireNonNull(field);
      Objects.requireNonNull(jaxBsonIgnoreTransient);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonIgnoreTransient);
      return this;
    }

    public JaxBsonIgnoreTransient remove(final Field field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonIgnoreTransientFieldMapEntry
      implements Comparable<JaxBsonIgnoreTransientFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonIgnoreTransientImpl jaxBsonIgnoreTransient;

    private JaxBsonIgnoreTransientFieldMapEntry() {}

    private JaxBsonIgnoreTransientFieldMapEntry(final Field field,
        final JaxBsonIgnoreTransient jaxBsonIgnoreTransient) {
      this.fieldClass = field.getDeclaringClass().getName();
      this.fieldName = field.getName();
      if (jaxBsonIgnoreTransient != null) {
        if (jaxBsonIgnoreTransient instanceof JaxBsonIgnoreTransientImpl) {
          this.jaxBsonIgnoreTransient = (JaxBsonIgnoreTransientImpl) jaxBsonIgnoreTransient;
        } else {
          this.jaxBsonIgnoreTransient = new JaxBsonIgnoreTransientImpl(jaxBsonIgnoreTransient);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonIgnoreTransientFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonIgnoreTransientFieldMapEntry other = (JaxBsonIgnoreTransientFieldMapEntry) obj;
      if (this.fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!this.fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (this.fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!this.fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.fieldClass == null ? 0 : this.fieldClass.hashCode());
      result = (prime * result) + (this.fieldName == null ? 0 : this.fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonIgnoreTransientFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonIgnoreTransientFieldMapEntry> set;

    private void add(final JaxBsonIgnoreTransientFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonIgnoreTransientFieldMapType, JaxBsonIgnoreTransientFieldMap> {

    @Override
    public JaxBsonIgnoreTransientFieldMapType marshal(final JaxBsonIgnoreTransientFieldMap v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonIgnoreTransientFieldMapType ret = new JaxBsonIgnoreTransientFieldMapType();
      for (final Entry<Field, JaxBsonIgnoreTransient> e : v.map.entrySet()) {
        ret.add(new JaxBsonIgnoreTransientFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonIgnoreTransientFieldMap unmarshal(final JaxBsonIgnoreTransientFieldMapType v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonIgnoreTransientFieldMap.Builder ret =
          new JaxBsonIgnoreTransientFieldMap.Builder();
      for (final JaxBsonIgnoreTransientFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonIgnoreTransientMap field: " + e.fieldClass + "."
                  + e.fieldName);
        }
        ret.put(field, e.jaxBsonIgnoreTransient);
      }
      return ret.build();
    }
  }

  private final Map<Field, JaxBsonIgnoreTransient> map;

  private JaxBsonIgnoreTransientFieldMap() {
    this.map = null;
  }

  private JaxBsonIgnoreTransientFieldMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Field key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonIgnoreTransient get(final Field key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Field> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
