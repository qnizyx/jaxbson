package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;

@JaxBsonName(name = "jaxBsonIgnoreTransient")
@SuppressWarnings({"all"})
public class JaxBsonIgnoreTransientImpl implements JaxBsonIgnoreTransient {

  public JaxBsonIgnoreTransientImpl() {}

  public JaxBsonIgnoreTransientImpl(final JaxBsonIgnoreTransient annotation) {}

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonIgnoreTransient.class;
  }
}
