package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredMethodByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import static org.qnixyz.jbson.impl.Utils.compareStringLists;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonToObjectPreMethodMap {

  public static class Builder {

    private Map<Method, JaxBsonToObjectPre> map;

    public JaxBsonToObjectPreMethodMap build() {
      return new JaxBsonToObjectPreMethodMap(this);
    }

    public boolean containsKey(final Method key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonToObjectPre get(final Method key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Method> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Method field, final JaxBsonToObjectPre jaxBsonIgnoreTransient) {
      Objects.requireNonNull(field);
      Objects.requireNonNull(jaxBsonIgnoreTransient);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonIgnoreTransient);
      return this;
    }

    public JaxBsonToObjectPre remove(final Method field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonToObjectPreMethodMapEntry
      implements Comparable<JaxBsonToObjectPreMethodMapEntry> {

    private JaxBsonToObjectPreImpl jaxBsonToObjectPre;

    private String methodClass;

    private String methodName;

    private List<String> methodParameterTypes;

    private JaxBsonToObjectPreMethodMapEntry() {}

    private JaxBsonToObjectPreMethodMapEntry(final Method method,
        final JaxBsonToObjectPre jaxBsonToObjectPre) {
      this.methodClass = method.getDeclaringClass().getName();
      this.methodName = method.getName();
      if (method.getParameterTypes() != null) {
        for (final Class<?> pt : method.getParameterTypes()) {
          addParameterType(pt);
        }
      }
      if (jaxBsonToObjectPre != null) {
        if (jaxBsonToObjectPre instanceof JaxBsonToObjectPreImpl) {
          this.jaxBsonToObjectPre = (JaxBsonToObjectPreImpl) jaxBsonToObjectPre;
        } else {
          this.jaxBsonToObjectPre = new JaxBsonToObjectPreImpl(jaxBsonToObjectPre);
        }
      }
    }

    private void addParameterType(final Class<?> pt) {
      if (this.methodParameterTypes == null) {
        this.methodParameterTypes = new ArrayList<>();
      }
      this.methodParameterTypes.add(pt.getName());
    }

    @Override
    public int compareTo(final JaxBsonToObjectPreMethodMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.methodClass, o.methodClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.methodName, o.methodName);
      if (ret != 0) {
        return ret;
      }
      ret = compareStringLists(this.methodParameterTypes, o.methodParameterTypes);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonToObjectPreMethodMapEntry other = (JaxBsonToObjectPreMethodMapEntry) obj;
      if (this.methodClass == null) {
        if (other.methodClass != null) {
          return false;
        }
      } else if (!this.methodClass.equals(other.methodClass)) {
        return false;
      }
      if (this.methodName == null) {
        if (other.methodName != null) {
          return false;
        }
      } else if (!this.methodName.equals(other.methodName)) {
        return false;
      }
      if (this.methodParameterTypes == null) {
        if (other.methodParameterTypes != null) {
          return false;
        }
      } else if (!this.methodParameterTypes.equals(other.methodParameterTypes)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.methodClass == null ? 0 : this.methodClass.hashCode());
      result = (prime * result) + (this.methodName == null ? 0 : this.methodName.hashCode());
      return result;
    }
  }

  private static class JaxBsonToObjectPreMethodMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonToObjectPreMethodMapEntry> set;

    private void add(final JaxBsonToObjectPreMethodMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonToObjectPreMethodMapType, JaxBsonToObjectPreMethodMap> {

    @Override
    public JaxBsonToObjectPreMethodMapType marshal(final JaxBsonToObjectPreMethodMap v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonToObjectPreMethodMapType ret = new JaxBsonToObjectPreMethodMapType();
      for (final Entry<Method, JaxBsonToObjectPre> e : v.map.entrySet()) {
        ret.add(new JaxBsonToObjectPreMethodMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonToObjectPreMethodMap unmarshal(final JaxBsonToObjectPreMethodMapType v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonToObjectPreMethodMap.Builder ret = new JaxBsonToObjectPreMethodMap.Builder();
      for (final JaxBsonToObjectPreMethodMapEntry e : v.set) {
        final Method method =
            declaredMethodByName(e.methodClass, e.methodName, e.methodParameterTypes);
        if (ret.containsKey(method)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonToObjectPreMap method: "
              + e.methodClass + "." + e.methodName);
        }
        ret.put(method, e.jaxBsonToObjectPre);
      }
      return ret.build();
    }
  }

  private final Map<Method, JaxBsonToObjectPre> map;

  private JaxBsonToObjectPreMethodMap() {
    this.map = null;
  }

  private JaxBsonToObjectPreMethodMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Method key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonToObjectPre get(final Method key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Method> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
