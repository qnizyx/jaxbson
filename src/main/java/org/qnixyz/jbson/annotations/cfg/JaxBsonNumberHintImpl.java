package org.qnixyz.jbson.annotations.cfg;

import java.lang.annotation.Annotation;
import java.math.RoundingMode;
import java.util.Objects;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;

@JaxBsonName(name = "jaxBsonNumberHint")
@SuppressWarnings({"all"})
public class JaxBsonNumberHintImpl implements JaxBsonNumberHint {

  public static class Builder {
    private boolean asString = AS_STRING_DEFAULT;
    private int precision = PRECISION_DEFAULT;
    private RoundingMode roundingMode = RoundingMode.HALF_UP;

    public Builder() {}

    public Builder(final JaxBsonNumberHint other) {
      other(other);
    }

    public Builder asString(final boolean asString) {
      this.asString = asString;
      return this;
    }

    public JaxBsonNumberHintImpl build() {
      return new JaxBsonNumberHintImpl(this);
    }

    public Builder other(final JaxBsonNumberHint other) {
      asString(other.asString());
      precision(other.precision());
      roundingMode(other.roundingMode());
      return this;
    }

    public Builder precision(final int precision) {
      this.precision = precision;
      return this;
    }

    public Builder roundingMode(final RoundingMode roundingMode) {
      this.roundingMode = Objects.requireNonNull(roundingMode);
      return this;
    }
  }

  public static final JaxBsonNumberHintImpl DEFAULT = new JaxBsonNumberHintImpl.Builder().build();

  private final boolean asString;
  private final int precision;
  private final RoundingMode roundingMode;

  private JaxBsonNumberHintImpl() {
    this.asString = AS_STRING_DEFAULT;
    this.precision = PRECISION_DEFAULT;
    this.roundingMode = RoundingMode.HALF_UP;
  }

  private JaxBsonNumberHintImpl(final Builder b) {
    this.asString = b.asString;
    this.precision = b.precision;
    this.roundingMode = Objects.requireNonNull(b.roundingMode);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return JaxBsonNumberHint.class;
  }

  @Override
  public boolean asString() {
    return this.asString;
  }

  @Override
  public int precision() {
    return this.precision;
  }

  @Override
  public RoundingMode roundingMode() {
    return this.roundingMode;
  }
}
