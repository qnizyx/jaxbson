package org.qnixyz.jbson.annotations.cfg;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

public class Utils {

  private static final Class<?>[] NO_PARAMETER_TYPES = {};

  public static Class<?> classByNamne(final String className) {
    return classByNamne(className, true);
  }

  public static Class<?> classByNamne(final String className, final boolean throwException) {
    Objects.requireNonNull(className);
    try {
      return Class.forName(className);
    } catch (final ClassNotFoundException e) {
      if (throwException) {
        throw new IllegalStateException("No class found where className=" + className, e);
      }
      return null;
    }
  }

  public static Field declaredFieldByName(final Class<?> fieldClass, final String fieldName) {
    Objects.requireNonNull(fieldClass);
    Objects.requireNonNull(fieldName);
    try {
      return fieldClass.getDeclaredField(fieldName);
    } catch (final NoSuchFieldException e) {
      throw new IllegalStateException("No declared field found where fieldClass="
          + fieldClass.getName() + " and fieldName=" + fieldName, e);
    } catch (final SecurityException e) {
      throw new IllegalStateException("Cannot access declared field where fieldClass="
          + fieldClass.getName() + " and fieldName=" + fieldName, e);
    }
  }

  public static Field declaredFieldByName(final String fieldClass, final String fieldName) {
    return declaredFieldByName(classByNamne(fieldClass), fieldName);
  }

  public static Method declaredMethodByName(final Class<?> methodClass, final String methodName,
      final Class<?>... methodParameterTypes) {
    try {
      return methodClass.getDeclaredMethod(methodName, methodParameterTypes);
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException(
          "No declared method where methodClass=" + methodClass.getName() + ", methodName="
              + methodName + ", methodParameterTypes=" + methodParameterTypes,
          e);
    } catch (final SecurityException e) {
      throw new IllegalStateException(
          "Cannot access declared method where methodClass=" + methodClass.getName()
              + ", methodName=" + methodName + ", methodParameterTypes=" + methodParameterTypes,
          e);
    }
  }

  public static Method declaredMethodByName(final String fieldClass, final String methodName,
      final List<String> methodParameterTypes) {
    return declaredMethodByName(classByNamne(fieldClass), methodName,
        parameterTypesByName(methodParameterTypes));
  }

  public static Class<?>[] parameterTypesByName(final List<String> parameterTypes) {
    if (parameterTypes == null || parameterTypes.isEmpty()) {
      return NO_PARAMETER_TYPES;
    }
    final Class<?>[] ret = new Class<?>[parameterTypes.size()];
    for (int i = 0; i < parameterTypes.size(); i++) {
      final String className = parameterTypes.get(i);
      ret[i] = classByNamne(className, false);
      if (ret[i] == null) {
        throw new IllegalStateException(
            "Class not found for parameter type[" + i + "]: " + className);
      }
    }
    return ret;
  }
}
