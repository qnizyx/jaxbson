package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonXmlAnyAttributeMappingsFieldMap {

  public static class Builder {

    private Map<Field, JaxBsonXmlAnyAttributeMappings> map;

    public JaxBsonXmlAnyAttributeMappingsFieldMap build() {
      return new JaxBsonXmlAnyAttributeMappingsFieldMap(this);
    }

    public boolean containsKey(final Field key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonXmlAnyAttributeMappings get(final Field key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Field> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Field field,
        final JaxBsonXmlAnyAttributeMappings jaxBsonIgnoreTransient) {
      Objects.requireNonNull(field);
      Objects.requireNonNull(jaxBsonIgnoreTransient);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonIgnoreTransient);
      return this;
    }

    public JaxBsonXmlAnyAttributeMappings remove(final Field field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonXmlAnyAttributeMappingsFieldMapEntry
      implements Comparable<JaxBsonXmlAnyAttributeMappingsFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    @JaxBsonName(name = "jaxBsonXmlAnyAttributeMappings")
    private JaxBsonXmlAnyAttributeMappingImpl[] jaxBsonXmlAnyAttributeMappings;

    private JaxBsonXmlAnyAttributeMappingsFieldMapEntry() {}

    private JaxBsonXmlAnyAttributeMappingsFieldMapEntry(final Field field,
        final JaxBsonXmlAnyAttributeMappings jaxBsonXmlAnyAttributeMappings) {
      this.fieldClass = field.getDeclaringClass().getName();
      this.fieldName = field.getName();
      if (jaxBsonXmlAnyAttributeMappings != null) {
        final JaxBsonXmlAnyAttributeMapping[] mappings = jaxBsonXmlAnyAttributeMappings.mappings();
        if (mappings != null) {
          this.jaxBsonXmlAnyAttributeMappings =
              new JaxBsonXmlAnyAttributeMappingImpl[mappings.length];
          for (int i = 0; i < mappings.length; i++) {
            final JaxBsonXmlAnyAttributeMapping mapping = mappings[i];
            if (mapping instanceof JaxBsonXmlAnyAttributeMappingImpl) {
              this.jaxBsonXmlAnyAttributeMappings[i] = (JaxBsonXmlAnyAttributeMappingImpl) mapping;
            } else {
              this.jaxBsonXmlAnyAttributeMappings[i] =
                  new JaxBsonXmlAnyAttributeMappingImpl(mapping);
            }
          }
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonXmlAnyAttributeMappingsFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMapEntry other =
          (JaxBsonXmlAnyAttributeMappingsFieldMapEntry) obj;
      if (this.fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!this.fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (this.fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!this.fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.fieldClass == null ? 0 : this.fieldClass.hashCode());
      result = (prime * result) + (this.fieldName == null ? 0 : this.fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonXmlAnyAttributeMappingsFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonXmlAnyAttributeMappingsFieldMapEntry> set;

    private void add(final JaxBsonXmlAnyAttributeMappingsFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter extends
      JaxBsonAdapter<JaxBsonXmlAnyAttributeMappingsFieldMapType, JaxBsonXmlAnyAttributeMappingsFieldMap> {

    @Override
    public JaxBsonXmlAnyAttributeMappingsFieldMapType marshal(
        final JaxBsonXmlAnyAttributeMappingsFieldMap v) throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMapType ret =
          new JaxBsonXmlAnyAttributeMappingsFieldMapType();
      for (final Entry<Field, JaxBsonXmlAnyAttributeMappings> e : v.map.entrySet()) {
        ret.add(new JaxBsonXmlAnyAttributeMappingsFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonXmlAnyAttributeMappingsFieldMap unmarshal(
        final JaxBsonXmlAnyAttributeMappingsFieldMapType v) throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonXmlAnyAttributeMappingsFieldMap.Builder ret =
          new JaxBsonXmlAnyAttributeMappingsFieldMap.Builder();
      for (final JaxBsonXmlAnyAttributeMappingsFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonXmlAnyAttributeMappingsMap field: " + e.fieldClass
                  + "." + e.fieldName);
        }
        ret.put(field, new JaxBsonXmlAnyAttributeMappingsImpl(e.jaxBsonXmlAnyAttributeMappings));
      }
      return ret.build();
    }
  }

  private final Map<Field, JaxBsonXmlAnyAttributeMappings> map;

  private JaxBsonXmlAnyAttributeMappingsFieldMap() {
    this.map = null;
  }

  private JaxBsonXmlAnyAttributeMappingsFieldMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Field key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonXmlAnyAttributeMappings get(final Field key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Field> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
