package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonTransientFieldMap {

  public static class Builder {

    private Map<Field, JaxBsonTransient> map;

    public JaxBsonTransientFieldMap build() {
      return new JaxBsonTransientFieldMap(this);
    }

    public boolean containsKey(final Field key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonTransient get(final Field key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Field> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Field field, final JaxBsonTransient jaxBsonIgnoreTransient) {
      Objects.requireNonNull(field);
      Objects.requireNonNull(jaxBsonIgnoreTransient);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonIgnoreTransient);
      return this;
    }

    public JaxBsonTransient remove(final Field field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonTransientFieldMapEntry
      implements Comparable<JaxBsonTransientFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonTransientImpl jaxBsonTransient;

    private JaxBsonTransientFieldMapEntry() {}

    private JaxBsonTransientFieldMapEntry(final Field field,
        final JaxBsonTransient jaxBsonTransient) {
      this.fieldClass = field.getDeclaringClass().getName();
      this.fieldName = field.getName();
      if (jaxBsonTransient != null) {
        if (jaxBsonTransient instanceof JaxBsonTransientImpl) {
          this.jaxBsonTransient = (JaxBsonTransientImpl) jaxBsonTransient;
        } else {
          this.jaxBsonTransient = new JaxBsonTransientImpl(jaxBsonTransient);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonTransientFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonTransientFieldMapEntry other = (JaxBsonTransientFieldMapEntry) obj;
      if (this.fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!this.fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (this.fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!this.fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.fieldClass == null ? 0 : this.fieldClass.hashCode());
      result = (prime * result) + (this.fieldName == null ? 0 : this.fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonTransientFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonTransientFieldMapEntry> set;

    private void add(final JaxBsonTransientFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonTransientFieldMapType, JaxBsonTransientFieldMap> {

    @Override
    public JaxBsonTransientFieldMapType marshal(final JaxBsonTransientFieldMap v) throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonTransientFieldMapType ret = new JaxBsonTransientFieldMapType();
      for (final Entry<Field, JaxBsonTransient> e : v.map.entrySet()) {
        ret.add(new JaxBsonTransientFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonTransientFieldMap unmarshal(final JaxBsonTransientFieldMapType v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonTransientFieldMap.Builder ret = new JaxBsonTransientFieldMap.Builder();
      for (final JaxBsonTransientFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonTransientMap field: "
              + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonTransient);
      }
      return ret.build();
    }
  }

  private final Map<Field, JaxBsonTransient> map;

  private JaxBsonTransientFieldMap() {
    this.map = null;
  }

  private JaxBsonTransientFieldMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Field key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonTransient get(final Field key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Field> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
