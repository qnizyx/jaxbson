package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonNumberHintFieldMap {

  public static class Builder {

    private Map<Field, JaxBsonNumberHint> map;

    public JaxBsonNumberHintFieldMap build() {
      return new JaxBsonNumberHintFieldMap(this);
    }

    public boolean containsKey(final Field key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonNumberHint get(final Field key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Field> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Field field, final JaxBsonNumberHint jaxBsonNumberHint) {
      Objects.requireNonNull(field);
      Objects.requireNonNull(jaxBsonNumberHint);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonNumberHint);
      return this;
    }

    public JaxBsonNumberHint remove(final Field field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonNumberHintFieldMapEntry
      implements Comparable<JaxBsonNumberHintFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonNumberHintImpl jaxBsonNumberHint;

    private JaxBsonNumberHintFieldMapEntry() {}

    private JaxBsonNumberHintFieldMapEntry(final Field field,
        final JaxBsonNumberHint jaxBsonNumberHint) {
      this.fieldClass = field.getDeclaringClass().getName();
      this.fieldName = field.getName();
      if (jaxBsonNumberHint != null) {
        if (jaxBsonNumberHint instanceof JaxBsonNumberHintImpl) {
          this.jaxBsonNumberHint = (JaxBsonNumberHintImpl) jaxBsonNumberHint;
        } else {
          this.jaxBsonNumberHint = new JaxBsonNumberHintImpl.Builder(jaxBsonNumberHint).build();
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonNumberHintFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonNumberHintFieldMapEntry other = (JaxBsonNumberHintFieldMapEntry) obj;
      if (this.fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!this.fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (this.fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!this.fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.fieldClass == null ? 0 : this.fieldClass.hashCode());
      result = (prime * result) + (this.fieldName == null ? 0 : this.fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonNumberHintFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonNumberHintFieldMapEntry> set;

    private void add(final JaxBsonNumberHintFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonNumberHintFieldMapType, JaxBsonNumberHintFieldMap> {

    @Override
    public JaxBsonNumberHintFieldMapType marshal(final JaxBsonNumberHintFieldMap v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonNumberHintFieldMapType ret = new JaxBsonNumberHintFieldMapType();
      for (final Entry<Field, JaxBsonNumberHint> e : v.map.entrySet()) {
        ret.add(new JaxBsonNumberHintFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonNumberHintFieldMap unmarshal(final JaxBsonNumberHintFieldMapType v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonNumberHintFieldMap.Builder ret = new JaxBsonNumberHintFieldMap.Builder();
      for (final JaxBsonNumberHintFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonNumberHintMap field: "
              + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonNumberHint);
      }
      return ret.build();
    }
  }

  private final Map<Field, JaxBsonNumberHint> map;

  private JaxBsonNumberHintFieldMap() {
    this.map = null;
  }

  private JaxBsonNumberHintFieldMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Field key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonNumberHint get(final Field key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Field> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
