package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredFieldByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonNameFieldMap {

  public static class Builder {

    private Map<Field, JaxBsonName> map;

    public JaxBsonNameFieldMap build() {
      return new JaxBsonNameFieldMap(this);
    }

    public boolean containsKey(final Field key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonName get(final Field key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Field> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Field field, final JaxBsonName jaxBsonName) {
      Objects.requireNonNull(field);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(field, jaxBsonName);
      return this;
    }

    public JaxBsonName remove(final Field field) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(field);
    }
  }

  private static class JaxBsonNameFieldMapEntry implements Comparable<JaxBsonNameFieldMapEntry> {

    private String fieldClass;

    private String fieldName;

    private JaxBsonNameImpl jaxBsonName;

    private JaxBsonNameFieldMapEntry() {}

    private JaxBsonNameFieldMapEntry(final Field field, final JaxBsonName jaxBsonName) {
      this.fieldClass = field.getDeclaringClass().getName();
      this.fieldName = field.getName();
      if (jaxBsonName != null) {
        if (jaxBsonName instanceof JaxBsonNameImpl) {
          this.jaxBsonName = (JaxBsonNameImpl) jaxBsonName;
        } else {
          this.jaxBsonName = new JaxBsonNameImpl(jaxBsonName);
        }
      }
    }

    @Override
    public int compareTo(final JaxBsonNameFieldMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.fieldClass, o.fieldClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.fieldName, o.fieldName);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonNameFieldMapEntry other = (JaxBsonNameFieldMapEntry) obj;
      if (this.fieldClass == null) {
        if (other.fieldClass != null) {
          return false;
        }
      } else if (!this.fieldClass.equals(other.fieldClass)) {
        return false;
      }
      if (this.fieldName == null) {
        if (other.fieldName != null) {
          return false;
        }
      } else if (!this.fieldName.equals(other.fieldName)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.fieldClass == null ? 0 : this.fieldClass.hashCode());
      result = (prime * result) + (this.fieldName == null ? 0 : this.fieldName.hashCode());
      return result;
    }
  }

  private static class JaxBsonNameFieldMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonNameFieldMapEntry> set;

    private void add(final JaxBsonNameFieldMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonNameFieldMapType, JaxBsonNameFieldMap> {

    @Override
    public JaxBsonNameFieldMapType marshal(final JaxBsonNameFieldMap v) throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameFieldMapType ret = new JaxBsonNameFieldMapType();
      for (final Entry<Field, JaxBsonName> e : v.map.entrySet()) {
        ret.add(new JaxBsonNameFieldMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonNameFieldMap unmarshal(final JaxBsonNameFieldMapType v) throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonNameFieldMap.Builder ret = new JaxBsonNameFieldMap.Builder();
      for (final JaxBsonNameFieldMapEntry e : v.set) {
        final Field field = declaredFieldByName(e.fieldClass, e.fieldName);
        if (ret.containsKey(field)) {
          throw new IllegalStateException(
              "Multiple definitions of JaxBsonNameMap field: " + e.fieldClass + "." + e.fieldName);
        }
        ret.put(field, e.jaxBsonName);
      }
      return ret.build();
    }
  }

  private final Map<Field, JaxBsonName> map;

  private JaxBsonNameFieldMap() {
    this.map = null;
  }

  private JaxBsonNameFieldMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Field key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonName get(final Field key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Field> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
