package org.qnixyz.jbson.annotations.cfg;

import static org.qnixyz.jbson.annotations.cfg.Utils.declaredMethodByName;
import static org.qnixyz.jbson.impl.Utils.compare;
import static org.qnixyz.jbson.impl.Utils.compareStringLists;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonToBsonPostMethodMap {

  public static class Builder {

    private Map<Method, JaxBsonToBsonPost> map;

    public JaxBsonToBsonPostMethodMap build() {
      return new JaxBsonToBsonPostMethodMap(this);
    }

    public boolean containsKey(final Method key) {
      return this.map == null ? false : this.map.containsKey(key);
    }

    public JaxBsonToBsonPost get(final Method key) {
      Objects.requireNonNull(key);
      return this.map == null ? null : this.map.get(key);
    }

    public boolean isEmpty() {
      return (this.map == null) || this.map.isEmpty();
    }

    public Set<Method> keySet() {
      return this.map == null ? Collections.emptySet() : this.map.keySet();
    }

    public Builder put(final Method method, final JaxBsonToBsonPost jaxBsonToBsonPost) {
      Objects.requireNonNull(method);
      Objects.requireNonNull(jaxBsonToBsonPost);
      if (this.map == null) {
        this.map = new HashMap<>();
      }
      this.map.put(method, jaxBsonToBsonPost);
      return this;
    }

    public JaxBsonToBsonPost remove(final Method method) {
      if (this.map == null) {
        return null;
      }
      return this.map.remove(method);
    }
  }

  private static class JaxBsonToBsonPostMethodMapEntry
      implements Comparable<JaxBsonToBsonPostMethodMapEntry> {

    private JaxBsonToBsonPostImpl jaxBsonToBsonPost;

    private String methodClass;

    private String methodName;

    private List<String> methodParameterTypes;

    private JaxBsonToBsonPostMethodMapEntry() {}

    private JaxBsonToBsonPostMethodMapEntry(final Method method,
        final JaxBsonToBsonPost jaxBsonToBsonPost) {
      this.methodClass = method.getDeclaringClass().getName();
      this.methodName = method.getName();
      if (method.getParameterTypes() != null) {
        for (final Class<?> pt : method.getParameterTypes()) {
          addParameterType(pt);
        }
      }
      if (jaxBsonToBsonPost != null) {
        if (jaxBsonToBsonPost instanceof JaxBsonToBsonPostImpl) {
          this.jaxBsonToBsonPost = (JaxBsonToBsonPostImpl) jaxBsonToBsonPost;
        } else {
          this.jaxBsonToBsonPost = new JaxBsonToBsonPostImpl(jaxBsonToBsonPost);
        }
      }
    }

    private void addParameterType(final Class<?> pt) {
      if (this.methodParameterTypes == null) {
        this.methodParameterTypes = new ArrayList<>();
      }
      this.methodParameterTypes.add(pt.getName());
    }

    @Override
    public int compareTo(final JaxBsonToBsonPostMethodMapEntry o) {
      if (o == null) {
        return 1;
      }
      int ret = compare(this.methodClass, o.methodClass);
      if (ret != 0) {
        return ret;
      }
      ret = compare(this.methodName, o.methodName);
      if (ret != 0) {
        return ret;
      }
      ret = compareStringLists(this.methodParameterTypes, o.methodParameterTypes);
      if (ret != 0) {
        return ret;
      }
      return 0;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (getClass() != obj.getClass()) {
        return false;
      }
      final JaxBsonToBsonPostMethodMapEntry other = (JaxBsonToBsonPostMethodMapEntry) obj;
      if (this.methodClass == null) {
        if (other.methodClass != null) {
          return false;
        }
      } else if (!this.methodClass.equals(other.methodClass)) {
        return false;
      }
      if (this.methodName == null) {
        if (other.methodName != null) {
          return false;
        }
      } else if (!this.methodName.equals(other.methodName)) {
        return false;
      }
      if (this.methodParameterTypes == null) {
        if (other.methodParameterTypes != null) {
          return false;
        }
      } else if (!this.methodParameterTypes.equals(other.methodParameterTypes)) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + (this.methodClass == null ? 0 : this.methodClass.hashCode());
      result = (prime * result) + (this.methodName == null ? 0 : this.methodName.hashCode());
      result = (prime * result)
          + (this.methodParameterTypes == null ? 0 : this.methodParameterTypes.hashCode());
      return result;
    }
  }

  private static class JaxBsonToBsonPostMethodMapType {

    @JaxBsonName(name = "entries")
    private SortedSet<JaxBsonToBsonPostMethodMapEntry> set;

    private void add(final JaxBsonToBsonPostMethodMapEntry e) {
      Objects.requireNonNull(e);
      if (this.set == null) {
        this.set = new TreeSet<>();
      }
      this.set.add(e);
    }

    private boolean isEmpty() {
      return (this.set == null) || this.set.isEmpty();
    }
  }

  public static class XmlAdapter
      extends JaxBsonAdapter<JaxBsonToBsonPostMethodMapType, JaxBsonToBsonPostMethodMap> {

    @Override
    public JaxBsonToBsonPostMethodMapType marshal(final JaxBsonToBsonPostMethodMap v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonToBsonPostMethodMapType ret = new JaxBsonToBsonPostMethodMapType();
      for (final Entry<Method, JaxBsonToBsonPost> e : v.map.entrySet()) {
        ret.add(new JaxBsonToBsonPostMethodMapEntry(e.getKey(), e.getValue()));
      }
      return ret;
    }

    @Override
    public JaxBsonToBsonPostMethodMap unmarshal(final JaxBsonToBsonPostMethodMapType v)
        throws Exception {
      if ((v == null) || v.isEmpty()) {
        return null;
      }
      final JaxBsonToBsonPostMethodMap.Builder ret = new JaxBsonToBsonPostMethodMap.Builder();
      for (final JaxBsonToBsonPostMethodMapEntry e : v.set) {
        final Method method =
            declaredMethodByName(e.methodClass, e.methodName, e.methodParameterTypes);
        if (ret.containsKey(method)) {
          throw new IllegalStateException("Multiple definitions of JaxBsonToBsonPostMap method: "
              + e.methodClass + "." + e.methodName);
        }
        ret.put(method, e.jaxBsonToBsonPost);
      }
      return ret.build();
    }
  }

  private final Map<Method, JaxBsonToBsonPost> map;

  private JaxBsonToBsonPostMethodMap() {
    this.map = null;
  }

  private JaxBsonToBsonPostMethodMap(final Builder b) {
    this.map = b.map == null ? Collections.emptyMap() : Collections.unmodifiableMap(b.map);
  }

  public boolean containsKey(final Method key) {
    return this.map == null ? false : this.map.containsKey(key);
  }

  public JaxBsonToBsonPost get(final Method key) {
    Objects.requireNonNull(key);
    return this.map == null ? null : this.map.get(key);
  }

  public boolean isEmpty() {
    return (this.map == null) || this.map.isEmpty();
  }

  public Set<Method> keySet() {
    return this.map == null ? Collections.emptySet() : this.map.keySet();
  }
}
