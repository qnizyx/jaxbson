package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlTransientHelperJavax extends org.qnixyz.jbson.helpers.XmlTransientHelper {

  private final javax.xml.bind.annotation.XmlTransient xmlTransient;

  XmlTransientHelperJavax(final javax.xml.bind.annotation.XmlTransient xmlTransient) {
    this.xmlTransient = Objects.requireNonNull(xmlTransient);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlTransient.annotationType();
  }
}
