package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlTypeHelperJakarta extends org.qnixyz.jbson.helpers.XmlTypeHelper {

  private final jakarta.xml.bind.annotation.XmlType xmlType;

  XmlTypeHelperJakarta(final jakarta.xml.bind.annotation.XmlType xmlType) {
    this.xmlType = Objects.requireNonNull(xmlType);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlType.annotationType();
  }

  @Override
  public Class<?> factoryClass() {
    return this.xmlType.factoryClass();
  }

  @Override
  public String factoryMethod() {
    return this.xmlType.factoryMethod();
  }

  @Override
  public String name() {
    return this.xmlType.name();
  }

  @Override
  public String namespace() {
    return this.xmlType.namespace();
  }

  @Override
  public String[] propOrder() {
    return this.xmlType.propOrder();
  }
}
