package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;

public class JaxBsonJaxb {

  public static Object instance(final Annotation a) {
    Object ret = XmlAnyAttributeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlAnyElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlAttributeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementsHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementRefHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementRefsHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementWrapperHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlEnumValueHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = JaxBsonJavaTypeAdapterHelper.jaxBsonJavaTypeAdapter(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlRootElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlSeeAlsoHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlTransientHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlTypeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlValueHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    return null;
  }

  public static Object instance(final Annotation[] a) {
    Object ret = XmlAnyAttributeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlAnyElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlAttributeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementsHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementRefHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementRefsHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlElementWrapperHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlEnumValueHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = JaxBsonJavaTypeAdapterHelper.jaxBsonJavaTypeAdapter(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlRootElementHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlSeeAlsoHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlTransientHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlTypeHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    ret = XmlValueHelper.instance(a);
    if (ret != null) {
      return ret;
    }
    return null;
  }
}
