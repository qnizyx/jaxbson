package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlValueHelperJavax extends org.qnixyz.jbson.helpers.XmlValueHelper {

  private final javax.xml.bind.annotation.XmlValue xmlValue;

  XmlValueHelperJavax(final javax.xml.bind.annotation.XmlValue xmlValue) {
    this.xmlValue = Objects.requireNonNull(xmlValue);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlValue.annotationType();
  }
}
