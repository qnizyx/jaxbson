package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlEnumValueHelperJavax extends org.qnixyz.jbson.helpers.XmlEnumValueHelper {

  private final javax.xml.bind.annotation.XmlEnumValue xmlEnumValue;

  XmlEnumValueHelperJavax(final javax.xml.bind.annotation.XmlEnumValue xmlEnumValue) {
    this.xmlEnumValue = Objects.requireNonNull(xmlEnumValue);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlEnumValue.annotationType();
  }

  @Override
  public String value() {
    return this.xmlEnumValue.value();
  }
}
