package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;

public class JaxBsonSeeAlsoHelper {

  private static void add(final Set<Class<?>> set, final Class<?> cls) {
    if (cls != null) {
      set.add(cls);
    }
  }

  private static void add(final Set<Class<?>> set, final Class<?>[] classes) {
    if (classes != null) {
      for (final Class<?> cls : classes) {
        add(set, cls);
      }
    }
  }

  private static JaxBsonSeeAlso create(final Set<Class<?>> set) {
    if ((set == null) || set.isEmpty()) {
      return null;
    }
    final Class<?>[] arr = set.toArray(new Class<?>[set.size()]);
    return new JaxBsonSeeAlso() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return JaxBsonSeeAlso.class;
      }

      @Override
      public Class<?>[] value() {
        return arr;
      }
    };
  }

  public static JaxBsonSeeAlso instance(final Annotation[] as, final boolean forField) {
    if (as == null) {
      return null;
    }
    final Set<Class<?>> set = new HashSet<>();
    for (final Annotation a : as) {
      {
        if (a.annotationType().equals(JaxBsonSeeAlso.class)) {
          add(set, ((JaxBsonSeeAlso) a).value());
          continue;
        }
      }
      {
        final XmlSeeAlsoHelper h = XmlSeeAlsoHelper.instance(a);
        if (h != null) {
          add(set, h.value());
          continue;
        }
      }
      {
        final XmlElementHelper h = XmlElementHelper.instance(a);
        if ((h != null) && !h.isDefaultType()) {
          add(set, h.type());
          continue;
        }
      }
      {
        final XmlElementsHelper hs = XmlElementsHelper.instance(a);
        if (hs != null) {
          if (hs.value() != null) {
            for (final XmlElementHelper h : hs.value()) {
              if ((h != null) && !h.isDefaultType()) {
                add(set, h.type());
                continue;
              }
            }
          }
          continue;
        }
      }
      {
        final XmlElementRefHelper h = XmlElementRefHelper.instance(a);
        if (h != null) {
          add(set, h.type());
          continue;
        }
      }
      {
        final XmlElementRefsHelper hs = XmlElementRefsHelper.instance(a);
        if (hs != null) {
          if (hs.value() != null) {
            for (final XmlElementRefHelper h : hs.value()) {
              if (h != null) {
                add(set, h.type());
                continue;
              }
            }
          }
          continue;
        }
      }
    }
    return create(set);
  }

  public static JaxBsonSeeAlso instance(final Class<?> type) {
    return instance(type.getAnnotations(), false);
  }

  public static JaxBsonSeeAlso instance(final Field field) {
    return instance(field.getAnnotations(), true);
  }
}
