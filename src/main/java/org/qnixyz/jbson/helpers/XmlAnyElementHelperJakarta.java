package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.util.Objects;

class XmlAnyElementHelperJakarta extends org.qnixyz.jbson.helpers.XmlAnyElementHelper {

  private final jakarta.xml.bind.annotation.XmlAnyElement xmlAnyElement;

  XmlAnyElementHelperJakarta(final jakarta.xml.bind.annotation.XmlAnyElement xmlAnyElement) {
    this.xmlAnyElement = Objects.requireNonNull(xmlAnyElement);
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return this.xmlAnyElement.annotationType();
  }

  @Override
  public boolean lax() {
    return this.xmlAnyElement.lax();
  }
}
