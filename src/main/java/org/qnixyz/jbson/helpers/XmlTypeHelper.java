package org.qnixyz.jbson.helpers;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class XmlTypeHelper {

  private static final Class<?> JAKARTA_JAXB_CLASS = loadJaxbClass("jakarta");
  private static final Constructor<? extends XmlTypeHelper> JAKARTA_JAXBSON_CONSTRUCTOR =
      getJaxBsonConstructor(JAKARTA_JAXB_CLASS, "Jakarta");

  private static final Class<?> JAVAX_JAXB_CLASS = loadJaxbClass("javax");
  private static final Constructor<? extends XmlTypeHelper> JAVAX_JAXBSON_CONSTRUCTOR =
      getJaxBsonConstructor(JAVAX_JAXB_CLASS, "Javax");

  private static Constructor<? extends XmlTypeHelper> getJaxBsonConstructor(
      final Class<?> clsPrefix, final String suffix) {
    if (clsPrefix == null) {
      return null;
    }
    try {
      @SuppressWarnings("unchecked")
      final Class<? extends XmlTypeHelper> cls =
          (Class<? extends XmlTypeHelper>) Thread.currentThread().getContextClassLoader()
              .loadClass(XmlTypeHelper.class.getName() + suffix);
      final Constructor<? extends XmlTypeHelper> ret = cls.getDeclaredConstructor(clsPrefix);
      ret.setAccessible(true);
      return ret;
    } catch (final Exception e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  public static XmlTypeHelper instance(final Annotation a) {
    if (a.annotationType().equals(JAKARTA_JAXB_CLASS)) {
      return instanceJakarta(a);
    }
    if (a.annotationType().equals(JAVAX_JAXB_CLASS)) {
      return instanceJavax(a);
    }
    return null;
  }

  public static XmlTypeHelper instance(final Annotation[] as) {
    if (as == null) {
      return null;
    }
    for (final Annotation a : as) {
      final XmlTypeHelper ret = XmlTypeHelper.instance(a);
      if (ret != null) {
        return ret;
      }
    }
    return null;
  }

  public static XmlTypeHelper instance(final Class<?> type) {
    return instance(type.getAnnotations());
  }

  private static XmlTypeHelper instanceJakarta(final Annotation a) {
    try {
      return JAKARTA_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static XmlTypeHelper instanceJavax(final Annotation a) {
    try {
      return JAVAX_JAXBSON_CONSTRUCTOR.newInstance(a);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException("This is a bug", e);
    }
  }

  private static Class<?> loadJaxbClass(final String prefix) {
    try {
      return Thread.currentThread().getContextClassLoader()
          .loadClass(prefix + ".xml.bind.annotation.XmlType");
    } catch (final ClassNotFoundException e) {
      return null;
    }
  }

  public abstract Class<? extends Annotation> annotationType();

  public abstract Class<?> factoryClass();

  public abstract String factoryMethod();

  public abstract String name();

  public abstract String namespace();

  public abstract String[] propOrder();
}
