package org.qnixyz.jbson.helpers;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;

public class JaxBsonAdapterHelperJakarta extends JaxBsonAdapter<Object, Object> {

  private final jakarta.xml.bind.annotation.adapters.XmlAdapter<Object, Object> xmlAdapter;
  private final Class<?> xmlAdapterClass;

  public JaxBsonAdapterHelperJakarta(final Class<?> xmlAdapterClass) {
    this.xmlAdapterClass = Objects.requireNonNull(xmlAdapterClass);
    if (!jakarta.xml.bind.annotation.adapters.XmlAdapter.class.isAssignableFrom(xmlAdapterClass)) {
      throw new IllegalStateException("Class " + xmlAdapterClass + " is not a subclass of "
          + jakarta.xml.bind.annotation.adapters.XmlAdapter.class);
    }
    this.xmlAdapter = instance();
  }

  @Override
  public Class<?> boundType(final Class<?> valueType, final String descr) {
    try {
      final Method method = this.xmlAdapterClass.getDeclaredMethod("unmarshal", valueType);
      return method.getReturnType();
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException(
          "JaxBson XML java type adapter class '" + this.xmlAdapterClass.getName()
              + "' doesn't have a 'unmarshal' method to convert '" + valueType + "', for " + descr,
          e);
    } catch (final SecurityException e) {
      throw new IllegalStateException(
          "Cannot access method 'unmarshal' through reflection for class '"
              + this.xmlAdapterClass.getName() + "' and value type '" + valueType + "' for "
              + descr,
          e);
    }
  }

  private jakarta.xml.bind.annotation.adapters.XmlAdapter<Object, Object> instance() {
    try {
      @SuppressWarnings("unchecked")
      final Constructor<jakarta.xml.bind.annotation.adapters.XmlAdapter<Object, Object>> c =
          (Constructor<jakarta.xml.bind.annotation.adapters.XmlAdapter<Object, Object>>) this.xmlAdapterClass
              .getDeclaredConstructor();
      c.setAccessible(true);
      return c.newInstance();
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException("Class has no empty constructor: " + this.xmlAdapterClass, e);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException e) {
      throw new IllegalStateException(
          "Failed to instantiate class with empty constructor: " + this.xmlAdapterClass, e);
    }
  }

  @Override
  public Object marshal(final Object v) throws Exception {
    return this.xmlAdapter.marshal(v);
  }

  @Override
  public Object unmarshal(final Object v) throws Exception {
    return this.xmlAdapter.unmarshal(v);
  }

  @Override
  public Class<?> valueType(final Class<?> boundType, final String descr) {
    try {
      final Method method = this.xmlAdapterClass.getDeclaredMethod("marshal", boundType);
      return method.getReturnType();
    } catch (final NoSuchMethodException e) {
      throw new IllegalStateException(
          "XML java type adapter class '" + this.xmlAdapterClass.getName()
              + "' doesn't have a 'marshal' method to convert '" + boundType + "', for " + descr,
          e);
    } catch (final SecurityException e) {
      throw new IllegalStateException(
          "Cannot access method 'marshal' through reflection for class '"
              + this.xmlAdapterClass.getName() + "' and value type '" + boundType + "' for "
              + descr,
          e);
    }
  }
}
