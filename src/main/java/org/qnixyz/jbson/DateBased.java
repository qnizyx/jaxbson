package org.qnixyz.jbson;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class DateBased {

  @FunctionalInterface
  public interface Checker {
    boolean check(Class<?> type);
  }

  private static class FuncMap {

    private final Map<Class<?>, ToBson> toBsonMap = new HashMap<>();

    private final Map<Class<?>, ToObject> toObjectMap = new HashMap<>();

    private FuncMap() {
      putToObjectMap();
      putToBsonMap();
    }

    private ToBson getToBson(final Class<?> type) {
      return this.toBsonMap.get(type);
    }

    private ToObject getToObject(final Class<?> type) {
      return this.toObjectMap.get(type);
    }

    private void putToBsonMap() {
      this.toBsonMap.put(Date.class, (value) -> (Date) value);
      this.toBsonMap.put(GregorianCalendar.class, (value) -> ((GregorianCalendar) value).getTime());
      this.toBsonMap.put(XMLGregorianCalendar.class,
          (value) -> ((XMLGregorianCalendar) value).toGregorianCalendar().getTime());
    }

    private void putToObjectMap() {
      this.toObjectMap.put(Calendar.class, (__, value) -> {
        final Calendar ret = Calendar.getInstance();
        ret.setTime(value);
        return ret;
      });
      this.toObjectMap.put(Date.class, (__, value) -> value);
      this.toObjectMap.put(GregorianCalendar.class, (__, value) -> {
        final GregorianCalendar ret = new GregorianCalendar();
        ret.setTime(value);
        return ret;
      });
      this.toObjectMap.put(XMLGregorianCalendar.class, (__, value) -> {
        try {
          final GregorianCalendar ret = new GregorianCalendar();
          ret.setTime(value);
          return DatatypeFactory.newInstance().newXMLGregorianCalendar(ret);
        } catch (final DatatypeConfigurationException e) {
          throw new IllegalStateException(
              "Failed to convert value to Java XMLGregorianCalendar object: " + value, e);
        }
      });
    }
  }

  @FunctionalInterface
  public interface ToBson {
    Date convert(Object value);
  }

  @FunctionalInterface
  public interface ToObject {
    Object convert(Class<?> type, Date value);
  }

  private static final FuncMap FUNC_MAP = new FuncMap();

  private static Class<?>[] TYPES = {//
      Calendar.class, //
      Date.class, //
      GregorianCalendar.class, //
      XMLGregorianCalendar.class, //
  };

  public static Set<Class<?>> TYPES_SET =
      Collections.unmodifiableSet(new HashSet<>(Arrays.asList(TYPES)));

  private final Set<Class<?>> _checkerFalse = new HashSet<>();

  private final Set<Class<?>> _checkerTrue = new HashSet<>();

  private final Checker checker = (type) -> {
    if (this._checkerTrue.contains(type)) {
      return true;
    }
    if (this._checkerFalse.contains(type)) {
      return false;
    }
    for (final Class<?> e : TYPES_SET) {
      if (e.isAssignableFrom(type)) {
        this._checkerTrue.add(type);
        return true;
      }
    }
    this._checkerFalse.add(type);
    return false;
  };

  private final ToBson toBson = (value) -> {
    if (value == null) {
      return null;
    }
    final Class<?> type = value.getClass();
    final ToBson func = FUNC_MAP.getToBson(type);
    if (func != null) {
      return func.convert(value);
    }
    if (XMLGregorianCalendar.class.isAssignableFrom(type)) {
      return ((XMLGregorianCalendar) value).toGregorianCalendar().getTime();
    }
    throw new IllegalStateException(
        "Failed to convert value to Bson: " + value + " of type " + value.getClass().getName());
  };

  private final ToObject toObject = (type, value) -> {
    if (value == null) {
      return null;
    }
    final ToObject func = FUNC_MAP.getToObject(type);
    if (func != null) {
      return func.convert(null, value);
    }
    if (XMLGregorianCalendar.class.isAssignableFrom(type)) {
      try {
        final GregorianCalendar ret = new GregorianCalendar();
        ret.setTime(value);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(ret);
      } catch (final DatatypeConfigurationException e) {
        throw new IllegalStateException(
            "Failed to convert value to Java XMLGregorianCalendar object: " + value, e);
      }
    }
    throw new IllegalStateException("Failed to convert value to Java object: " + value + " of type "
        + value.getClass().getName());
  };

  public Checker getChecker() {
    return this.checker;
  }

  public ToBson getToBson() {
    return this.toBson;
  }

  public ToObject getToObject() {
    return this.toObject;
  }
}
