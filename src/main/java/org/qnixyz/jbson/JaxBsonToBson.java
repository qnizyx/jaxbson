package org.qnixyz.jbson;

import static org.qnixyz.jbson.impl.Utils.toObjectArray;
import static org.qnixyz.jbson.impl.Utils.toPrimitiveArray;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.bson.types.Binary;
import org.bson.types.ObjectId;

public class JaxBsonToBson {

  @FunctionalInterface
  public interface BinaryCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BooleanArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Boolean[] src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, boolean[] src);
  }

  @FunctionalInterface
  public interface BooleanPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  public static class Builder {

    private BinaryCollectionToBson binaryCollectionToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      src.forEach(e -> ret
          .add(fieldCtx.getConfiguration().getToBson().getBinaryToBson().convert(fieldCtx, e)));
      return ret;
    };

    private BinaryToBson binaryToBson = (fieldCtx, src) -> {
      return src;
    };

    private BooleanArrayToBson booleanArrayToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Boolean e : src) {
        ret.add(fieldCtx.getConfiguration().getToBson().getBooleanToBson().convert(fieldCtx, e));
      }
      return ret;
    };

    private BooleanCollectionToBson booleanCollectionToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      src.forEach(e -> ret
          .add(fieldCtx.getConfiguration().getToBson().getBooleanToBson().convert(fieldCtx, e)));
      return ret;
    };

    private BooleanPrimArrayToBson booleanPrimArrayToBson =
        (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

    private BooleanPrimToBson booleanPrimToBson = (fieldCtx, src) -> {
      return src;
    };

    private BooleanToBson booleanToBson = (fieldCtx, src) -> {
      return src;
    };

    private ByteArrayToBson byteArrayToBson = (fieldCtx, src) -> new Binary(toPrimitiveArray(src));

    private ByteCollectionToBson byteCollectionToBson = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final Byte e : src) {
        if (e != null) {
          ret[i] = e;
        }
        i++;
      }
      return new Binary(ret);
    };

    private BytePrimArrayToBson bytePrimArrayToBson = (fieldCtx, src) -> new Binary(src);

    private BytePrimToBson bytePrimToBson = (fieldCtx, src) -> new Binary(new byte[] {src});

    private ByteToBson byteToBson = (fieldCtx, src) -> new Binary(new byte[] {src});

    private CharacterArrayToBson characterArrayToBson = (fieldCtx, src) -> {
      return new String(toPrimitiveArray(src));
    };

    private CharacterCollectionToBson characterCollectionToBson = (fieldCtx, src) -> {
      final char[] ret = new char[src.size()];
      int i = 0;
      for (final Character e : src) {
        if (e != null) {
          ret[i] = e;
        }
        i++;
      }
      return new String(ret);
    };

    private CharacterPrimArrayToBson characterPrimArrayToBson = (fieldCtx, src) -> {
      return new String(src);
    };

    private CharacterPrimToBson characterPrimToBson = (fieldCtx, src) -> {
      return Character.valueOf(src).toString();
    };

    private CharacterToBson characterToBson = (fieldCtx, src) -> src.toString();

    private DateBasedArrayToBson dateBasedArrayToBson = (fieldCtx, src) -> {
      final List<Date> ret = new ArrayList<>();
      for (final Object e : src) {
        ret.add(fieldCtx.getConfiguration().getDateBased().getToBson().convert(e));
      }
      return ret;
    };

    private DateBasedCollectionToBson dateBasedCollectionToBson = (fieldCtx, src) -> {
      final List<Date> ret = new ArrayList<>();
      src.forEach(e -> ret.add(fieldCtx.getConfiguration().getDateBased().getToBson().convert(e)));
      return ret;
    };

    private DateBasedToBson dateBasedToBson =
        (fieldCtx, src) -> fieldCtx.getConfiguration().getDateBased().getToBson().convert(src);

    private DoublePrimArrayToBson doublePrimArrayToBson =
        (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

    private EnumArrayToBson enumArrayToBson = (fieldCtx, enumType, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Enum<?> e : src) {
        ret.add(
            fieldCtx.getConfiguration().getToBson().getEnumToBson().convert(fieldCtx, enumType, e));
      }
      return ret;
    };

    private EnumCollectionToBson enumCollectionToBson = (fieldCtx, enumType, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Enum<?> e : src) {
        ret.add(
            fieldCtx.getConfiguration().getToBson().getEnumToBson().convert(fieldCtx, enumType, e));
      }
      return ret;
    };

    private EnumToBson enumToBson = (fieldCtx, enumType, src) -> fieldCtx.getConfiguration()
        .getIndexedEnum(enumType).enumToString(src);

    private FloatPrimArrayToBson floatPrimArrayToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Float e : src) {
        ret.add(fieldCtx.getConfiguration().getToBson().getFloatPrimToBson().convert(fieldCtx, e));
      }
      return ret;
    };

    private FloatPrimToBson floatPrimToBson = (fieldCtx, src) -> {
      return Float.valueOf(src).doubleValue();
    };

    private IntegerPrimArrayToBson integerPrimArrayToBson =
        (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

    private LongPrimArrayToBson longPrimArrayToBson =
        (fieldCtx, src) -> Arrays.asList(toObjectArray(src));

    private NumberBasedArrayToBson numberBasedArrayToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Object e : src) {
        final NumberBased.BsonNumber bsonNumber =
            fieldCtx.getConfiguration().getNumberBased().getToBson().convert(fieldCtx, e);
        ret.add(bsonNumber == null ? null : bsonNumber.getValue());
      }
      return ret;
    };

    private NumberBasedCollectionToBson numberBasedCollectionToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      src.forEach(e -> {
        final NumberBased.BsonNumber bsonNumber =
            fieldCtx.getConfiguration().getNumberBased().getToBson().convert(fieldCtx, e);
        ret.add(bsonNumber == null ? null : bsonNumber.getValue());
      });
      return ret;
    };

    private NumberBasedToBson numberBasedToBson = (fieldCtx, src) -> fieldCtx.getConfiguration()
        .getNumberBased().getToBson().convert(fieldCtx, src).getValue();

    private ObjectIdArrayToBson objectIdArrayToBson = (fieldCtx, src) -> Arrays.asList(src);

    private ObjectIdCollectionToBson objectIdCollectionToBson = (fieldCtx, src) -> src;

    private ObjectIdToBson objectIdToBson = (fieldCtx, src) -> src;

    private ShortPrimArrayToBson shortPrimArrayToBson = (fieldCtx, src) -> {
      final List<Object> ret = new ArrayList<>();
      for (final Short e : src) {
        ret.add(fieldCtx.getConfiguration().getToBson().getShortPrimToBson().convert(fieldCtx, e));
      }
      return ret;
    };

    private ShortPrimToBson shortPrimToBson = (fieldCtx, src) -> Short.valueOf(src).intValue();

    private StringBasedArrayToBson stringBasedArrayToBson = (fieldCtx, src) -> {
      final List<String> ret = new ArrayList<>();
      for (final Object e : src) {
        ret.add(fieldCtx.getConfiguration().getStringBased().getToBson().convert(e));
      }
      return ret;
    };

    private StringBasedCollectionToBson stringBasedCollectionToBson = (fieldCtx, src) -> {
      final List<String> ret = new ArrayList<>();
      src.forEach(
          e -> ret.add(fieldCtx.getConfiguration().getStringBased().getToBson().convert(e)));
      return ret;
    };

    private StringBasedToBson stringBasedToBson =
        (fieldCtx, src) -> fieldCtx.getConfiguration().getStringBased().getToBson().convert(src);

    public Builder binaryCollectionToBson(final BinaryCollectionToBson binaryCollectionToBson) {
      this.binaryCollectionToBson = Objects.requireNonNull(binaryCollectionToBson);
      return this;
    }

    public Builder binaryToBson(final BinaryToBson binaryToBson) {
      this.binaryToBson = Objects.requireNonNull(binaryToBson);
      return this;
    }

    public Builder booleanArrayToBson(final BooleanArrayToBson booleanArrayToBson) {
      this.booleanArrayToBson = Objects.requireNonNull(booleanArrayToBson);
      return this;
    }

    public Builder booleanCollectionToBson(final BooleanCollectionToBson booleanCollectionToBson) {
      this.booleanCollectionToBson = Objects.requireNonNull(booleanCollectionToBson);
      return this;
    }

    public Builder booleanPrimArrayToBson(final BooleanPrimArrayToBson booleanPrimArrayToBson) {
      this.booleanPrimArrayToBson = Objects.requireNonNull(booleanPrimArrayToBson);
      return this;
    }

    public Builder booleanPrimToBson(final BooleanPrimToBson booleanPrimToBson) {
      this.booleanPrimToBson = Objects.requireNonNull(booleanPrimToBson);
      return this;
    }

    public Builder booleanToBson(final BooleanToBson booleanToBson) {
      this.booleanToBson = Objects.requireNonNull(booleanToBson);
      return this;
    }

    public JaxBsonToBson build() {
      return new JaxBsonToBson(this);
    }

    public Builder byteArrayToBson(final ByteArrayToBson byteArrayToBson) {
      this.byteArrayToBson = Objects.requireNonNull(byteArrayToBson);
      return this;
    }

    public Builder byteCollectionToBson(final ByteCollectionToBson byteCollectionToBson) {
      this.byteCollectionToBson = Objects.requireNonNull(byteCollectionToBson);
      return this;
    }

    public Builder bytePrimArrayToBson(final BytePrimArrayToBson bytePrimArrayToBson) {
      this.bytePrimArrayToBson = Objects.requireNonNull(bytePrimArrayToBson);
      return this;
    }

    public Builder bytePrimToBson(final BytePrimToBson bytePrimToBson) {
      this.bytePrimToBson = Objects.requireNonNull(bytePrimToBson);
      return this;
    }

    public Builder byteToBson(final ByteToBson byteToBson) {
      this.byteToBson = Objects.requireNonNull(byteToBson);
      return this;
    }

    public Builder characterArrayToBson(final CharacterArrayToBson characterArrayToBson) {
      this.characterArrayToBson = Objects.requireNonNull(characterArrayToBson);
      return this;
    }

    public Builder characterCollectionToBson(
        final CharacterCollectionToBson characterCollectionToBson) {
      this.characterCollectionToBson = Objects.requireNonNull(characterCollectionToBson);
      return this;
    }

    public Builder characterPrimArrayToBson(
        final CharacterPrimArrayToBson characterPrimArrayToBson) {
      this.characterPrimArrayToBson = Objects.requireNonNull(characterPrimArrayToBson);
      return this;
    }

    public Builder characterPrimToBson(final CharacterPrimToBson characterPrimToBson) {
      this.characterPrimToBson = Objects.requireNonNull(characterPrimToBson);
      return this;
    }

    public Builder characterToBson(final CharacterToBson characterToBson) {
      this.characterToBson = Objects.requireNonNull(characterToBson);
      return this;
    }

    public Builder dateBasedArrayToBson(final DateBasedArrayToBson dateBasedArrayToBson) {
      this.dateBasedArrayToBson = Objects.requireNonNull(dateBasedArrayToBson);
      return this;
    }

    public Builder dateBasedCollectionToBson(
        final DateBasedCollectionToBson dateBasedCollectionToBson) {
      this.dateBasedCollectionToBson = Objects.requireNonNull(dateBasedCollectionToBson);
      return this;
    }

    public Builder dateBasedToBson(final DateBasedToBson dateBasedToBson) {
      this.dateBasedToBson = Objects.requireNonNull(dateBasedToBson);
      return this;
    }

    public Builder doublePrimArrayToBson(final DoublePrimArrayToBson doublePrimArrayToBson) {
      this.doublePrimArrayToBson = Objects.requireNonNull(doublePrimArrayToBson);
      return this;
    }

    public Builder enumArrayToBson(final EnumArrayToBson enumArrayToBson) {
      this.enumArrayToBson = Objects.requireNonNull(enumArrayToBson);
      return this;
    }

    public Builder enumCollectionToBson(final EnumCollectionToBson enumCollectionToBson) {
      this.enumCollectionToBson = Objects.requireNonNull(enumCollectionToBson);
      return this;
    }

    public Builder enumToBson(final EnumToBson enumToBson) {
      this.enumToBson = Objects.requireNonNull(enumToBson);
      return this;
    }

    public Builder floatPrimArrayToBson(final FloatPrimArrayToBson floatPrimArrayToBson) {
      this.floatPrimArrayToBson = Objects.requireNonNull(floatPrimArrayToBson);
      return this;
    }

    public Builder floatPrimToBson(final FloatPrimToBson floatPrimToBson) {
      this.floatPrimToBson = Objects.requireNonNull(floatPrimToBson);
      return this;
    }

    public Builder integerPrimArrayToBson(final IntegerPrimArrayToBson integerPrimArrayToBson) {
      this.integerPrimArrayToBson = Objects.requireNonNull(integerPrimArrayToBson);
      return this;
    }

    public Builder longPrimArrayToBson(final LongPrimArrayToBson longPrimArrayToBson) {
      this.longPrimArrayToBson = Objects.requireNonNull(longPrimArrayToBson);
      return this;
    }

    public Builder numberBasedArrayToBson(final NumberBasedArrayToBson numberBasedArrayToBson) {
      this.numberBasedArrayToBson = Objects.requireNonNull(numberBasedArrayToBson);
      return this;
    }

    public Builder numberBasedCollectionToBson(
        final NumberBasedCollectionToBson numberBasedCollectionToBson) {
      this.numberBasedCollectionToBson = Objects.requireNonNull(numberBasedCollectionToBson);
      return this;
    }

    public Builder numberBasedToBson(final NumberBasedToBson numberBasedToBson) {
      this.numberBasedToBson = Objects.requireNonNull(numberBasedToBson);
      return this;
    }

    public Builder objectIdArrayToBson(final ObjectIdArrayToBson objectIdArrayToBson) {
      this.objectIdArrayToBson = Objects.requireNonNull(objectIdArrayToBson);
      return this;
    }

    public Builder objectIdCollectionToBson(
        final ObjectIdCollectionToBson objectIdCollectionToBson) {
      this.objectIdCollectionToBson = Objects.requireNonNull(objectIdCollectionToBson);
      return this;
    }

    public Builder objectIdToBson(final ObjectIdToBson objectIdToBson) {
      this.objectIdToBson = Objects.requireNonNull(objectIdToBson);
      return this;
    }

    public Builder shortPrimArrayToBson(final ShortPrimArrayToBson shortPrimArrayToBson) {
      this.shortPrimArrayToBson = Objects.requireNonNull(shortPrimArrayToBson);
      return this;
    }

    public Builder shortPrimToBson(final ShortPrimToBson shortPrimToBson) {
      this.shortPrimToBson = Objects.requireNonNull(shortPrimToBson);
      return this;
    }

    public Builder stringBasedArrayToBson(final StringBasedArrayToBson stringBasedArrayToBson) {
      this.stringBasedArrayToBson = Objects.requireNonNull(stringBasedArrayToBson);
      return this;
    }

    public Builder stringBasedCollectionToBson(
        final StringBasedCollectionToBson stringBasedCollectionToBson) {
      this.stringBasedCollectionToBson = Objects.requireNonNull(stringBasedCollectionToBson);
      return this;
    }

    public Builder stringBasedToBson(final StringBasedToBson stringBasedToBson) {
      this.stringBasedToBson = Objects.requireNonNull(stringBasedToBson);
      return this;
    }
  }

  @FunctionalInterface
  public interface ByteArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Byte[] src);
  }

  @FunctionalInterface
  public interface ByteCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Byte> src);
  }

  @FunctionalInterface
  public interface BytePrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, byte[] src);
  }

  @FunctionalInterface
  public interface BytePrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, byte src);
  }

  @FunctionalInterface
  public interface ByteToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Byte src);
  }

  @FunctionalInterface
  public interface CharacterArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Character[] src);
  }

  @FunctionalInterface
  public interface CharacterCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<Character> src);
  }

  @FunctionalInterface
  public interface CharacterPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, char[] src);
  }

  @FunctionalInterface
  public interface CharacterPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, char src);
  }

  @FunctionalInterface
  public interface CharacterToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Character src);
  }

  @FunctionalInterface
  public interface DateBasedArrayToBson {
    List<Date> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface DateBasedCollectionToBson {
    List<Date> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface DateBasedToBson {
    Date convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  @FunctionalInterface
  public interface DoublePrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, double[] src);
  }

  @FunctionalInterface
  public interface EnumArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Enum<?>[] src);
  }

  @FunctionalInterface
  public interface EnumCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Enum<?>> src);
  }

  @FunctionalInterface
  public interface EnumToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Enum<?> src);
  }

  @FunctionalInterface
  public interface FloatPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, float[] src);
  }

  @FunctionalInterface
  public interface FloatPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, float src);
  }

  @FunctionalInterface
  public interface IntegerPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, int[] src);
  }

  @FunctionalInterface
  public interface LongPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, long[] src);
  }

  @FunctionalInterface
  public interface NumberBasedArrayToBson {
    List<Object> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface NumberBasedCollectionToBson {
    List<Object> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface NumberBasedToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  @FunctionalInterface
  public interface ObjectIdArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, ObjectId[] src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBson {
    Object convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdToBson {
    Object convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ShortPrimArrayToBson {
    Object convert(JaxBsonFieldContext fieldCtx, short[] src);
  }

  @FunctionalInterface
  public interface ShortPrimToBson {
    Object convert(JaxBsonFieldContext fieldCtx, short src);
  }

  @FunctionalInterface
  public interface StringBasedArrayToBson {
    List<String> convert(JaxBsonFieldContext fieldCtx, Object[] src);
  }

  @FunctionalInterface
  public interface StringBasedCollectionToBson {
    List<String> convert(JaxBsonFieldContext fieldCtx, Collection<Object> src);
  }

  @FunctionalInterface
  public interface StringBasedToBson {
    String convert(JaxBsonFieldContext fieldCtx, Object src);
  }

  public static final JaxBsonToBson DEFAULT = new JaxBsonToBson.Builder().build();

  private final BinaryCollectionToBson binaryCollectionToBson;
  private final BinaryToBson binaryToBson;
  private final BooleanArrayToBson booleanArrayToBson;
  private final BooleanCollectionToBson booleanCollectionToBson;
  private final BooleanPrimArrayToBson booleanPrimArrayToBson;
  private final BooleanPrimToBson booleanPrimToBson;
  private final BooleanToBson booleanToBson;
  private final ByteArrayToBson byteArrayToBson;
  private final ByteCollectionToBson byteCollectionToBson;
  private final BytePrimArrayToBson bytePrimArrayToBson;
  private final BytePrimToBson bytePrimToBson;
  private final ByteToBson byteToBson;
  private final CharacterArrayToBson characterArrayToBson;
  private final CharacterCollectionToBson characterCollectionToBson;
  private final CharacterPrimArrayToBson characterPrimArrayToBson;
  private final CharacterPrimToBson characterPrimToBson;
  private final CharacterToBson characterToBson;
  private final DateBasedArrayToBson dateBasedArrayToBson;
  private final DateBasedCollectionToBson dateBasedCollectionToBson;
  private final DateBasedToBson dateBasedToBson;
  private final DoublePrimArrayToBson doublePrimArrayToBson;
  private final EnumArrayToBson enumArrayToBson;
  private final EnumCollectionToBson enumCollectionToBson;
  private final EnumToBson enumToBson;
  private final FloatPrimArrayToBson floatPrimArrayToBson;
  private final FloatPrimToBson floatPrimToBson;
  private final IntegerPrimArrayToBson integerPrimArrayToBson;
  private final LongPrimArrayToBson longPrimArrayToBson;
  private final NumberBasedArrayToBson numberBasedArrayToBson;
  private final NumberBasedCollectionToBson numberBasedCollectionToBson;
  private final NumberBasedToBson numberBasedToBson;
  private final ObjectIdArrayToBson objectIdArrayToBson;
  private final ObjectIdCollectionToBson objectIdCollectionToBson;
  private final ObjectIdToBson objectIdToBson;
  private final ShortPrimArrayToBson shortPrimArrayToBson;
  private final ShortPrimToBson shortPrimToBson;
  private final StringBasedArrayToBson stringBasedArrayToBson;
  private final StringBasedCollectionToBson stringBasedCollectionToBson;
  private final StringBasedToBson stringBasedToBson;

  private JaxBsonToBson() {
    this.binaryCollectionToBson = null;
    this.binaryToBson = null;
    this.booleanArrayToBson = null;
    this.booleanCollectionToBson = null;
    this.booleanPrimArrayToBson = null;
    this.booleanPrimToBson = null;
    this.booleanToBson = null;
    this.byteArrayToBson = null;
    this.byteCollectionToBson = null;
    this.bytePrimArrayToBson = null;
    this.bytePrimToBson = null;
    this.byteToBson = null;
    this.characterArrayToBson = null;
    this.characterCollectionToBson = null;
    this.characterPrimArrayToBson = null;
    this.characterPrimToBson = null;
    this.characterToBson = null;
    this.dateBasedArrayToBson = null;
    this.dateBasedCollectionToBson = null;
    this.dateBasedToBson = null;
    this.doublePrimArrayToBson = null;
    this.enumArrayToBson = null;
    this.enumCollectionToBson = null;
    this.enumToBson = null;
    this.floatPrimArrayToBson = null;
    this.floatPrimToBson = null;
    this.integerPrimArrayToBson = null;
    this.longPrimArrayToBson = null;
    this.numberBasedArrayToBson = null;
    this.numberBasedCollectionToBson = null;
    this.numberBasedToBson = null;
    this.objectIdArrayToBson = null;
    this.objectIdCollectionToBson = null;
    this.objectIdToBson = null;
    this.shortPrimArrayToBson = null;
    this.shortPrimToBson = null;
    this.stringBasedArrayToBson = null;
    this.stringBasedCollectionToBson = null;
    this.stringBasedToBson = null;
  }

  private JaxBsonToBson(final Builder b) {
    this.binaryCollectionToBson = b.binaryCollectionToBson;
    this.binaryToBson = b.binaryToBson;
    this.booleanArrayToBson = b.booleanArrayToBson;
    this.booleanCollectionToBson = b.booleanCollectionToBson;
    this.booleanPrimArrayToBson = b.booleanPrimArrayToBson;
    this.booleanPrimToBson = b.booleanPrimToBson;
    this.booleanToBson = b.booleanToBson;
    this.byteArrayToBson = b.byteArrayToBson;
    this.byteCollectionToBson = b.byteCollectionToBson;
    this.bytePrimArrayToBson = b.bytePrimArrayToBson;
    this.bytePrimToBson = b.bytePrimToBson;
    this.byteToBson = b.byteToBson;
    this.characterArrayToBson = b.characterArrayToBson;
    this.characterCollectionToBson = b.characterCollectionToBson;
    this.characterPrimArrayToBson = b.characterPrimArrayToBson;
    this.characterPrimToBson = b.characterPrimToBson;
    this.characterToBson = b.characterToBson;
    this.dateBasedArrayToBson = b.dateBasedArrayToBson;
    this.dateBasedCollectionToBson = b.dateBasedCollectionToBson;
    this.dateBasedToBson = b.dateBasedToBson;
    this.doublePrimArrayToBson = b.doublePrimArrayToBson;
    this.enumArrayToBson = b.enumArrayToBson;
    this.enumCollectionToBson = b.enumCollectionToBson;
    this.enumToBson = b.enumToBson;
    this.floatPrimArrayToBson = b.floatPrimArrayToBson;
    this.floatPrimToBson = b.floatPrimToBson;
    this.integerPrimArrayToBson = b.integerPrimArrayToBson;
    this.longPrimArrayToBson = b.longPrimArrayToBson;
    this.numberBasedArrayToBson = b.numberBasedArrayToBson;
    this.numberBasedCollectionToBson = b.numberBasedCollectionToBson;
    this.numberBasedToBson = b.numberBasedToBson;
    this.objectIdArrayToBson = b.objectIdArrayToBson;
    this.objectIdCollectionToBson = b.objectIdCollectionToBson;
    this.objectIdToBson = b.objectIdToBson;
    this.shortPrimArrayToBson = b.shortPrimArrayToBson;
    this.shortPrimToBson = b.shortPrimToBson;
    this.stringBasedArrayToBson = b.stringBasedArrayToBson;
    this.stringBasedCollectionToBson = b.stringBasedCollectionToBson;
    this.stringBasedToBson = b.stringBasedToBson;
  }

  public BinaryCollectionToBson getBinaryCollectionToBson() {
    return this.binaryCollectionToBson;
  }

  public BinaryToBson getBinaryToBson() {
    return this.binaryToBson;
  }

  public BooleanArrayToBson getBooleanArrayToBson() {
    return this.booleanArrayToBson;
  }

  public BooleanCollectionToBson getBooleanCollectionToBson() {
    return this.booleanCollectionToBson;
  }

  public BooleanPrimArrayToBson getBooleanPrimArrayToBson() {
    return this.booleanPrimArrayToBson;
  }

  public BooleanPrimToBson getBooleanPrimToBson() {
    return this.booleanPrimToBson;
  }

  public BooleanToBson getBooleanToBson() {
    return this.booleanToBson;
  }

  public ByteArrayToBson getByteArrayToBson() {
    return this.byteArrayToBson;
  }

  public ByteCollectionToBson getByteCollectionToBson() {
    return this.byteCollectionToBson;
  }

  public BytePrimArrayToBson getBytePrimArrayToBson() {
    return this.bytePrimArrayToBson;
  }

  public BytePrimToBson getBytePrimToBson() {
    return this.bytePrimToBson;
  }

  public ByteToBson getByteToBson() {
    return this.byteToBson;
  }

  public CharacterArrayToBson getCharacterArrayToBson() {
    return this.characterArrayToBson;
  }

  public CharacterCollectionToBson getCharacterCollectionToBson() {
    return this.characterCollectionToBson;
  }

  public CharacterPrimArrayToBson getCharacterPrimArrayToBson() {
    return this.characterPrimArrayToBson;
  }

  public CharacterPrimToBson getCharacterPrimToBson() {
    return this.characterPrimToBson;
  }

  public CharacterToBson getCharacterToBson() {
    return this.characterToBson;
  }

  public DateBasedArrayToBson getDateBasedArrayToBson() {
    return this.dateBasedArrayToBson;
  }

  public DateBasedCollectionToBson getDateBasedCollectionToBson() {
    return this.dateBasedCollectionToBson;
  }

  public DateBasedToBson getDateBasedToBson() {
    return this.dateBasedToBson;
  }

  public DoublePrimArrayToBson getDoublePrimArrayToBson() {
    return this.doublePrimArrayToBson;
  }

  public EnumArrayToBson getEnumArrayToBson() {
    return this.enumArrayToBson;
  }

  public EnumCollectionToBson getEnumCollectionToBson() {
    return this.enumCollectionToBson;
  }

  public EnumToBson getEnumToBson() {
    return this.enumToBson;
  }

  public FloatPrimArrayToBson getFloatPrimArrayToBson() {
    return this.floatPrimArrayToBson;
  }

  public FloatPrimToBson getFloatPrimToBson() {
    return this.floatPrimToBson;
  }

  public IntegerPrimArrayToBson getIntegerPrimArrayToBson() {
    return this.integerPrimArrayToBson;
  }

  public LongPrimArrayToBson getLongPrimArrayToBson() {
    return this.longPrimArrayToBson;
  }

  public NumberBasedArrayToBson getNumberBasedArrayToBson() {
    return this.numberBasedArrayToBson;
  }

  public NumberBasedCollectionToBson getNumberBasedCollectionToBson() {
    return this.numberBasedCollectionToBson;
  }

  public NumberBasedToBson getNumberBasedToBson() {
    return this.numberBasedToBson;
  }

  public ObjectIdArrayToBson getObjectIdArrayToBson() {
    return this.objectIdArrayToBson;
  }

  public ObjectIdCollectionToBson getObjectIdCollectionToBson() {
    return this.objectIdCollectionToBson;
  }

  public ObjectIdToBson getObjectIdToBson() {
    return this.objectIdToBson;
  }

  public ShortPrimArrayToBson getShortPrimArrayToBson() {
    return this.shortPrimArrayToBson;
  }

  public ShortPrimToBson getShortPrimToBson() {
    return this.shortPrimToBson;
  }

  public StringBasedArrayToBson getStringBasedArrayToBson() {
    return this.stringBasedArrayToBson;
  }

  public StringBasedCollectionToBson getStringBasedCollectionToBson() {
    return this.stringBasedCollectionToBson;
  }

  public StringBasedToBson getStringBasedToBson() {
    return this.stringBasedToBson;
  }
}
