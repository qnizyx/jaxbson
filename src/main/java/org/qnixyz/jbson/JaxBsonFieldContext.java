package org.qnixyz.jbson;

import java.lang.reflect.Field;
import org.qnixyz.jbson.annotations.JaxBsonIgnoreTransient;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;
import org.qnixyz.jbson.annotations.JaxBsonSeeAlso;
import org.qnixyz.jbson.annotations.JaxBsonTransient;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.annotations.adapters.JaxBsonAdapter;
import org.qnixyz.jbson.helpers.XmlAnyAttributeHelper;
import org.qnixyz.jbson.helpers.XmlAnyElementHelper;
import org.qnixyz.jbson.helpers.XmlElementRefHelper;
import org.qnixyz.jbson.helpers.XmlElementRefsHelper;
import org.qnixyz.jbson.helpers.XmlTransientHelper;
import org.qnixyz.jbson.helpers.XmlValueHelper;

public interface JaxBsonFieldContext {

  public JaxBsonConfiguration getConfiguration();

  public JaxBsonContext getContext();

  public Field getField();

  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransient();

  public JaxBsonIgnoreTransient getJaxBsonIgnoreTransientCfg();

  public JaxBsonName getJaxBsonName();

  public JaxBsonName getJaxBsonNameCfg();

  public JaxBsonNumberHint getJaxBsonNumberHint();

  public JaxBsonNumberHint getJaxBsonNumberHintCfg();

  public JaxBsonSeeAlso getJaxBsonSeeAlso();

  public JaxBsonTransient getJaxBsonTransient();

  public JaxBsonTransient getJaxBsonTransientCfg();

  public JaxBsonAdapter<Object, Object> getJaxBsonXmlAdapter();

  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMapping();

  public JaxBsonXmlAnyAttributeMapping getJaxBsonXmlAnyAttributeMappingCfg();

  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappings();

  public JaxBsonXmlAnyAttributeMappings getJaxBsonXmlAnyAttributeMappingsCfg();

  public XmlAnyAttributeHelper getXmlAnyAttribute();

  public XmlAnyElementHelper getXmlAnyElement();

  public XmlElementRefHelper getXmlElementRef();

  public XmlElementRefsHelper getXmlElementRefs();

  public XmlTransientHelper getXmlTransient();

  public XmlValueHelper getXmlValue();
}
