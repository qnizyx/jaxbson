package org.qnixyz.jbson;

import static org.qnixyz.jbson.impl.Utils.toObjectArray;
import static org.qnixyz.jbson.impl.Utils.toPrimitiveArray;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import org.bson.types.Binary;
import org.bson.types.ObjectId;

public class JaxBsonToObject {

  @FunctionalInterface
  public interface BinaryCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Binary> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToLongPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BinaryCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src);
  }

  @FunctionalInterface
  public interface BinaryCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Binary> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BinaryToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BinaryToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Binary src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BinaryToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Binary src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BinaryToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Binary src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BinaryToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BinaryToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Binary src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BinaryToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src);
  }

  @FunctionalInterface
  public interface BinaryToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Binary src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Boolean> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BooleanCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src);
  }

  @FunctionalInterface
  public interface BooleanCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Boolean> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface BooleanToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface BooleanToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Boolean src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface BooleanToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Boolean src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface BooleanToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Boolean src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface BooleanToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface BooleanToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Boolean src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface BooleanToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src);
  }

  @FunctionalInterface
  public interface BooleanToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Boolean src,
        Collection<Object> dst);
  }

  public static class Builder {

    private BinaryCollectionToBinary binaryCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBinary");
    };

    private BinaryCollectionToBinaryArray binaryCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToBinaryCollection binaryCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, e)));
          return dst;
        };

    private BinaryCollectionToBoolean binaryCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBoolean");
    };

    private BinaryCollectionToBooleanArray binaryCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToBooleanCollection binaryCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, e)));
          return dst;
        };

    private BinaryCollectionToBooleanPrim binaryCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToBooleanPrim");
    };

    private BinaryCollectionToBooleanPrimArray binaryCollectionToBooleanPrimArray =
        (fieldCtx, src) -> {
          final boolean[] ret = new boolean[src.size()];
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToBooleanPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToByte binaryCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToByte");
    };

    private BinaryCollectionToByteArray binaryCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToByteCollection binaryCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, e)));
          return dst;
        };

    private BinaryCollectionToBytePrim binaryCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToBytePrim");
    };

    private BinaryCollectionToBytePrimArray binaryCollectionToBytePrimArray = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToBytePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToCharacter binaryCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToCharacter");
    };

    private BinaryCollectionToCharacterArray binaryCollectionToCharacterArray = (fieldCtx, src) -> {
      final Character[] ret = new Character[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToCharacter().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToCharacterCollection binaryCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToCharacter()
              .convert(fieldCtx, e)));
          return dst;
        };

    private BinaryCollectionToCharacterPrim binaryCollectionToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToCharacterPrim");
    };

    private BinaryCollectionToCharacterPrimArray binaryCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToDateBased binaryCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToDateBased");
    };

    private BinaryCollectionToDateBasedArray binaryCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToDateBasedCollection binaryCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private BinaryCollectionToDoublePrim binaryCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToDoublePrim");
    };

    private BinaryCollectionToDoublePrimArray binaryCollectionToDoublePrimArray =
        (fieldCtx, src) -> {
          final double[] ret = new double[src.size()];
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToDoublePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToEnum binaryCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToEnum");
    };

    private BinaryCollectionToEnumArray binaryCollectionToEnumArray = (fieldCtx, enumType, src) -> {
      final Enum<?>[] ret = new Enum[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToEnum().convert(fieldCtx,
            enumType, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToEnumCollection binaryCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private BinaryCollectionToFloatPrim binaryCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToFloatPrim");
    };

    private BinaryCollectionToFloatPrimArray binaryCollectionToFloatPrimArray = (fieldCtx, src) -> {
      final float[] ret = new float[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToFloatPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToIntegerPrim binaryCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToIntegerPrim");
    };

    private BinaryCollectionToIntegerPrimArray binaryCollectionToIntegerPrimArray =
        (fieldCtx, src) -> {
          final int[] ret = new int[src.size()];
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToIntegerPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToLongPrim binaryCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToLongPrim");
    };

    private BinaryCollectionToLongPrimArray binaryCollectionToLongPrimArray = (fieldCtx, src) -> {
      final long[] ret = new long[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToLongPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToNumberBased binaryCollectionToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToNumberBased");
    };

    private BinaryCollectionToNumberBasedArray binaryCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final Binary e : src) {
            final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
                .convert(fieldCtx, type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private BinaryCollectionToNumberBasedCollection binaryCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getBinaryToNumberBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private BinaryCollectionToObjectId binaryCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToObjectId");
    };

    private BinaryCollectionToObjectIdArray binaryCollectionToObjectIdArray = (fieldCtx, src) -> {
      final ObjectId[] ret = new ObjectId[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToObjectId().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToObjectIdCollection binaryCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToObjectId()
              .convert(fieldCtx, e)));
          return dst;
        };

    private BinaryCollectionToShortPrim binaryCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryCollectionToShortPrim");
    };

    private BinaryCollectionToShortPrimArray binaryCollectionToShortPrimArray = (fieldCtx, src) -> {
      final short[] ret = new short[src.size()];
      int i = 0;
      for (final Binary e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBinaryToShortPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BinaryCollectionToStringBased binaryCollectionToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BinaryCollectionToStringBased");
    };

    private BinaryCollectionToStringBasedArray binaryCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Binary e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private BinaryCollectionToStringBasedCollection binaryCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getBinaryToStringBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private BinaryToBinary binaryToBinary = (fieldCtx, src) -> src;

    private BinaryToBinaryArray binaryToBinaryArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBinaryArray");
    };

    private BinaryToBinaryCollection binaryToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToBinary().convert(fieldCtx, src));
      return dst;
    };

    private BinaryToBoolean binaryToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBoolean");
    };

    private BinaryToBooleanArray binaryToBooleanArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanArray");
    };

    private BinaryToBooleanCollection binaryToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBinaryToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private BinaryToBooleanPrim binaryToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanPrim");
    };

    private BinaryToBooleanPrimArray binaryToBooleanPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToBooleanPrimArray");
    };

    private BinaryToByte binaryToByte = (fieldCtx, src) -> {
      if ((src == null) || (src.getData() == null) || (src.getData().length < 1)) {
        return null;
      }
      if (src.getData().length > 1) {
        throw new IllegalStateException("Size of data too long: " + src);
      }
      return src.getData()[0];
    };

    private BinaryToByteArray binaryToByteArray = (fieldCtx, src) -> {
      if (src == null) {
        return null;
      }
      return toObjectArray(src.getData());
    };

    private BinaryToByteCollection binaryToByteCollection = (fieldCtx, src, dst) -> {
      final Byte[] ret =
          fieldCtx.getConfiguration().getToObject().getBinaryToByteArray().convert(fieldCtx, src);
      dst.addAll(Arrays.asList(ret));
      return dst;
    };

    private BinaryToBytePrim binaryToBytePrim = (fieldCtx, src) -> {
      final Byte ret =
          fieldCtx.getConfiguration().getToObject().getBinaryToByte().convert(fieldCtx, src);
      if (ret == null) {
        return fieldCtx.getConfiguration().getValueBytePrimNull();
      }
      return ret;
    };

    private BinaryToBytePrimArray binaryToBytePrimArray = (fieldCtx, src) -> {
      final Byte[] ret =
          fieldCtx.getConfiguration().getToObject().getBinaryToByteArray().convert(fieldCtx, src);
      return toPrimitiveArray(ret);
    };

    private BinaryToCharacter binaryToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacter");
    };

    private BinaryToCharacterArray binaryToCharacterArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterArray");
    };

    private BinaryToCharacterCollection binaryToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBinaryToCharacter().convert(fieldCtx, src));
      return dst;
    };

    private BinaryToCharacterPrim binaryToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterPrim");
    };

    private BinaryToCharacterPrimArray binaryToCharacterPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToCharacterPrimArray");
    };

    private BinaryToDateBased binaryToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToDateBased");
    };

    private BinaryToDateBasedArray binaryToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BinaryToDateBasedCollection binaryToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToDateBased().convert(fieldCtx,
              type, src));
          return dst;
        };

    private BinaryToDoublePrim binaryToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToDoublePrim");
    };

    private BinaryToDoublePrimArray binaryToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getBinaryToDoublePrim().convert(fieldCtx, src)};

    private BinaryToEnum binaryToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToEnum");
    };

    private BinaryToEnumArray binaryToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToEnumArray");
    };

    private BinaryToEnumCollection binaryToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToEnum().convert(fieldCtx,
          enumType, src));
      return dst;
    };

    private BinaryToFloatPrim binaryToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToFloatPrim");
    };

    private BinaryToFloatPrimArray binaryToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getBinaryToFloatPrim().convert(fieldCtx, src)};

    private BinaryToIntegerPrim binaryToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToIntegerPrim");
    };

    private BinaryToIntegerPrimArray binaryToIntegerPrimArray = (fieldCtx, src) -> new int[] {
        fieldCtx.getConfiguration().getToObject().getBinaryToIntegerPrim().convert(fieldCtx, src)};

    private BinaryToLongPrim binaryToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToLongPrim");
    };

    private BinaryToLongPrimArray binaryToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getBinaryToLongPrim().convert(fieldCtx, src)};

    private BinaryToNumberBased binaryToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToNumberBased");
    };

    private BinaryToNumberBasedArray binaryToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BinaryToNumberBasedCollection binaryToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToNumberBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private BinaryToObjectId binaryToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToObjectId");
    };

    private BinaryToObjectIdArray binaryToObjectIdArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToObjectIdArray");
    };

    private BinaryToObjectIdCollection binaryToObjectIdCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBinaryToObjectId().convert(fieldCtx, src));
      return dst;
    };

    private BinaryToShortPrim binaryToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToShortPrim");
    };

    private BinaryToShortPrimArray binaryToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getBinaryToShortPrim().convert(fieldCtx, src)};

    private BinaryToStringBased binaryToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BinaryToStringBased");
    };

    private BinaryToStringBasedArray binaryToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BinaryToStringBasedCollection binaryToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBinaryToStringBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private BooleanCollectionToBinary booleanCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBinary");
    };

    private BooleanCollectionToBinaryArray booleanCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToBinaryCollection booleanCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, e)));
          return dst;
        };

    private BooleanCollectionToBoolean booleanCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBoolean");
    };

    private BooleanCollectionToBooleanArray booleanCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToBooleanCollection booleanCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToBoolean()
              .convert(fieldCtx, e)));
          return dst;
        };

    private BooleanCollectionToBooleanPrim booleanCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToBooleanPrim");
    };

    private BooleanCollectionToBooleanPrimArray booleanCollectionToBooleanPrimArray =
        (fieldCtx, src) -> {
          final boolean[] ret = new boolean[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToBooleanPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToByte booleanCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToByte");
    };

    private BooleanCollectionToByteArray booleanCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToByteCollection booleanCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, e)));
          return dst;
        };

    private BooleanCollectionToBytePrim booleanCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToBytePrim");
    };

    private BooleanCollectionToBytePrimArray booleanCollectionToBytePrimArray = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBooleanToBytePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToCharacter booleanCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToCharacter");
    };

    private BooleanCollectionToCharacterArray booleanCollectionToCharacterArray =
        (fieldCtx, src) -> {
          final Character[] ret = new Character[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToCharacter()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToCharacterCollection booleanCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToCharacter()
              .convert(fieldCtx, e)));
          return dst;
        };

    private BooleanCollectionToCharacterPrim booleanCollectionToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToCharacterPrim");
    };

    private BooleanCollectionToCharacterPrimArray booleanCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToDateBased booleanCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToDateBased");
    };

    private BooleanCollectionToDateBasedArray booleanCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToDateBasedCollection booleanCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private BooleanCollectionToDoublePrim booleanCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToDoublePrim");
    };

    private BooleanCollectionToDoublePrimArray booleanCollectionToDoublePrimArray =
        (fieldCtx, src) -> {
          final double[] ret = new double[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToDoublePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToEnum booleanCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToEnum");
    };

    private BooleanCollectionToEnumArray booleanCollectionToEnumArray =
        (fieldCtx, enumType, src) -> {
          final Enum<?>[] ret = new Enum[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToEnum().convert(fieldCtx,
                enumType, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToEnumCollection booleanCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private BooleanCollectionToFloatPrim booleanCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToFloatPrim");
    };

    private BooleanCollectionToFloatPrimArray booleanCollectionToFloatPrimArray =
        (fieldCtx, src) -> {
          final float[] ret = new float[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToFloatPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToIntegerPrim booleanCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToIntegerPrim");
    };

    private BooleanCollectionToIntegerPrimArray booleanCollectionToIntegerPrimArray =
        (fieldCtx, src) -> {
          final int[] ret = new int[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToIntegerPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToLongPrim booleanCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToLongPrim");
    };

    private BooleanCollectionToLongPrimArray booleanCollectionToLongPrimArray = (fieldCtx, src) -> {
      final long[] ret = new long[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBooleanToLongPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToNumberBased booleanCollectionToNumberBased =
        (fieldCtx, type, src) -> {
          throw new UnsupportedOperationException(
              "Not (yet) implemented: BooleanCollectionToNumberBased");
        };

    private BooleanCollectionToNumberBasedArray booleanCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final Boolean e : src) {
            final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
                .convert(fieldCtx, type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private BooleanCollectionToNumberBasedCollection booleanCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getBooleanToNumberBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private BooleanCollectionToObjectId booleanCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanCollectionToObjectId");
    };

    private BooleanCollectionToObjectIdArray booleanCollectionToObjectIdArray = (fieldCtx, src) -> {
      final ObjectId[] ret = new ObjectId[src.size()];
      int i = 0;
      for (final Boolean e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getBooleanToObjectId().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private BooleanCollectionToObjectIdCollection booleanCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToObjectId()
              .convert(fieldCtx, e)));
          return dst;
        };

    private BooleanCollectionToShortPrim booleanCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanCollectionToShortPrim");
    };

    private BooleanCollectionToShortPrimArray booleanCollectionToShortPrimArray =
        (fieldCtx, src) -> {
          final short[] ret = new short[src.size()];
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToShortPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToStringBased booleanCollectionToStringBased =
        (fieldCtx, type, src) -> {
          throw new UnsupportedOperationException(
              "Not (yet) implemented: BooleanCollectionToStringBased");
        };

    private BooleanCollectionToStringBasedArray booleanCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Boolean e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private BooleanCollectionToStringBasedCollection booleanCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getBooleanToStringBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private BooleanToBinary booleanToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToBinary");
    };

    private BooleanToBinaryArray booleanToBinaryArray = (fieldCtx, src) -> {
      return new Binary[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, src)};
    };

    private BooleanToBinaryCollection booleanToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBooleanToBinary().convert(fieldCtx, src));
      return dst;
    };

    private BooleanToBoolean booleanToBoolean = (fieldCtx, src) -> src;

    private BooleanToBooleanArray booleanToBooleanArray = (fieldCtx, src) -> {
      return new Boolean[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src)};
    };

    private BooleanToBooleanCollection booleanToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private BooleanToBooleanPrim booleanToBooleanPrim = (fieldCtx, src) -> {
      src = fieldCtx.getConfiguration().getToObject().getBooleanToBoolean().convert(fieldCtx, src);
      if (src == null) {
        return fieldCtx.getConfiguration().isValueBooleanPrimBooleanNull();
      }
      return src;
    };

    private BooleanToBooleanPrimArray booleanToBooleanPrimArray = (fieldCtx, src) -> {
      return new boolean[] {fieldCtx.getConfiguration().getToObject().getBooleanToBooleanPrim()
          .convert(fieldCtx, src)};
    };

    private BooleanToByte booleanToByte = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueByteBooleanNull();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueByteBooleanTrue();
      }
      return fieldCtx.getConfiguration().getValueByteBooleanFalse();
    };

    private BooleanToByteArray booleanToByteArray = (fieldCtx, src) -> {
      return new Byte[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src)};
    };

    private BooleanToByteCollection booleanToByteCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src));
      return dst;
    };

    private BooleanToBytePrim booleanToBytePrim = (fieldCtx, src) -> {
      final Byte ret =
          fieldCtx.getConfiguration().getToObject().getBooleanToByte().convert(fieldCtx, src);
      if (ret == null) {
        return fieldCtx.getConfiguration().getValueBytePrimBooleanNull();
      }
      return ret;
    };

    private BooleanToBytePrimArray booleanToBytePrimArray = (fieldCtx, src) -> {
      return new byte[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToBytePrim().convert(fieldCtx, src)};
    };

    private BooleanToCharacter booleanToCharacter = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueCharacterBooleanNull();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueCharacterBooleanTrue();
      }
      return fieldCtx.getConfiguration().getValueCharacterBooleanFalse();
    };

    private BooleanToCharacterArray booleanToCharacterArray = (fieldCtx, src) -> {
      return new Character[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src)};
    };

    private BooleanToCharacterCollection booleanToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src));
      return dst;
    };

    private BooleanToCharacterPrim booleanToCharacterPrim = (fieldCtx, src) -> {
      final Character ret =
          fieldCtx.getConfiguration().getToObject().getBooleanToCharacter().convert(fieldCtx, src);
      if (ret == null) {
        return fieldCtx.getConfiguration().getValueCharacterPrimBooleanNull();
      }
      return ret;
    };

    private BooleanToCharacterPrimArray booleanToCharacterPrimArray = (fieldCtx, src) -> {
      return new char[] {fieldCtx.getConfiguration().getToObject().getBooleanToCharacterPrim()
          .convert(fieldCtx, src)};
    };

    private BooleanToDateBased booleanToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToDateBased");
    };

    private BooleanToDateBasedArray booleanToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BooleanToDateBasedCollection booleanToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToDateBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private BooleanToDoublePrim booleanToDoublePrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueNumberBooleanTrue().doubleValue();
      }
      return fieldCtx.getConfiguration().getValueNumberBooleanFalse().doubleValue();
    };

    private BooleanToDoublePrimArray booleanToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToDoublePrim().convert(fieldCtx, src)};

    private BooleanToEnum booleanToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToEnum");
    };

    private BooleanToEnumArray booleanToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToEnumArray");
    };

    private BooleanToEnumCollection booleanToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToEnum().convert(fieldCtx,
          enumType, src));
      return dst;
    };

    private BooleanToFloatPrim booleanToFloatPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueNumberBooleanTrue().floatValue();
      }
      return fieldCtx.getConfiguration().getValueNumberBooleanFalse().floatValue();
    };

    private BooleanToFloatPrimArray booleanToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToFloatPrim().convert(fieldCtx, src)};

    private BooleanToIntegerPrim booleanToIntegerPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueNumberBooleanTrue().intValue();
      }
      return fieldCtx.getConfiguration().getValueNumberBooleanFalse().intValue();
    };

    private BooleanToIntegerPrimArray booleanToIntegerPrimArray = (fieldCtx, src) -> new int[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToIntegerPrim().convert(fieldCtx, src)};

    private BooleanToLongPrim booleanToLongPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueNumberBooleanTrue().longValue();
      }
      return fieldCtx.getConfiguration().getValueNumberBooleanFalse().longValue();
    };

    private BooleanToLongPrimArray booleanToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToLongPrim().convert(fieldCtx, src)};

    private BooleanToNumberBased booleanToNumberBased = (fieldCtx, type, src) -> {
      if (type.isPrimitive() && (src == null)) {
        return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
            fieldCtx.getConfiguration().getValueNumberPrimBooleanNull());
      }
      if (src == null) {
        return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
            fieldCtx.getConfiguration().getValueNumberBooleanNull());
      }
      if (src) {
        return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
            fieldCtx.getConfiguration().getValueNumberBooleanTrue());
      }
      return fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type,
          fieldCtx.getConfiguration().getValueNumberBooleanFalse());
    };

    private BooleanToNumberBasedArray booleanToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BooleanToNumberBasedCollection booleanToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToNumberBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private BooleanToObjectId booleanToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToObjectId");
    };

    private BooleanToObjectIdArray booleanToObjectIdArray = (fieldCtx, src) -> {
      return new ObjectId[] {
          fieldCtx.getConfiguration().getToObject().getBooleanToObjectId().convert(fieldCtx, src)};
    };

    private BooleanToObjectIdCollection booleanToObjectIdCollection = (fieldCtx, src, dst) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: BooleanToObjectIdCollection");
    };

    private BooleanToShortPrim booleanToShortPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
      }
      if (src) {
        return fieldCtx.getConfiguration().getValueNumberBooleanTrue().shortValue();
      }
      return fieldCtx.getConfiguration().getValueNumberBooleanFalse().shortValue();
    };

    private BooleanToShortPrimArray booleanToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getBooleanToShortPrim().convert(fieldCtx, src)};

    private BooleanToStringBased booleanToStringBased = (fieldCtx, type, src) -> {
      if (String.class.isAssignableFrom(type)) {
        if (src == null) {
          return fieldCtx.getConfiguration().getValueStringBooleanNull();
        }
        if (src) {
          return fieldCtx.getConfiguration().getValueStringBooleanTrue();
        }
        return fieldCtx.getConfiguration().getValueStringBooleanFalse();
      }
      throw new UnsupportedOperationException(
          "Not (yet) implemented: BooleanToStringBased for type " + type);
    };

    private BooleanToStringBasedArray booleanToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private BooleanToStringBasedCollection booleanToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getBooleanToStringBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private DateCollectionToBinary dateCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBinary");
    };

    private DateCollectionToBinaryArray dateCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToBinaryCollection dateCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, e)));
          return dst;
        };

    private DateCollectionToBoolean dateCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBoolean");
    };

    private DateCollectionToBooleanArray dateCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToBooleanCollection dateCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, e)));
          return dst;
        };

    private DateCollectionToBooleanPrim dateCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBooleanPrim");
    };

    private DateCollectionToBooleanPrimArray dateCollectionToBooleanPrimArray = (fieldCtx, src) -> {
      final boolean[] ret = new boolean[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToBooleanPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToByte dateCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToByte");
    };

    private DateCollectionToByteArray dateCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToByteCollection dateCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst
              .add(fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, e)));
          return dst;
        };

    private DateCollectionToBytePrim dateCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToBytePrim");
    };

    private DateCollectionToBytePrimArray dateCollectionToBytePrimArray = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToBytePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToCharacter dateCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToCharacter");
    };

    private DateCollectionToCharacterArray dateCollectionToCharacterArray = (fieldCtx, src) -> {
      final Character[] ret = new Character[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToCharacterCollection dateCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, e)));
          return dst;
        };

    private DateCollectionToCharacterPrim dateCollectionToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: DateCollectionToCharacterPrim");
    };

    private DateCollectionToCharacterPrimArray dateCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final Date e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getDateToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private DateCollectionToDateBased dateCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToDateBased");
    };

    private DateCollectionToDateBasedArray dateCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Date e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getDateToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private DateCollectionToDateBasedCollection dateCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToDateBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private DateCollectionToDoublePrim dateCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToDoublePrim");
    };

    private DateCollectionToDoublePrimArray dateCollectionToDoublePrimArray = (fieldCtx, src) -> {
      final double[] ret = new double[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToDoublePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToEnum dateCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToEnum");
    };

    private DateCollectionToEnumArray dateCollectionToEnumArray = (fieldCtx, enumType, src) -> {
      final Enum<?>[] ret = new Enum[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToEnum().convert(fieldCtx,
            enumType, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToEnumCollection dateCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private DateCollectionToFloatPrim dateCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToFloatPrim");
    };

    private DateCollectionToFloatPrimArray dateCollectionToFloatPrimArray = (fieldCtx, src) -> {
      final float[] ret = new float[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToFloatPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToIntegerPrim dateCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToIntegerPrim");
    };

    private DateCollectionToIntegerPrimArray dateCollectionToIntegerPrimArray = (fieldCtx, src) -> {
      final int[] ret = new int[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToIntegerPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToLongPrim dateCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToLongPrim");
    };

    private DateCollectionToLongPrimArray dateCollectionToLongPrimArray = (fieldCtx, src) -> {
      final long[] ret = new long[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToLongPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToNumberBased dateCollectionToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToNumberBased");
    };

    private DateCollectionToNumberBasedArray dateCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final Date e : src) {
            final Object value = fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
                .convert(fieldCtx, type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private DateCollectionToNumberBasedCollection dateCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private DateCollectionToObjectId dateCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToObjectId");
    };

    private DateCollectionToObjectIdArray dateCollectionToObjectIdArray = (fieldCtx, src) -> {
      final ObjectId[] ret = new ObjectId[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToObjectIdCollection dateCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, e)));
          return dst;
        };

    private DateCollectionToShortPrim dateCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToShortPrim");
    };

    private DateCollectionToShortPrimArray dateCollectionToShortPrimArray = (fieldCtx, src) -> {
      final short[] ret = new short[src.size()];
      int i = 0;
      for (final Date e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getDateToShortPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private DateCollectionToStringBased dateCollectionToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateCollectionToStringBased");
    };

    private DateCollectionToStringBasedArray dateCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Date e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getDateToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private DateCollectionToStringBasedCollection dateCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getDateToStringBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private DateToBinary dateToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBinary");
    };

    private DateToBinaryArray dateToBinaryArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBinaryArray");
    };

    private DateToBinaryCollection dateToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToBinary().convert(fieldCtx, src));
      return dst;
    };

    private DateToBoolean dateToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBoolean");
    };

    private DateToBooleanArray dateToBooleanArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanArray");
    };

    private DateToBooleanCollection dateToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private DateToBooleanPrim dateToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanPrim");
    };

    private DateToBooleanPrimArray dateToBooleanPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBooleanPrimArray");
    };

    private DateToByte dateToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToByte");
    };

    private DateToByteArray dateToByteArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToByteArray");
    };

    private DateToByteCollection dateToByteCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToByte().convert(fieldCtx, src));
      return dst;
    };

    private DateToBytePrim dateToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBytePrim");
    };

    private DateToBytePrimArray dateToBytePrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToBytePrimArray");
    };

    private DateToCharacter dateToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacter");
    };

    private DateToCharacterArray dateToCharacterArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterArray");
    };

    private DateToCharacterCollection dateToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getDateToCharacter().convert(fieldCtx, src));
      return dst;
    };

    private DateToCharacterPrim dateToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterPrim");
    };

    private DateToCharacterPrimArray dateToCharacterPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToCharacterPrimArray");
    };

    private DateToDateBased dateToDateBased = (fieldCtx, type, src) -> fieldCtx.getConfiguration()
        .getDateBased().getToObject().convert(type, src);

    private DateToDateBasedArray dateToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getDateToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private DateToDateBasedCollection dateToDateBasedCollection = (fieldCtx, type, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToDateBased().convert(fieldCtx, type,
          src));
      return dst;
    };

    private DateToDoublePrim dateToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToDoublePrim");
    };

    private DateToDoublePrimArray dateToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getDateToDoublePrim().convert(fieldCtx, src)};

    private DateToEnum dateToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToEnum");
    };

    private DateToEnumArray dateToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToEnumArray");
    };

    private DateToEnumCollection dateToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToEnum().convert(fieldCtx, enumType,
          src));
      return dst;
    };

    private DateToFloatPrim dateToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToFloatPrim");
    };

    private DateToFloatPrimArray dateToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getDateToFloatPrim().convert(fieldCtx, src)};

    private DateToIntegerPrim dateToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToIntegerPrim");
    };

    private DateToIntegerPrimArray dateToIntegerPrimArray = (fieldCtx, src) -> new int[] {
        fieldCtx.getConfiguration().getToObject().getDateToIntegerPrim().convert(fieldCtx, src)};

    private DateToLongPrim dateToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToLongPrim");
    };

    private DateToLongPrimArray dateToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getDateToLongPrim().convert(fieldCtx, src)};

    private DateToNumberBased dateToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToNumberBased");
    };

    private DateToNumberBasedArray dateToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getDateToNumberBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private DateToNumberBasedCollection dateToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getDateToNumberBased().convert(fieldCtx,
              type, src));
          return dst;
        };

    private DateToObjectId dateToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToObjectId");
    };

    private DateToObjectIdArray dateToObjectIdArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToObjectIdArray");
    };

    private DateToObjectIdCollection dateToObjectIdCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getDateToObjectId().convert(fieldCtx, src));
      return dst;
    };

    private DateToShortPrim dateToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToShortPrim");
    };

    private DateToShortPrimArray dateToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getDateToShortPrim().convert(fieldCtx, src)};

    private DateToStringBased dateToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: DateToStringBased");
    };

    private DateToStringBasedArray dateToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getDateToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private DateToStringBasedCollection dateToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getDateToStringBased().convert(fieldCtx,
              type, src));
          return dst;
        };

    private NumberCollectionToBinary numberCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBinary");
    };

    private NumberCollectionToBinaryArray numberCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToBinaryCollection numberCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, e)));
          return dst;
        };

    private NumberCollectionToBoolean numberCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBoolean");
    };

    private NumberCollectionToBooleanArray numberCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToBooleanCollection numberCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, e)));
          return dst;
        };

    private NumberCollectionToBooleanPrim numberCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToBooleanPrim");
    };

    private NumberCollectionToBooleanPrimArray numberCollectionToBooleanPrimArray =
        (fieldCtx, src) -> {
          final boolean[] ret = new boolean[src.size()];
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToBooleanPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToByte numberCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToByte");
    };

    private NumberCollectionToByteArray numberCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToByteCollection numberCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, e)));
          return dst;
        };

    private NumberCollectionToBytePrim numberCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToBytePrim");
    };

    private NumberCollectionToBytePrimArray numberCollectionToBytePrimArray = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToBytePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToCharacter numberCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToCharacter");
    };

    private NumberCollectionToCharacterArray numberCollectionToCharacterArray = (fieldCtx, src) -> {
      final Character[] ret = new Character[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToCharacterCollection numberCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToCharacter()
              .convert(fieldCtx, e)));
          return dst;
        };

    private NumberCollectionToCharacterPrim numberCollectionToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToCharacterPrim");
    };

    private NumberCollectionToCharacterPrimArray numberCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToDateBased numberCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToDateBased");
    };

    private NumberCollectionToDateBasedArray numberCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToDateBasedCollection numberCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private NumberCollectionToDoublePrim numberCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToDoublePrim");
    };

    private NumberCollectionToDoublePrimArray numberCollectionToDoublePrimArray =
        (fieldCtx, src) -> {
          final double[] ret = new double[src.size()];
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToDoublePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToEnum numberCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToEnum");
    };

    private NumberCollectionToEnumArray numberCollectionToEnumArray = (fieldCtx, enumType, src) -> {
      final Enum<?>[] ret = new Enum[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToEnum().convert(fieldCtx,
            enumType, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToEnumCollection numberCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private NumberCollectionToFloatPrim numberCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToFloatPrim");
    };

    private NumberCollectionToFloatPrimArray numberCollectionToFloatPrimArray = (fieldCtx, src) -> {
      final float[] ret = new float[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToFloatPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToIntegerPrim numberCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToIntegerPrim");
    };

    private NumberCollectionToIntegerPrimArray numberCollectionToIntegerPrimArray =
        (fieldCtx, src) -> {
          final int[] ret = new int[src.size()];
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToIntegerPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToLongPrim numberCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToLongPrim");
    };

    private NumberCollectionToLongPrimArray numberCollectionToLongPrimArray = (fieldCtx, src) -> {
      final long[] ret = new long[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToLongPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToNumberBased numberCollectionToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToNumberBased");
    };

    private NumberCollectionToNumberBasedArray numberCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final Number e : src) {
            final Object value =
                fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private NumberCollectionToNumberBasedCollection numberCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst
              .add(fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, e)));
          return dst;
        };

    private NumberCollectionToObjectId numberCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToObjectId");
    };

    private NumberCollectionToObjectIdArray numberCollectionToObjectIdArray = (fieldCtx, src) -> {
      final ObjectId[] ret = new ObjectId[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToObjectId().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToObjectIdCollection numberCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getNumberToObjectId()
              .convert(fieldCtx, e)));
          return dst;
        };

    private NumberCollectionToShortPrim numberCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberCollectionToShortPrim");
    };

    private NumberCollectionToShortPrimArray numberCollectionToShortPrimArray = (fieldCtx, src) -> {
      final short[] ret = new short[src.size()];
      int i = 0;
      for (final Number e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getNumberToShortPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private NumberCollectionToStringBased numberCollectionToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: NumberCollectionToStringBased");
    };

    private NumberCollectionToStringBasedArray numberCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final Number e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private NumberCollectionToStringBasedCollection numberCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getNumberToStringBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private NumberToBinary numberToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToBinary");
    };

    private NumberToBinaryArray numberToBinaryArray = (fieldCtx, src) -> src == null ? null
        : new Binary[] {
            fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, src)};

    private NumberToBinaryCollection numberToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getNumberToBinary().convert(fieldCtx, src));
      return dst;
    };

    private NumberToBoolean numberToBoolean = (fieldCtx, src) -> {
      if (src == null) {
        return null;
      }
      if (src.longValue() == 0) {
        return Boolean.FALSE;
      }
      return Boolean.TRUE;
    };

    private NumberToBooleanArray numberToBooleanArray = (fieldCtx, src) -> src == null ? null
        : new Boolean[] {
            fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src)};

    private NumberToBooleanCollection numberToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private NumberToBooleanPrim numberToBooleanPrim = (fieldCtx, src) -> {
      final Boolean ret =
          fieldCtx.getConfiguration().getToObject().getNumberToBoolean().convert(fieldCtx, src);
      if (ret == null) {
        return fieldCtx.getConfiguration().isValueBooleanPrimBooleanNull();
      }
      return ret;
    };

    private NumberToBooleanPrimArray numberToBooleanPrimArray =
        (fieldCtx, src) -> src == null ? null
            : new boolean[] {fieldCtx.getConfiguration().getToObject().getNumberToBooleanPrim()
                .convert(fieldCtx, src)};

    private NumberToByte numberToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToByte");
    };

    private NumberToByteArray numberToByteArray = (fieldCtx, src) -> src == null ? null
        : new Byte[] {
            fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, src)};

    private NumberToByteCollection numberToByteCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getNumberToByte().convert(fieldCtx, src));
      return dst;
    };

    private NumberToBytePrim numberToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToBytePrim");
    };

    private NumberToBytePrimArray numberToBytePrimArray = (fieldCtx, src) -> src == null ? null
        : new byte[] {
            fieldCtx.getConfiguration().getToObject().getNumberToBytePrim().convert(fieldCtx, src)};

    private NumberToCharacter numberToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToCharacter");
    };

    private NumberToCharacterArray numberToCharacterArray = (fieldCtx, src) -> src == null ? null
        : new Character[] {fieldCtx.getConfiguration().getToObject().getNumberToCharacter()
            .convert(fieldCtx, src)};

    private NumberToCharacterCollection numberToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getNumberToCharacter().convert(fieldCtx, src));
      return dst;
    };

    private NumberToCharacterPrim numberToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToCharacterPrim");
    };

    private NumberToCharacterPrimArray numberToCharacterPrimArray =
        (fieldCtx, src) -> src == null ? null
            : new char[] {fieldCtx.getConfiguration().getToObject().getNumberToCharacterPrim()
                .convert(fieldCtx, src)};

    private NumberToDateBased numberToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToDateBased");
    };

    private NumberToDateBasedArray numberToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getNumberToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private NumberToDateBasedCollection numberToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getNumberToDateBased().convert(fieldCtx,
              type, src));
          return dst;
        };

    private NumberToDoublePrim numberToDoublePrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
      }
      return src.doubleValue();
    };

    private NumberToDoublePrimArray numberToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getNumberToDoublePrim().convert(fieldCtx, src)};

    private NumberToEnum numberToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToEnum");
    };

    private NumberToEnumArray numberToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToEnumArray");
    };

    private NumberToEnumCollection numberToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getNumberToEnum().convert(fieldCtx,
          enumType, src));
      return dst;
    };

    private NumberToFloatPrim numberToFloatPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
      }
      return src.floatValue();
    };

    private NumberToFloatPrimArray numberToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getNumberToFloatPrim().convert(fieldCtx, src)};

    private NumberToIntegerPrim numberToIntegerPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
      }
      return src.intValue();
    };

    private NumberToIntegerPrimArray numberToIntegerPrimArray = (fieldCtx, src) -> new int[] {
        fieldCtx.getConfiguration().getToObject().getNumberToIntegerPrim().convert(fieldCtx, src)};

    private NumberToLongPrim numberToLongPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
      }
      return src.longValue();
    };

    private NumberToLongPrimArray numberToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getNumberToLongPrim().convert(fieldCtx, src)};

    private NumberToNumberBased numberToNumberBased = (fieldCtx, type, src) -> fieldCtx
        .getConfiguration().getNumberBased().getToObject().convert(type, src);

    private NumberToNumberBasedArray numberToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value =
          fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private NumberToNumberBasedCollection numberToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getNumberBased().getToObject().convert(type, src));
          return dst;

        };

    private NumberToObjectId numberToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToObjectId");
    };

    private NumberToObjectIdArray numberToObjectIdArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToObjectIdArray");
    };

    private NumberToObjectIdCollection numberToObjectIdCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getNumberToObjectId().convert(fieldCtx, src));
      return dst;
    };

    private NumberToShortPrim numberToShortPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
      }
      return src.shortValue();
    };

    private NumberToShortPrimArray numberToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getNumberToShortPrim().convert(fieldCtx, src)};

    private NumberToStringBased numberToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: NumberToStringBased");
    };

    private NumberToStringBasedArray numberToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private NumberToStringBasedCollection numberToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getNumberToStringBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private ObjectIdCollectionToBinary objectIdCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToBinary");
    };

    private ObjectIdCollectionToBinaryArray objectIdCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final ObjectId e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getObjectIdToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private ObjectIdCollectionToBinaryCollection objectIdCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToBinary()
              .convert(fieldCtx, e)));
          return dst;
        };

    private ObjectIdCollectionToBoolean objectIdCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToBoolean");
    };

    private ObjectIdCollectionToBooleanArray objectIdCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final ObjectId e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private ObjectIdCollectionToBooleanCollection objectIdCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean()
              .convert(fieldCtx, e)));
          return dst;
        };

    private ObjectIdCollectionToBooleanPrim objectIdCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToBooleanPrim");
    };

    private ObjectIdCollectionToBooleanPrimArray objectIdCollectionToBooleanPrimArray =
        (fieldCtx, src) -> {
          final boolean[] ret = new boolean[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToBooleanPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToByte objectIdCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToByte");
    };

    private ObjectIdCollectionToByteArray objectIdCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final ObjectId e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private ObjectIdCollectionToByteCollection objectIdCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, e)));
          return dst;
        };

    private ObjectIdCollectionToBytePrim objectIdCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToBytePrim");
    };

    private ObjectIdCollectionToBytePrimArray objectIdCollectionToBytePrimArray =
        (fieldCtx, src) -> {
          final byte[] ret = new byte[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToBytePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToCharacter objectIdCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToCharacter");
    };

    private ObjectIdCollectionToCharacterArray objectIdCollectionToCharacterArray =
        (fieldCtx, src) -> {
          final Character[] ret = new Character[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToCharacter()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToCharacterCollection objectIdCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getObjectIdToCharacter().convert(fieldCtx, e)));
          return dst;
        };

    private ObjectIdCollectionToCharacterPrim objectIdCollectionToCharacterPrim =
        (fieldCtx, src) -> {
          throw new UnsupportedOperationException(
              "Not (yet) implemented: ObjectIdCollectionToCharacterPrim");
        };

    private ObjectIdCollectionToCharacterPrimArray objectIdCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToDateBased objectIdCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToDateBased");
    };

    private ObjectIdCollectionToDateBasedArray objectIdCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToDateBasedCollection objectIdCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getObjectIdToDateBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private ObjectIdCollectionToDoublePrim objectIdCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToDoublePrim");
    };

    private ObjectIdCollectionToDoublePrimArray objectIdCollectionToDoublePrimArray =
        (fieldCtx, src) -> {
          final double[] ret = new double[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToDoublePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToEnum objectIdCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdCollectionToEnum");
    };

    private ObjectIdCollectionToEnumArray objectIdCollectionToEnumArray =
        (fieldCtx, enumType, src) -> {
          final Enum<?>[] ret = new Enum[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToEnum().convert(fieldCtx,
                enumType, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToEnumCollection objectIdCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private ObjectIdCollectionToFloatPrim objectIdCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToFloatPrim");
    };

    private ObjectIdCollectionToFloatPrimArray objectIdCollectionToFloatPrimArray =
        (fieldCtx, src) -> {
          final float[] ret = new float[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToFloatPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToIntegerPrim objectIdCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToIntegerPrim");
    };

    private ObjectIdCollectionToIntegerPrimArray objectIdCollectionToIntegerPrimArray =
        (fieldCtx, src) -> {
          final int[] ret = new int[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToIntegerPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToLongPrim objectIdCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToLongPrim");
    };

    private ObjectIdCollectionToLongPrimArray objectIdCollectionToLongPrimArray =
        (fieldCtx, src) -> {
          final long[] ret = new long[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToLongPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToNumberBased objectIdCollectionToNumberBased =
        (fieldCtx, type, src) -> {
          throw new UnsupportedOperationException(
              "Not (yet) implemented: ObjectIdCollectionToNumberBased");
        };

    private ObjectIdCollectionToNumberBasedArray objectIdCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final ObjectId e : src) {
            final Object value = fieldCtx.getConfiguration().getToObject()
                .getObjectIdToNumberBased().convert(fieldCtx, type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private ObjectIdCollectionToNumberBasedCollection objectIdCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getObjectIdToNumberBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private ObjectIdCollectionToObjectId objectIdCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToObjectId");
    };

    private ObjectIdCollectionToObjectIdArray objectIdCollectionToObjectIdArray =
        (fieldCtx, src) -> {
          final ObjectId[] ret = new ObjectId[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToObjectIdCollection objectIdCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId()
              .convert(fieldCtx, e)));
          return dst;
        };

    private ObjectIdCollectionToShortPrim objectIdCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdCollectionToShortPrim");
    };

    private ObjectIdCollectionToShortPrimArray objectIdCollectionToShortPrimArray =
        (fieldCtx, src) -> {
          final short[] ret = new short[src.size()];
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToShortPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToStringBased objectIdCollectionToStringBased =
        (fieldCtx, type, src) -> {
          throw new UnsupportedOperationException(
              "Not (yet) implemented: ObjectIdCollectionToStringBased");
        };

    private ObjectIdCollectionToStringBasedArray objectIdCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final ObjectId e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private ObjectIdCollectionToStringBasedCollection objectIdCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getObjectIdToStringBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private ObjectIdToBinary objectIdToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBinary");
    };

    private ObjectIdToBinaryArray objectIdToBinaryArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBinaryArray");
    };

    private ObjectIdToBinaryCollection objectIdToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getObjectIdToBinary().convert(fieldCtx, src));
      return dst;
    };

    private ObjectIdToBoolean objectIdToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBoolean");
    };

    private ObjectIdToBooleanArray objectIdToBooleanArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanArray");
    };

    private ObjectIdToBooleanCollection objectIdToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getObjectIdToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private ObjectIdToBooleanPrim objectIdToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanPrim");
    };

    private ObjectIdToBooleanPrimArray objectIdToBooleanPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBooleanPrimArray");
    };

    private ObjectIdToByte objectIdToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToByte");
    };

    private ObjectIdToByteArray objectIdToByteArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToByteArray");
    };

    private ObjectIdToByteCollection objectIdToByteCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToByte().convert(fieldCtx, src));
      return dst;
    };

    private ObjectIdToBytePrim objectIdToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBytePrim");
    };

    private ObjectIdToBytePrimArray objectIdToBytePrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToBytePrimArray");
    };

    private ObjectIdToCharacter objectIdToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacter");
    };

    private ObjectIdToCharacterArray objectIdToCharacterArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacterArray");
    };

    private ObjectIdToCharacterCollection objectIdToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToCharacter().convert(fieldCtx,
          src));
      return dst;
    };

    private ObjectIdToCharacterPrim objectIdToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToCharacterPrim");
    };

    private ObjectIdToCharacterPrimArray objectIdToCharacterPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: ObjectIdToCharacterPrimArray");
    };

    private ObjectIdToDateBased objectIdToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToDateBased");
    };

    private ObjectIdToDateBasedArray objectIdToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private ObjectIdToDateBasedCollection objectIdToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToDateBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private ObjectIdToDoublePrim objectIdToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToDoublePrim");
    };

    private ObjectIdToDoublePrimArray objectIdToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getObjectIdToDoublePrim().convert(fieldCtx, src)};

    private ObjectIdToEnum objectIdToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToEnum");
    };

    private ObjectIdToEnumArray objectIdToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToEnumArray");
    };

    private ObjectIdToEnumCollection objectIdToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToEnum().convert(fieldCtx,
          enumType, src));
      return dst;
    };

    private ObjectIdToFloatPrim objectIdToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToFloatPrim");
    };

    private ObjectIdToFloatPrimArray objectIdToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getObjectIdToFloatPrim().convert(fieldCtx, src)};

    private ObjectIdToIntegerPrim objectIdToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToIntegerPrim");
    };

    private ObjectIdToIntegerPrimArray objectIdToIntegerPrimArray =
        (fieldCtx, src) -> new int[] {fieldCtx.getConfiguration().getToObject()
            .getObjectIdToIntegerPrim().convert(fieldCtx, src)};

    private ObjectIdToLongPrim objectIdToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToLongPrim");
    };

    private ObjectIdToLongPrimArray objectIdToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getObjectIdToLongPrim().convert(fieldCtx, src)};

    private ObjectIdToNumberBased objectIdToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToNumberBased");
    };

    private ObjectIdToNumberBasedArray objectIdToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToNumberBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private ObjectIdToNumberBasedCollection objectIdToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToNumberBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private ObjectIdToObjectId objectIdToObjectId = (fieldCtx, src) -> src;

    private ObjectIdToObjectIdArray objectIdToObjectIdArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToObjectIdArray");
    };

    private ObjectIdToObjectIdCollection objectIdToObjectIdCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getObjectIdToObjectId().convert(fieldCtx, src));
      return dst;
    };

    private ObjectIdToShortPrim objectIdToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToShortPrim");
    };

    private ObjectIdToShortPrimArray objectIdToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getObjectIdToShortPrim().convert(fieldCtx, src)};

    private ObjectIdToStringBased objectIdToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: ObjectIdToStringBased");
    };

    private ObjectIdToStringBasedArray objectIdToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private ObjectIdToStringBasedCollection objectIdToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getObjectIdToStringBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private StringCollectionToBinary stringCollectionToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBinary");
    };

    private StringCollectionToBinaryArray stringCollectionToBinaryArray = (fieldCtx, src) -> {
      final Binary[] ret = new Binary[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToBinaryCollection stringCollectionToBinaryCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, e)));
          return dst;
        };

    private StringCollectionToBoolean stringCollectionToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBoolean");
    };

    private StringCollectionToBooleanArray stringCollectionToBooleanArray = (fieldCtx, src) -> {
      final Boolean[] ret = new Boolean[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToBooleanCollection stringCollectionToBooleanCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, e)));
          return dst;
        };

    private StringCollectionToBooleanPrim stringCollectionToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToBooleanPrim");
    };

    private StringCollectionToBooleanPrimArray stringCollectionToBooleanPrimArray =
        (fieldCtx, src) -> {
          final boolean[] ret = new boolean[src.size()];
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToBooleanPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToByte stringCollectionToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToByte");
    };

    private StringCollectionToByteArray stringCollectionToByteArray = (fieldCtx, src) -> {
      final Byte[] ret = new Byte[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToByteCollection stringCollectionToByteCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(
              fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, e)));
          return dst;
        };

    private StringCollectionToBytePrim stringCollectionToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToBytePrim");
    };

    private StringCollectionToBytePrimArray stringCollectionToBytePrimArray = (fieldCtx, src) -> {
      final byte[] ret = new byte[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToBytePrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToCharacter stringCollectionToCharacter = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToCharacter");
    };

    private StringCollectionToCharacterArray stringCollectionToCharacterArray = (fieldCtx, src) -> {
      final Character[] ret = new Character[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToCharacter().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToCharacterCollection stringCollectionToCharacterCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToCharacter()
              .convert(fieldCtx, e)));
          return dst;
        };

    private StringCollectionToCharacterPrim stringCollectionToCharacterPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToCharacterPrim");
    };

    private StringCollectionToCharacterPrimArray stringCollectionToCharacterPrimArray =
        (fieldCtx, src) -> {
          final char[] ret = new char[src.size()];
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToCharacterPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToDateBased stringCollectionToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToDateBased");
    };

    private StringCollectionToDateBasedArray stringCollectionToDateBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToDateBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToDateBasedCollection stringCollectionToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToDateBased()
              .convert(fieldCtx, type, e)));
          return dst;
        };

    private StringCollectionToDoublePrim stringCollectionToDoublePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToDoublePrim");
    };

    private StringCollectionToDoublePrimArray stringCollectionToDoublePrimArray =
        (fieldCtx, src) -> {
          final double[] ret = new double[src.size()];
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToDoublePrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToEnum stringCollectionToEnum = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToEnum");
    };

    private StringCollectionToEnumArray stringCollectionToEnumArray = (fieldCtx, enumType, src) -> {
      final Enum<?>[] ret = new Enum[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] = fieldCtx.getConfiguration().getToObject().getStringToEnum().convert(fieldCtx,
            enumType, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToEnumCollection stringCollectionToEnumCollection =
        (fieldCtx, enumType, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToEnum()
              .convert(fieldCtx, enumType, e)));
          return dst;
        };

    private StringCollectionToFloatPrim stringCollectionToFloatPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToFloatPrim");
    };

    private StringCollectionToFloatPrimArray stringCollectionToFloatPrimArray = (fieldCtx, src) -> {
      final float[] ret = new float[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToFloatPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToIntegerPrim stringCollectionToIntegerPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToIntegerPrim");
    };

    private StringCollectionToIntegerPrimArray stringCollectionToIntegerPrimArray =
        (fieldCtx, src) -> {
          final int[] ret = new int[src.size()];
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToIntegerPrim()
                .convert(fieldCtx, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToLongPrim stringCollectionToLongPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToLongPrim");
    };

    private StringCollectionToLongPrimArray stringCollectionToLongPrimArray = (fieldCtx, src) -> {
      final long[] ret = new long[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToLongPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToNumberBased stringCollectionToNumberBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToNumberBased");
    };

    private StringCollectionToNumberBasedArray stringCollectionToNumberBasedArray =
        (fieldCtx, type, src) -> {
          final Object ret = Array.newInstance(type, src.size());
          int i = 0;
          for (final String e : src) {
            final Object value = fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
                .convert(fieldCtx, type, e);
            setArrayComponent(type, ret, i, value);
            i++;
          }
          return (Object[]) ret;
        };

    private StringCollectionToNumberBasedCollection stringCollectionToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getStringToNumberBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private StringCollectionToObjectId stringCollectionToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToObjectId");
    };

    private StringCollectionToObjectIdArray stringCollectionToObjectIdArray = (fieldCtx, src) -> {
      final ObjectId[] ret = new ObjectId[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToObjectId().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToObjectIdCollection stringCollectionToObjectIdCollection =
        (fieldCtx, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject().getStringToObjectId()
              .convert(fieldCtx, e)));
          return dst;
        };

    private StringCollectionToShortPrim stringCollectionToShortPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringCollectionToShortPrim");
    };

    private StringCollectionToShortPrimArray stringCollectionToShortPrimArray = (fieldCtx, src) -> {
      final short[] ret = new short[src.size()];
      int i = 0;
      for (final String e : src) {
        ret[i] =
            fieldCtx.getConfiguration().getToObject().getStringToShortPrim().convert(fieldCtx, e);
        i++;
      }
      return ret;
    };

    private StringCollectionToStringBased stringCollectionToStringBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException(
          "Not (yet) implemented: StringCollectionToStringBased");
    };

    private StringCollectionToStringBasedArray stringCollectionToStringBasedArray =
        (fieldCtx, type, src) -> {
          final Object[] ret = (Object[]) Array.newInstance(type, src.size());
          int i = 0;
          for (final String e : src) {
            ret[i] = fieldCtx.getConfiguration().getToObject().getStringToStringBased()
                .convert(fieldCtx, type, e);
            i++;
          }
          return ret;
        };

    private StringCollectionToStringBasedCollection stringCollectionToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          src.forEach(e -> dst.add(fieldCtx.getConfiguration().getToObject()
              .getStringToStringBased().convert(fieldCtx, type, e)));
          return dst;
        };

    private StringToBinary stringToBinary = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBinary");
    };

    private StringToBinaryArray stringToBinaryArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBinaryArray");
    };

    private StringToBinaryCollection stringToBinaryCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getStringToBinary().convert(fieldCtx, src));
      return dst;
    };

    private StringToBoolean stringToBoolean = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBoolean");
    };

    private StringToBooleanArray stringToBooleanArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanArray");
    };

    private StringToBooleanCollection stringToBooleanCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getStringToBoolean().convert(fieldCtx, src));
      return dst;
    };

    private StringToBooleanPrim stringToBooleanPrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanPrim");
    };

    private StringToBooleanPrimArray stringToBooleanPrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBooleanPrimArray");
    };

    private StringToByte stringToByte = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToByte");
    };

    private StringToByteArray stringToByteArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToByteArray");
    };

    private StringToByteCollection stringToByteCollection = (fieldCtx, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getStringToByte().convert(fieldCtx, src));
      return dst;
    };

    private StringToBytePrim stringToBytePrim = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBytePrim");
    };

    private StringToBytePrimArray stringToBytePrimArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToBytePrimArray");
    };

    private StringToCharacter stringToCharacter = (fieldCtx, src) -> {
      if ((src == null) || (src.length() < 1)) {
        return null;
      }
      if (src.length() > 1) {
        throw new IllegalStateException("Size of data too long: " + src);
      }
      return src.charAt(0);
    };

    private StringToCharacterArray stringToCharacterArray = (fieldCtx, src) -> {
      if (src == null) {
        return null;
      }
      return toObjectArray(src.toCharArray());
    };

    private StringToCharacterCollection stringToCharacterCollection = (fieldCtx, src, dst) -> {
      dst.addAll(Arrays.asList(toObjectArray(src.toCharArray())));
      return dst;
    };

    private StringToCharacterPrim stringToCharacterPrim = (fieldCtx, src) -> {
      final Character ret =
          fieldCtx.getConfiguration().getToObject().getStringToCharacter().convert(fieldCtx, src);
      if (ret == null) {
        return fieldCtx.getConfiguration().getValueCharacterPrimNull();
      }
      return ret;
    };

    private StringToCharacterPrimArray stringToCharacterPrimArray = (fieldCtx, src) -> {
      if (src == null) {
        return null;
      }
      return src.toCharArray();
    };

    private StringToDateBased stringToDateBased = (fieldCtx, type, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToDateBased");
    };

    private StringToDateBasedArray stringToDateBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getStringToDateBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private StringToDateBasedCollection stringToDateBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getStringToDateBased().convert(fieldCtx,
              type, src));
          return dst;
        };

    private StringToDoublePrim stringToDoublePrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().doubleValue();
      }
      return Double.parseDouble(src);
    };

    private StringToDoublePrimArray stringToDoublePrimArray = (fieldCtx, src) -> new double[] {
        fieldCtx.getConfiguration().getToObject().getStringToDoublePrim().convert(fieldCtx, src)};

    private StringToEnum stringToEnum = (fieldCtx, enumType,
        src) -> (Enum<?>) fieldCtx.getConfiguration().getIndexedEnum(enumType).stringToEnum(src);

    private StringToEnumArray stringToEnumArray = (fieldCtx, enumType, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToEnumArray");
    };

    private StringToEnumCollection stringToEnumCollection = (fieldCtx, enumType, src, dst) -> {
      dst.add(fieldCtx.getConfiguration().getToObject().getStringToEnum().convert(fieldCtx,
          enumType, src));
      return dst;
    };

    private StringToFloatPrim stringToFloatPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().floatValue();
      }
      return Float.parseFloat(src);
    };

    private StringToFloatPrimArray stringToFloatPrimArray = (fieldCtx, src) -> new float[] {
        fieldCtx.getConfiguration().getToObject().getStringToFloatPrim().convert(fieldCtx, src)};

    private StringToIntegerPrim stringToIntegerPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().intValue();
      }
      return Integer.parseInt(src);
    };

    private StringToIntegerPrimArray stringToIntegerPrimArray = (fieldCtx, src) -> new int[] {
        fieldCtx.getConfiguration().getToObject().getStringToIntegerPrim().convert(fieldCtx, src)};

    private StringToLongPrim stringToLongPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().longValue();
      }
      return Long.parseLong(src);
    };

    private StringToLongPrimArray stringToLongPrimArray = (fieldCtx, src) -> new long[] {
        fieldCtx.getConfiguration().getToObject().getStringToLongPrim().convert(fieldCtx, src)};

    private StringToNumberBased stringToNumberBased = (fieldCtx, type, src) -> fieldCtx
        .getConfiguration().getStringBased().getToObject().convert(type, src);

    private StringToNumberBasedArray stringToNumberBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private StringToNumberBasedCollection stringToNumberBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getStringToNumberBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    private StringToObjectId stringToObjectId = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToObjectId");
    };

    private StringToObjectIdArray stringToObjectIdArray = (fieldCtx, src) -> {
      throw new UnsupportedOperationException("Not (yet) implemented: StringToObjectIdArray");
    };

    private StringToObjectIdCollection stringToObjectIdCollection = (fieldCtx, src, dst) -> {
      dst.add(
          fieldCtx.getConfiguration().getToObject().getStringToObjectId().convert(fieldCtx, src));
      return dst;
    };

    private StringToShortPrim stringToShortPrim = (fieldCtx, src) -> {
      if (src == null) {
        return fieldCtx.getConfiguration().getValueNumberPrimNull().shortValue();
      }
      return Short.parseShort(src);
    };

    private StringToShortPrimArray stringToShortPrimArray = (fieldCtx, src) -> new short[] {
        fieldCtx.getConfiguration().getToObject().getStringToShortPrim().convert(fieldCtx, src)};

    private StringToStringBased stringToStringBased = (fieldCtx, type, src) -> fieldCtx
        .getConfiguration().getStringBased().getToObject().convert(type, src);

    private StringToStringBasedArray stringToStringBasedArray = (fieldCtx, type, src) -> {
      final Object ret = Array.newInstance(type, 1);
      final Object value = fieldCtx.getConfiguration().getToObject().getStringToStringBased()
          .convert(fieldCtx, type, src);
      setArrayComponent(type, ret, 0, value);
      return (Object[]) ret;
    };

    private StringToStringBasedCollection stringToStringBasedCollection =
        (fieldCtx, type, src, dst) -> {
          dst.add(fieldCtx.getConfiguration().getToObject().getStringToStringBased()
              .convert(fieldCtx, type, src));
          return dst;
        };

    public Builder binaryCollectionToBinary(
        final BinaryCollectionToBinary binaryCollectionToBinary) {
      this.binaryCollectionToBinary = binaryCollectionToBinary;
      return this;
    }

    public Builder binaryCollectionToBinaryArray(
        final BinaryCollectionToBinaryArray binaryCollectionToBinaryArray) {
      this.binaryCollectionToBinaryArray = binaryCollectionToBinaryArray;
      return this;
    }

    public Builder binaryCollectionToBinaryCollection(
        final BinaryCollectionToBinaryCollection binaryCollectionToBinaryCollection) {
      this.binaryCollectionToBinaryCollection = binaryCollectionToBinaryCollection;
      return this;
    }

    public Builder binaryCollectionToBoolean(
        final BinaryCollectionToBoolean binaryCollectionToBoolean) {
      this.binaryCollectionToBoolean = binaryCollectionToBoolean;
      return this;
    }

    public Builder binaryCollectionToBooleanArray(
        final BinaryCollectionToBooleanArray binaryCollectionToBooleanArray) {
      this.binaryCollectionToBooleanArray = binaryCollectionToBooleanArray;
      return this;
    }

    public Builder binaryCollectionToBooleanCollection(
        final BinaryCollectionToBooleanCollection binaryCollectionToBooleanCollection) {
      this.binaryCollectionToBooleanCollection = binaryCollectionToBooleanCollection;
      return this;
    }

    public Builder binaryCollectionToBooleanPrim(
        final BinaryCollectionToBooleanPrim binaryCollectionToBooleanPrim) {
      this.binaryCollectionToBooleanPrim = binaryCollectionToBooleanPrim;
      return this;
    }

    public Builder binaryCollectionToBooleanPrimArray(
        final BinaryCollectionToBooleanPrimArray binaryCollectionToBooleanPrimArray) {
      this.binaryCollectionToBooleanPrimArray = binaryCollectionToBooleanPrimArray;
      return this;
    }

    public Builder binaryCollectionToByte(final BinaryCollectionToByte binaryCollectionToByte) {
      this.binaryCollectionToByte = binaryCollectionToByte;
      return this;
    }

    public Builder binaryCollectionToByteArray(
        final BinaryCollectionToByteArray binaryCollectionToByteArray) {
      this.binaryCollectionToByteArray = binaryCollectionToByteArray;
      return this;
    }

    public Builder binaryCollectionToByteCollection(
        final BinaryCollectionToByteCollection binaryCollectionToByteCollection) {
      this.binaryCollectionToByteCollection = binaryCollectionToByteCollection;
      return this;
    }

    public Builder binaryCollectionToBytePrim(
        final BinaryCollectionToBytePrim binaryCollectionToBytePrim) {
      this.binaryCollectionToBytePrim = binaryCollectionToBytePrim;
      return this;
    }

    public Builder binaryCollectionToBytePrimArray(
        final BinaryCollectionToBytePrimArray binaryCollectionToBytePrimArray) {
      this.binaryCollectionToBytePrimArray = binaryCollectionToBytePrimArray;
      return this;
    }

    public Builder binaryCollectionToCharacter(
        final BinaryCollectionToCharacter binaryCollectionToCharacter) {
      this.binaryCollectionToCharacter = binaryCollectionToCharacter;
      return this;
    }

    public Builder binaryCollectionToCharacterArray(
        final BinaryCollectionToCharacterArray binaryCollectionToCharacterArray) {
      this.binaryCollectionToCharacterArray = binaryCollectionToCharacterArray;
      return this;
    }

    public Builder binaryCollectionToCharacterCollection(
        final BinaryCollectionToCharacterCollection binaryCollectionToCharacterCollection) {
      this.binaryCollectionToCharacterCollection = binaryCollectionToCharacterCollection;
      return this;
    }

    public Builder binaryCollectionToCharacterPrim(
        final BinaryCollectionToCharacterPrim binaryCollectionToCharacterPrim) {
      this.binaryCollectionToCharacterPrim = binaryCollectionToCharacterPrim;
      return this;
    }

    public Builder binaryCollectionToCharacterPrimArray(
        final BinaryCollectionToCharacterPrimArray binaryCollectionToCharacterPrimArray) {
      this.binaryCollectionToCharacterPrimArray = binaryCollectionToCharacterPrimArray;
      return this;
    }

    public Builder binaryCollectionToDateBased(
        final BinaryCollectionToDateBased binaryCollectionToDateBased) {
      this.binaryCollectionToDateBased = binaryCollectionToDateBased;
      return this;
    }

    public Builder binaryCollectionToDateBasedArray(
        final BinaryCollectionToDateBasedArray binaryCollectionToDateBasedArray) {
      this.binaryCollectionToDateBasedArray = binaryCollectionToDateBasedArray;
      return this;
    }

    public Builder binaryCollectionToDateBasedCollection(
        final BinaryCollectionToDateBasedCollection binaryCollectionToDateBasedCollection) {
      this.binaryCollectionToDateBasedCollection = binaryCollectionToDateBasedCollection;
      return this;
    }

    public Builder binaryCollectionToDoublePrim(
        final BinaryCollectionToDoublePrim binaryCollectionToDoublePrim) {
      this.binaryCollectionToDoublePrim = binaryCollectionToDoublePrim;
      return this;
    }

    public Builder binaryCollectionToDoublePrimArray(
        final BinaryCollectionToDoublePrimArray binaryCollectionToDoublePrimArray) {
      this.binaryCollectionToDoublePrimArray = binaryCollectionToDoublePrimArray;
      return this;
    }

    public Builder binaryCollectionToEnum(final BinaryCollectionToEnum binaryCollectionToEnum) {
      this.binaryCollectionToEnum = binaryCollectionToEnum;
      return this;
    }

    public Builder binaryCollectionToEnumArray(
        final BinaryCollectionToEnumArray binaryCollectionToEnumArray) {
      this.binaryCollectionToEnumArray = binaryCollectionToEnumArray;
      return this;
    }

    public Builder binaryCollectionToEnumCollection(
        final BinaryCollectionToEnumCollection binaryCollectionToEnumCollection) {
      this.binaryCollectionToEnumCollection = binaryCollectionToEnumCollection;
      return this;
    }

    public Builder binaryCollectionToFloatPrim(
        final BinaryCollectionToFloatPrim binaryCollectionToFloatPrim) {
      this.binaryCollectionToFloatPrim = binaryCollectionToFloatPrim;
      return this;
    }

    public Builder binaryCollectionToFloatPrimArray(
        final BinaryCollectionToFloatPrimArray binaryCollectionToFloatPrimArray) {
      this.binaryCollectionToFloatPrimArray = binaryCollectionToFloatPrimArray;
      return this;
    }

    public Builder binaryCollectionToIntegerPrim(
        final BinaryCollectionToIntegerPrim binaryCollectionToIntegerPrim) {
      this.binaryCollectionToIntegerPrim = binaryCollectionToIntegerPrim;
      return this;
    }

    public Builder binaryCollectionToIntegerPrimArray(
        final BinaryCollectionToIntegerPrimArray binaryCollectionToIntegerPrimArray) {
      this.binaryCollectionToIntegerPrimArray = binaryCollectionToIntegerPrimArray;
      return this;
    }

    public Builder binaryCollectionToLongPrim(
        final BinaryCollectionToLongPrim binaryCollectionToLongPrim) {
      this.binaryCollectionToLongPrim = binaryCollectionToLongPrim;
      return this;
    }

    public Builder binaryCollectionToLongPrimArray(
        final BinaryCollectionToLongPrimArray binaryCollectionToLongPrimArray) {
      this.binaryCollectionToLongPrimArray = binaryCollectionToLongPrimArray;
      return this;
    }

    public Builder binaryCollectionToNumberBased(
        final BinaryCollectionToNumberBased binaryCollectionToNumberBased) {
      this.binaryCollectionToNumberBased = binaryCollectionToNumberBased;
      return this;
    }

    public Builder binaryCollectionToNumberBasedArray(
        final BinaryCollectionToNumberBasedArray binaryCollectionToNumberBasedArray) {
      this.binaryCollectionToNumberBasedArray = binaryCollectionToNumberBasedArray;
      return this;
    }

    public Builder binaryCollectionToNumberBasedCollection(
        final BinaryCollectionToNumberBasedCollection binaryCollectionToNumberBasedCollection) {
      this.binaryCollectionToNumberBasedCollection = binaryCollectionToNumberBasedCollection;
      return this;
    }

    public Builder binaryCollectionToObjectId(
        final BinaryCollectionToObjectId binaryCollectionToObjectId) {
      this.binaryCollectionToObjectId = binaryCollectionToObjectId;
      return this;
    }

    public Builder binaryCollectionToObjectIdArray(
        final BinaryCollectionToObjectIdArray binaryCollectionToObjectIdArray) {
      this.binaryCollectionToObjectIdArray = binaryCollectionToObjectIdArray;
      return this;
    }

    public Builder binaryCollectionToObjectIdCollection(
        final BinaryCollectionToObjectIdCollection binaryCollectionToObjectIdCollection) {
      this.binaryCollectionToObjectIdCollection = binaryCollectionToObjectIdCollection;
      return this;
    }

    public Builder binaryCollectionToShortPrim(
        final BinaryCollectionToShortPrim binaryCollectionToShortPrim) {
      this.binaryCollectionToShortPrim = binaryCollectionToShortPrim;
      return this;
    }

    public Builder binaryCollectionToShortPrimArray(
        final BinaryCollectionToShortPrimArray binaryCollectionToShortPrimArray) {
      this.binaryCollectionToShortPrimArray = binaryCollectionToShortPrimArray;
      return this;
    }

    public Builder binaryCollectionToStringBased(
        final BinaryCollectionToStringBased binaryCollectionToStringBased) {
      this.binaryCollectionToStringBased = binaryCollectionToStringBased;
      return this;
    }

    public Builder binaryCollectionToStringBasedArray(
        final BinaryCollectionToStringBasedArray binaryCollectionToStringBasedArray) {
      this.binaryCollectionToStringBasedArray = binaryCollectionToStringBasedArray;
      return this;
    }

    public Builder binaryCollectionToStringBasedCollection(
        final BinaryCollectionToStringBasedCollection binaryCollectionToStringBasedCollection) {
      this.binaryCollectionToStringBasedCollection = binaryCollectionToStringBasedCollection;
      return this;
    }

    public Builder binaryToBinary(final BinaryToBinary binaryToBinary) {
      this.binaryToBinary = binaryToBinary;
      return this;
    }

    public Builder binaryToBinaryArray(final BinaryToBinaryArray binaryToBinaryArray) {
      this.binaryToBinaryArray = binaryToBinaryArray;
      return this;
    }

    public Builder binaryToBinaryCollection(
        final BinaryToBinaryCollection binaryToBinaryCollection) {
      this.binaryToBinaryCollection = binaryToBinaryCollection;
      return this;
    }

    public Builder binaryToBoolean(final BinaryToBoolean binaryToBoolean) {
      this.binaryToBoolean = binaryToBoolean;
      return this;
    }

    public Builder binaryToBooleanArray(final BinaryToBooleanArray binaryToBooleanArray) {
      this.binaryToBooleanArray = binaryToBooleanArray;
      return this;
    }

    public Builder binaryToBooleanCollection(
        final BinaryToBooleanCollection binaryToBooleanCollection) {
      this.binaryToBooleanCollection = binaryToBooleanCollection;
      return this;
    }

    public Builder binaryToBooleanPrim(final BinaryToBooleanPrim binaryToBooleanPrim) {
      this.binaryToBooleanPrim = binaryToBooleanPrim;
      return this;
    }

    public Builder binaryToBooleanPrimArray(
        final BinaryToBooleanPrimArray binaryToBooleanPrimArray) {
      this.binaryToBooleanPrimArray = binaryToBooleanPrimArray;
      return this;
    }

    public Builder binaryToByte(final BinaryToByte binaryToByte) {
      this.binaryToByte = binaryToByte;
      return this;
    }

    public Builder binaryToByteArray(final BinaryToByteArray binaryToByteArray) {
      this.binaryToByteArray = binaryToByteArray;
      return this;
    }

    public Builder binaryToByteCollection(final BinaryToByteCollection binaryToByteCollection) {
      this.binaryToByteCollection = binaryToByteCollection;
      return this;
    }

    public Builder binaryToBytePrim(final BinaryToBytePrim binaryToBytePrim) {
      this.binaryToBytePrim = binaryToBytePrim;
      return this;
    }

    public Builder binaryToBytePrimArray(final BinaryToBytePrimArray binaryToBytePrimArray) {
      this.binaryToBytePrimArray = binaryToBytePrimArray;
      return this;
    }

    public Builder binaryToCharacter(final BinaryToCharacter binaryToCharacter) {
      this.binaryToCharacter = binaryToCharacter;
      return this;
    }

    public Builder binaryToCharacterArray(final BinaryToCharacterArray binaryToCharacterArray) {
      this.binaryToCharacterArray = binaryToCharacterArray;
      return this;
    }

    public Builder binaryToCharacterCollection(
        final BinaryToCharacterCollection binaryToCharacterCollection) {
      this.binaryToCharacterCollection = binaryToCharacterCollection;
      return this;
    }

    public Builder binaryToCharacterPrim(final BinaryToCharacterPrim binaryToCharacterPrim) {
      this.binaryToCharacterPrim = binaryToCharacterPrim;
      return this;
    }

    public Builder binaryToCharacterPrimArray(
        final BinaryToCharacterPrimArray binaryToCharacterPrimArray) {
      this.binaryToCharacterPrimArray = binaryToCharacterPrimArray;
      return this;
    }

    public Builder binaryToDateBased(final BinaryToDateBased binaryToDateBased) {
      this.binaryToDateBased = binaryToDateBased;
      return this;
    }

    public Builder binaryToDateBasedArray(final BinaryToDateBasedArray binaryToDateBasedArray) {
      this.binaryToDateBasedArray = binaryToDateBasedArray;
      return this;
    }

    public Builder binaryToDateBasedCollection(
        final BinaryToDateBasedCollection binaryToDateBasedCollection) {
      this.binaryToDateBasedCollection = binaryToDateBasedCollection;
      return this;
    }

    public Builder binaryToDoublePrim(final BinaryToDoublePrim binaryToDoublePrim) {
      this.binaryToDoublePrim = binaryToDoublePrim;
      return this;
    }

    public Builder binaryToDoublePrimArray(final BinaryToDoublePrimArray binaryToDoublePrimArray) {
      this.binaryToDoublePrimArray = binaryToDoublePrimArray;
      return this;
    }

    public Builder binaryToEnum(final BinaryToEnum binaryToEnum) {
      this.binaryToEnum = binaryToEnum;
      return this;
    }

    public Builder binaryToEnumArray(final BinaryToEnumArray binaryToEnumArray) {
      this.binaryToEnumArray = binaryToEnumArray;
      return this;
    }

    public Builder binaryToEnumCollection(final BinaryToEnumCollection binaryToEnumCollection) {
      this.binaryToEnumCollection = binaryToEnumCollection;
      return this;
    }

    public Builder binaryToFloatPrim(final BinaryToFloatPrim binaryToFloatPrim) {
      this.binaryToFloatPrim = binaryToFloatPrim;
      return this;
    }

    public Builder binaryToFloatPrimArray(final BinaryToFloatPrimArray binaryToFloatPrimArray) {
      this.binaryToFloatPrimArray = binaryToFloatPrimArray;
      return this;
    }

    public Builder binaryToIntegerPrim(final BinaryToIntegerPrim binaryToIntegerPrim) {
      this.binaryToIntegerPrim = binaryToIntegerPrim;
      return this;
    }

    public Builder binaryToIntegerPrimArray(
        final BinaryToIntegerPrimArray binaryToIntegerPrimArray) {
      this.binaryToIntegerPrimArray = binaryToIntegerPrimArray;
      return this;
    }

    public Builder binaryToLongPrim(final BinaryToLongPrim binaryToLongPrim) {
      this.binaryToLongPrim = binaryToLongPrim;
      return this;
    }

    public Builder binaryToLongPrimArray(final BinaryToLongPrimArray binaryToLongPrimArray) {
      this.binaryToLongPrimArray = binaryToLongPrimArray;
      return this;
    }

    public Builder binaryToNumberBased(final BinaryToNumberBased binaryToNumberBased) {
      this.binaryToNumberBased = binaryToNumberBased;
      return this;
    }

    public Builder binaryToNumberBasedArray(
        final BinaryToNumberBasedArray binaryToNumberBasedArray) {
      this.binaryToNumberBasedArray = binaryToNumberBasedArray;
      return this;
    }

    public Builder binaryToNumberBasedCollection(
        final BinaryToNumberBasedCollection binaryToNumberBasedCollection) {
      this.binaryToNumberBasedCollection = binaryToNumberBasedCollection;
      return this;
    }

    public Builder binaryToObjectId(final BinaryToObjectId binaryToObjectId) {
      this.binaryToObjectId = binaryToObjectId;
      return this;
    }

    public Builder binaryToObjectIdArray(final BinaryToObjectIdArray binaryToObjectIdArray) {
      this.binaryToObjectIdArray = binaryToObjectIdArray;
      return this;
    }

    public Builder binaryToObjectIdCollection(
        final BinaryToObjectIdCollection binaryToObjectIdCollection) {
      this.binaryToObjectIdCollection = binaryToObjectIdCollection;
      return this;
    }

    public Builder binaryToShortPrim(final BinaryToShortPrim binaryToShortPrim) {
      this.binaryToShortPrim = binaryToShortPrim;
      return this;
    }

    public Builder binaryToShortPrimArray(final BinaryToShortPrimArray binaryToShortPrimArray) {
      this.binaryToShortPrimArray = binaryToShortPrimArray;
      return this;
    }

    public Builder binaryToStringBased(final BinaryToStringBased binaryToStringBased) {
      this.binaryToStringBased = binaryToStringBased;
      return this;
    }

    public Builder binaryToStringBasedArray(
        final BinaryToStringBasedArray binaryToStringBasedArray) {
      this.binaryToStringBasedArray = binaryToStringBasedArray;
      return this;
    }

    public Builder binaryToStringBasedCollection(
        final BinaryToStringBasedCollection binaryToStringBasedCollection) {
      this.binaryToStringBasedCollection = binaryToStringBasedCollection;
      return this;
    }

    public Builder booleanCollectionToBinary(
        final BooleanCollectionToBinary booleanCollectionToBinary) {
      this.booleanCollectionToBinary = booleanCollectionToBinary;
      return this;
    }

    public Builder booleanCollectionToBinaryArray(
        final BooleanCollectionToBinaryArray booleanCollectionToBinaryArray) {
      this.booleanCollectionToBinaryArray = booleanCollectionToBinaryArray;
      return this;
    }

    public Builder booleanCollectionToBinaryCollection(
        final BooleanCollectionToBinaryCollection booleanCollectionToBinaryCollection) {
      this.booleanCollectionToBinaryCollection = booleanCollectionToBinaryCollection;
      return this;
    }

    public Builder booleanCollectionToBoolean(
        final BooleanCollectionToBoolean booleanCollectionToBoolean) {
      this.booleanCollectionToBoolean = booleanCollectionToBoolean;
      return this;
    }

    public Builder booleanCollectionToBooleanArray(
        final BooleanCollectionToBooleanArray booleanCollectionToBooleanArray) {
      this.booleanCollectionToBooleanArray = booleanCollectionToBooleanArray;
      return this;
    }

    public Builder booleanCollectionToBooleanCollection(
        final BooleanCollectionToBooleanCollection booleanCollectionToBooleanCollection) {
      this.booleanCollectionToBooleanCollection = booleanCollectionToBooleanCollection;
      return this;
    }

    public Builder booleanCollectionToBooleanPrim(
        final BooleanCollectionToBooleanPrim booleanCollectionToBooleanPrim) {
      this.booleanCollectionToBooleanPrim = booleanCollectionToBooleanPrim;
      return this;
    }

    public Builder booleanCollectionToBooleanPrimArray(
        final BooleanCollectionToBooleanPrimArray booleanCollectionToBooleanPrimArray) {
      this.booleanCollectionToBooleanPrimArray = booleanCollectionToBooleanPrimArray;
      return this;
    }

    public Builder booleanCollectionToByte(final BooleanCollectionToByte booleanCollectionToByte) {
      this.booleanCollectionToByte = booleanCollectionToByte;
      return this;
    }

    public Builder booleanCollectionToByteArray(
        final BooleanCollectionToByteArray booleanCollectionToByteArray) {
      this.booleanCollectionToByteArray = booleanCollectionToByteArray;
      return this;
    }

    public Builder booleanCollectionToByteCollection(
        final BooleanCollectionToByteCollection booleanCollectionToByteCollection) {
      this.booleanCollectionToByteCollection = booleanCollectionToByteCollection;
      return this;
    }

    public Builder booleanCollectionToBytePrim(
        final BooleanCollectionToBytePrim booleanCollectionToBytePrim) {
      this.booleanCollectionToBytePrim = booleanCollectionToBytePrim;
      return this;
    }

    public Builder booleanCollectionToBytePrimArray(
        final BooleanCollectionToBytePrimArray booleanCollectionToBytePrimArray) {
      this.booleanCollectionToBytePrimArray = booleanCollectionToBytePrimArray;
      return this;
    }

    public Builder booleanCollectionToCharacter(
        final BooleanCollectionToCharacter booleanCollectionToCharacter) {
      this.booleanCollectionToCharacter = booleanCollectionToCharacter;
      return this;
    }

    public Builder booleanCollectionToCharacterArray(
        final BooleanCollectionToCharacterArray booleanCollectionToCharacterArray) {
      this.booleanCollectionToCharacterArray = booleanCollectionToCharacterArray;
      return this;
    }

    public Builder booleanCollectionToCharacterCollection(
        final BooleanCollectionToCharacterCollection booleanCollectionToCharacterCollection) {
      this.booleanCollectionToCharacterCollection = booleanCollectionToCharacterCollection;
      return this;
    }

    public Builder booleanCollectionToCharacterPrim(
        final BooleanCollectionToCharacterPrim booleanCollectionToCharacterPrim) {
      this.booleanCollectionToCharacterPrim = booleanCollectionToCharacterPrim;
      return this;
    }

    public Builder booleanCollectionToCharacterPrimArray(
        final BooleanCollectionToCharacterPrimArray booleanCollectionToCharacterPrimArray) {
      this.booleanCollectionToCharacterPrimArray = booleanCollectionToCharacterPrimArray;
      return this;
    }

    public Builder booleanCollectionToDateBased(
        final BooleanCollectionToDateBased booleanCollectionToDateBased) {
      this.booleanCollectionToDateBased = booleanCollectionToDateBased;
      return this;
    }

    public Builder booleanCollectionToDateBasedArray(
        final BooleanCollectionToDateBasedArray booleanCollectionToDateBasedArray) {
      this.booleanCollectionToDateBasedArray = booleanCollectionToDateBasedArray;
      return this;
    }

    public Builder booleanCollectionToDateBasedCollection(
        final BooleanCollectionToDateBasedCollection booleanCollectionToDateBasedCollection) {
      this.booleanCollectionToDateBasedCollection = booleanCollectionToDateBasedCollection;
      return this;
    }

    public Builder booleanCollectionToDoublePrim(
        final BooleanCollectionToDoublePrim booleanCollectionToDoublePrim) {
      this.booleanCollectionToDoublePrim = booleanCollectionToDoublePrim;
      return this;
    }

    public Builder booleanCollectionToDoublePrimArray(
        final BooleanCollectionToDoublePrimArray booleanCollectionToDoublePrimArray) {
      this.booleanCollectionToDoublePrimArray = booleanCollectionToDoublePrimArray;
      return this;
    }

    public Builder booleanCollectionToEnum(final BooleanCollectionToEnum booleanCollectionToEnum) {
      this.booleanCollectionToEnum = booleanCollectionToEnum;
      return this;
    }

    public Builder booleanCollectionToEnumArray(
        final BooleanCollectionToEnumArray booleanCollectionToEnumArray) {
      this.booleanCollectionToEnumArray = booleanCollectionToEnumArray;
      return this;
    }

    public Builder booleanCollectionToEnumCollection(
        final BooleanCollectionToEnumCollection booleanCollectionToEnumCollection) {
      this.booleanCollectionToEnumCollection = booleanCollectionToEnumCollection;
      return this;
    }

    public Builder booleanCollectionToFloatPrim(
        final BooleanCollectionToFloatPrim booleanCollectionToFloatPrim) {
      this.booleanCollectionToFloatPrim = booleanCollectionToFloatPrim;
      return this;
    }

    public Builder booleanCollectionToFloatPrimArray(
        final BooleanCollectionToFloatPrimArray booleanCollectionToFloatPrimArray) {
      this.booleanCollectionToFloatPrimArray = booleanCollectionToFloatPrimArray;
      return this;
    }

    public Builder booleanCollectionToIntegerPrim(
        final BooleanCollectionToIntegerPrim booleanCollectionToIntegerPrim) {
      this.booleanCollectionToIntegerPrim = booleanCollectionToIntegerPrim;
      return this;
    }

    public Builder booleanCollectionToIntegerPrimArray(
        final BooleanCollectionToIntegerPrimArray booleanCollectionToIntegerPrimArray) {
      this.booleanCollectionToIntegerPrimArray = booleanCollectionToIntegerPrimArray;
      return this;
    }

    public Builder booleanCollectionToLongPrim(
        final BooleanCollectionToLongPrim booleanCollectionToLongPrim) {
      this.booleanCollectionToLongPrim = booleanCollectionToLongPrim;
      return this;
    }

    public Builder booleanCollectionToLongPrimArray(
        final BooleanCollectionToLongPrimArray booleanCollectionToLongPrimArray) {
      this.booleanCollectionToLongPrimArray = booleanCollectionToLongPrimArray;
      return this;
    }

    public Builder booleanCollectionToNumberBased(
        final BooleanCollectionToNumberBased booleanCollectionToNumberBased) {
      this.booleanCollectionToNumberBased = booleanCollectionToNumberBased;
      return this;
    }

    public Builder booleanCollectionToNumberBasedArray(
        final BooleanCollectionToNumberBasedArray booleanCollectionToNumberBasedArray) {
      this.booleanCollectionToNumberBasedArray = booleanCollectionToNumberBasedArray;
      return this;
    }

    public Builder booleanCollectionToNumberBasedCollection(
        final BooleanCollectionToNumberBasedCollection booleanCollectionToNumberBasedCollection) {
      this.booleanCollectionToNumberBasedCollection = booleanCollectionToNumberBasedCollection;
      return this;
    }

    public Builder booleanCollectionToObjectId(
        final BooleanCollectionToObjectId booleanCollectionToObjectId) {
      this.booleanCollectionToObjectId = booleanCollectionToObjectId;
      return this;
    }

    public Builder booleanCollectionToObjectIdArray(
        final BooleanCollectionToObjectIdArray booleanCollectionToObjectIdArray) {
      this.booleanCollectionToObjectIdArray = booleanCollectionToObjectIdArray;
      return this;
    }

    public Builder booleanCollectionToObjectIdCollection(
        final BooleanCollectionToObjectIdCollection booleanCollectionToObjectIdCollection) {
      this.booleanCollectionToObjectIdCollection = booleanCollectionToObjectIdCollection;
      return this;
    }

    public Builder booleanCollectionToShortPrim(
        final BooleanCollectionToShortPrim booleanCollectionToShortPrim) {
      this.booleanCollectionToShortPrim = booleanCollectionToShortPrim;
      return this;
    }

    public Builder booleanCollectionToShortPrimArray(
        final BooleanCollectionToShortPrimArray booleanCollectionToShortPrimArray) {
      this.booleanCollectionToShortPrimArray = booleanCollectionToShortPrimArray;
      return this;
    }

    public Builder booleanCollectionToStringBased(
        final BooleanCollectionToStringBased booleanCollectionToStringBased) {
      this.booleanCollectionToStringBased = booleanCollectionToStringBased;
      return this;
    }

    public Builder booleanCollectionToStringBasedArray(
        final BooleanCollectionToStringBasedArray booleanCollectionToStringBasedArray) {
      this.booleanCollectionToStringBasedArray = booleanCollectionToStringBasedArray;
      return this;
    }

    public Builder booleanCollectionToStringBasedCollection(
        final BooleanCollectionToStringBasedCollection booleanCollectionToStringBasedCollection) {
      this.booleanCollectionToStringBasedCollection = booleanCollectionToStringBasedCollection;
      return this;
    }

    public Builder booleanToBinary(final BooleanToBinary booleanToBinary) {
      this.booleanToBinary = booleanToBinary;
      return this;
    }

    public Builder booleanToBinaryArray(final BooleanToBinaryArray booleanToBinaryArray) {
      this.booleanToBinaryArray = booleanToBinaryArray;
      return this;
    }

    public Builder booleanToBinaryCollection(
        final BooleanToBinaryCollection booleanToBinaryCollection) {
      this.booleanToBinaryCollection = booleanToBinaryCollection;
      return this;
    }

    public Builder booleanToBoolean(final BooleanToBoolean booleanToBoolean) {
      this.booleanToBoolean = booleanToBoolean;
      return this;
    }

    public Builder booleanToBooleanArray(final BooleanToBooleanArray booleanToBooleanArray) {
      this.booleanToBooleanArray = booleanToBooleanArray;
      return this;
    }

    public Builder booleanToBooleanCollection(
        final BooleanToBooleanCollection booleanToBooleanCollection) {
      this.booleanToBooleanCollection = booleanToBooleanCollection;
      return this;
    }

    public Builder booleanToBooleanPrim(final BooleanToBooleanPrim booleanToBooleanPrim) {
      this.booleanToBooleanPrim = booleanToBooleanPrim;
      return this;
    }

    public Builder booleanToBooleanPrimArray(
        final BooleanToBooleanPrimArray booleanToBooleanPrimArray) {
      this.booleanToBooleanPrimArray = booleanToBooleanPrimArray;
      return this;
    }

    public Builder booleanToByte(final BooleanToByte booleanToByte) {
      this.booleanToByte = booleanToByte;
      return this;
    }

    public Builder booleanToByteArray(final BooleanToByteArray booleanToByteArray) {
      this.booleanToByteArray = booleanToByteArray;
      return this;
    }

    public Builder booleanToByteCollection(final BooleanToByteCollection booleanToByteCollection) {
      this.booleanToByteCollection = booleanToByteCollection;
      return this;
    }

    public Builder booleanToBytePrim(final BooleanToBytePrim booleanToBytePrim) {
      this.booleanToBytePrim = booleanToBytePrim;
      return this;
    }

    public Builder booleanToBytePrimArray(final BooleanToBytePrimArray booleanToBytePrimArray) {
      this.booleanToBytePrimArray = booleanToBytePrimArray;
      return this;
    }

    public Builder booleanToCharacter(final BooleanToCharacter booleanToCharacter) {
      this.booleanToCharacter = booleanToCharacter;
      return this;
    }

    public Builder booleanToCharacterArray(final BooleanToCharacterArray booleanToCharacterArray) {
      this.booleanToCharacterArray = booleanToCharacterArray;
      return this;
    }

    public Builder booleanToCharacterCollection(
        final BooleanToCharacterCollection booleanToCharacterCollection) {
      this.booleanToCharacterCollection = booleanToCharacterCollection;
      return this;
    }

    public Builder booleanToCharacterPrim(final BooleanToCharacterPrim booleanToCharacterPrim) {
      this.booleanToCharacterPrim = booleanToCharacterPrim;
      return this;
    }

    public Builder booleanToCharacterPrimArray(
        final BooleanToCharacterPrimArray booleanToCharacterPrimArray) {
      this.booleanToCharacterPrimArray = booleanToCharacterPrimArray;
      return this;
    }

    public Builder booleanToDateBased(final BooleanToDateBased booleanToDateBased) {
      this.booleanToDateBased = booleanToDateBased;
      return this;
    }

    public Builder booleanToDateBasedArray(final BooleanToDateBasedArray booleanToDateBasedArray) {
      this.booleanToDateBasedArray = booleanToDateBasedArray;
      return this;
    }

    public Builder booleanToDateBasedCollection(
        final BooleanToDateBasedCollection booleanToDateBasedCollection) {
      this.booleanToDateBasedCollection = booleanToDateBasedCollection;
      return this;
    }

    public Builder booleanToDoublePrim(final BooleanToDoublePrim booleanToDoublePrim) {
      this.booleanToDoublePrim = booleanToDoublePrim;
      return this;
    }

    public Builder booleanToDoublePrimArray(
        final BooleanToDoublePrimArray booleanToDoublePrimArray) {
      this.booleanToDoublePrimArray = booleanToDoublePrimArray;
      return this;
    }

    public Builder booleanToEnum(final BooleanToEnum booleanToEnum) {
      this.booleanToEnum = booleanToEnum;
      return this;
    }

    public Builder booleanToEnumArray(final BooleanToEnumArray booleanToEnumArray) {
      this.booleanToEnumArray = booleanToEnumArray;
      return this;
    }

    public Builder booleanToEnumCollection(final BooleanToEnumCollection booleanToEnumCollection) {
      this.booleanToEnumCollection = booleanToEnumCollection;
      return this;
    }

    public Builder booleanToFloatPrim(final BooleanToFloatPrim booleanToFloatPrim) {
      this.booleanToFloatPrim = booleanToFloatPrim;
      return this;
    }

    public Builder booleanToFloatPrimArray(final BooleanToFloatPrimArray booleanToFloatPrimArray) {
      this.booleanToFloatPrimArray = booleanToFloatPrimArray;
      return this;
    }

    public Builder booleanToIntegerPrim(final BooleanToIntegerPrim booleanToIntegerPrim) {
      this.booleanToIntegerPrim = booleanToIntegerPrim;
      return this;
    }

    public Builder booleanToIntegerPrimArray(
        final BooleanToIntegerPrimArray booleanToIntegerPrimArray) {
      this.booleanToIntegerPrimArray = booleanToIntegerPrimArray;
      return this;
    }

    public Builder booleanToLongPrim(final BooleanToLongPrim booleanToLongPrim) {
      this.booleanToLongPrim = booleanToLongPrim;
      return this;
    }

    public Builder booleanToLongPrimArray(final BooleanToLongPrimArray booleanToLongPrimArray) {
      this.booleanToLongPrimArray = booleanToLongPrimArray;
      return this;
    }

    public Builder booleanToNumberBased(final BooleanToNumberBased booleanToNumberBased) {
      this.booleanToNumberBased = booleanToNumberBased;
      return this;
    }

    public Builder booleanToNumberBasedArray(
        final BooleanToNumberBasedArray booleanToNumberBasedArray) {
      this.booleanToNumberBasedArray = booleanToNumberBasedArray;
      return this;
    }

    public Builder booleanToNumberBasedCollection(
        final BooleanToNumberBasedCollection booleanToNumberBasedCollection) {
      this.booleanToNumberBasedCollection = booleanToNumberBasedCollection;
      return this;
    }

    public Builder booleanToObjectId(final BooleanToObjectId booleanToObjectId) {
      this.booleanToObjectId = booleanToObjectId;
      return this;
    }

    public Builder booleanToObjectIdArray(final BooleanToObjectIdArray booleanToObjectIdArray) {
      this.booleanToObjectIdArray = booleanToObjectIdArray;
      return this;
    }

    public Builder booleanToObjectIdCollection(
        final BooleanToObjectIdCollection booleanToObjectIdCollection) {
      this.booleanToObjectIdCollection = booleanToObjectIdCollection;
      return this;
    }

    public Builder booleanToShortPrim(final BooleanToShortPrim booleanToShortPrim) {
      this.booleanToShortPrim = booleanToShortPrim;
      return this;
    }

    public Builder booleanToShortPrimArray(final BooleanToShortPrimArray booleanToShortPrimArray) {
      this.booleanToShortPrimArray = booleanToShortPrimArray;
      return this;
    }

    public Builder booleanToStringBased(final BooleanToStringBased booleanToStringBased) {
      this.booleanToStringBased = booleanToStringBased;
      return this;
    }

    public Builder booleanToStringBasedArray(
        final BooleanToStringBasedArray booleanToStringBasedArray) {
      this.booleanToStringBasedArray = booleanToStringBasedArray;
      return this;
    }

    public Builder booleanToStringBasedCollection(
        final BooleanToStringBasedCollection booleanToStringBasedCollection) {
      this.booleanToStringBasedCollection = booleanToStringBasedCollection;
      return this;
    }

    public JaxBsonToObject build() {
      return new JaxBsonToObject(this);
    }

    public Builder dateCollectionToBinary(final DateCollectionToBinary dateCollectionToBinary) {
      this.dateCollectionToBinary = dateCollectionToBinary;
      return this;
    }

    public Builder dateCollectionToBinaryArray(
        final DateCollectionToBinaryArray dateCollectionToBinaryArray) {
      this.dateCollectionToBinaryArray = dateCollectionToBinaryArray;
      return this;
    }

    public Builder dateCollectionToBinaryCollection(
        final DateCollectionToBinaryCollection dateCollectionToBinaryCollection) {
      this.dateCollectionToBinaryCollection = dateCollectionToBinaryCollection;
      return this;
    }

    public Builder dateCollectionToBoolean(final DateCollectionToBoolean dateCollectionToBoolean) {
      this.dateCollectionToBoolean = dateCollectionToBoolean;
      return this;
    }

    public Builder dateCollectionToBooleanArray(
        final DateCollectionToBooleanArray dateCollectionToBooleanArray) {
      this.dateCollectionToBooleanArray = dateCollectionToBooleanArray;
      return this;
    }

    public Builder dateCollectionToBooleanCollection(
        final DateCollectionToBooleanCollection dateCollectionToBooleanCollection) {
      this.dateCollectionToBooleanCollection = dateCollectionToBooleanCollection;
      return this;
    }

    public Builder dateCollectionToBooleanPrim(
        final DateCollectionToBooleanPrim dateCollectionToBooleanPrim) {
      this.dateCollectionToBooleanPrim = dateCollectionToBooleanPrim;
      return this;
    }

    public Builder dateCollectionToBooleanPrimArray(
        final DateCollectionToBooleanPrimArray dateCollectionToBooleanPrimArray) {
      this.dateCollectionToBooleanPrimArray = dateCollectionToBooleanPrimArray;
      return this;
    }

    public Builder dateCollectionToByte(final DateCollectionToByte dateCollectionToByte) {
      this.dateCollectionToByte = dateCollectionToByte;
      return this;
    }

    public Builder dateCollectionToByteArray(
        final DateCollectionToByteArray dateCollectionToByteArray) {
      this.dateCollectionToByteArray = dateCollectionToByteArray;
      return this;
    }

    public Builder dateCollectionToByteCollection(
        final DateCollectionToByteCollection dateCollectionToByteCollection) {
      this.dateCollectionToByteCollection = dateCollectionToByteCollection;
      return this;
    }

    public Builder dateCollectionToBytePrim(
        final DateCollectionToBytePrim dateCollectionToBytePrim) {
      this.dateCollectionToBytePrim = dateCollectionToBytePrim;
      return this;
    }

    public Builder dateCollectionToBytePrimArray(
        final DateCollectionToBytePrimArray dateCollectionToBytePrimArray) {
      this.dateCollectionToBytePrimArray = dateCollectionToBytePrimArray;
      return this;
    }

    public Builder dateCollectionToCharacter(
        final DateCollectionToCharacter dateCollectionToCharacter) {
      this.dateCollectionToCharacter = dateCollectionToCharacter;
      return this;
    }

    public Builder dateCollectionToCharacterArray(
        final DateCollectionToCharacterArray dateCollectionToCharacterArray) {
      this.dateCollectionToCharacterArray = dateCollectionToCharacterArray;
      return this;
    }

    public Builder dateCollectionToCharacterCollection(
        final DateCollectionToCharacterCollection dateCollectionToCharacterCollection) {
      this.dateCollectionToCharacterCollection = dateCollectionToCharacterCollection;
      return this;
    }

    public Builder dateCollectionToCharacterPrim(
        final DateCollectionToCharacterPrim dateCollectionToCharacterPrim) {
      this.dateCollectionToCharacterPrim = dateCollectionToCharacterPrim;
      return this;
    }

    public Builder dateCollectionToCharacterPrimArray(
        final DateCollectionToCharacterPrimArray dateCollectionToCharacterPrimArray) {
      this.dateCollectionToCharacterPrimArray = dateCollectionToCharacterPrimArray;
      return this;
    }

    public Builder dateCollectionToDateBased(
        final DateCollectionToDateBased dateCollectionToDateBased) {
      this.dateCollectionToDateBased = dateCollectionToDateBased;
      return this;
    }

    public Builder dateCollectionToDateBasedArray(
        final DateCollectionToDateBasedArray dateCollectionToDateBasedArray) {
      this.dateCollectionToDateBasedArray = dateCollectionToDateBasedArray;
      return this;
    }

    public Builder dateCollectionToDateBasedCollection(
        final DateCollectionToDateBasedCollection dateCollectionToDateBasedCollection) {
      this.dateCollectionToDateBasedCollection = dateCollectionToDateBasedCollection;
      return this;
    }

    public Builder dateCollectionToDoublePrim(
        final DateCollectionToDoublePrim dateCollectionToDoublePrim) {
      this.dateCollectionToDoublePrim = dateCollectionToDoublePrim;
      return this;
    }

    public Builder dateCollectionToDoublePrimArray(
        final DateCollectionToDoublePrimArray dateCollectionToDoublePrimArray) {
      this.dateCollectionToDoublePrimArray = dateCollectionToDoublePrimArray;
      return this;
    }

    public Builder dateCollectionToEnum(final DateCollectionToEnum dateCollectionToEnum) {
      this.dateCollectionToEnum = dateCollectionToEnum;
      return this;
    }

    public Builder dateCollectionToEnumArray(
        final DateCollectionToEnumArray dateCollectionToEnumArray) {
      this.dateCollectionToEnumArray = dateCollectionToEnumArray;
      return this;
    }

    public Builder dateCollectionToEnumCollection(
        final DateCollectionToEnumCollection dateCollectionToEnumCollection) {
      this.dateCollectionToEnumCollection = dateCollectionToEnumCollection;
      return this;
    }

    public Builder dateCollectionToFloatPrim(
        final DateCollectionToFloatPrim dateCollectionToFloatPrim) {
      this.dateCollectionToFloatPrim = dateCollectionToFloatPrim;
      return this;
    }

    public Builder dateCollectionToFloatPrimArray(
        final DateCollectionToFloatPrimArray dateCollectionToFloatPrimArray) {
      this.dateCollectionToFloatPrimArray = dateCollectionToFloatPrimArray;
      return this;
    }

    public Builder dateCollectionToIntegerPrim(
        final DateCollectionToIntegerPrim dateCollectionToIntegerPrim) {
      this.dateCollectionToIntegerPrim = dateCollectionToIntegerPrim;
      return this;
    }

    public Builder dateCollectionToIntegerPrimArray(
        final DateCollectionToIntegerPrimArray dateCollectionToIntegerPrimArray) {
      this.dateCollectionToIntegerPrimArray = dateCollectionToIntegerPrimArray;
      return this;
    }

    public Builder dateCollectionToLongPrim(
        final DateCollectionToLongPrim dateCollectionToLongPrim) {
      this.dateCollectionToLongPrim = dateCollectionToLongPrim;
      return this;
    }

    public Builder dateCollectionToLongPrimArray(
        final DateCollectionToLongPrimArray dateCollectionToLongPrimArray) {
      this.dateCollectionToLongPrimArray = dateCollectionToLongPrimArray;
      return this;
    }

    public Builder dateCollectionToNumberBased(
        final DateCollectionToNumberBased dateCollectionToNumberBased) {
      this.dateCollectionToNumberBased = dateCollectionToNumberBased;
      return this;
    }

    public Builder dateCollectionToNumberBasedArray(
        final DateCollectionToNumberBasedArray dateCollectionToNumberBasedArray) {
      this.dateCollectionToNumberBasedArray = dateCollectionToNumberBasedArray;
      return this;
    }

    public Builder dateCollectionToNumberBasedCollection(
        final DateCollectionToNumberBasedCollection dateCollectionToNumberBasedCollection) {
      this.dateCollectionToNumberBasedCollection = dateCollectionToNumberBasedCollection;
      return this;
    }

    public Builder dateCollectionToObjectId(
        final DateCollectionToObjectId dateCollectionToObjectId) {
      this.dateCollectionToObjectId = dateCollectionToObjectId;
      return this;
    }

    public Builder dateCollectionToObjectIdArray(
        final DateCollectionToObjectIdArray dateCollectionToObjectIdArray) {
      this.dateCollectionToObjectIdArray = dateCollectionToObjectIdArray;
      return this;
    }

    public Builder dateCollectionToObjectIdCollection(
        final DateCollectionToObjectIdCollection dateCollectionToObjectIdCollection) {
      this.dateCollectionToObjectIdCollection = dateCollectionToObjectIdCollection;
      return this;
    }

    public Builder dateCollectionToShortPrim(
        final DateCollectionToShortPrim dateCollectionToShortPrim) {
      this.dateCollectionToShortPrim = dateCollectionToShortPrim;
      return this;
    }

    public Builder dateCollectionToShortPrimArray(
        final DateCollectionToShortPrimArray dateCollectionToShortPrimArray) {
      this.dateCollectionToShortPrimArray = dateCollectionToShortPrimArray;
      return this;
    }

    public Builder dateCollectionToStringBased(
        final DateCollectionToStringBased dateCollectionToStringBased) {
      this.dateCollectionToStringBased = dateCollectionToStringBased;
      return this;
    }

    public Builder dateCollectionToStringBasedArray(
        final DateCollectionToStringBasedArray dateCollectionToStringBasedArray) {
      this.dateCollectionToStringBasedArray = dateCollectionToStringBasedArray;
      return this;
    }

    public Builder dateCollectionToStringBasedCollection(
        final DateCollectionToStringBasedCollection dateCollectionToStringBasedCollection) {
      this.dateCollectionToStringBasedCollection = dateCollectionToStringBasedCollection;
      return this;
    }

    public Builder dateToBinary(final DateToBinary dateToBinary) {
      this.dateToBinary = dateToBinary;
      return this;
    }

    public Builder dateToBinaryArray(final DateToBinaryArray dateToBinaryArray) {
      this.dateToBinaryArray = dateToBinaryArray;
      return this;
    }

    public Builder dateToBinaryCollection(final DateToBinaryCollection dateToBinaryCollection) {
      this.dateToBinaryCollection = dateToBinaryCollection;
      return this;
    }

    public Builder dateToBoolean(final DateToBoolean dateToBoolean) {
      this.dateToBoolean = dateToBoolean;
      return this;
    }

    public Builder dateToBooleanArray(final DateToBooleanArray dateToBooleanArray) {
      this.dateToBooleanArray = dateToBooleanArray;
      return this;
    }

    public Builder dateToBooleanCollection(final DateToBooleanCollection dateToBooleanCollection) {
      this.dateToBooleanCollection = dateToBooleanCollection;
      return this;
    }

    public Builder dateToBooleanPrim(final DateToBooleanPrim dateToBooleanPrim) {
      this.dateToBooleanPrim = dateToBooleanPrim;
      return this;
    }

    public Builder dateToBooleanPrimArray(final DateToBooleanPrimArray dateToBooleanPrimArray) {
      this.dateToBooleanPrimArray = dateToBooleanPrimArray;
      return this;
    }

    public Builder dateToByte(final DateToByte dateToByte) {
      this.dateToByte = dateToByte;
      return this;
    }

    public Builder dateToByteArray(final DateToByteArray dateToByteArray) {
      this.dateToByteArray = dateToByteArray;
      return this;
    }

    public Builder dateToByteCollection(final DateToByteCollection dateToByteCollection) {
      this.dateToByteCollection = dateToByteCollection;
      return this;
    }

    public Builder dateToBytePrim(final DateToBytePrim dateToBytePrim) {
      this.dateToBytePrim = dateToBytePrim;
      return this;
    }

    public Builder dateToBytePrimArray(final DateToBytePrimArray dateToBytePrimArray) {
      this.dateToBytePrimArray = dateToBytePrimArray;
      return this;
    }

    public Builder dateToCharacter(final DateToCharacter dateToCharacter) {
      this.dateToCharacter = dateToCharacter;
      return this;
    }

    public Builder dateToCharacterArray(final DateToCharacterArray dateToCharacterArray) {
      this.dateToCharacterArray = dateToCharacterArray;
      return this;
    }

    public Builder dateToCharacterCollection(
        final DateToCharacterCollection dateToCharacterCollection) {
      this.dateToCharacterCollection = dateToCharacterCollection;
      return this;
    }

    public Builder dateToCharacterPrim(final DateToCharacterPrim dateToCharacterPrim) {
      this.dateToCharacterPrim = dateToCharacterPrim;
      return this;
    }

    public Builder dateToCharacterPrimArray(
        final DateToCharacterPrimArray dateToCharacterPrimArray) {
      this.dateToCharacterPrimArray = dateToCharacterPrimArray;
      return this;
    }

    public Builder dateToDateBased(final DateToDateBased dateToDateBased) {
      this.dateToDateBased = dateToDateBased;
      return this;
    }

    public Builder dateToDateBasedArray(final DateToDateBasedArray dateToDateBasedArray) {
      this.dateToDateBasedArray = dateToDateBasedArray;
      return this;
    }

    public Builder dateToDateBasedCollection(
        final DateToDateBasedCollection dateToDateBasedCollection) {
      this.dateToDateBasedCollection = dateToDateBasedCollection;
      return this;
    }

    public Builder dateToDoublePrim(final DateToDoublePrim dateToDoublePrim) {
      this.dateToDoublePrim = dateToDoublePrim;
      return this;
    }

    public Builder dateToDoublePrimArray(final DateToDoublePrimArray dateToDoublePrimArray) {
      this.dateToDoublePrimArray = dateToDoublePrimArray;
      return this;
    }

    public Builder dateToEnum(final DateToEnum dateToEnum) {
      this.dateToEnum = dateToEnum;
      return this;
    }

    public Builder dateToEnumArray(final DateToEnumArray dateToEnumArray) {
      this.dateToEnumArray = dateToEnumArray;
      return this;
    }

    public Builder dateToEnumCollection(final DateToEnumCollection dateToEnumCollection) {
      this.dateToEnumCollection = dateToEnumCollection;
      return this;
    }

    public Builder dateToFloatPrim(final DateToFloatPrim dateToFloatPrim) {
      this.dateToFloatPrim = dateToFloatPrim;
      return this;
    }

    public Builder dateToFloatPrimArray(final DateToFloatPrimArray dateToFloatPrimArray) {
      this.dateToFloatPrimArray = dateToFloatPrimArray;
      return this;
    }

    public Builder dateToIntegerPrim(final DateToIntegerPrim dateToIntegerPrim) {
      this.dateToIntegerPrim = dateToIntegerPrim;
      return this;
    }

    public Builder dateToIntegerPrimArray(final DateToIntegerPrimArray dateToIntegerPrimArray) {
      this.dateToIntegerPrimArray = dateToIntegerPrimArray;
      return this;
    }

    public Builder dateToLongPrim(final DateToLongPrim dateToLongPrim) {
      this.dateToLongPrim = dateToLongPrim;
      return this;
    }

    public Builder dateToLongPrimArray(final DateToLongPrimArray dateToLongPrimArray) {
      this.dateToLongPrimArray = dateToLongPrimArray;
      return this;
    }

    public Builder dateToNumberBased(final DateToNumberBased dateToNumberBased) {
      this.dateToNumberBased = dateToNumberBased;
      return this;
    }

    public Builder dateToNumberBasedArray(final DateToNumberBasedArray dateToNumberBasedArray) {
      this.dateToNumberBasedArray = dateToNumberBasedArray;
      return this;
    }

    public Builder dateToNumberBasedCollection(
        final DateToNumberBasedCollection dateToNumberBasedCollection) {
      this.dateToNumberBasedCollection = dateToNumberBasedCollection;
      return this;
    }

    public Builder dateToObjectId(final DateToObjectId dateToObjectId) {
      this.dateToObjectId = dateToObjectId;
      return this;
    }

    public Builder dateToObjectIdArray(final DateToObjectIdArray dateToObjectIdArray) {
      this.dateToObjectIdArray = dateToObjectIdArray;
      return this;
    }

    public Builder dateToObjectIdCollection(
        final DateToObjectIdCollection dateToObjectIdCollection) {
      this.dateToObjectIdCollection = dateToObjectIdCollection;
      return this;
    }

    public Builder dateToShortPrim(final DateToShortPrim dateToShortPrim) {
      this.dateToShortPrim = dateToShortPrim;
      return this;
    }

    public Builder dateToShortPrimArray(final DateToShortPrimArray dateToShortPrimArray) {
      this.dateToShortPrimArray = dateToShortPrimArray;
      return this;
    }

    public Builder dateToStringBased(final DateToStringBased dateToStringBased) {
      this.dateToStringBased = dateToStringBased;
      return this;
    }

    public Builder dateToStringBasedArray(final DateToStringBasedArray dateToStringBasedArray) {
      this.dateToStringBasedArray = dateToStringBasedArray;
      return this;
    }

    public Builder dateToStringBasedCollection(
        final DateToStringBasedCollection dateToStringBasedCollection) {
      this.dateToStringBasedCollection = dateToStringBasedCollection;
      return this;
    }

    public Builder numberCollectionToBinary(
        final NumberCollectionToBinary numberCollectionToBinary) {
      this.numberCollectionToBinary = numberCollectionToBinary;
      return this;
    }

    public Builder numberCollectionToBinaryArray(
        final NumberCollectionToBinaryArray numberCollectionToBinaryArray) {
      this.numberCollectionToBinaryArray = numberCollectionToBinaryArray;
      return this;
    }

    public Builder numberCollectionToBinaryCollection(
        final NumberCollectionToBinaryCollection numberCollectionToBinaryCollection) {
      this.numberCollectionToBinaryCollection = numberCollectionToBinaryCollection;
      return this;
    }

    public Builder numberCollectionToBoolean(
        final NumberCollectionToBoolean numberCollectionToBoolean) {
      this.numberCollectionToBoolean = numberCollectionToBoolean;
      return this;
    }

    public Builder numberCollectionToBooleanArray(
        final NumberCollectionToBooleanArray numberCollectionToBooleanArray) {
      this.numberCollectionToBooleanArray = numberCollectionToBooleanArray;
      return this;
    }

    public Builder numberCollectionToBooleanCollection(
        final NumberCollectionToBooleanCollection numberCollectionToBooleanCollection) {
      this.numberCollectionToBooleanCollection = numberCollectionToBooleanCollection;
      return this;
    }

    public Builder numberCollectionToBooleanPrim(
        final NumberCollectionToBooleanPrim numberCollectionToBooleanPrim) {
      this.numberCollectionToBooleanPrim = numberCollectionToBooleanPrim;
      return this;
    }

    public Builder numberCollectionToBooleanPrimArray(
        final NumberCollectionToBooleanPrimArray numberCollectionToBooleanPrimArray) {
      this.numberCollectionToBooleanPrimArray = numberCollectionToBooleanPrimArray;
      return this;
    }

    public Builder numberCollectionToByte(final NumberCollectionToByte numberCollectionToByte) {
      this.numberCollectionToByte = numberCollectionToByte;
      return this;
    }

    public Builder numberCollectionToByteArray(
        final NumberCollectionToByteArray numberCollectionToByteArray) {
      this.numberCollectionToByteArray = numberCollectionToByteArray;
      return this;
    }

    public Builder numberCollectionToByteCollection(
        final NumberCollectionToByteCollection numberCollectionToByteCollection) {
      this.numberCollectionToByteCollection = numberCollectionToByteCollection;
      return this;
    }

    public Builder numberCollectionToBytePrim(
        final NumberCollectionToBytePrim numberCollectionToBytePrim) {
      this.numberCollectionToBytePrim = numberCollectionToBytePrim;
      return this;
    }

    public Builder numberCollectionToBytePrimArray(
        final NumberCollectionToBytePrimArray numberCollectionToBytePrimArray) {
      this.numberCollectionToBytePrimArray = numberCollectionToBytePrimArray;
      return this;
    }

    public Builder numberCollectionToCharacter(
        final NumberCollectionToCharacter numberCollectionToCharacter) {
      this.numberCollectionToCharacter = numberCollectionToCharacter;
      return this;
    }

    public Builder numberCollectionToCharacterArray(
        final NumberCollectionToCharacterArray numberCollectionToCharacterArray) {
      this.numberCollectionToCharacterArray = numberCollectionToCharacterArray;
      return this;
    }

    public Builder numberCollectionToCharacterCollection(
        final NumberCollectionToCharacterCollection numberCollectionToCharacterCollection) {
      this.numberCollectionToCharacterCollection = numberCollectionToCharacterCollection;
      return this;
    }

    public Builder numberCollectionToCharacterPrim(
        final NumberCollectionToCharacterPrim numberCollectionToCharacterPrim) {
      this.numberCollectionToCharacterPrim = numberCollectionToCharacterPrim;
      return this;
    }

    public Builder numberCollectionToCharacterPrimArray(
        final NumberCollectionToCharacterPrimArray numberCollectionToCharacterPrimArray) {
      this.numberCollectionToCharacterPrimArray = numberCollectionToCharacterPrimArray;
      return this;
    }

    public Builder numberCollectionToDateBased(
        final NumberCollectionToDateBased numberCollectionToDateBased) {
      this.numberCollectionToDateBased = numberCollectionToDateBased;
      return this;
    }

    public Builder numberCollectionToDateBasedArray(
        final NumberCollectionToDateBasedArray numberCollectionToDateBasedArray) {
      this.numberCollectionToDateBasedArray = numberCollectionToDateBasedArray;
      return this;
    }

    public Builder numberCollectionToDateBasedCollection(
        final NumberCollectionToDateBasedCollection numberCollectionToDateBasedCollection) {
      this.numberCollectionToDateBasedCollection = numberCollectionToDateBasedCollection;
      return this;
    }

    public Builder numberCollectionToDoublePrim(
        final NumberCollectionToDoublePrim numberCollectionToDoublePrim) {
      this.numberCollectionToDoublePrim = numberCollectionToDoublePrim;
      return this;
    }

    public Builder numberCollectionToDoublePrimArray(
        final NumberCollectionToDoublePrimArray numberCollectionToDoublePrimArray) {
      this.numberCollectionToDoublePrimArray = numberCollectionToDoublePrimArray;
      return this;
    }

    public Builder numberCollectionToEnum(final NumberCollectionToEnum numberCollectionToEnum) {
      this.numberCollectionToEnum = numberCollectionToEnum;
      return this;
    }

    public Builder numberCollectionToEnumArray(
        final NumberCollectionToEnumArray numberCollectionToEnumArray) {
      this.numberCollectionToEnumArray = numberCollectionToEnumArray;
      return this;
    }

    public Builder numberCollectionToEnumCollection(
        final NumberCollectionToEnumCollection numberCollectionToEnumCollection) {
      this.numberCollectionToEnumCollection = numberCollectionToEnumCollection;
      return this;
    }

    public Builder numberCollectionToFloatPrim(
        final NumberCollectionToFloatPrim numberCollectionToFloatPrim) {
      this.numberCollectionToFloatPrim = numberCollectionToFloatPrim;
      return this;
    }

    public Builder numberCollectionToFloatPrimArray(
        final NumberCollectionToFloatPrimArray numberCollectionToFloatPrimArray) {
      this.numberCollectionToFloatPrimArray = numberCollectionToFloatPrimArray;
      return this;
    }

    public Builder numberCollectionToIntegerPrim(
        final NumberCollectionToIntegerPrim numberCollectionToIntegerPrim) {
      this.numberCollectionToIntegerPrim = numberCollectionToIntegerPrim;
      return this;
    }

    public Builder numberCollectionToIntegerPrimArray(
        final NumberCollectionToIntegerPrimArray numberCollectionToIntegerPrimArray) {
      this.numberCollectionToIntegerPrimArray = numberCollectionToIntegerPrimArray;
      return this;
    }

    public Builder numberCollectionToLongPrim(
        final NumberCollectionToLongPrim numberCollectionToLongPrim) {
      this.numberCollectionToLongPrim = numberCollectionToLongPrim;
      return this;
    }

    public Builder numberCollectionToLongPrimArray(
        final NumberCollectionToLongPrimArray numberCollectionToLongPrimArray) {
      this.numberCollectionToLongPrimArray = numberCollectionToLongPrimArray;
      return this;
    }

    public Builder numberCollectionToNumberBased(
        final NumberCollectionToNumberBased numberCollectionToNumberBased) {
      this.numberCollectionToNumberBased = numberCollectionToNumberBased;
      return this;
    }

    public Builder numberCollectionToNumberBasedArray(
        final NumberCollectionToNumberBasedArray numberCollectionToNumberBasedArray) {
      this.numberCollectionToNumberBasedArray = numberCollectionToNumberBasedArray;
      return this;
    }

    public Builder numberCollectionToNumberBasedCollection(
        final NumberCollectionToNumberBasedCollection numberCollectionToNumberBasedCollection) {
      this.numberCollectionToNumberBasedCollection = numberCollectionToNumberBasedCollection;
      return this;
    }

    public Builder numberCollectionToObjectId(
        final NumberCollectionToObjectId numberCollectionToObjectId) {
      this.numberCollectionToObjectId = numberCollectionToObjectId;
      return this;
    }

    public Builder numberCollectionToObjectIdArray(
        final NumberCollectionToObjectIdArray numberCollectionToObjectIdArray) {
      this.numberCollectionToObjectIdArray = numberCollectionToObjectIdArray;
      return this;
    }

    public Builder numberCollectionToObjectIdCollection(
        final NumberCollectionToObjectIdCollection numberCollectionToObjectIdCollection) {
      this.numberCollectionToObjectIdCollection = numberCollectionToObjectIdCollection;
      return this;
    }

    public Builder numberCollectionToShortPrim(
        final NumberCollectionToShortPrim numberCollectionToShortPrim) {
      this.numberCollectionToShortPrim = numberCollectionToShortPrim;
      return this;
    }

    public Builder numberCollectionToShortPrimArray(
        final NumberCollectionToShortPrimArray numberCollectionToShortPrimArray) {
      this.numberCollectionToShortPrimArray = numberCollectionToShortPrimArray;
      return this;
    }

    public Builder numberCollectionToStringBased(
        final NumberCollectionToStringBased numberCollectionToStringBased) {
      this.numberCollectionToStringBased = numberCollectionToStringBased;
      return this;
    }

    public Builder numberCollectionToStringBasedArray(
        final NumberCollectionToStringBasedArray numberCollectionToStringBasedArray) {
      this.numberCollectionToStringBasedArray = numberCollectionToStringBasedArray;
      return this;
    }

    public Builder numberCollectionToStringBasedCollection(
        final NumberCollectionToStringBasedCollection numberCollectionToStringBasedCollection) {
      this.numberCollectionToStringBasedCollection = numberCollectionToStringBasedCollection;
      return this;
    }

    public Builder numberToBinary(final NumberToBinary numberToBinary) {
      this.numberToBinary = numberToBinary;
      return this;
    }

    public Builder numberToBinaryArray(final NumberToBinaryArray numberToBinaryArray) {
      this.numberToBinaryArray = numberToBinaryArray;
      return this;
    }

    public Builder numberToBinaryCollection(
        final NumberToBinaryCollection numberToBinaryCollection) {
      this.numberToBinaryCollection = numberToBinaryCollection;
      return this;
    }

    public Builder numberToBoolean(final NumberToBoolean numberToBoolean) {
      this.numberToBoolean = numberToBoolean;
      return this;
    }

    public Builder numberToBooleanArray(final NumberToBooleanArray numberToBooleanArray) {
      this.numberToBooleanArray = numberToBooleanArray;
      return this;
    }

    public Builder numberToBooleanCollection(
        final NumberToBooleanCollection numberToBooleanCollection) {
      this.numberToBooleanCollection = numberToBooleanCollection;
      return this;
    }

    public Builder numberToBooleanPrim(final NumberToBooleanPrim numberToBooleanPrim) {
      this.numberToBooleanPrim = numberToBooleanPrim;
      return this;
    }

    public Builder numberToBooleanPrimArray(
        final NumberToBooleanPrimArray numberToBooleanPrimArray) {
      this.numberToBooleanPrimArray = numberToBooleanPrimArray;
      return this;
    }

    public Builder numberToByte(final NumberToByte numberToByte) {
      this.numberToByte = numberToByte;
      return this;
    }

    public Builder numberToByteArray(final NumberToByteArray numberToByteArray) {
      this.numberToByteArray = numberToByteArray;
      return this;
    }

    public Builder numberToByteCollection(final NumberToByteCollection numberToByteCollection) {
      this.numberToByteCollection = numberToByteCollection;
      return this;
    }

    public Builder numberToBytePrim(final NumberToBytePrim numberToBytePrim) {
      this.numberToBytePrim = numberToBytePrim;
      return this;
    }

    public Builder numberToBytePrimArray(final NumberToBytePrimArray numberToBytePrimArray) {
      this.numberToBytePrimArray = numberToBytePrimArray;
      return this;
    }

    public Builder numberToCharacter(final NumberToCharacter numberToCharacter) {
      this.numberToCharacter = numberToCharacter;
      return this;
    }

    public Builder numberToCharacterArray(final NumberToCharacterArray numberToCharacterArray) {
      this.numberToCharacterArray = numberToCharacterArray;
      return this;
    }

    public Builder numberToCharacterCollection(
        final NumberToCharacterCollection numberToCharacterCollection) {
      this.numberToCharacterCollection = numberToCharacterCollection;
      return this;
    }

    public Builder numberToCharacterPrim(final NumberToCharacterPrim numberToCharacterPrim) {
      this.numberToCharacterPrim = numberToCharacterPrim;
      return this;
    }

    public Builder numberToCharacterPrimArray(
        final NumberToCharacterPrimArray numberToCharacterPrimArray) {
      this.numberToCharacterPrimArray = numberToCharacterPrimArray;
      return this;
    }

    public Builder numberToDateBased(final NumberToDateBased numberToDateBased) {
      this.numberToDateBased = numberToDateBased;
      return this;
    }

    public Builder numberToDateBasedArray(final NumberToDateBasedArray numberToDateBasedArray) {
      this.numberToDateBasedArray = numberToDateBasedArray;
      return this;
    }

    public Builder numberToDateBasedCollection(
        final NumberToDateBasedCollection numberToDateBasedCollection) {
      this.numberToDateBasedCollection = numberToDateBasedCollection;
      return this;
    }

    public Builder numberToDoublePrim(final NumberToDoublePrim numberToDoublePrim) {
      this.numberToDoublePrim = numberToDoublePrim;
      return this;
    }

    public Builder numberToDoublePrimArray(final NumberToDoublePrimArray numberToDoublePrimArray) {
      this.numberToDoublePrimArray = numberToDoublePrimArray;
      return this;
    }

    public Builder numberToEnum(final NumberToEnum numberToEnum) {
      this.numberToEnum = numberToEnum;
      return this;
    }

    public Builder numberToEnumArray(final NumberToEnumArray numberToEnumArray) {
      this.numberToEnumArray = numberToEnumArray;
      return this;
    }

    public Builder numberToEnumCollection(final NumberToEnumCollection numberToEnumCollection) {
      this.numberToEnumCollection = numberToEnumCollection;
      return this;
    }

    public Builder numberToFloatPrim(final NumberToFloatPrim numberToFloatPrim) {
      this.numberToFloatPrim = numberToFloatPrim;
      return this;
    }

    public Builder numberToFloatPrimArray(final NumberToFloatPrimArray numberToFloatPrimArray) {
      this.numberToFloatPrimArray = numberToFloatPrimArray;
      return this;
    }

    public Builder numberToIntegerPrim(final NumberToIntegerPrim numberToIntegerPrim) {
      this.numberToIntegerPrim = numberToIntegerPrim;
      return this;
    }

    public Builder numberToIntegerPrimArray(
        final NumberToIntegerPrimArray numberToIntegerPrimArray) {
      this.numberToIntegerPrimArray = numberToIntegerPrimArray;
      return this;
    }

    public Builder numberToLongPrim(final NumberToLongPrim numberToLongPrim) {
      this.numberToLongPrim = numberToLongPrim;
      return this;
    }

    public Builder numberToLongPrimArray(final NumberToLongPrimArray numberToLongPrimArray) {
      this.numberToLongPrimArray = numberToLongPrimArray;
      return this;
    }

    public Builder numberToNumberBased(final NumberToNumberBased numberToNumberBased) {
      this.numberToNumberBased = numberToNumberBased;
      return this;
    }

    public Builder numberToNumberBasedArray(
        final NumberToNumberBasedArray numberToNumberBasedArray) {
      this.numberToNumberBasedArray = numberToNumberBasedArray;
      return this;
    }

    public Builder numberToNumberBasedCollection(
        final NumberToNumberBasedCollection numberToNumberBasedCollection) {
      this.numberToNumberBasedCollection = numberToNumberBasedCollection;
      return this;
    }

    public Builder numberToObjectId(final NumberToObjectId numberToObjectId) {
      this.numberToObjectId = numberToObjectId;
      return this;
    }

    public Builder numberToObjectIdArray(final NumberToObjectIdArray numberToObjectIdArray) {
      this.numberToObjectIdArray = numberToObjectIdArray;
      return this;
    }

    public Builder numberToObjectIdCollection(
        final NumberToObjectIdCollection numberToObjectIdCollection) {
      this.numberToObjectIdCollection = numberToObjectIdCollection;
      return this;
    }

    public Builder numberToShortPrim(final NumberToShortPrim numberToShortPrim) {
      this.numberToShortPrim = numberToShortPrim;
      return this;
    }

    public Builder numberToShortPrimArray(final NumberToShortPrimArray numberToShortPrimArray) {
      this.numberToShortPrimArray = numberToShortPrimArray;
      return this;
    }

    public Builder numberToStringBased(final NumberToStringBased numberToStringBased) {
      this.numberToStringBased = numberToStringBased;
      return this;
    }

    public Builder numberToStringBasedArray(
        final NumberToStringBasedArray numberToStringBasedArray) {
      this.numberToStringBasedArray = numberToStringBasedArray;
      return this;
    }

    public Builder numberToStringBasedCollection(
        final NumberToStringBasedCollection numberToStringBasedCollection) {
      this.numberToStringBasedCollection = numberToStringBasedCollection;
      return this;
    }

    public Builder objectIdCollectionToBinary(
        final ObjectIdCollectionToBinary objectIdCollectionToBinary) {
      this.objectIdCollectionToBinary = objectIdCollectionToBinary;
      return this;
    }

    public Builder objectIdCollectionToBinaryArray(
        final ObjectIdCollectionToBinaryArray objectIdCollectionToBinaryArray) {
      this.objectIdCollectionToBinaryArray = objectIdCollectionToBinaryArray;
      return this;
    }

    public Builder objectIdCollectionToBinaryCollection(
        final ObjectIdCollectionToBinaryCollection objectIdCollectionToBinaryCollection) {
      this.objectIdCollectionToBinaryCollection = objectIdCollectionToBinaryCollection;
      return this;
    }

    public Builder objectIdCollectionToBoolean(
        final ObjectIdCollectionToBoolean objectIdCollectionToBoolean) {
      this.objectIdCollectionToBoolean = objectIdCollectionToBoolean;
      return this;
    }

    public Builder objectIdCollectionToBooleanArray(
        final ObjectIdCollectionToBooleanArray objectIdCollectionToBooleanArray) {
      this.objectIdCollectionToBooleanArray = objectIdCollectionToBooleanArray;
      return this;
    }

    public Builder objectIdCollectionToBooleanCollection(
        final ObjectIdCollectionToBooleanCollection objectIdCollectionToBooleanCollection) {
      this.objectIdCollectionToBooleanCollection = objectIdCollectionToBooleanCollection;
      return this;
    }

    public Builder objectIdCollectionToBooleanPrim(
        final ObjectIdCollectionToBooleanPrim objectIdCollectionToBooleanPrim) {
      this.objectIdCollectionToBooleanPrim = objectIdCollectionToBooleanPrim;
      return this;
    }

    public Builder objectIdCollectionToBooleanPrimArray(
        final ObjectIdCollectionToBooleanPrimArray objectIdCollectionToBooleanPrimArray) {
      this.objectIdCollectionToBooleanPrimArray = objectIdCollectionToBooleanPrimArray;
      return this;
    }

    public Builder objectIdCollectionToByte(
        final ObjectIdCollectionToByte objectIdCollectionToByte) {
      this.objectIdCollectionToByte = objectIdCollectionToByte;
      return this;
    }

    public Builder objectIdCollectionToByteArray(
        final ObjectIdCollectionToByteArray objectIdCollectionToByteArray) {
      this.objectIdCollectionToByteArray = objectIdCollectionToByteArray;
      return this;
    }

    public Builder objectIdCollectionToByteCollection(
        final ObjectIdCollectionToByteCollection objectIdCollectionToByteCollection) {
      this.objectIdCollectionToByteCollection = objectIdCollectionToByteCollection;
      return this;
    }

    public Builder objectIdCollectionToBytePrim(
        final ObjectIdCollectionToBytePrim objectIdCollectionToBytePrim) {
      this.objectIdCollectionToBytePrim = objectIdCollectionToBytePrim;
      return this;
    }

    public Builder objectIdCollectionToBytePrimArray(
        final ObjectIdCollectionToBytePrimArray objectIdCollectionToBytePrimArray) {
      this.objectIdCollectionToBytePrimArray = objectIdCollectionToBytePrimArray;
      return this;
    }

    public Builder objectIdCollectionToCharacter(
        final ObjectIdCollectionToCharacter objectIdCollectionToCharacter) {
      this.objectIdCollectionToCharacter = objectIdCollectionToCharacter;
      return this;
    }

    public Builder objectIdCollectionToCharacterArray(
        final ObjectIdCollectionToCharacterArray objectIdCollectionToCharacterArray) {
      this.objectIdCollectionToCharacterArray = objectIdCollectionToCharacterArray;
      return this;
    }

    public Builder objectIdCollectionToCharacterCollection(
        final ObjectIdCollectionToCharacterCollection objectIdCollectionToCharacterCollection) {
      this.objectIdCollectionToCharacterCollection = objectIdCollectionToCharacterCollection;
      return this;
    }

    public Builder objectIdCollectionToCharacterPrim(
        final ObjectIdCollectionToCharacterPrim objectIdCollectionToCharacterPrim) {
      this.objectIdCollectionToCharacterPrim = objectIdCollectionToCharacterPrim;
      return this;
    }

    public Builder objectIdCollectionToCharacterPrimArray(
        final ObjectIdCollectionToCharacterPrimArray objectIdCollectionToCharacterPrimArray) {
      this.objectIdCollectionToCharacterPrimArray = objectIdCollectionToCharacterPrimArray;
      return this;
    }

    public Builder objectIdCollectionToDateBased(
        final ObjectIdCollectionToDateBased objectIdCollectionToDateBased) {
      this.objectIdCollectionToDateBased = objectIdCollectionToDateBased;
      return this;
    }

    public Builder objectIdCollectionToDateBasedArray(
        final ObjectIdCollectionToDateBasedArray objectIdCollectionToDateBasedArray) {
      this.objectIdCollectionToDateBasedArray = objectIdCollectionToDateBasedArray;
      return this;
    }

    public Builder objectIdCollectionToDateBasedCollection(
        final ObjectIdCollectionToDateBasedCollection objectIdCollectionToDateBasedCollection) {
      this.objectIdCollectionToDateBasedCollection = objectIdCollectionToDateBasedCollection;
      return this;
    }

    public Builder objectIdCollectionToDoublePrim(
        final ObjectIdCollectionToDoublePrim objectIdCollectionToDoublePrim) {
      this.objectIdCollectionToDoublePrim = objectIdCollectionToDoublePrim;
      return this;
    }

    public Builder objectIdCollectionToDoublePrimArray(
        final ObjectIdCollectionToDoublePrimArray objectIdCollectionToDoublePrimArray) {
      this.objectIdCollectionToDoublePrimArray = objectIdCollectionToDoublePrimArray;
      return this;
    }

    public Builder objectIdCollectionToEnum(
        final ObjectIdCollectionToEnum objectIdCollectionToEnum) {
      this.objectIdCollectionToEnum = objectIdCollectionToEnum;
      return this;
    }

    public Builder objectIdCollectionToEnumArray(
        final ObjectIdCollectionToEnumArray objectIdCollectionToEnumArray) {
      this.objectIdCollectionToEnumArray = objectIdCollectionToEnumArray;
      return this;
    }

    public Builder objectIdCollectionToEnumCollection(
        final ObjectIdCollectionToEnumCollection objectIdCollectionToEnumCollection) {
      this.objectIdCollectionToEnumCollection = objectIdCollectionToEnumCollection;
      return this;
    }

    public Builder objectIdCollectionToFloatPrim(
        final ObjectIdCollectionToFloatPrim objectIdCollectionToFloatPrim) {
      this.objectIdCollectionToFloatPrim = objectIdCollectionToFloatPrim;
      return this;
    }

    public Builder objectIdCollectionToFloatPrimArray(
        final ObjectIdCollectionToFloatPrimArray objectIdCollectionToFloatPrimArray) {
      this.objectIdCollectionToFloatPrimArray = objectIdCollectionToFloatPrimArray;
      return this;
    }

    public Builder objectIdCollectionToIntegerPrim(
        final ObjectIdCollectionToIntegerPrim objectIdCollectionToIntegerPrim) {
      this.objectIdCollectionToIntegerPrim = objectIdCollectionToIntegerPrim;
      return this;
    }

    public Builder objectIdCollectionToIntegerPrimArray(
        final ObjectIdCollectionToIntegerPrimArray objectIdCollectionToIntegerPrimArray) {
      this.objectIdCollectionToIntegerPrimArray = objectIdCollectionToIntegerPrimArray;
      return this;
    }

    public Builder objectIdCollectionToLongPrim(
        final ObjectIdCollectionToLongPrim objectIdCollectionToLongPrim) {
      this.objectIdCollectionToLongPrim = objectIdCollectionToLongPrim;
      return this;
    }

    public Builder objectIdCollectionToLongPrimArray(
        final ObjectIdCollectionToLongPrimArray objectIdCollectionToLongPrimArray) {
      this.objectIdCollectionToLongPrimArray = objectIdCollectionToLongPrimArray;
      return this;
    }

    public Builder objectIdCollectionToNumberBased(
        final ObjectIdCollectionToNumberBased objectIdCollectionToNumberBased) {
      this.objectIdCollectionToNumberBased = objectIdCollectionToNumberBased;
      return this;
    }

    public Builder objectIdCollectionToNumberBasedArray(
        final ObjectIdCollectionToNumberBasedArray objectIdCollectionToNumberBasedArray) {
      this.objectIdCollectionToNumberBasedArray = objectIdCollectionToNumberBasedArray;
      return this;
    }

    public Builder objectIdCollectionToNumberBasedCollection(
        final ObjectIdCollectionToNumberBasedCollection objectIdCollectionToNumberBasedCollection) {
      this.objectIdCollectionToNumberBasedCollection = objectIdCollectionToNumberBasedCollection;
      return this;
    }

    public Builder objectIdCollectionToObjectId(
        final ObjectIdCollectionToObjectId objectIdCollectionToObjectId) {
      this.objectIdCollectionToObjectId = objectIdCollectionToObjectId;
      return this;
    }

    public Builder objectIdCollectionToObjectIdArray(
        final ObjectIdCollectionToObjectIdArray objectIdCollectionToObjectIdArray) {
      this.objectIdCollectionToObjectIdArray = objectIdCollectionToObjectIdArray;
      return this;
    }

    public Builder objectIdCollectionToObjectIdCollection(
        final ObjectIdCollectionToObjectIdCollection objectIdCollectionToObjectIdCollection) {
      this.objectIdCollectionToObjectIdCollection = objectIdCollectionToObjectIdCollection;
      return this;
    }

    public Builder objectIdCollectionToShortPrim(
        final ObjectIdCollectionToShortPrim objectIdCollectionToShortPrim) {
      this.objectIdCollectionToShortPrim = objectIdCollectionToShortPrim;
      return this;
    }

    public Builder objectIdCollectionToShortPrimArray(
        final ObjectIdCollectionToShortPrimArray objectIdCollectionToShortPrimArray) {
      this.objectIdCollectionToShortPrimArray = objectIdCollectionToShortPrimArray;
      return this;
    }

    public Builder objectIdCollectionToStringBased(
        final ObjectIdCollectionToStringBased objectIdCollectionToStringBased) {
      this.objectIdCollectionToStringBased = objectIdCollectionToStringBased;
      return this;
    }

    public Builder objectIdCollectionToStringBasedArray(
        final ObjectIdCollectionToStringBasedArray objectIdCollectionToStringBasedArray) {
      this.objectIdCollectionToStringBasedArray = objectIdCollectionToStringBasedArray;
      return this;
    }

    public Builder objectIdCollectionToStringBasedCollection(
        final ObjectIdCollectionToStringBasedCollection objectIdCollectionToStringBasedCollection) {
      this.objectIdCollectionToStringBasedCollection = objectIdCollectionToStringBasedCollection;
      return this;
    }

    public Builder objectIdToBinary(final ObjectIdToBinary objectIdToBinary) {
      this.objectIdToBinary = objectIdToBinary;
      return this;
    }

    public Builder objectIdToBinaryArray(final ObjectIdToBinaryArray objectIdToBinaryArray) {
      this.objectIdToBinaryArray = objectIdToBinaryArray;
      return this;
    }

    public Builder objectIdToBinaryCollection(
        final ObjectIdToBinaryCollection objectIdToBinaryCollection) {
      this.objectIdToBinaryCollection = objectIdToBinaryCollection;
      return this;
    }

    public Builder objectIdToBoolean(final ObjectIdToBoolean objectIdToBoolean) {
      this.objectIdToBoolean = objectIdToBoolean;
      return this;
    }

    public Builder objectIdToBooleanArray(final ObjectIdToBooleanArray objectIdToBooleanArray) {
      this.objectIdToBooleanArray = objectIdToBooleanArray;
      return this;
    }

    public Builder objectIdToBooleanCollection(
        final ObjectIdToBooleanCollection objectIdToBooleanCollection) {
      this.objectIdToBooleanCollection = objectIdToBooleanCollection;
      return this;
    }

    public Builder objectIdToBooleanPrim(final ObjectIdToBooleanPrim objectIdToBooleanPrim) {
      this.objectIdToBooleanPrim = objectIdToBooleanPrim;
      return this;
    }

    public Builder objectIdToBooleanPrimArray(
        final ObjectIdToBooleanPrimArray objectIdToBooleanPrimArray) {
      this.objectIdToBooleanPrimArray = objectIdToBooleanPrimArray;
      return this;
    }

    public Builder objectIdToByte(final ObjectIdToByte objectIdToByte) {
      this.objectIdToByte = objectIdToByte;
      return this;
    }

    public Builder objectIdToByteArray(final ObjectIdToByteArray objectIdToByteArray) {
      this.objectIdToByteArray = objectIdToByteArray;
      return this;
    }

    public Builder objectIdToByteCollection(
        final ObjectIdToByteCollection objectIdToByteCollection) {
      this.objectIdToByteCollection = objectIdToByteCollection;
      return this;
    }

    public Builder objectIdToBytePrim(final ObjectIdToBytePrim objectIdToBytePrim) {
      this.objectIdToBytePrim = objectIdToBytePrim;
      return this;
    }

    public Builder objectIdToBytePrimArray(final ObjectIdToBytePrimArray objectIdToBytePrimArray) {
      this.objectIdToBytePrimArray = objectIdToBytePrimArray;
      return this;
    }

    public Builder objectIdToCharacter(final ObjectIdToCharacter objectIdToCharacter) {
      this.objectIdToCharacter = objectIdToCharacter;
      return this;
    }

    public Builder objectIdToCharacterArray(
        final ObjectIdToCharacterArray objectIdToCharacterArray) {
      this.objectIdToCharacterArray = objectIdToCharacterArray;
      return this;
    }

    public Builder objectIdToCharacterCollection(
        final ObjectIdToCharacterCollection objectIdToCharacterCollection) {
      this.objectIdToCharacterCollection = objectIdToCharacterCollection;
      return this;
    }

    public Builder objectIdToCharacterPrim(final ObjectIdToCharacterPrim objectIdToCharacterPrim) {
      this.objectIdToCharacterPrim = objectIdToCharacterPrim;
      return this;
    }

    public Builder objectIdToCharacterPrimArray(
        final ObjectIdToCharacterPrimArray objectIdToCharacterPrimArray) {
      this.objectIdToCharacterPrimArray = objectIdToCharacterPrimArray;
      return this;
    }

    public Builder objectIdToDateBased(final ObjectIdToDateBased objectIdToDateBased) {
      this.objectIdToDateBased = objectIdToDateBased;
      return this;
    }

    public Builder objectIdToDateBasedArray(
        final ObjectIdToDateBasedArray objectIdToDateBasedArray) {
      this.objectIdToDateBasedArray = objectIdToDateBasedArray;
      return this;
    }

    public Builder objectIdToDateBasedCollection(
        final ObjectIdToDateBasedCollection objectIdToDateBasedCollection) {
      this.objectIdToDateBasedCollection = objectIdToDateBasedCollection;
      return this;
    }

    public Builder objectIdToDoublePrim(final ObjectIdToDoublePrim objectIdToDoublePrim) {
      this.objectIdToDoublePrim = objectIdToDoublePrim;
      return this;
    }

    public Builder objectIdToDoublePrimArray(
        final ObjectIdToDoublePrimArray objectIdToDoublePrimArray) {
      this.objectIdToDoublePrimArray = objectIdToDoublePrimArray;
      return this;
    }

    public Builder objectIdToEnum(final ObjectIdToEnum objectIdToEnum) {
      this.objectIdToEnum = objectIdToEnum;
      return this;
    }

    public Builder objectIdToEnumArray(final ObjectIdToEnumArray objectIdToEnumArray) {
      this.objectIdToEnumArray = objectIdToEnumArray;
      return this;
    }

    public Builder objectIdToEnumCollection(
        final ObjectIdToEnumCollection objectIdToEnumCollection) {
      this.objectIdToEnumCollection = objectIdToEnumCollection;
      return this;
    }

    public Builder objectIdToFloatPrim(final ObjectIdToFloatPrim objectIdToFloatPrim) {
      this.objectIdToFloatPrim = objectIdToFloatPrim;
      return this;
    }

    public Builder objectIdToFloatPrimArray(
        final ObjectIdToFloatPrimArray objectIdToFloatPrimArray) {
      this.objectIdToFloatPrimArray = objectIdToFloatPrimArray;
      return this;
    }

    public Builder objectIdToIntegerPrim(final ObjectIdToIntegerPrim objectIdToIntegerPrim) {
      this.objectIdToIntegerPrim = objectIdToIntegerPrim;
      return this;
    }

    public Builder objectIdToIntegerPrimArray(
        final ObjectIdToIntegerPrimArray objectIdToIntegerPrimArray) {
      this.objectIdToIntegerPrimArray = objectIdToIntegerPrimArray;
      return this;
    }

    public Builder objectIdToLongPrim(final ObjectIdToLongPrim objectIdToLongPrim) {
      this.objectIdToLongPrim = objectIdToLongPrim;
      return this;
    }

    public Builder objectIdToLongPrimArray(final ObjectIdToLongPrimArray objectIdToLongPrimArray) {
      this.objectIdToLongPrimArray = objectIdToLongPrimArray;
      return this;
    }

    public Builder objectIdToNumberBased(final ObjectIdToNumberBased objectIdToNumberBased) {
      this.objectIdToNumberBased = objectIdToNumberBased;
      return this;
    }

    public Builder objectIdToNumberBasedArray(
        final ObjectIdToNumberBasedArray objectIdToNumberBasedArray) {
      this.objectIdToNumberBasedArray = objectIdToNumberBasedArray;
      return this;
    }

    public Builder objectIdToNumberBasedCollection(
        final ObjectIdToNumberBasedCollection objectIdToNumberBasedCollection) {
      this.objectIdToNumberBasedCollection = objectIdToNumberBasedCollection;
      return this;
    }

    public Builder objectIdToObjectId(final ObjectIdToObjectId objectIdToObjectId) {
      this.objectIdToObjectId = objectIdToObjectId;
      return this;
    }

    public Builder objectIdToObjectIdArray(final ObjectIdToObjectIdArray objectIdToObjectIdArray) {
      this.objectIdToObjectIdArray = objectIdToObjectIdArray;
      return this;
    }

    public Builder objectIdToObjectIdCollection(
        final ObjectIdToObjectIdCollection objectIdToObjectIdCollection) {
      this.objectIdToObjectIdCollection = objectIdToObjectIdCollection;
      return this;
    }

    public Builder objectIdToShortPrim(final ObjectIdToShortPrim objectIdToShortPrim) {
      this.objectIdToShortPrim = objectIdToShortPrim;
      return this;
    }

    public Builder objectIdToShortPrimArray(
        final ObjectIdToShortPrimArray objectIdToShortPrimArray) {
      this.objectIdToShortPrimArray = objectIdToShortPrimArray;
      return this;
    }

    public Builder objectIdToStringBased(final ObjectIdToStringBased objectIdToStringBased) {
      this.objectIdToStringBased = objectIdToStringBased;
      return this;
    }

    public Builder objectIdToStringBasedArray(
        final ObjectIdToStringBasedArray objectIdToStringBasedArray) {
      this.objectIdToStringBasedArray = objectIdToStringBasedArray;
      return this;
    }

    public Builder objectIdToStringBasedCollection(
        final ObjectIdToStringBasedCollection objectIdToStringBasedCollection) {
      this.objectIdToStringBasedCollection = objectIdToStringBasedCollection;
      return this;
    }

    public Builder stringCollectionToBinary(
        final StringCollectionToBinary stringCollectionToBinary) {
      this.stringCollectionToBinary = stringCollectionToBinary;
      return this;
    }

    public Builder stringCollectionToBinaryArray(
        final StringCollectionToBinaryArray stringCollectionToBinaryArray) {
      this.stringCollectionToBinaryArray = stringCollectionToBinaryArray;
      return this;
    }

    public Builder stringCollectionToBinaryCollection(
        final StringCollectionToBinaryCollection stringCollectionToBinaryCollection) {
      this.stringCollectionToBinaryCollection = stringCollectionToBinaryCollection;
      return this;
    }

    public Builder stringCollectionToBoolean(
        final StringCollectionToBoolean stringCollectionToBoolean) {
      this.stringCollectionToBoolean = stringCollectionToBoolean;
      return this;
    }

    public Builder stringCollectionToBooleanArray(
        final StringCollectionToBooleanArray stringCollectionToBooleanArray) {
      this.stringCollectionToBooleanArray = stringCollectionToBooleanArray;
      return this;
    }

    public Builder stringCollectionToBooleanCollection(
        final StringCollectionToBooleanCollection stringCollectionToBooleanCollection) {
      this.stringCollectionToBooleanCollection = stringCollectionToBooleanCollection;
      return this;
    }

    public Builder stringCollectionToBooleanPrim(
        final StringCollectionToBooleanPrim stringCollectionToBooleanPrim) {
      this.stringCollectionToBooleanPrim = stringCollectionToBooleanPrim;
      return this;
    }

    public Builder stringCollectionToBooleanPrimArray(
        final StringCollectionToBooleanPrimArray stringCollectionToBooleanPrimArray) {
      this.stringCollectionToBooleanPrimArray = stringCollectionToBooleanPrimArray;
      return this;
    }

    public Builder stringCollectionToByte(final StringCollectionToByte stringCollectionToByte) {
      this.stringCollectionToByte = stringCollectionToByte;
      return this;
    }

    public Builder stringCollectionToByteArray(
        final StringCollectionToByteArray stringCollectionToByteArray) {
      this.stringCollectionToByteArray = stringCollectionToByteArray;
      return this;
    }

    public Builder stringCollectionToByteCollection(
        final StringCollectionToByteCollection stringCollectionToByteCollection) {
      this.stringCollectionToByteCollection = stringCollectionToByteCollection;
      return this;
    }

    public Builder stringCollectionToBytePrim(
        final StringCollectionToBytePrim stringCollectionToBytePrim) {
      this.stringCollectionToBytePrim = stringCollectionToBytePrim;
      return this;
    }

    public Builder stringCollectionToBytePrimArray(
        final StringCollectionToBytePrimArray stringCollectionToBytePrimArray) {
      this.stringCollectionToBytePrimArray = stringCollectionToBytePrimArray;
      return this;
    }

    public Builder stringCollectionToCharacter(
        final StringCollectionToCharacter stringCollectionToCharacter) {
      this.stringCollectionToCharacter = stringCollectionToCharacter;
      return this;
    }

    public Builder stringCollectionToCharacterArray(
        final StringCollectionToCharacterArray stringCollectionToCharacterArray) {
      this.stringCollectionToCharacterArray = stringCollectionToCharacterArray;
      return this;
    }

    public Builder stringCollectionToCharacterCollection(
        final StringCollectionToCharacterCollection stringCollectionToCharacterCollection) {
      this.stringCollectionToCharacterCollection = stringCollectionToCharacterCollection;
      return this;
    }

    public Builder stringCollectionToCharacterPrim(
        final StringCollectionToCharacterPrim stringCollectionToCharacterPrim) {
      this.stringCollectionToCharacterPrim = stringCollectionToCharacterPrim;
      return this;
    }

    public Builder stringCollectionToCharacterPrimArray(
        final StringCollectionToCharacterPrimArray stringCollectionToCharacterPrimArray) {
      this.stringCollectionToCharacterPrimArray = stringCollectionToCharacterPrimArray;
      return this;
    }

    public Builder stringCollectionToDateBased(
        final StringCollectionToDateBased stringCollectionToDateBased) {
      this.stringCollectionToDateBased = stringCollectionToDateBased;
      return this;
    }

    public Builder stringCollectionToDateBasedArray(
        final StringCollectionToDateBasedArray stringCollectionToDateBasedArray) {
      this.stringCollectionToDateBasedArray = stringCollectionToDateBasedArray;
      return this;
    }

    public Builder stringCollectionToDateBasedCollection(
        final StringCollectionToDateBasedCollection stringCollectionToDateBasedCollection) {
      this.stringCollectionToDateBasedCollection = stringCollectionToDateBasedCollection;
      return this;
    }

    public Builder stringCollectionToDoublePrim(
        final StringCollectionToDoublePrim stringCollectionToDoublePrim) {
      this.stringCollectionToDoublePrim = stringCollectionToDoublePrim;
      return this;
    }

    public Builder stringCollectionToDoublePrimArray(
        final StringCollectionToDoublePrimArray stringCollectionToDoublePrimArray) {
      this.stringCollectionToDoublePrimArray = stringCollectionToDoublePrimArray;
      return this;
    }

    public Builder stringCollectionToEnum(final StringCollectionToEnum stringCollectionToEnum) {
      this.stringCollectionToEnum = stringCollectionToEnum;
      return this;
    }

    public Builder stringCollectionToEnumArray(
        final StringCollectionToEnumArray stringCollectionToEnumArray) {
      this.stringCollectionToEnumArray = stringCollectionToEnumArray;
      return this;
    }

    public Builder stringCollectionToEnumCollection(
        final StringCollectionToEnumCollection stringCollectionToEnumCollection) {
      this.stringCollectionToEnumCollection = stringCollectionToEnumCollection;
      return this;
    }

    public Builder stringCollectionToFloatPrim(
        final StringCollectionToFloatPrim stringCollectionToFloatPrim) {
      this.stringCollectionToFloatPrim = stringCollectionToFloatPrim;
      return this;
    }

    public Builder stringCollectionToFloatPrimArray(
        final StringCollectionToFloatPrimArray stringCollectionToFloatPrimArray) {
      this.stringCollectionToFloatPrimArray = stringCollectionToFloatPrimArray;
      return this;
    }

    public Builder stringCollectionToIntegerPrim(
        final StringCollectionToIntegerPrim stringCollectionToIntegerPrim) {
      this.stringCollectionToIntegerPrim = stringCollectionToIntegerPrim;
      return this;
    }

    public Builder stringCollectionToIntegerPrimArray(
        final StringCollectionToIntegerPrimArray stringCollectionToIntegerPrimArray) {
      this.stringCollectionToIntegerPrimArray = stringCollectionToIntegerPrimArray;
      return this;
    }

    public Builder stringCollectionToLongPrim(
        final StringCollectionToLongPrim stringCollectionToLongPrim) {
      this.stringCollectionToLongPrim = stringCollectionToLongPrim;
      return this;
    }

    public Builder stringCollectionToLongPrimArray(
        final StringCollectionToLongPrimArray stringCollectionToLongPrimArray) {
      this.stringCollectionToLongPrimArray = stringCollectionToLongPrimArray;
      return this;
    }

    public Builder stringCollectionToNumberBased(
        final StringCollectionToNumberBased stringCollectionToNumberBased) {
      this.stringCollectionToNumberBased = stringCollectionToNumberBased;
      return this;
    }

    public Builder stringCollectionToNumberBasedArray(
        final StringCollectionToNumberBasedArray stringCollectionToNumberBasedArray) {
      this.stringCollectionToNumberBasedArray = stringCollectionToNumberBasedArray;
      return this;
    }

    public Builder stringCollectionToNumberBasedCollection(
        final StringCollectionToNumberBasedCollection stringCollectionToNumberBasedCollection) {
      this.stringCollectionToNumberBasedCollection = stringCollectionToNumberBasedCollection;
      return this;
    }

    public Builder stringCollectionToObjectId(
        final StringCollectionToObjectId stringCollectionToObjectId) {
      this.stringCollectionToObjectId = stringCollectionToObjectId;
      return this;
    }

    public Builder stringCollectionToObjectIdArray(
        final StringCollectionToObjectIdArray stringCollectionToObjectIdArray) {
      this.stringCollectionToObjectIdArray = stringCollectionToObjectIdArray;
      return this;
    }

    public Builder stringCollectionToObjectIdCollection(
        final StringCollectionToObjectIdCollection stringCollectionToObjectIdCollection) {
      this.stringCollectionToObjectIdCollection = stringCollectionToObjectIdCollection;
      return this;
    }

    public Builder stringCollectionToShortPrim(
        final StringCollectionToShortPrim stringCollectionToShortPrim) {
      this.stringCollectionToShortPrim = stringCollectionToShortPrim;
      return this;
    }

    public Builder stringCollectionToShortPrimArray(
        final StringCollectionToShortPrimArray stringCollectionToShortPrimArray) {
      this.stringCollectionToShortPrimArray = stringCollectionToShortPrimArray;
      return this;
    }

    public Builder stringCollectionToStringBased(
        final StringCollectionToStringBased stringCollectionToStringBased) {
      this.stringCollectionToStringBased = stringCollectionToStringBased;
      return this;
    }

    public Builder stringCollectionToStringBasedArray(
        final StringCollectionToStringBasedArray stringCollectionToStringBasedArray) {
      this.stringCollectionToStringBasedArray = stringCollectionToStringBasedArray;
      return this;
    }

    public Builder stringCollectionToStringBasedCollection(
        final StringCollectionToStringBasedCollection stringCollectionToStringBasedCollection) {
      this.stringCollectionToStringBasedCollection = stringCollectionToStringBasedCollection;
      return this;
    }

    public Builder stringToBinary(final StringToBinary stringToBinary) {
      this.stringToBinary = stringToBinary;
      return this;
    }

    public Builder stringToBinaryArray(final StringToBinaryArray stringToBinaryArray) {
      this.stringToBinaryArray = stringToBinaryArray;
      return this;
    }

    public Builder stringToBinaryCollection(
        final StringToBinaryCollection stringToBinaryCollection) {
      this.stringToBinaryCollection = stringToBinaryCollection;
      return this;
    }

    public Builder stringToBoolean(final StringToBoolean stringToBoolean) {
      this.stringToBoolean = stringToBoolean;
      return this;
    }

    public Builder stringToBooleanArray(final StringToBooleanArray stringToBooleanArray) {
      this.stringToBooleanArray = stringToBooleanArray;
      return this;
    }

    public Builder stringToBooleanCollection(
        final StringToBooleanCollection stringToBooleanCollection) {
      this.stringToBooleanCollection = stringToBooleanCollection;
      return this;
    }

    public Builder stringToBooleanPrim(final StringToBooleanPrim stringToBooleanPrim) {
      this.stringToBooleanPrim = stringToBooleanPrim;
      return this;
    }

    public Builder stringToBooleanPrimArray(
        final StringToBooleanPrimArray stringToBooleanPrimArray) {
      this.stringToBooleanPrimArray = stringToBooleanPrimArray;
      return this;
    }

    public Builder stringToByte(final StringToByte stringToByte) {
      this.stringToByte = stringToByte;
      return this;
    }

    public Builder stringToByteArray(final StringToByteArray stringToByteArray) {
      this.stringToByteArray = stringToByteArray;
      return this;
    }

    public Builder stringToByteCollection(final StringToByteCollection stringToByteCollection) {
      this.stringToByteCollection = stringToByteCollection;
      return this;
    }

    public Builder stringToBytePrim(final StringToBytePrim stringToBytePrim) {
      this.stringToBytePrim = stringToBytePrim;
      return this;
    }

    public Builder stringToBytePrimArray(final StringToBytePrimArray stringToBytePrimArray) {
      this.stringToBytePrimArray = stringToBytePrimArray;
      return this;
    }

    public Builder stringToCharacter(final StringToCharacter stringToCharacter) {
      this.stringToCharacter = stringToCharacter;
      return this;
    }

    public Builder stringToCharacterArray(final StringToCharacterArray stringToCharacterArray) {
      this.stringToCharacterArray = stringToCharacterArray;
      return this;
    }

    public Builder stringToCharacterCollection(
        final StringToCharacterCollection stringToCharacterCollection) {
      this.stringToCharacterCollection = stringToCharacterCollection;
      return this;
    }

    public Builder stringToCharacterPrim(final StringToCharacterPrim stringToCharacterPrim) {
      this.stringToCharacterPrim = stringToCharacterPrim;
      return this;
    }

    public Builder stringToCharacterPrimArray(
        final StringToCharacterPrimArray stringToCharacterPrimArray) {
      this.stringToCharacterPrimArray = stringToCharacterPrimArray;
      return this;
    }

    public Builder stringToDateBased(final StringToDateBased stringToDateBased) {
      this.stringToDateBased = stringToDateBased;
      return this;
    }

    public Builder stringToDateBasedArray(final StringToDateBasedArray stringToDateBasedArray) {
      this.stringToDateBasedArray = stringToDateBasedArray;
      return this;
    }

    public Builder stringToDateBasedCollection(
        final StringToDateBasedCollection stringToDateBasedCollection) {
      this.stringToDateBasedCollection = stringToDateBasedCollection;
      return this;
    }

    public Builder stringToDoublePrim(final StringToDoublePrim stringToDoublePrim) {
      this.stringToDoublePrim = stringToDoublePrim;
      return this;
    }

    public Builder stringToDoublePrimArray(final StringToDoublePrimArray stringToDoublePrimArray) {
      this.stringToDoublePrimArray = stringToDoublePrimArray;
      return this;
    }

    public Builder stringToEnum(final StringToEnum stringToEnum) {
      this.stringToEnum = stringToEnum;
      return this;
    }

    public Builder stringToEnumArray(final StringToEnumArray stringToEnumArray) {
      this.stringToEnumArray = stringToEnumArray;
      return this;
    }

    public Builder stringToEnumCollection(final StringToEnumCollection stringToEnumCollection) {
      this.stringToEnumCollection = stringToEnumCollection;
      return this;
    }

    public Builder stringToFloatPrim(final StringToFloatPrim stringToFloatPrim) {
      this.stringToFloatPrim = stringToFloatPrim;
      return this;
    }

    public Builder stringToFloatPrimArray(final StringToFloatPrimArray stringToFloatPrimArray) {
      this.stringToFloatPrimArray = stringToFloatPrimArray;
      return this;
    }

    public Builder stringToIntegerPrim(final StringToIntegerPrim stringToIntegerPrim) {
      this.stringToIntegerPrim = stringToIntegerPrim;
      return this;
    }

    public Builder stringToIntegerPrimArray(
        final StringToIntegerPrimArray stringToIntegerPrimArray) {
      this.stringToIntegerPrimArray = stringToIntegerPrimArray;
      return this;
    }

    public Builder stringToLongPrim(final StringToLongPrim stringToLongPrim) {
      this.stringToLongPrim = stringToLongPrim;
      return this;
    }

    public Builder stringToLongPrimArray(final StringToLongPrimArray stringToLongPrimArray) {
      this.stringToLongPrimArray = stringToLongPrimArray;
      return this;
    }

    public Builder stringToNumberBased(final StringToNumberBased stringToNumberBased) {
      this.stringToNumberBased = stringToNumberBased;
      return this;
    }

    public Builder stringToNumberBasedArray(
        final StringToNumberBasedArray stringToNumberBasedArray) {
      this.stringToNumberBasedArray = stringToNumberBasedArray;
      return this;
    }

    public Builder stringToNumberBasedCollection(
        final StringToNumberBasedCollection stringToNumberBasedCollection) {
      this.stringToNumberBasedCollection = stringToNumberBasedCollection;
      return this;
    }

    public Builder stringToObjectId(final StringToObjectId stringToObjectId) {
      this.stringToObjectId = stringToObjectId;
      return this;
    }

    public Builder stringToObjectIdArray(final StringToObjectIdArray stringToObjectIdArray) {
      this.stringToObjectIdArray = stringToObjectIdArray;
      return this;
    }

    public Builder stringToObjectIdCollection(
        final StringToObjectIdCollection stringToObjectIdCollection) {
      this.stringToObjectIdCollection = stringToObjectIdCollection;
      return this;
    }

    public Builder stringToShortPrim(final StringToShortPrim stringToShortPrim) {
      this.stringToShortPrim = stringToShortPrim;
      return this;
    }

    public Builder stringToShortPrimArray(final StringToShortPrimArray stringToShortPrimArray) {
      this.stringToShortPrimArray = stringToShortPrimArray;
      return this;
    }

    public Builder stringToStringBased(final StringToStringBased stringToStringBased) {
      this.stringToStringBased = stringToStringBased;
      return this;
    }

    public Builder stringToStringBasedArray(
        final StringToStringBasedArray stringToStringBasedArray) {
      this.stringToStringBasedArray = stringToStringBasedArray;
      return this;
    }

    public Builder stringToStringBasedCollection(
        final StringToStringBasedCollection stringToStringBasedCollection) {
      this.stringToStringBasedCollection = stringToStringBasedCollection;
      return this;
    }
  }

  @FunctionalInterface
  public interface DateCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Date> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Date> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface DateCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src);
  }

  @FunctionalInterface
  public interface DateCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Date> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface DateToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface DateToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface DateToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Date src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface DateToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src);
  }

  @FunctionalInterface
  public interface DateToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src);
  }

  @FunctionalInterface
  public interface DateToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Date src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface DateToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface DateToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Date src, Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface DateToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src);
  }

  @FunctionalInterface
  public interface DateToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Date src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<Number> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<Number> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface NumberCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src);
  }

  @FunctionalInterface
  public interface NumberCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<Number> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface NumberToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface NumberToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Number src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface NumberToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Number src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface NumberToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src);
  }

  @FunctionalInterface
  public interface NumberToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Number src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface NumberToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface NumberToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Number src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface NumberToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src);
  }

  @FunctionalInterface
  public interface NumberToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Number src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<ObjectId> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<ObjectId> src);
  }

  @FunctionalInterface
  public interface ObjectIdCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type,
        Collection<ObjectId> src, Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, ObjectId src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, ObjectId src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, ObjectId src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, ObjectId src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface ObjectIdToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src);
  }

  @FunctionalInterface
  public interface ObjectIdToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, ObjectId src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType,
        Collection<String> src, Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, Collection<String> src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface StringCollectionToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src);
  }

  @FunctionalInterface
  public interface StringCollectionToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, Collection<String> src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToBinary {
    Binary convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBinaryArray {
    Binary[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBinaryCollection {
    Collection<Binary> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Binary> dst);
  }

  @FunctionalInterface
  public interface StringToBoolean {
    Boolean convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanArray {
    Boolean[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanCollection {
    Collection<Boolean> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Boolean> dst);
  }

  @FunctionalInterface
  public interface StringToBooleanPrim {
    boolean convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBooleanPrimArray {
    boolean[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByte {
    Byte convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByteArray {
    Byte[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToByteCollection {
    Collection<Byte> convert(JaxBsonFieldContext fieldCtx, String src, Collection<Byte> dst);
  }

  @FunctionalInterface
  public interface StringToBytePrim {
    byte convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToBytePrimArray {
    byte[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacter {
    Character convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterArray {
    Character[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterCollection {
    Collection<Character> convert(JaxBsonFieldContext fieldCtx, String src,
        Collection<Character> dst);
  }

  @FunctionalInterface
  public interface StringToCharacterPrim {
    char convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToCharacterPrimArray {
    char[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToDateBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToDateBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToDateBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToDoublePrim {
    double convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToDoublePrimArray {
    double[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToEnum {
    Enum<?> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src);
  }

  @FunctionalInterface
  public interface StringToEnumArray {
    Enum<?>[] convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src);
  }

  @FunctionalInterface
  public interface StringToEnumCollection {
    Collection<Enum<?>> convert(JaxBsonFieldContext fieldCtx, Class<?> enumType, String src,
        Collection<Enum<?>> dst);
  }

  @FunctionalInterface
  public interface StringToFloatPrim {
    float convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToFloatPrimArray {
    float[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToIntegerPrim {
    int convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToIntegerPrimArray {
    int[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToLongPrim {
    long convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToLongPrimArray {
    long[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToNumberBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  @FunctionalInterface
  public interface StringToObjectId {
    ObjectId convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToObjectIdArray {
    ObjectId[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToObjectIdCollection {
    Collection<ObjectId> convert(JaxBsonFieldContext fieldCtx, String src,
        Collection<ObjectId> dst);
  }

  @FunctionalInterface
  public interface StringToShortPrim {
    short convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToShortPrimArray {
    short[] convert(JaxBsonFieldContext fieldCtx, String src);
  }

  @FunctionalInterface
  public interface StringToStringBased {
    Object convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToStringBasedArray {
    Object[] convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src);
  }

  @FunctionalInterface
  public interface StringToStringBasedCollection {
    Collection<Object> convert(JaxBsonFieldContext fieldCtx, Class<?> type, String src,
        Collection<Object> dst);
  }

  public static final JaxBsonToObject DEFAULT = new JaxBsonToObject.Builder().build();

  public static void setArrayComponent(final Class<?> type, final Object array, final int i,
      final Object value) {
    if (Boolean.TYPE.isAssignableFrom(type)) {
      Array.setBoolean(array, i, (boolean) value);
    } else if (Byte.TYPE.isAssignableFrom(type)) {
      Array.setByte(array, i, (byte) value);
    } else if (Character.TYPE.isAssignableFrom(type)) {
      Array.setChar(array, i, (char) value);
    } else if (Double.TYPE.isAssignableFrom(type)) {
      Array.setDouble(array, i, (double) value);
    } else if (Float.TYPE.isAssignableFrom(type)) {
      Array.setFloat(array, i, (float) value);
    } else if (Integer.TYPE.isAssignableFrom(type)) {
      Array.setInt(array, i, (int) value);
    } else if (Long.TYPE.isAssignableFrom(type)) {
      Array.setLong(array, i, (long) value);
    } else if (Short.TYPE.isAssignableFrom(type)) {
      Array.setShort(array, i, (short) value);
    } else {
      Array.set(array, i, value);
    }
  }

  private final BinaryCollectionToBinary binaryCollectionToBinary;
  private final BinaryCollectionToBinaryArray binaryCollectionToBinaryArray;
  private final BinaryCollectionToBinaryCollection binaryCollectionToBinaryCollection;
  private final BinaryCollectionToBoolean binaryCollectionToBoolean;
  private final BinaryCollectionToBooleanArray binaryCollectionToBooleanArray;
  private final BinaryCollectionToBooleanCollection binaryCollectionToBooleanCollection;
  private final BinaryCollectionToBooleanPrim binaryCollectionToBooleanPrim;
  private final BinaryCollectionToBooleanPrimArray binaryCollectionToBooleanPrimArray;
  private final BinaryCollectionToByte binaryCollectionToByte;
  private final BinaryCollectionToByteArray binaryCollectionToByteArray;
  private final BinaryCollectionToByteCollection binaryCollectionToByteCollection;
  private final BinaryCollectionToBytePrim binaryCollectionToBytePrim;
  private final BinaryCollectionToBytePrimArray binaryCollectionToBytePrimArray;
  private final BinaryCollectionToCharacter binaryCollectionToCharacter;
  private final BinaryCollectionToCharacterArray binaryCollectionToCharacterArray;
  private final BinaryCollectionToCharacterCollection binaryCollectionToCharacterCollection;
  private final BinaryCollectionToCharacterPrim binaryCollectionToCharacterPrim;
  private final BinaryCollectionToCharacterPrimArray binaryCollectionToCharacterPrimArray;
  private final BinaryCollectionToDateBased binaryCollectionToDateBased;
  private final BinaryCollectionToDateBasedArray binaryCollectionToDateBasedArray;
  private final BinaryCollectionToDateBasedCollection binaryCollectionToDateBasedCollection;
  private final BinaryCollectionToDoublePrim binaryCollectionToDoublePrim;
  private final BinaryCollectionToDoublePrimArray binaryCollectionToDoublePrimArray;
  private final BinaryCollectionToEnum binaryCollectionToEnum;
  private final BinaryCollectionToEnumArray binaryCollectionToEnumArray;
  private final BinaryCollectionToEnumCollection binaryCollectionToEnumCollection;
  private final BinaryCollectionToFloatPrim binaryCollectionToFloatPrim;
  private final BinaryCollectionToFloatPrimArray binaryCollectionToFloatPrimArray;
  private final BinaryCollectionToIntegerPrim binaryCollectionToIntegerPrim;
  private final BinaryCollectionToIntegerPrimArray binaryCollectionToIntegerPrimArray;
  private final BinaryCollectionToLongPrim binaryCollectionToLongPrim;
  private final BinaryCollectionToLongPrimArray binaryCollectionToLongPrimArray;
  private final BinaryCollectionToNumberBased binaryCollectionToNumberBased;
  private final BinaryCollectionToNumberBasedArray binaryCollectionToNumberBasedArray;
  private final BinaryCollectionToNumberBasedCollection binaryCollectionToNumberBasedCollection;
  private final BinaryCollectionToObjectId binaryCollectionToObjectId;
  private final BinaryCollectionToObjectIdArray binaryCollectionToObjectIdArray;
  private final BinaryCollectionToObjectIdCollection binaryCollectionToObjectIdCollection;
  private final BinaryCollectionToShortPrim binaryCollectionToShortPrim;
  private final BinaryCollectionToShortPrimArray binaryCollectionToShortPrimArray;
  private final BinaryCollectionToStringBased binaryCollectionToStringBased;
  private final BinaryCollectionToStringBasedArray binaryCollectionToStringBasedArray;
  private final BinaryCollectionToStringBasedCollection binaryCollectionToStringBasedCollection;
  private final BinaryToBinary binaryToBinary;
  private final BinaryToBinaryArray binaryToBinaryArray;
  private final BinaryToBinaryCollection binaryToBinaryCollection;
  private final BinaryToBoolean binaryToBoolean;
  private final BinaryToBooleanArray binaryToBooleanArray;
  private final BinaryToBooleanCollection binaryToBooleanCollection;
  private final BinaryToBooleanPrim binaryToBooleanPrim;
  private final BinaryToBooleanPrimArray binaryToBooleanPrimArray;
  private final BinaryToByte binaryToByte;
  private final BinaryToByteArray binaryToByteArray;
  private final BinaryToByteCollection binaryToByteCollection;
  private final BinaryToBytePrim binaryToBytePrim;
  private final BinaryToBytePrimArray binaryToBytePrimArray;
  private final BinaryToCharacter binaryToCharacter;
  private final BinaryToCharacterArray binaryToCharacterArray;
  private final BinaryToCharacterCollection binaryToCharacterCollection;
  private final BinaryToCharacterPrim binaryToCharacterPrim;
  private final BinaryToCharacterPrimArray binaryToCharacterPrimArray;
  private final BinaryToDateBased binaryToDateBased;
  private final BinaryToDateBasedArray binaryToDateBasedArray;
  private final BinaryToDateBasedCollection binaryToDateBasedCollection;
  private final BinaryToDoublePrim binaryToDoublePrim;
  private final BinaryToDoublePrimArray binaryToDoublePrimArray;
  private final BinaryToEnum binaryToEnum;
  private final BinaryToEnumArray binaryToEnumArray;
  private final BinaryToEnumCollection binaryToEnumCollection;
  private final BinaryToFloatPrim binaryToFloatPrim;
  private final BinaryToFloatPrimArray binaryToFloatPrimArray;
  private final BinaryToIntegerPrim binaryToIntegerPrim;
  private final BinaryToIntegerPrimArray binaryToIntegerPrimArray;
  private final BinaryToLongPrim binaryToLongPrim;
  private final BinaryToLongPrimArray binaryToLongPrimArray;
  private final BinaryToNumberBased binaryToNumberBased;
  private final BinaryToNumberBasedArray binaryToNumberBasedArray;
  private final BinaryToNumberBasedCollection binaryToNumberBasedCollection;
  private final BinaryToObjectId binaryToObjectId;
  private final BinaryToObjectIdArray binaryToObjectIdArray;
  private final BinaryToObjectIdCollection binaryToObjectIdCollection;
  private final BinaryToShortPrim binaryToShortPrim;
  private final BinaryToShortPrimArray binaryToShortPrimArray;
  private final BinaryToStringBased binaryToStringBased;
  private final BinaryToStringBasedArray binaryToStringBasedArray;
  private final BinaryToStringBasedCollection binaryToStringBasedCollection;
  private final BooleanCollectionToBinary booleanCollectionToBinary;
  private final BooleanCollectionToBinaryArray booleanCollectionToBinaryArray;
  private final BooleanCollectionToBinaryCollection booleanCollectionToBinaryCollection;
  private final BooleanCollectionToBoolean booleanCollectionToBoolean;
  private final BooleanCollectionToBooleanArray booleanCollectionToBooleanArray;
  private final BooleanCollectionToBooleanCollection booleanCollectionToBooleanCollection;
  private final BooleanCollectionToBooleanPrim booleanCollectionToBooleanPrim;
  private final BooleanCollectionToBooleanPrimArray booleanCollectionToBooleanPrimArray;
  private final BooleanCollectionToByte booleanCollectionToByte;
  private final BooleanCollectionToByteArray booleanCollectionToByteArray;
  private final BooleanCollectionToByteCollection booleanCollectionToByteCollection;
  private final BooleanCollectionToBytePrim booleanCollectionToBytePrim;
  private final BooleanCollectionToBytePrimArray booleanCollectionToBytePrimArray;
  private final BooleanCollectionToCharacter booleanCollectionToCharacter;
  private final BooleanCollectionToCharacterArray booleanCollectionToCharacterArray;
  private final BooleanCollectionToCharacterCollection booleanCollectionToCharacterCollection;
  private final BooleanCollectionToCharacterPrim booleanCollectionToCharacterPrim;
  private final BooleanCollectionToCharacterPrimArray booleanCollectionToCharacterPrimArray;
  private final BooleanCollectionToDateBased booleanCollectionToDateBased;
  private final BooleanCollectionToDateBasedArray booleanCollectionToDateBasedArray;
  private final BooleanCollectionToDateBasedCollection booleanCollectionToDateBasedCollection;
  private final BooleanCollectionToDoublePrim booleanCollectionToDoublePrim;
  private final BooleanCollectionToDoublePrimArray booleanCollectionToDoublePrimArray;
  private final BooleanCollectionToEnum booleanCollectionToEnum;
  private final BooleanCollectionToEnumArray booleanCollectionToEnumArray;
  private final BooleanCollectionToEnumCollection booleanCollectionToEnumCollection;
  private final BooleanCollectionToFloatPrim booleanCollectionToFloatPrim;
  private final BooleanCollectionToFloatPrimArray booleanCollectionToFloatPrimArray;
  private final BooleanCollectionToIntegerPrim booleanCollectionToIntegerPrim;
  private final BooleanCollectionToIntegerPrimArray booleanCollectionToIntegerPrimArray;
  private final BooleanCollectionToLongPrim booleanCollectionToLongPrim;
  private final BooleanCollectionToLongPrimArray booleanCollectionToLongPrimArray;
  private final BooleanCollectionToNumberBased booleanCollectionToNumberBased;
  private final BooleanCollectionToNumberBasedArray booleanCollectionToNumberBasedArray;
  private final BooleanCollectionToNumberBasedCollection booleanCollectionToNumberBasedCollection;
  private final BooleanCollectionToObjectId booleanCollectionToObjectId;
  private final BooleanCollectionToObjectIdArray booleanCollectionToObjectIdArray;
  private final BooleanCollectionToObjectIdCollection booleanCollectionToObjectIdCollection;
  private final BooleanCollectionToShortPrim booleanCollectionToShortPrim;
  private final BooleanCollectionToShortPrimArray booleanCollectionToShortPrimArray;
  private final BooleanCollectionToStringBased booleanCollectionToStringBased;
  private final BooleanCollectionToStringBasedArray booleanCollectionToStringBasedArray;
  private final BooleanCollectionToStringBasedCollection booleanCollectionToStringBasedCollection;
  private final BooleanToBinary booleanToBinary;
  private final BooleanToBinaryArray booleanToBinaryArray;
  private final BooleanToBinaryCollection booleanToBinaryCollection;
  private final BooleanToBoolean booleanToBoolean;
  private final BooleanToBooleanArray booleanToBooleanArray;
  private final BooleanToBooleanCollection booleanToBooleanCollection;
  private final BooleanToBooleanPrim booleanToBooleanPrim;
  private final BooleanToBooleanPrimArray booleanToBooleanPrimArray;
  private final BooleanToByte booleanToByte;
  private final BooleanToByteArray booleanToByteArray;
  private final BooleanToByteCollection booleanToByteCollection;
  private final BooleanToBytePrim booleanToBytePrim;
  private final BooleanToBytePrimArray booleanToBytePrimArray;
  private final BooleanToCharacter booleanToCharacter;
  private final BooleanToCharacterArray booleanToCharacterArray;
  private final BooleanToCharacterCollection booleanToCharacterCollection;
  private final BooleanToCharacterPrim booleanToCharacterPrim;
  private final BooleanToCharacterPrimArray booleanToCharacterPrimArray;
  private final BooleanToDateBased booleanToDateBased;
  private final BooleanToDateBasedArray booleanToDateBasedArray;
  private final BooleanToDateBasedCollection booleanToDateBasedCollection;
  private final BooleanToDoublePrim booleanToDoublePrim;
  private final BooleanToDoublePrimArray booleanToDoublePrimArray;
  private final BooleanToEnum booleanToEnum;
  private final BooleanToEnumArray booleanToEnumArray;
  private final BooleanToEnumCollection booleanToEnumCollection;
  private final BooleanToFloatPrim booleanToFloatPrim;
  private final BooleanToFloatPrimArray booleanToFloatPrimArray;
  private final BooleanToIntegerPrim booleanToIntegerPrim;
  private final BooleanToIntegerPrimArray booleanToIntegerPrimArray;
  private final BooleanToLongPrim booleanToLongPrim;
  private final BooleanToLongPrimArray booleanToLongPrimArray;
  private final BooleanToNumberBased booleanToNumberBased;
  private final BooleanToNumberBasedArray booleanToNumberBasedArray;
  private final BooleanToNumberBasedCollection booleanToNumberBasedCollection;
  private final BooleanToObjectId booleanToObjectId;
  private final BooleanToObjectIdArray booleanToObjectIdArray;
  private final BooleanToObjectIdCollection booleanToObjectIdCollection;
  private final BooleanToShortPrim booleanToShortPrim;
  private final BooleanToShortPrimArray booleanToShortPrimArray;
  private final BooleanToStringBased booleanToStringBased;
  private final BooleanToStringBasedArray booleanToStringBasedArray;
  private final BooleanToStringBasedCollection booleanToStringBasedCollection;
  private final DateCollectionToBinary dateCollectionToBinary;
  private final DateCollectionToBinaryArray dateCollectionToBinaryArray;
  private final DateCollectionToBinaryCollection dateCollectionToBinaryCollection;
  private final DateCollectionToBoolean dateCollectionToBoolean;
  private final DateCollectionToBooleanArray dateCollectionToBooleanArray;
  private final DateCollectionToBooleanCollection dateCollectionToBooleanCollection;
  private final DateCollectionToBooleanPrim dateCollectionToBooleanPrim;
  private final DateCollectionToBooleanPrimArray dateCollectionToBooleanPrimArray;
  private final DateCollectionToByte dateCollectionToByte;
  private final DateCollectionToByteArray dateCollectionToByteArray;
  private final DateCollectionToByteCollection dateCollectionToByteCollection;
  private final DateCollectionToBytePrim dateCollectionToBytePrim;
  private final DateCollectionToBytePrimArray dateCollectionToBytePrimArray;
  private final DateCollectionToCharacter dateCollectionToCharacter;
  private final DateCollectionToCharacterArray dateCollectionToCharacterArray;
  private final DateCollectionToCharacterCollection dateCollectionToCharacterCollection;
  private final DateCollectionToCharacterPrim dateCollectionToCharacterPrim;
  private final DateCollectionToCharacterPrimArray dateCollectionToCharacterPrimArray;
  private final DateCollectionToDateBased dateCollectionToDateBased;
  private final DateCollectionToDateBasedArray dateCollectionToDateBasedArray;
  private final DateCollectionToDateBasedCollection dateCollectionToDateBasedCollection;
  private final DateCollectionToDoublePrim dateCollectionToDoublePrim;
  private final DateCollectionToDoublePrimArray dateCollectionToDoublePrimArray;
  private final DateCollectionToEnum dateCollectionToEnum;
  private final DateCollectionToEnumArray dateCollectionToEnumArray;
  private final DateCollectionToEnumCollection dateCollectionToEnumCollection;
  private final DateCollectionToFloatPrim dateCollectionToFloatPrim;
  private final DateCollectionToFloatPrimArray dateCollectionToFloatPrimArray;
  private final DateCollectionToIntegerPrim dateCollectionToIntegerPrim;
  private final DateCollectionToIntegerPrimArray dateCollectionToIntegerPrimArray;
  private final DateCollectionToLongPrim dateCollectionToLongPrim;
  private final DateCollectionToLongPrimArray dateCollectionToLongPrimArray;
  private final DateCollectionToNumberBased dateCollectionToNumberBased;
  private final DateCollectionToNumberBasedArray dateCollectionToNumberBasedArray;
  private final DateCollectionToNumberBasedCollection dateCollectionToNumberBasedCollection;
  private final DateCollectionToObjectId dateCollectionToObjectId;
  private final DateCollectionToObjectIdArray dateCollectionToObjectIdArray;
  private final DateCollectionToObjectIdCollection dateCollectionToObjectIdCollection;
  private final DateCollectionToShortPrim dateCollectionToShortPrim;
  private final DateCollectionToShortPrimArray dateCollectionToShortPrimArray;
  private final DateCollectionToStringBased dateCollectionToStringBased;
  private final DateCollectionToStringBasedArray dateCollectionToStringBasedArray;
  private final DateCollectionToStringBasedCollection dateCollectionToStringBasedCollection;
  private final DateToBinary dateToBinary;
  private final DateToBinaryArray dateToBinaryArray;
  private final DateToBinaryCollection dateToBinaryCollection;
  private final DateToBoolean dateToBoolean;
  private final DateToBooleanArray dateToBooleanArray;
  private final DateToBooleanCollection dateToBooleanCollection;
  private final DateToBooleanPrim dateToBooleanPrim;
  private final DateToBooleanPrimArray dateToBooleanPrimArray;
  private final DateToByte dateToByte;
  private final DateToByteArray dateToByteArray;
  private final DateToByteCollection dateToByteCollection;
  private final DateToBytePrim dateToBytePrim;
  private final DateToBytePrimArray dateToBytePrimArray;
  private final DateToCharacter dateToCharacter;
  private final DateToCharacterArray dateToCharacterArray;
  private final DateToCharacterCollection dateToCharacterCollection;
  private final DateToCharacterPrim dateToCharacterPrim;
  private final DateToCharacterPrimArray dateToCharacterPrimArray;
  private final DateToDateBased dateToDateBased;
  private final DateToDateBasedArray dateToDateBasedArray;
  private final DateToDateBasedCollection dateToDateBasedCollection;
  private final DateToDoublePrim dateToDoublePrim;
  private final DateToDoublePrimArray dateToDoublePrimArray;
  private final DateToEnum dateToEnum;
  private final DateToEnumArray dateToEnumArray;
  private final DateToEnumCollection dateToEnumCollection;
  private final DateToFloatPrim dateToFloatPrim;
  private final DateToFloatPrimArray dateToFloatPrimArray;
  private final DateToIntegerPrim dateToIntegerPrim;
  private final DateToIntegerPrimArray dateToIntegerPrimArray;
  private final DateToLongPrim dateToLongPrim;
  private final DateToLongPrimArray dateToLongPrimArray;
  private final DateToNumberBased dateToNumberBased;
  private final DateToNumberBasedArray dateToNumberBasedArray;
  private final DateToNumberBasedCollection dateToNumberBasedCollection;
  private final DateToObjectId dateToObjectId;
  private final DateToObjectIdArray dateToObjectIdArray;
  private final DateToObjectIdCollection dateToObjectIdCollection;
  private final DateToShortPrim dateToShortPrim;
  private final DateToShortPrimArray dateToShortPrimArray;
  private final DateToStringBased dateToStringBased;
  private final DateToStringBasedArray dateToStringBasedArray;
  private final DateToStringBasedCollection dateToStringBasedCollection;
  private final NumberCollectionToBinary numberCollectionToBinary;
  private final NumberCollectionToBinaryArray numberCollectionToBinaryArray;
  private final NumberCollectionToBinaryCollection numberCollectionToBinaryCollection;
  private final NumberCollectionToBoolean numberCollectionToBoolean;
  private final NumberCollectionToBooleanArray numberCollectionToBooleanArray;
  private final NumberCollectionToBooleanCollection numberCollectionToBooleanCollection;
  private final NumberCollectionToBooleanPrim numberCollectionToBooleanPrim;
  private final NumberCollectionToBooleanPrimArray numberCollectionToBooleanPrimArray;
  private final NumberCollectionToByte numberCollectionToByte;
  private final NumberCollectionToByteArray numberCollectionToByteArray;
  private final NumberCollectionToByteCollection numberCollectionToByteCollection;
  private final NumberCollectionToBytePrim numberCollectionToBytePrim;
  private final NumberCollectionToBytePrimArray numberCollectionToBytePrimArray;
  private final NumberCollectionToCharacter numberCollectionToCharacter;
  private final NumberCollectionToCharacterArray numberCollectionToCharacterArray;
  private final NumberCollectionToCharacterCollection numberCollectionToCharacterCollection;
  private final NumberCollectionToCharacterPrim numberCollectionToCharacterPrim;
  private final NumberCollectionToCharacterPrimArray numberCollectionToCharacterPrimArray;
  private final NumberCollectionToDateBased numberCollectionToDateBased;
  private final NumberCollectionToDateBasedArray numberCollectionToDateBasedArray;
  private final NumberCollectionToDateBasedCollection numberCollectionToDateBasedCollection;
  private final NumberCollectionToDoublePrim numberCollectionToDoublePrim;
  private final NumberCollectionToDoublePrimArray numberCollectionToDoublePrimArray;
  private final NumberCollectionToEnum numberCollectionToEnum;
  private final NumberCollectionToEnumArray numberCollectionToEnumArray;
  private final NumberCollectionToEnumCollection numberCollectionToEnumCollection;
  private final NumberCollectionToFloatPrim numberCollectionToFloatPrim;
  private final NumberCollectionToFloatPrimArray numberCollectionToFloatPrimArray;
  private final NumberCollectionToIntegerPrim numberCollectionToIntegerPrim;
  private final NumberCollectionToIntegerPrimArray numberCollectionToIntegerPrimArray;
  private final NumberCollectionToLongPrim numberCollectionToLongPrim;
  private final NumberCollectionToLongPrimArray numberCollectionToLongPrimArray;
  private final NumberCollectionToNumberBased numberCollectionToNumberBased;
  private final NumberCollectionToNumberBasedArray numberCollectionToNumberBasedArray;
  private final NumberCollectionToNumberBasedCollection numberCollectionToNumberBasedCollection;
  private final NumberCollectionToObjectId numberCollectionToObjectId;
  private final NumberCollectionToObjectIdArray numberCollectionToObjectIdArray;
  private final NumberCollectionToObjectIdCollection numberCollectionToObjectIdCollection;
  private final NumberCollectionToShortPrim numberCollectionToShortPrim;
  private final NumberCollectionToShortPrimArray numberCollectionToShortPrimArray;
  private final NumberCollectionToStringBased numberCollectionToStringBased;
  private final NumberCollectionToStringBasedArray numberCollectionToStringBasedArray;
  private final NumberCollectionToStringBasedCollection numberCollectionToStringBasedCollection;
  private final NumberToBinary numberToBinary;
  private final NumberToBinaryArray numberToBinaryArray;
  private final NumberToBinaryCollection numberToBinaryCollection;
  private final NumberToBoolean numberToBoolean;
  private final NumberToBooleanArray numberToBooleanArray;
  private final NumberToBooleanCollection numberToBooleanCollection;
  private final NumberToBooleanPrim numberToBooleanPrim;
  private final NumberToBooleanPrimArray numberToBooleanPrimArray;
  private final NumberToByte numberToByte;
  private final NumberToByteArray numberToByteArray;
  private final NumberToByteCollection numberToByteCollection;
  private final NumberToBytePrim numberToBytePrim;
  private final NumberToBytePrimArray numberToBytePrimArray;
  private final NumberToCharacter numberToCharacter;
  private final NumberToCharacterArray numberToCharacterArray;
  private final NumberToCharacterCollection numberToCharacterCollection;
  private final NumberToCharacterPrim numberToCharacterPrim;
  private final NumberToCharacterPrimArray numberToCharacterPrimArray;
  private final NumberToDateBased numberToDateBased;
  private final NumberToDateBasedArray numberToDateBasedArray;
  private final NumberToDateBasedCollection numberToDateBasedCollection;
  private final NumberToDoublePrim numberToDoublePrim;
  private final NumberToDoublePrimArray numberToDoublePrimArray;
  private final NumberToEnum numberToEnum;
  private final NumberToEnumArray numberToEnumArray;
  private final NumberToEnumCollection numberToEnumCollection;
  private final NumberToFloatPrim numberToFloatPrim;
  private final NumberToFloatPrimArray numberToFloatPrimArray;
  private final NumberToIntegerPrim numberToIntegerPrim;
  private final NumberToIntegerPrimArray numberToIntegerPrimArray;
  private final NumberToLongPrim numberToLongPrim;
  private final NumberToLongPrimArray numberToLongPrimArray;
  private final NumberToNumberBased numberToNumberBased;
  private final NumberToNumberBasedArray numberToNumberBasedArray;
  private final NumberToNumberBasedCollection numberToNumberBasedCollection;
  private final NumberToObjectId numberToObjectId;
  private final NumberToObjectIdArray numberToObjectIdArray;
  private final NumberToObjectIdCollection numberToObjectIdCollection;
  private final NumberToShortPrim numberToShortPrim;
  private final NumberToShortPrimArray numberToShortPrimArray;
  private final NumberToStringBased numberToStringBased;
  private final NumberToStringBasedArray numberToStringBasedArray;
  private final NumberToStringBasedCollection numberToStringBasedCollection;
  private final ObjectIdCollectionToBinary objectIdCollectionToBinary;
  private final ObjectIdCollectionToBinaryArray objectIdCollectionToBinaryArray;
  private final ObjectIdCollectionToBinaryCollection objectIdCollectionToBinaryCollection;
  private final ObjectIdCollectionToBoolean objectIdCollectionToBoolean;
  private final ObjectIdCollectionToBooleanArray objectIdCollectionToBooleanArray;
  private final ObjectIdCollectionToBooleanCollection objectIdCollectionToBooleanCollection;
  private final ObjectIdCollectionToBooleanPrim objectIdCollectionToBooleanPrim;
  private final ObjectIdCollectionToBooleanPrimArray objectIdCollectionToBooleanPrimArray;
  private final ObjectIdCollectionToByte objectIdCollectionToByte;
  private final ObjectIdCollectionToByteArray objectIdCollectionToByteArray;
  private final ObjectIdCollectionToByteCollection objectIdCollectionToByteCollection;
  private final ObjectIdCollectionToBytePrim objectIdCollectionToBytePrim;
  private final ObjectIdCollectionToBytePrimArray objectIdCollectionToBytePrimArray;
  private final ObjectIdCollectionToCharacter objectIdCollectionToCharacter;
  private final ObjectIdCollectionToCharacterArray objectIdCollectionToCharacterArray;
  private final ObjectIdCollectionToCharacterCollection objectIdCollectionToCharacterCollection;
  private final ObjectIdCollectionToCharacterPrim objectIdCollectionToCharacterPrim;
  private final ObjectIdCollectionToCharacterPrimArray objectIdCollectionToCharacterPrimArray;
  private final ObjectIdCollectionToDateBased objectIdCollectionToDateBased;
  private final ObjectIdCollectionToDateBasedArray objectIdCollectionToDateBasedArray;
  private final ObjectIdCollectionToDateBasedCollection objectIdCollectionToDateBasedCollection;
  private final ObjectIdCollectionToDoublePrim objectIdCollectionToDoublePrim;
  private final ObjectIdCollectionToDoublePrimArray objectIdCollectionToDoublePrimArray;
  private final ObjectIdCollectionToEnum objectIdCollectionToEnum;
  private final ObjectIdCollectionToEnumArray objectIdCollectionToEnumArray;
  private final ObjectIdCollectionToEnumCollection objectIdCollectionToEnumCollection;
  private final ObjectIdCollectionToFloatPrim objectIdCollectionToFloatPrim;
  private final ObjectIdCollectionToFloatPrimArray objectIdCollectionToFloatPrimArray;
  private final ObjectIdCollectionToIntegerPrim objectIdCollectionToIntegerPrim;
  private final ObjectIdCollectionToIntegerPrimArray objectIdCollectionToIntegerPrimArray;
  private final ObjectIdCollectionToLongPrim objectIdCollectionToLongPrim;
  private final ObjectIdCollectionToLongPrimArray objectIdCollectionToLongPrimArray;
  private final ObjectIdCollectionToNumberBased objectIdCollectionToNumberBased;
  private final ObjectIdCollectionToNumberBasedArray objectIdCollectionToNumberBasedArray;
  private final ObjectIdCollectionToNumberBasedCollection objectIdCollectionToNumberBasedCollection;
  private final ObjectIdCollectionToObjectId objectIdCollectionToObjectId;
  private final ObjectIdCollectionToObjectIdArray objectIdCollectionToObjectIdArray;
  private final ObjectIdCollectionToObjectIdCollection objectIdCollectionToObjectIdCollection;
  private final ObjectIdCollectionToShortPrim objectIdCollectionToShortPrim;
  private final ObjectIdCollectionToShortPrimArray objectIdCollectionToShortPrimArray;
  private final ObjectIdCollectionToStringBased objectIdCollectionToStringBased;
  private final ObjectIdCollectionToStringBasedArray objectIdCollectionToStringBasedArray;
  private final ObjectIdCollectionToStringBasedCollection objectIdCollectionToStringBasedCollection;
  private final ObjectIdToBinary objectIdToBinary;
  private final ObjectIdToBinaryArray objectIdToBinaryArray;
  private final ObjectIdToBinaryCollection objectIdToBinaryCollection;
  private final ObjectIdToBoolean objectIdToBoolean;
  private final ObjectIdToBooleanArray objectIdToBooleanArray;
  private final ObjectIdToBooleanCollection objectIdToBooleanCollection;
  private final ObjectIdToBooleanPrim objectIdToBooleanPrim;
  private final ObjectIdToBooleanPrimArray objectIdToBooleanPrimArray;
  private final ObjectIdToByte objectIdToByte;
  private final ObjectIdToByteArray objectIdToByteArray;
  private final ObjectIdToByteCollection objectIdToByteCollection;
  private final ObjectIdToBytePrim objectIdToBytePrim;
  private final ObjectIdToBytePrimArray objectIdToBytePrimArray;
  private final ObjectIdToCharacter objectIdToCharacter;
  private final ObjectIdToCharacterArray objectIdToCharacterArray;
  private final ObjectIdToCharacterCollection objectIdToCharacterCollection;
  private final ObjectIdToCharacterPrim objectIdToCharacterPrim;
  private final ObjectIdToCharacterPrimArray objectIdToCharacterPrimArray;
  private final ObjectIdToDateBased objectIdToDateBased;
  private final ObjectIdToDateBasedArray objectIdToDateBasedArray;
  private final ObjectIdToDateBasedCollection objectIdToDateBasedCollection;
  private final ObjectIdToDoublePrim objectIdToDoublePrim;
  private final ObjectIdToDoublePrimArray objectIdToDoublePrimArray;
  private final ObjectIdToEnum objectIdToEnum;
  private final ObjectIdToEnumArray objectIdToEnumArray;
  private final ObjectIdToEnumCollection objectIdToEnumCollection;
  private final ObjectIdToFloatPrim objectIdToFloatPrim;
  private final ObjectIdToFloatPrimArray objectIdToFloatPrimArray;
  private final ObjectIdToIntegerPrim objectIdToIntegerPrim;
  private final ObjectIdToIntegerPrimArray objectIdToIntegerPrimArray;
  private final ObjectIdToLongPrim objectIdToLongPrim;
  private final ObjectIdToLongPrimArray objectIdToLongPrimArray;
  private final ObjectIdToNumberBased objectIdToNumberBased;
  private final ObjectIdToNumberBasedArray objectIdToNumberBasedArray;
  private final ObjectIdToNumberBasedCollection objectIdToNumberBasedCollection;
  private final ObjectIdToObjectId objectIdToObjectId;
  private final ObjectIdToObjectIdArray objectIdToObjectIdArray;
  private final ObjectIdToObjectIdCollection objectIdToObjectIdCollection;
  private final ObjectIdToShortPrim objectIdToShortPrim;
  private final ObjectIdToShortPrimArray objectIdToShortPrimArray;
  private final ObjectIdToStringBased objectIdToStringBased;
  private final ObjectIdToStringBasedArray objectIdToStringBasedArray;
  private final ObjectIdToStringBasedCollection objectIdToStringBasedCollection;
  private final StringCollectionToBinary stringCollectionToBinary;
  private final StringCollectionToBinaryArray stringCollectionToBinaryArray;
  private final StringCollectionToBinaryCollection stringCollectionToBinaryCollection;
  private final StringCollectionToBoolean stringCollectionToBoolean;
  private final StringCollectionToBooleanArray stringCollectionToBooleanArray;
  private final StringCollectionToBooleanCollection stringCollectionToBooleanCollection;
  private final StringCollectionToBooleanPrim stringCollectionToBooleanPrim;
  private final StringCollectionToBooleanPrimArray stringCollectionToBooleanPrimArray;
  private final StringCollectionToByte stringCollectionToByte;
  private final StringCollectionToByteArray stringCollectionToByteArray;
  private final StringCollectionToByteCollection stringCollectionToByteCollection;
  private final StringCollectionToBytePrim stringCollectionToBytePrim;
  private final StringCollectionToBytePrimArray stringCollectionToBytePrimArray;
  private final StringCollectionToCharacter stringCollectionToCharacter;
  private final StringCollectionToCharacterArray stringCollectionToCharacterArray;
  private final StringCollectionToCharacterCollection stringCollectionToCharacterCollection;
  private final StringCollectionToCharacterPrim stringCollectionToCharacterPrim;
  private final StringCollectionToCharacterPrimArray stringCollectionToCharacterPrimArray;
  private final StringCollectionToDateBased stringCollectionToDateBased;
  private final StringCollectionToDateBasedArray stringCollectionToDateBasedArray;
  private final StringCollectionToDateBasedCollection stringCollectionToDateBasedCollection;
  private final StringCollectionToDoublePrim stringCollectionToDoublePrim;
  private final StringCollectionToDoublePrimArray stringCollectionToDoublePrimArray;
  private final StringCollectionToEnum stringCollectionToEnum;
  private final StringCollectionToEnumArray stringCollectionToEnumArray;
  private final StringCollectionToEnumCollection stringCollectionToEnumCollection;
  private final StringCollectionToFloatPrim stringCollectionToFloatPrim;
  private final StringCollectionToFloatPrimArray stringCollectionToFloatPrimArray;
  private final StringCollectionToIntegerPrim stringCollectionToIntegerPrim;
  private final StringCollectionToIntegerPrimArray stringCollectionToIntegerPrimArray;
  private final StringCollectionToLongPrim stringCollectionToLongPrim;
  private final StringCollectionToLongPrimArray stringCollectionToLongPrimArray;
  private final StringCollectionToNumberBased stringCollectionToNumberBased;
  private final StringCollectionToNumberBasedArray stringCollectionToNumberBasedArray;
  private final StringCollectionToNumberBasedCollection stringCollectionToNumberBasedCollection;
  private final StringCollectionToObjectId stringCollectionToObjectId;
  private final StringCollectionToObjectIdArray stringCollectionToObjectIdArray;
  private final StringCollectionToObjectIdCollection stringCollectionToObjectIdCollection;
  private final StringCollectionToShortPrim stringCollectionToShortPrim;
  private final StringCollectionToShortPrimArray stringCollectionToShortPrimArray;
  private final StringCollectionToStringBased stringCollectionToStringBased;
  private final StringCollectionToStringBasedArray stringCollectionToStringBasedArray;
  private final StringCollectionToStringBasedCollection stringCollectionToStringBasedCollection;
  private final StringToBinary stringToBinary;
  private final StringToBinaryArray stringToBinaryArray;
  private final StringToBinaryCollection stringToBinaryCollection;
  private final StringToBoolean stringToBoolean;
  private final StringToBooleanArray stringToBooleanArray;
  private final StringToBooleanCollection stringToBooleanCollection;
  private final StringToBooleanPrim stringToBooleanPrim;
  private final StringToBooleanPrimArray stringToBooleanPrimArray;
  private final StringToByte stringToByte;
  private final StringToByteArray stringToByteArray;
  private final StringToByteCollection stringToByteCollection;
  private final StringToBytePrim stringToBytePrim;
  private final StringToBytePrimArray stringToBytePrimArray;
  private final StringToCharacter stringToCharacter;
  private final StringToCharacterArray stringToCharacterArray;
  private final StringToCharacterCollection stringToCharacterCollection;
  private final StringToCharacterPrim stringToCharacterPrim;
  private final StringToCharacterPrimArray stringToCharacterPrimArray;
  private final StringToDateBased stringToDateBased;
  private final StringToDateBasedArray stringToDateBasedArray;
  private final StringToDateBasedCollection stringToDateBasedCollection;
  private final StringToDoublePrim stringToDoublePrim;
  private final StringToDoublePrimArray stringToDoublePrimArray;
  private final StringToEnum stringToEnum;
  private final StringToEnumArray stringToEnumArray;
  private final StringToEnumCollection stringToEnumCollection;
  private final StringToFloatPrim stringToFloatPrim;
  private final StringToFloatPrimArray stringToFloatPrimArray;
  private final StringToIntegerPrim stringToIntegerPrim;
  private final StringToIntegerPrimArray stringToIntegerPrimArray;
  private final StringToLongPrim stringToLongPrim;
  private final StringToLongPrimArray stringToLongPrimArray;
  private final StringToNumberBased stringToNumberBased;
  private final StringToNumberBasedArray stringToNumberBasedArray;
  private final StringToNumberBasedCollection stringToNumberBasedCollection;
  private final StringToObjectId stringToObjectId;
  private final StringToObjectIdArray stringToObjectIdArray;
  private final StringToObjectIdCollection stringToObjectIdCollection;
  private final StringToShortPrim stringToShortPrim;
  private final StringToShortPrimArray stringToShortPrimArray;
  private final StringToStringBased stringToStringBased;
  private final StringToStringBasedArray stringToStringBasedArray;
  private final StringToStringBasedCollection stringToStringBasedCollection;

  private JaxBsonToObject() {
    this.binaryCollectionToBinary = null;
    this.binaryCollectionToBinaryArray = null;
    this.binaryCollectionToBinaryCollection = null;
    this.binaryCollectionToBoolean = null;
    this.binaryCollectionToBooleanArray = null;
    this.binaryCollectionToBooleanCollection = null;
    this.binaryCollectionToBooleanPrim = null;
    this.binaryCollectionToBooleanPrimArray = null;
    this.binaryCollectionToByte = null;
    this.binaryCollectionToByteArray = null;
    this.binaryCollectionToByteCollection = null;
    this.binaryCollectionToBytePrim = null;
    this.binaryCollectionToBytePrimArray = null;
    this.binaryCollectionToCharacter = null;
    this.binaryCollectionToCharacterArray = null;
    this.binaryCollectionToCharacterCollection = null;
    this.binaryCollectionToCharacterPrim = null;
    this.binaryCollectionToCharacterPrimArray = null;
    this.binaryCollectionToDateBased = null;
    this.binaryCollectionToDateBasedArray = null;
    this.binaryCollectionToDateBasedCollection = null;
    this.binaryCollectionToDoublePrim = null;
    this.binaryCollectionToDoublePrimArray = null;
    this.binaryCollectionToEnum = null;
    this.binaryCollectionToEnumArray = null;
    this.binaryCollectionToEnumCollection = null;
    this.binaryCollectionToFloatPrim = null;
    this.binaryCollectionToFloatPrimArray = null;
    this.binaryCollectionToIntegerPrim = null;
    this.binaryCollectionToIntegerPrimArray = null;
    this.binaryCollectionToLongPrim = null;
    this.binaryCollectionToLongPrimArray = null;
    this.binaryCollectionToNumberBased = null;
    this.binaryCollectionToNumberBasedArray = null;
    this.binaryCollectionToNumberBasedCollection = null;
    this.binaryCollectionToObjectId = null;
    this.binaryCollectionToObjectIdArray = null;
    this.binaryCollectionToObjectIdCollection = null;
    this.binaryCollectionToShortPrim = null;
    this.binaryCollectionToShortPrimArray = null;
    this.binaryCollectionToStringBased = null;
    this.binaryCollectionToStringBasedArray = null;
    this.binaryCollectionToStringBasedCollection = null;
    this.binaryToBinary = null;
    this.binaryToBinaryArray = null;
    this.binaryToBinaryCollection = null;
    this.binaryToBoolean = null;
    this.binaryToBooleanArray = null;
    this.binaryToBooleanCollection = null;
    this.binaryToBooleanPrim = null;
    this.binaryToBooleanPrimArray = null;
    this.binaryToByte = null;
    this.binaryToByteArray = null;
    this.binaryToByteCollection = null;
    this.binaryToBytePrim = null;
    this.binaryToBytePrimArray = null;
    this.binaryToCharacter = null;
    this.binaryToCharacterArray = null;
    this.binaryToCharacterCollection = null;
    this.binaryToCharacterPrim = null;
    this.binaryToCharacterPrimArray = null;
    this.binaryToDateBased = null;
    this.binaryToDateBasedArray = null;
    this.binaryToDateBasedCollection = null;
    this.binaryToDoublePrim = null;
    this.binaryToDoublePrimArray = null;
    this.binaryToEnum = null;
    this.binaryToEnumArray = null;
    this.binaryToEnumCollection = null;
    this.binaryToFloatPrim = null;
    this.binaryToFloatPrimArray = null;
    this.binaryToIntegerPrim = null;
    this.binaryToIntegerPrimArray = null;
    this.binaryToLongPrim = null;
    this.binaryToLongPrimArray = null;
    this.binaryToNumberBased = null;
    this.binaryToNumberBasedArray = null;
    this.binaryToNumberBasedCollection = null;
    this.binaryToObjectId = null;
    this.binaryToObjectIdArray = null;
    this.binaryToObjectIdCollection = null;
    this.binaryToShortPrim = null;
    this.binaryToShortPrimArray = null;
    this.binaryToStringBased = null;
    this.binaryToStringBasedArray = null;
    this.binaryToStringBasedCollection = null;
    this.booleanCollectionToBinary = null;
    this.booleanCollectionToBinaryArray = null;
    this.booleanCollectionToBinaryCollection = null;
    this.booleanCollectionToBoolean = null;
    this.booleanCollectionToBooleanArray = null;
    this.booleanCollectionToBooleanCollection = null;
    this.booleanCollectionToBooleanPrim = null;
    this.booleanCollectionToBooleanPrimArray = null;
    this.booleanCollectionToByte = null;
    this.booleanCollectionToByteArray = null;
    this.booleanCollectionToByteCollection = null;
    this.booleanCollectionToBytePrim = null;
    this.booleanCollectionToBytePrimArray = null;
    this.booleanCollectionToCharacter = null;
    this.booleanCollectionToCharacterArray = null;
    this.booleanCollectionToCharacterCollection = null;
    this.booleanCollectionToCharacterPrim = null;
    this.booleanCollectionToCharacterPrimArray = null;
    this.booleanCollectionToDateBased = null;
    this.booleanCollectionToDateBasedArray = null;
    this.booleanCollectionToDateBasedCollection = null;
    this.booleanCollectionToDoublePrim = null;
    this.booleanCollectionToDoublePrimArray = null;
    this.booleanCollectionToEnum = null;
    this.booleanCollectionToEnumArray = null;
    this.booleanCollectionToEnumCollection = null;
    this.booleanCollectionToFloatPrim = null;
    this.booleanCollectionToFloatPrimArray = null;
    this.booleanCollectionToIntegerPrim = null;
    this.booleanCollectionToIntegerPrimArray = null;
    this.booleanCollectionToLongPrim = null;
    this.booleanCollectionToLongPrimArray = null;
    this.booleanCollectionToNumberBased = null;
    this.booleanCollectionToNumberBasedArray = null;
    this.booleanCollectionToNumberBasedCollection = null;
    this.booleanCollectionToObjectId = null;
    this.booleanCollectionToObjectIdArray = null;
    this.booleanCollectionToObjectIdCollection = null;
    this.booleanCollectionToShortPrim = null;
    this.booleanCollectionToShortPrimArray = null;
    this.booleanCollectionToStringBased = null;
    this.booleanCollectionToStringBasedArray = null;
    this.booleanCollectionToStringBasedCollection = null;
    this.booleanToBinary = null;
    this.booleanToBinaryArray = null;
    this.booleanToBinaryCollection = null;
    this.booleanToBoolean = null;
    this.booleanToBooleanArray = null;
    this.booleanToBooleanCollection = null;
    this.booleanToBooleanPrim = null;
    this.booleanToBooleanPrimArray = null;
    this.booleanToByte = null;
    this.booleanToByteArray = null;
    this.booleanToByteCollection = null;
    this.booleanToBytePrim = null;
    this.booleanToBytePrimArray = null;
    this.booleanToCharacter = null;
    this.booleanToCharacterArray = null;
    this.booleanToCharacterCollection = null;
    this.booleanToCharacterPrim = null;
    this.booleanToCharacterPrimArray = null;
    this.booleanToDateBased = null;
    this.booleanToDateBasedArray = null;
    this.booleanToDateBasedCollection = null;
    this.booleanToDoublePrim = null;
    this.booleanToDoublePrimArray = null;
    this.booleanToEnum = null;
    this.booleanToEnumArray = null;
    this.booleanToEnumCollection = null;
    this.booleanToFloatPrim = null;
    this.booleanToFloatPrimArray = null;
    this.booleanToIntegerPrim = null;
    this.booleanToIntegerPrimArray = null;
    this.booleanToLongPrim = null;
    this.booleanToLongPrimArray = null;
    this.booleanToNumberBased = null;
    this.booleanToNumberBasedArray = null;
    this.booleanToNumberBasedCollection = null;
    this.booleanToObjectId = null;
    this.booleanToObjectIdArray = null;
    this.booleanToObjectIdCollection = null;
    this.booleanToShortPrim = null;
    this.booleanToShortPrimArray = null;
    this.booleanToStringBased = null;
    this.booleanToStringBasedArray = null;
    this.booleanToStringBasedCollection = null;
    this.dateCollectionToBinary = null;
    this.dateCollectionToBinaryArray = null;
    this.dateCollectionToBinaryCollection = null;
    this.dateCollectionToBoolean = null;
    this.dateCollectionToBooleanArray = null;
    this.dateCollectionToBooleanCollection = null;
    this.dateCollectionToBooleanPrim = null;
    this.dateCollectionToBooleanPrimArray = null;
    this.dateCollectionToByte = null;
    this.dateCollectionToByteArray = null;
    this.dateCollectionToByteCollection = null;
    this.dateCollectionToBytePrim = null;
    this.dateCollectionToBytePrimArray = null;
    this.dateCollectionToCharacter = null;
    this.dateCollectionToCharacterArray = null;
    this.dateCollectionToCharacterCollection = null;
    this.dateCollectionToCharacterPrim = null;
    this.dateCollectionToCharacterPrimArray = null;
    this.dateCollectionToDateBased = null;
    this.dateCollectionToDateBasedArray = null;
    this.dateCollectionToDateBasedCollection = null;
    this.dateCollectionToDoublePrim = null;
    this.dateCollectionToDoublePrimArray = null;
    this.dateCollectionToEnum = null;
    this.dateCollectionToEnumArray = null;
    this.dateCollectionToEnumCollection = null;
    this.dateCollectionToFloatPrim = null;
    this.dateCollectionToFloatPrimArray = null;
    this.dateCollectionToIntegerPrim = null;
    this.dateCollectionToIntegerPrimArray = null;
    this.dateCollectionToLongPrim = null;
    this.dateCollectionToLongPrimArray = null;
    this.dateCollectionToNumberBased = null;
    this.dateCollectionToNumberBasedArray = null;
    this.dateCollectionToNumberBasedCollection = null;
    this.dateCollectionToObjectId = null;
    this.dateCollectionToObjectIdArray = null;
    this.dateCollectionToObjectIdCollection = null;
    this.dateCollectionToShortPrim = null;
    this.dateCollectionToShortPrimArray = null;
    this.dateCollectionToStringBased = null;
    this.dateCollectionToStringBasedArray = null;
    this.dateCollectionToStringBasedCollection = null;
    this.dateToBinary = null;
    this.dateToBinaryArray = null;
    this.dateToBinaryCollection = null;
    this.dateToBoolean = null;
    this.dateToBooleanArray = null;
    this.dateToBooleanCollection = null;
    this.dateToBooleanPrim = null;
    this.dateToBooleanPrimArray = null;
    this.dateToByte = null;
    this.dateToByteArray = null;
    this.dateToByteCollection = null;
    this.dateToBytePrim = null;
    this.dateToBytePrimArray = null;
    this.dateToCharacter = null;
    this.dateToCharacterArray = null;
    this.dateToCharacterCollection = null;
    this.dateToCharacterPrim = null;
    this.dateToCharacterPrimArray = null;
    this.dateToDateBased = null;
    this.dateToDateBasedArray = null;
    this.dateToDateBasedCollection = null;
    this.dateToDoublePrim = null;
    this.dateToDoublePrimArray = null;
    this.dateToEnum = null;
    this.dateToEnumArray = null;
    this.dateToEnumCollection = null;
    this.dateToFloatPrim = null;
    this.dateToFloatPrimArray = null;
    this.dateToIntegerPrim = null;
    this.dateToIntegerPrimArray = null;
    this.dateToLongPrim = null;
    this.dateToLongPrimArray = null;
    this.dateToNumberBased = null;
    this.dateToNumberBasedArray = null;
    this.dateToNumberBasedCollection = null;
    this.dateToObjectId = null;
    this.dateToObjectIdArray = null;
    this.dateToObjectIdCollection = null;
    this.dateToShortPrim = null;
    this.dateToShortPrimArray = null;
    this.dateToStringBased = null;
    this.dateToStringBasedArray = null;
    this.dateToStringBasedCollection = null;
    this.numberCollectionToBinary = null;
    this.numberCollectionToBinaryArray = null;
    this.numberCollectionToBinaryCollection = null;
    this.numberCollectionToBoolean = null;
    this.numberCollectionToBooleanArray = null;
    this.numberCollectionToBooleanCollection = null;
    this.numberCollectionToBooleanPrim = null;
    this.numberCollectionToBooleanPrimArray = null;
    this.numberCollectionToByte = null;
    this.numberCollectionToByteArray = null;
    this.numberCollectionToByteCollection = null;
    this.numberCollectionToBytePrim = null;
    this.numberCollectionToBytePrimArray = null;
    this.numberCollectionToCharacter = null;
    this.numberCollectionToCharacterArray = null;
    this.numberCollectionToCharacterCollection = null;
    this.numberCollectionToCharacterPrim = null;
    this.numberCollectionToCharacterPrimArray = null;
    this.numberCollectionToDateBased = null;
    this.numberCollectionToDateBasedArray = null;
    this.numberCollectionToDateBasedCollection = null;
    this.numberCollectionToDoublePrim = null;
    this.numberCollectionToDoublePrimArray = null;
    this.numberCollectionToEnum = null;
    this.numberCollectionToEnumArray = null;
    this.numberCollectionToEnumCollection = null;
    this.numberCollectionToFloatPrim = null;
    this.numberCollectionToFloatPrimArray = null;
    this.numberCollectionToIntegerPrim = null;
    this.numberCollectionToIntegerPrimArray = null;
    this.numberCollectionToLongPrim = null;
    this.numberCollectionToLongPrimArray = null;
    this.numberCollectionToNumberBased = null;
    this.numberCollectionToNumberBasedArray = null;
    this.numberCollectionToNumberBasedCollection = null;
    this.numberCollectionToObjectId = null;
    this.numberCollectionToObjectIdArray = null;
    this.numberCollectionToObjectIdCollection = null;
    this.numberCollectionToShortPrim = null;
    this.numberCollectionToShortPrimArray = null;
    this.numberCollectionToStringBased = null;
    this.numberCollectionToStringBasedArray = null;
    this.numberCollectionToStringBasedCollection = null;
    this.numberToBinary = null;
    this.numberToBinaryArray = null;
    this.numberToBinaryCollection = null;
    this.numberToBoolean = null;
    this.numberToBooleanArray = null;
    this.numberToBooleanCollection = null;
    this.numberToBooleanPrim = null;
    this.numberToBooleanPrimArray = null;
    this.numberToByte = null;
    this.numberToByteArray = null;
    this.numberToByteCollection = null;
    this.numberToBytePrim = null;
    this.numberToBytePrimArray = null;
    this.numberToCharacter = null;
    this.numberToCharacterArray = null;
    this.numberToCharacterCollection = null;
    this.numberToCharacterPrim = null;
    this.numberToCharacterPrimArray = null;
    this.numberToDateBased = null;
    this.numberToDateBasedArray = null;
    this.numberToDateBasedCollection = null;
    this.numberToDoublePrim = null;
    this.numberToDoublePrimArray = null;
    this.numberToEnum = null;
    this.numberToEnumArray = null;
    this.numberToEnumCollection = null;
    this.numberToFloatPrim = null;
    this.numberToFloatPrimArray = null;
    this.numberToIntegerPrim = null;
    this.numberToIntegerPrimArray = null;
    this.numberToLongPrim = null;
    this.numberToLongPrimArray = null;
    this.numberToNumberBased = null;
    this.numberToNumberBasedArray = null;
    this.numberToNumberBasedCollection = null;
    this.numberToObjectId = null;
    this.numberToObjectIdArray = null;
    this.numberToObjectIdCollection = null;
    this.numberToShortPrim = null;
    this.numberToShortPrimArray = null;
    this.numberToStringBased = null;
    this.numberToStringBasedArray = null;
    this.numberToStringBasedCollection = null;
    this.objectIdCollectionToBinary = null;
    this.objectIdCollectionToBinaryArray = null;
    this.objectIdCollectionToBinaryCollection = null;
    this.objectIdCollectionToBoolean = null;
    this.objectIdCollectionToBooleanArray = null;
    this.objectIdCollectionToBooleanCollection = null;
    this.objectIdCollectionToBooleanPrim = null;
    this.objectIdCollectionToBooleanPrimArray = null;
    this.objectIdCollectionToByte = null;
    this.objectIdCollectionToByteArray = null;
    this.objectIdCollectionToByteCollection = null;
    this.objectIdCollectionToBytePrim = null;
    this.objectIdCollectionToBytePrimArray = null;
    this.objectIdCollectionToCharacter = null;
    this.objectIdCollectionToCharacterArray = null;
    this.objectIdCollectionToCharacterCollection = null;
    this.objectIdCollectionToCharacterPrim = null;
    this.objectIdCollectionToCharacterPrimArray = null;
    this.objectIdCollectionToDateBased = null;
    this.objectIdCollectionToDateBasedArray = null;
    this.objectIdCollectionToDateBasedCollection = null;
    this.objectIdCollectionToDoublePrim = null;
    this.objectIdCollectionToDoublePrimArray = null;
    this.objectIdCollectionToEnum = null;
    this.objectIdCollectionToEnumArray = null;
    this.objectIdCollectionToEnumCollection = null;
    this.objectIdCollectionToFloatPrim = null;
    this.objectIdCollectionToFloatPrimArray = null;
    this.objectIdCollectionToIntegerPrim = null;
    this.objectIdCollectionToIntegerPrimArray = null;
    this.objectIdCollectionToLongPrim = null;
    this.objectIdCollectionToLongPrimArray = null;
    this.objectIdCollectionToNumberBased = null;
    this.objectIdCollectionToNumberBasedArray = null;
    this.objectIdCollectionToNumberBasedCollection = null;
    this.objectIdCollectionToObjectId = null;
    this.objectIdCollectionToObjectIdArray = null;
    this.objectIdCollectionToObjectIdCollection = null;
    this.objectIdCollectionToShortPrim = null;
    this.objectIdCollectionToShortPrimArray = null;
    this.objectIdCollectionToStringBased = null;
    this.objectIdCollectionToStringBasedArray = null;
    this.objectIdCollectionToStringBasedCollection = null;
    this.objectIdToBinary = null;
    this.objectIdToBinaryArray = null;
    this.objectIdToBinaryCollection = null;
    this.objectIdToBoolean = null;
    this.objectIdToBooleanArray = null;
    this.objectIdToBooleanCollection = null;
    this.objectIdToBooleanPrim = null;
    this.objectIdToBooleanPrimArray = null;
    this.objectIdToByte = null;
    this.objectIdToByteArray = null;
    this.objectIdToByteCollection = null;
    this.objectIdToBytePrim = null;
    this.objectIdToBytePrimArray = null;
    this.objectIdToCharacter = null;
    this.objectIdToCharacterArray = null;
    this.objectIdToCharacterCollection = null;
    this.objectIdToCharacterPrim = null;
    this.objectIdToCharacterPrimArray = null;
    this.objectIdToDateBased = null;
    this.objectIdToDateBasedArray = null;
    this.objectIdToDateBasedCollection = null;
    this.objectIdToDoublePrim = null;
    this.objectIdToDoublePrimArray = null;
    this.objectIdToEnum = null;
    this.objectIdToEnumArray = null;
    this.objectIdToEnumCollection = null;
    this.objectIdToFloatPrim = null;
    this.objectIdToFloatPrimArray = null;
    this.objectIdToIntegerPrim = null;
    this.objectIdToIntegerPrimArray = null;
    this.objectIdToLongPrim = null;
    this.objectIdToLongPrimArray = null;
    this.objectIdToNumberBased = null;
    this.objectIdToNumberBasedArray = null;
    this.objectIdToNumberBasedCollection = null;
    this.objectIdToObjectId = null;
    this.objectIdToObjectIdArray = null;
    this.objectIdToObjectIdCollection = null;
    this.objectIdToShortPrim = null;
    this.objectIdToShortPrimArray = null;
    this.objectIdToStringBased = null;
    this.objectIdToStringBasedArray = null;
    this.objectIdToStringBasedCollection = null;
    this.stringCollectionToBinary = null;
    this.stringCollectionToBinaryArray = null;
    this.stringCollectionToBinaryCollection = null;
    this.stringCollectionToBoolean = null;
    this.stringCollectionToBooleanArray = null;
    this.stringCollectionToBooleanCollection = null;
    this.stringCollectionToBooleanPrim = null;
    this.stringCollectionToBooleanPrimArray = null;
    this.stringCollectionToByte = null;
    this.stringCollectionToByteArray = null;
    this.stringCollectionToByteCollection = null;
    this.stringCollectionToBytePrim = null;
    this.stringCollectionToBytePrimArray = null;
    this.stringCollectionToCharacter = null;
    this.stringCollectionToCharacterArray = null;
    this.stringCollectionToCharacterCollection = null;
    this.stringCollectionToCharacterPrim = null;
    this.stringCollectionToCharacterPrimArray = null;
    this.stringCollectionToDateBased = null;
    this.stringCollectionToDateBasedArray = null;
    this.stringCollectionToDateBasedCollection = null;
    this.stringCollectionToDoublePrim = null;
    this.stringCollectionToDoublePrimArray = null;
    this.stringCollectionToEnum = null;
    this.stringCollectionToEnumArray = null;
    this.stringCollectionToEnumCollection = null;
    this.stringCollectionToFloatPrim = null;
    this.stringCollectionToFloatPrimArray = null;
    this.stringCollectionToIntegerPrim = null;
    this.stringCollectionToIntegerPrimArray = null;
    this.stringCollectionToLongPrim = null;
    this.stringCollectionToLongPrimArray = null;
    this.stringCollectionToNumberBased = null;
    this.stringCollectionToNumberBasedArray = null;
    this.stringCollectionToNumberBasedCollection = null;
    this.stringCollectionToObjectId = null;
    this.stringCollectionToObjectIdArray = null;
    this.stringCollectionToObjectIdCollection = null;
    this.stringCollectionToShortPrim = null;
    this.stringCollectionToShortPrimArray = null;
    this.stringCollectionToStringBased = null;
    this.stringCollectionToStringBasedArray = null;
    this.stringCollectionToStringBasedCollection = null;
    this.stringToBinary = null;
    this.stringToBinaryArray = null;
    this.stringToBinaryCollection = null;
    this.stringToBoolean = null;
    this.stringToBooleanArray = null;
    this.stringToBooleanCollection = null;
    this.stringToBooleanPrim = null;
    this.stringToBooleanPrimArray = null;
    this.stringToByte = null;
    this.stringToByteArray = null;
    this.stringToByteCollection = null;
    this.stringToBytePrim = null;
    this.stringToBytePrimArray = null;
    this.stringToCharacter = null;
    this.stringToCharacterArray = null;
    this.stringToCharacterCollection = null;
    this.stringToCharacterPrim = null;
    this.stringToCharacterPrimArray = null;
    this.stringToDateBased = null;
    this.stringToDateBasedArray = null;
    this.stringToDateBasedCollection = null;
    this.stringToDoublePrim = null;
    this.stringToDoublePrimArray = null;
    this.stringToEnum = null;
    this.stringToEnumArray = null;
    this.stringToEnumCollection = null;
    this.stringToFloatPrim = null;
    this.stringToFloatPrimArray = null;
    this.stringToIntegerPrim = null;
    this.stringToIntegerPrimArray = null;
    this.stringToLongPrim = null;
    this.stringToLongPrimArray = null;
    this.stringToNumberBased = null;
    this.stringToNumberBasedArray = null;
    this.stringToNumberBasedCollection = null;
    this.stringToObjectId = null;
    this.stringToObjectIdArray = null;
    this.stringToObjectIdCollection = null;
    this.stringToShortPrim = null;
    this.stringToShortPrimArray = null;
    this.stringToStringBased = null;
    this.stringToStringBasedArray = null;
    this.stringToStringBasedCollection = null;
  }

  private JaxBsonToObject(final Builder b) {
    this.binaryCollectionToBinary =
        Objects.requireNonNull(b.binaryCollectionToBinary, "Field 'binaryCollectionToBinary'");
    this.binaryCollectionToBinaryArray = Objects.requireNonNull(b.binaryCollectionToBinaryArray,
        "Field 'binaryCollectionToBinaryArray'");
    this.binaryCollectionToBinaryCollection = Objects.requireNonNull(
        b.binaryCollectionToBinaryCollection, "Field 'binaryCollectionToBinaryCollection'");
    this.binaryCollectionToBoolean =
        Objects.requireNonNull(b.binaryCollectionToBoolean, "Field 'binaryCollectionToBoolean'");
    this.binaryCollectionToBooleanArray = Objects.requireNonNull(b.binaryCollectionToBooleanArray,
        "Field 'binaryCollectionToBooleanArray'");
    this.binaryCollectionToBooleanCollection = Objects.requireNonNull(
        b.binaryCollectionToBooleanCollection, "Field 'binaryCollectionToBooleanCollection'");
    this.binaryCollectionToBooleanPrim = Objects.requireNonNull(b.binaryCollectionToBooleanPrim,
        "Field 'binaryCollectionToBooleanPrim'");
    this.binaryCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.binaryCollectionToBooleanPrimArray, "Field 'binaryCollectionToBooleanPrimArray'");
    this.binaryCollectionToByte =
        Objects.requireNonNull(b.binaryCollectionToByte, "Field 'binaryCollectionToByte'");
    this.binaryCollectionToByteArray = Objects.requireNonNull(b.binaryCollectionToByteArray,
        "Field 'binaryCollectionToByteArray'");
    this.binaryCollectionToByteCollection = Objects.requireNonNull(
        b.binaryCollectionToByteCollection, "Field 'binaryCollectionToByteCollection'");
    this.binaryCollectionToBytePrim =
        Objects.requireNonNull(b.binaryCollectionToBytePrim, "Field 'binaryCollectionToBytePrim'");
    this.binaryCollectionToBytePrimArray = Objects.requireNonNull(b.binaryCollectionToBytePrimArray,
        "Field 'binaryCollectionToBytePrimArray'");
    this.binaryCollectionToCharacter = Objects.requireNonNull(b.binaryCollectionToCharacter,
        "Field 'binaryCollectionToCharacter'");
    this.binaryCollectionToCharacterArray = Objects.requireNonNull(
        b.binaryCollectionToCharacterArray, "Field 'binaryCollectionToCharacterArray'");
    this.binaryCollectionToCharacterCollection = Objects.requireNonNull(
        b.binaryCollectionToCharacterCollection, "Field 'binaryCollectionToCharacterCollection'");
    this.binaryCollectionToCharacterPrim = Objects.requireNonNull(b.binaryCollectionToCharacterPrim,
        "Field 'binaryCollectionToCharacterPrim'");
    this.binaryCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.binaryCollectionToCharacterPrimArray, "Field 'binaryCollectionToCharacterPrimArray'");
    this.binaryCollectionToDateBased = Objects.requireNonNull(b.binaryCollectionToDateBased,
        "Field 'binaryCollectionToDateBased'");
    this.binaryCollectionToDateBasedArray = Objects.requireNonNull(
        b.binaryCollectionToDateBasedArray, "Field 'binaryCollectionToDateBasedArray'");
    this.binaryCollectionToDateBasedCollection = Objects.requireNonNull(
        b.binaryCollectionToDateBasedCollection, "Field 'binaryCollectionToDateBasedCollection'");
    this.binaryCollectionToDoublePrim = Objects.requireNonNull(b.binaryCollectionToDoublePrim,
        "Field 'binaryCollectionToDoublePrim'");
    this.binaryCollectionToDoublePrimArray = Objects.requireNonNull(
        b.binaryCollectionToDoublePrimArray, "Field 'binaryCollectionToDoublePrimArray'");
    this.binaryCollectionToEnum =
        Objects.requireNonNull(b.binaryCollectionToEnum, "Field 'binaryCollectionToEnum'");
    this.binaryCollectionToEnumArray = Objects.requireNonNull(b.binaryCollectionToEnumArray,
        "Field 'binaryCollectionToEnumArray'");
    this.binaryCollectionToEnumCollection = Objects.requireNonNull(
        b.binaryCollectionToEnumCollection, "Field 'binaryCollectionToEnumCollection'");
    this.binaryCollectionToFloatPrim = Objects.requireNonNull(b.binaryCollectionToFloatPrim,
        "Field 'binaryCollectionToFloatPrim'");
    this.binaryCollectionToFloatPrimArray = Objects.requireNonNull(
        b.binaryCollectionToFloatPrimArray, "Field 'binaryCollectionToFloatPrimArray'");
    this.binaryCollectionToIntegerPrim = Objects.requireNonNull(b.binaryCollectionToIntegerPrim,
        "Field 'binaryCollectionToIntegerPrim'");
    this.binaryCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.binaryCollectionToIntegerPrimArray, "Field 'binaryCollectionToIntegerPrimArray'");
    this.binaryCollectionToLongPrim =
        Objects.requireNonNull(b.binaryCollectionToLongPrim, "Field 'binaryCollectionToLongPrim'");
    this.binaryCollectionToLongPrimArray = Objects.requireNonNull(b.binaryCollectionToLongPrimArray,
        "Field 'binaryCollectionToLongPrimArray'");
    this.binaryCollectionToNumberBased = Objects.requireNonNull(b.binaryCollectionToNumberBased,
        "Field 'binaryCollectionToNumberBased'");
    this.binaryCollectionToNumberBasedArray = Objects.requireNonNull(
        b.binaryCollectionToNumberBasedArray, "Field 'binaryCollectionToNumberBasedArray'");
    this.binaryCollectionToNumberBasedCollection =
        Objects.requireNonNull(b.binaryCollectionToNumberBasedCollection,
            "Field 'binaryCollectionToNumberBasedCollection'");
    this.binaryCollectionToObjectId =
        Objects.requireNonNull(b.binaryCollectionToObjectId, "Field 'binaryCollectionToObjectId'");
    this.binaryCollectionToObjectIdArray = Objects.requireNonNull(b.binaryCollectionToObjectIdArray,
        "Field 'binaryCollectionToObjectIdArray'");
    this.binaryCollectionToObjectIdCollection = Objects.requireNonNull(
        b.binaryCollectionToObjectIdCollection, "Field 'binaryCollectionToObjectIdCollection'");
    this.binaryCollectionToShortPrim = Objects.requireNonNull(b.binaryCollectionToShortPrim,
        "Field 'binaryCollectionToShortPrim'");
    this.binaryCollectionToShortPrimArray = Objects.requireNonNull(
        b.binaryCollectionToShortPrimArray, "Field 'binaryCollectionToShortPrimArray'");
    this.binaryCollectionToStringBased = Objects.requireNonNull(b.binaryCollectionToStringBased,
        "Field 'binaryCollectionToStringBased'");
    this.binaryCollectionToStringBasedArray = Objects.requireNonNull(
        b.binaryCollectionToStringBasedArray, "Field 'binaryCollectionToStringBasedArray'");
    this.binaryCollectionToStringBasedCollection =
        Objects.requireNonNull(b.binaryCollectionToStringBasedCollection,
            "Field 'binaryCollectionToStringBasedCollection'");
    this.binaryToBinary = Objects.requireNonNull(b.binaryToBinary, "Field 'binaryToBinary'");
    this.binaryToBinaryArray =
        Objects.requireNonNull(b.binaryToBinaryArray, "Field 'binaryToBinaryArray'");
    this.binaryToBinaryCollection =
        Objects.requireNonNull(b.binaryToBinaryCollection, "Field 'binaryToBinaryCollection'");
    this.binaryToBoolean = Objects.requireNonNull(b.binaryToBoolean, "Field 'binaryToBoolean'");
    this.binaryToBooleanArray =
        Objects.requireNonNull(b.binaryToBooleanArray, "Field 'binaryToBooleanArray'");
    this.binaryToBooleanCollection =
        Objects.requireNonNull(b.binaryToBooleanCollection, "Field 'binaryToBooleanCollection'");
    this.binaryToBooleanPrim =
        Objects.requireNonNull(b.binaryToBooleanPrim, "Field 'binaryToBooleanPrim'");
    this.binaryToBooleanPrimArray =
        Objects.requireNonNull(b.binaryToBooleanPrimArray, "Field 'binaryToBooleanPrimArray'");
    this.binaryToByte = Objects.requireNonNull(b.binaryToByte, "Field 'binaryToByte'");
    this.binaryToByteArray =
        Objects.requireNonNull(b.binaryToByteArray, "Field 'binaryToByteArray'");
    this.binaryToByteCollection =
        Objects.requireNonNull(b.binaryToByteCollection, "Field 'binaryToByteCollection'");
    this.binaryToBytePrim = Objects.requireNonNull(b.binaryToBytePrim, "Field 'binaryToBytePrim'");
    this.binaryToBytePrimArray =
        Objects.requireNonNull(b.binaryToBytePrimArray, "Field 'binaryToBytePrimArray'");
    this.binaryToCharacter =
        Objects.requireNonNull(b.binaryToCharacter, "Field 'binaryToCharacter'");
    this.binaryToCharacterArray =
        Objects.requireNonNull(b.binaryToCharacterArray, "Field 'binaryToCharacterArray'");
    this.binaryToCharacterCollection = Objects.requireNonNull(b.binaryToCharacterCollection,
        "Field 'binaryToCharacterCollection'");
    this.binaryToCharacterPrim =
        Objects.requireNonNull(b.binaryToCharacterPrim, "Field 'binaryToCharacterPrim'");
    this.binaryToCharacterPrimArray =
        Objects.requireNonNull(b.binaryToCharacterPrimArray, "Field 'binaryToCharacterPrimArray'");
    this.binaryToDateBased =
        Objects.requireNonNull(b.binaryToDateBased, "Field 'binaryToDateBased'");
    this.binaryToDateBasedArray =
        Objects.requireNonNull(b.binaryToDateBasedArray, "Field 'binaryToDateBasedArray'");
    this.binaryToDateBasedCollection = Objects.requireNonNull(b.binaryToDateBasedCollection,
        "Field 'binaryToDateBasedCollection'");
    this.binaryToDoublePrim =
        Objects.requireNonNull(b.binaryToDoublePrim, "Field 'binaryToDoublePrim'");
    this.binaryToDoublePrimArray =
        Objects.requireNonNull(b.binaryToDoublePrimArray, "Field 'binaryToDoublePrimArray'");
    this.binaryToEnum = Objects.requireNonNull(b.binaryToEnum, "Field 'binaryToEnum'");
    this.binaryToEnumArray =
        Objects.requireNonNull(b.binaryToEnumArray, "Field 'binaryToEnumArray'");
    this.binaryToEnumCollection =
        Objects.requireNonNull(b.binaryToEnumCollection, "Field 'binaryToEnumCollection'");
    this.binaryToFloatPrim =
        Objects.requireNonNull(b.binaryToFloatPrim, "Field 'binaryToFloatPrim'");
    this.binaryToFloatPrimArray =
        Objects.requireNonNull(b.binaryToFloatPrimArray, "Field 'binaryToFloatPrimArray'");
    this.binaryToIntegerPrim =
        Objects.requireNonNull(b.binaryToIntegerPrim, "Field 'binaryToIntegerPrim'");
    this.binaryToIntegerPrimArray =
        Objects.requireNonNull(b.binaryToIntegerPrimArray, "Field 'binaryToIntegerPrimArray'");
    this.binaryToLongPrim = Objects.requireNonNull(b.binaryToLongPrim, "Field 'binaryToLongPrim'");
    this.binaryToLongPrimArray =
        Objects.requireNonNull(b.binaryToLongPrimArray, "Field 'binaryToLongPrimArray'");
    this.binaryToNumberBased =
        Objects.requireNonNull(b.binaryToNumberBased, "Field 'binaryToNumberBased'");
    this.binaryToNumberBasedArray =
        Objects.requireNonNull(b.binaryToNumberBasedArray, "Field 'binaryToNumberBasedArray'");
    this.binaryToNumberBasedCollection = Objects.requireNonNull(b.binaryToNumberBasedCollection,
        "Field 'binaryToNumberBasedCollection'");
    this.binaryToObjectId = Objects.requireNonNull(b.binaryToObjectId, "Field 'binaryToObjectId'");
    this.binaryToObjectIdArray =
        Objects.requireNonNull(b.binaryToObjectIdArray, "Field 'binaryToObjectIdArray'");
    this.binaryToObjectIdCollection =
        Objects.requireNonNull(b.binaryToObjectIdCollection, "Field 'binaryToObjectIdCollection'");
    this.binaryToShortPrim =
        Objects.requireNonNull(b.binaryToShortPrim, "Field 'binaryToShortPrim'");
    this.binaryToShortPrimArray =
        Objects.requireNonNull(b.binaryToShortPrimArray, "Field 'binaryToShortPrimArray'");
    this.binaryToStringBased =
        Objects.requireNonNull(b.binaryToStringBased, "Field 'binaryToStringBased'");
    this.binaryToStringBasedArray =
        Objects.requireNonNull(b.binaryToStringBasedArray, "Field 'binaryToStringBasedArray'");
    this.binaryToStringBasedCollection = Objects.requireNonNull(b.binaryToStringBasedCollection,
        "Field 'binaryToStringBasedCollection'");
    this.booleanCollectionToBinary =
        Objects.requireNonNull(b.booleanCollectionToBinary, "Field 'booleanCollectionToBinary'");
    this.booleanCollectionToBinaryArray = Objects.requireNonNull(b.booleanCollectionToBinaryArray,
        "Field 'booleanCollectionToBinaryArray'");
    this.booleanCollectionToBinaryCollection = Objects.requireNonNull(
        b.booleanCollectionToBinaryCollection, "Field 'booleanCollectionToBinaryCollection'");
    this.booleanCollectionToBoolean =
        Objects.requireNonNull(b.booleanCollectionToBoolean, "Field 'booleanCollectionToBoolean'");
    this.booleanCollectionToBooleanArray = Objects.requireNonNull(b.booleanCollectionToBooleanArray,
        "Field 'booleanCollectionToBooleanArray'");
    this.booleanCollectionToBooleanCollection = Objects.requireNonNull(
        b.booleanCollectionToBooleanCollection, "Field 'booleanCollectionToBooleanCollection'");
    this.booleanCollectionToBooleanPrim = Objects.requireNonNull(b.booleanCollectionToBooleanPrim,
        "Field 'booleanCollectionToBooleanPrim'");
    this.booleanCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.booleanCollectionToBooleanPrimArray, "Field 'booleanCollectionToBooleanPrimArray'");
    this.booleanCollectionToByte =
        Objects.requireNonNull(b.booleanCollectionToByte, "Field 'booleanCollectionToByte'");
    this.booleanCollectionToByteArray = Objects.requireNonNull(b.booleanCollectionToByteArray,
        "Field 'booleanCollectionToByteArray'");
    this.booleanCollectionToByteCollection = Objects.requireNonNull(
        b.booleanCollectionToByteCollection, "Field 'booleanCollectionToByteCollection'");
    this.booleanCollectionToBytePrim = Objects.requireNonNull(b.booleanCollectionToBytePrim,
        "Field 'booleanCollectionToBytePrim'");
    this.booleanCollectionToBytePrimArray = Objects.requireNonNull(
        b.booleanCollectionToBytePrimArray, "Field 'booleanCollectionToBytePrimArray'");
    this.booleanCollectionToCharacter = Objects.requireNonNull(b.booleanCollectionToCharacter,
        "Field 'booleanCollectionToCharacter'");
    this.booleanCollectionToCharacterArray = Objects.requireNonNull(
        b.booleanCollectionToCharacterArray, "Field 'booleanCollectionToCharacterArray'");
    this.booleanCollectionToCharacterCollection = Objects.requireNonNull(
        b.booleanCollectionToCharacterCollection, "Field 'booleanCollectionToCharacterCollection'");
    this.booleanCollectionToCharacterPrim = Objects.requireNonNull(
        b.booleanCollectionToCharacterPrim, "Field 'booleanCollectionToCharacterPrim'");
    this.booleanCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.booleanCollectionToCharacterPrimArray, "Field 'booleanCollectionToCharacterPrimArray'");
    this.booleanCollectionToDateBased = Objects.requireNonNull(b.booleanCollectionToDateBased,
        "Field 'booleanCollectionToDateBased'");
    this.booleanCollectionToDateBasedArray = Objects.requireNonNull(
        b.booleanCollectionToDateBasedArray, "Field 'booleanCollectionToDateBasedArray'");
    this.booleanCollectionToDateBasedCollection = Objects.requireNonNull(
        b.booleanCollectionToDateBasedCollection, "Field 'booleanCollectionToDateBasedCollection'");
    this.booleanCollectionToDoublePrim = Objects.requireNonNull(b.booleanCollectionToDoublePrim,
        "Field 'booleanCollectionToDoublePrim'");
    this.booleanCollectionToDoublePrimArray = Objects.requireNonNull(
        b.booleanCollectionToDoublePrimArray, "Field 'booleanCollectionToDoublePrimArray'");
    this.booleanCollectionToEnum =
        Objects.requireNonNull(b.booleanCollectionToEnum, "Field 'booleanCollectionToEnum'");
    this.booleanCollectionToEnumArray = Objects.requireNonNull(b.booleanCollectionToEnumArray,
        "Field 'booleanCollectionToEnumArray'");
    this.booleanCollectionToEnumCollection = Objects.requireNonNull(
        b.booleanCollectionToEnumCollection, "Field 'booleanCollectionToEnumCollection'");
    this.booleanCollectionToFloatPrim = Objects.requireNonNull(b.booleanCollectionToFloatPrim,
        "Field 'booleanCollectionToFloatPrim'");
    this.booleanCollectionToFloatPrimArray = Objects.requireNonNull(
        b.booleanCollectionToFloatPrimArray, "Field 'booleanCollectionToFloatPrimArray'");
    this.booleanCollectionToIntegerPrim = Objects.requireNonNull(b.booleanCollectionToIntegerPrim,
        "Field 'booleanCollectionToIntegerPrim'");
    this.booleanCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.booleanCollectionToIntegerPrimArray, "Field 'booleanCollectionToIntegerPrimArray'");
    this.booleanCollectionToLongPrim = Objects.requireNonNull(b.booleanCollectionToLongPrim,
        "Field 'booleanCollectionToLongPrim'");
    this.booleanCollectionToLongPrimArray = Objects.requireNonNull(
        b.booleanCollectionToLongPrimArray, "Field 'booleanCollectionToLongPrimArray'");
    this.booleanCollectionToNumberBased = Objects.requireNonNull(b.booleanCollectionToNumberBased,
        "Field 'booleanCollectionToNumberBased'");
    this.booleanCollectionToNumberBasedArray = Objects.requireNonNull(
        b.booleanCollectionToNumberBasedArray, "Field 'booleanCollectionToNumberBasedArray'");
    this.booleanCollectionToNumberBasedCollection =
        Objects.requireNonNull(b.booleanCollectionToNumberBasedCollection,
            "Field 'booleanCollectionToNumberBasedCollection'");
    this.booleanCollectionToObjectId = Objects.requireNonNull(b.booleanCollectionToObjectId,
        "Field 'booleanCollectionToObjectId'");
    this.booleanCollectionToObjectIdArray = Objects.requireNonNull(
        b.booleanCollectionToObjectIdArray, "Field 'booleanCollectionToObjectIdArray'");
    this.booleanCollectionToObjectIdCollection = Objects.requireNonNull(
        b.booleanCollectionToObjectIdCollection, "Field 'booleanCollectionToObjectIdCollection'");
    this.booleanCollectionToShortPrim = Objects.requireNonNull(b.booleanCollectionToShortPrim,
        "Field 'booleanCollectionToShortPrim'");
    this.booleanCollectionToShortPrimArray = Objects.requireNonNull(
        b.booleanCollectionToShortPrimArray, "Field 'booleanCollectionToShortPrimArray'");
    this.booleanCollectionToStringBased = Objects.requireNonNull(b.booleanCollectionToStringBased,
        "Field 'booleanCollectionToStringBased'");
    this.booleanCollectionToStringBasedArray = Objects.requireNonNull(
        b.booleanCollectionToStringBasedArray, "Field 'booleanCollectionToStringBasedArray'");
    this.booleanCollectionToStringBasedCollection =
        Objects.requireNonNull(b.booleanCollectionToStringBasedCollection,
            "Field 'booleanCollectionToStringBasedCollection'");
    this.booleanToBinary = Objects.requireNonNull(b.booleanToBinary, "Field 'booleanToBinary'");
    this.booleanToBinaryArray =
        Objects.requireNonNull(b.booleanToBinaryArray, "Field 'booleanToBinaryArray'");
    this.booleanToBinaryCollection =
        Objects.requireNonNull(b.booleanToBinaryCollection, "Field 'booleanToBinaryCollection'");
    this.booleanToBoolean = Objects.requireNonNull(b.booleanToBoolean, "Field 'booleanToBoolean'");
    this.booleanToBooleanArray =
        Objects.requireNonNull(b.booleanToBooleanArray, "Field 'booleanToBooleanArray'");
    this.booleanToBooleanCollection =
        Objects.requireNonNull(b.booleanToBooleanCollection, "Field 'booleanToBooleanCollection'");
    this.booleanToBooleanPrim =
        Objects.requireNonNull(b.booleanToBooleanPrim, "Field 'booleanToBooleanPrim'");
    this.booleanToBooleanPrimArray =
        Objects.requireNonNull(b.booleanToBooleanPrimArray, "Field 'booleanToBooleanPrimArray'");
    this.booleanToByte = Objects.requireNonNull(b.booleanToByte, "Field 'booleanToByte'");
    this.booleanToByteArray =
        Objects.requireNonNull(b.booleanToByteArray, "Field 'booleanToByteArray'");
    this.booleanToByteCollection =
        Objects.requireNonNull(b.booleanToByteCollection, "Field 'booleanToByteCollection'");
    this.booleanToBytePrim =
        Objects.requireNonNull(b.booleanToBytePrim, "Field 'booleanToBytePrim'");
    this.booleanToBytePrimArray =
        Objects.requireNonNull(b.booleanToBytePrimArray, "Field 'booleanToBytePrimArray'");
    this.booleanToCharacter =
        Objects.requireNonNull(b.booleanToCharacter, "Field 'booleanToCharacter'");
    this.booleanToCharacterArray =
        Objects.requireNonNull(b.booleanToCharacterArray, "Field 'booleanToCharacterArray'");
    this.booleanToCharacterCollection = Objects.requireNonNull(b.booleanToCharacterCollection,
        "Field 'booleanToCharacterCollection'");
    this.booleanToCharacterPrim =
        Objects.requireNonNull(b.booleanToCharacterPrim, "Field 'booleanToCharacterPrim'");
    this.booleanToCharacterPrimArray = Objects.requireNonNull(b.booleanToCharacterPrimArray,
        "Field 'booleanToCharacterPrimArray'");
    this.booleanToDateBased =
        Objects.requireNonNull(b.booleanToDateBased, "Field 'booleanToDateBased'");
    this.booleanToDateBasedArray =
        Objects.requireNonNull(b.booleanToDateBasedArray, "Field 'booleanToDateBasedArray'");
    this.booleanToDateBasedCollection = Objects.requireNonNull(b.booleanToDateBasedCollection,
        "Field 'booleanToDateBasedCollection'");
    this.booleanToDoublePrim =
        Objects.requireNonNull(b.booleanToDoublePrim, "Field 'booleanToDoublePrim'");
    this.booleanToDoublePrimArray =
        Objects.requireNonNull(b.booleanToDoublePrimArray, "Field 'booleanToDoublePrimArray'");
    this.booleanToEnum = Objects.requireNonNull(b.booleanToEnum, "Field 'booleanToEnum'");
    this.booleanToEnumArray =
        Objects.requireNonNull(b.booleanToEnumArray, "Field 'booleanToEnumArray'");
    this.booleanToEnumCollection =
        Objects.requireNonNull(b.booleanToEnumCollection, "Field 'booleanToEnumCollection'");
    this.booleanToFloatPrim =
        Objects.requireNonNull(b.booleanToFloatPrim, "Field 'booleanToFloatPrim'");
    this.booleanToFloatPrimArray =
        Objects.requireNonNull(b.booleanToFloatPrimArray, "Field 'booleanToFloatPrimArray'");
    this.booleanToIntegerPrim =
        Objects.requireNonNull(b.booleanToIntegerPrim, "Field 'booleanToIntegerPrim'");
    this.booleanToIntegerPrimArray =
        Objects.requireNonNull(b.booleanToIntegerPrimArray, "Field 'booleanToIntegerPrimArray'");
    this.booleanToLongPrim =
        Objects.requireNonNull(b.booleanToLongPrim, "Field 'booleanToLongPrim'");
    this.booleanToLongPrimArray =
        Objects.requireNonNull(b.booleanToLongPrimArray, "Field 'booleanToLongPrimArray'");
    this.booleanToNumberBased =
        Objects.requireNonNull(b.booleanToNumberBased, "Field 'booleanToNumberBased'");
    this.booleanToNumberBasedArray =
        Objects.requireNonNull(b.booleanToNumberBasedArray, "Field 'booleanToNumberBasedArray'");
    this.booleanToNumberBasedCollection = Objects.requireNonNull(b.booleanToNumberBasedCollection,
        "Field 'booleanToNumberBasedCollection'");
    this.booleanToObjectId =
        Objects.requireNonNull(b.booleanToObjectId, "Field 'booleanToObjectId'");
    this.booleanToObjectIdArray =
        Objects.requireNonNull(b.booleanToObjectIdArray, "Field 'booleanToObjectIdArray'");
    this.booleanToObjectIdCollection = Objects.requireNonNull(b.booleanToObjectIdCollection,
        "Field 'booleanToObjectIdCollection'");
    this.booleanToShortPrim =
        Objects.requireNonNull(b.booleanToShortPrim, "Field 'booleanToShortPrim'");
    this.booleanToShortPrimArray =
        Objects.requireNonNull(b.booleanToShortPrimArray, "Field 'booleanToShortPrimArray'");
    this.booleanToStringBased =
        Objects.requireNonNull(b.booleanToStringBased, "Field 'booleanToStringBased'");
    this.booleanToStringBasedArray =
        Objects.requireNonNull(b.booleanToStringBasedArray, "Field 'booleanToStringBasedArray'");
    this.booleanToStringBasedCollection = Objects.requireNonNull(b.booleanToStringBasedCollection,
        "Field 'booleanToStringBasedCollection'");
    this.dateCollectionToBinary =
        Objects.requireNonNull(b.dateCollectionToBinary, "Field 'dateCollectionToBinary'");
    this.dateCollectionToBinaryArray = Objects.requireNonNull(b.dateCollectionToBinaryArray,
        "Field 'dateCollectionToBinaryArray'");
    this.dateCollectionToBinaryCollection = Objects.requireNonNull(
        b.dateCollectionToBinaryCollection, "Field 'dateCollectionToBinaryCollection'");
    this.dateCollectionToBoolean =
        Objects.requireNonNull(b.dateCollectionToBoolean, "Field 'dateCollectionToBoolean'");
    this.dateCollectionToBooleanArray = Objects.requireNonNull(b.dateCollectionToBooleanArray,
        "Field 'dateCollectionToBooleanArray'");
    this.dateCollectionToBooleanCollection = Objects.requireNonNull(
        b.dateCollectionToBooleanCollection, "Field 'dateCollectionToBooleanCollection'");
    this.dateCollectionToBooleanPrim = Objects.requireNonNull(b.dateCollectionToBooleanPrim,
        "Field 'dateCollectionToBooleanPrim'");
    this.dateCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.dateCollectionToBooleanPrimArray, "Field 'dateCollectionToBooleanPrimArray'");
    this.dateCollectionToByte =
        Objects.requireNonNull(b.dateCollectionToByte, "Field 'dateCollectionToByte'");
    this.dateCollectionToByteArray =
        Objects.requireNonNull(b.dateCollectionToByteArray, "Field 'dateCollectionToByteArray'");
    this.dateCollectionToByteCollection = Objects.requireNonNull(b.dateCollectionToByteCollection,
        "Field 'dateCollectionToByteCollection'");
    this.dateCollectionToBytePrim =
        Objects.requireNonNull(b.dateCollectionToBytePrim, "Field 'dateCollectionToBytePrim'");
    this.dateCollectionToBytePrimArray = Objects.requireNonNull(b.dateCollectionToBytePrimArray,
        "Field 'dateCollectionToBytePrimArray'");
    this.dateCollectionToCharacter =
        Objects.requireNonNull(b.dateCollectionToCharacter, "Field 'dateCollectionToCharacter'");
    this.dateCollectionToCharacterArray = Objects.requireNonNull(b.dateCollectionToCharacterArray,
        "Field 'dateCollectionToCharacterArray'");
    this.dateCollectionToCharacterCollection = Objects.requireNonNull(
        b.dateCollectionToCharacterCollection, "Field 'dateCollectionToCharacterCollection'");
    this.dateCollectionToCharacterPrim = Objects.requireNonNull(b.dateCollectionToCharacterPrim,
        "Field 'dateCollectionToCharacterPrim'");
    this.dateCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.dateCollectionToCharacterPrimArray, "Field 'dateCollectionToCharacterPrimArray'");
    this.dateCollectionToDateBased =
        Objects.requireNonNull(b.dateCollectionToDateBased, "Field 'dateCollectionToDateBased'");
    this.dateCollectionToDateBasedArray = Objects.requireNonNull(b.dateCollectionToDateBasedArray,
        "Field 'dateCollectionToDateBasedArray'");
    this.dateCollectionToDateBasedCollection = Objects.requireNonNull(
        b.dateCollectionToDateBasedCollection, "Field 'dateCollectionToDateBasedCollection'");
    this.dateCollectionToDoublePrim =
        Objects.requireNonNull(b.dateCollectionToDoublePrim, "Field 'dateCollectionToDoublePrim'");
    this.dateCollectionToDoublePrimArray = Objects.requireNonNull(b.dateCollectionToDoublePrimArray,
        "Field 'dateCollectionToDoublePrimArray'");
    this.dateCollectionToEnum =
        Objects.requireNonNull(b.dateCollectionToEnum, "Field 'dateCollectionToEnum'");
    this.dateCollectionToEnumArray =
        Objects.requireNonNull(b.dateCollectionToEnumArray, "Field 'dateCollectionToEnumArray'");
    this.dateCollectionToEnumCollection = Objects.requireNonNull(b.dateCollectionToEnumCollection,
        "Field 'dateCollectionToEnumCollection'");
    this.dateCollectionToFloatPrim =
        Objects.requireNonNull(b.dateCollectionToFloatPrim, "Field 'dateCollectionToFloatPrim'");
    this.dateCollectionToFloatPrimArray = Objects.requireNonNull(b.dateCollectionToFloatPrimArray,
        "Field 'dateCollectionToFloatPrimArray'");
    this.dateCollectionToIntegerPrim = Objects.requireNonNull(b.dateCollectionToIntegerPrim,
        "Field 'dateCollectionToIntegerPrim'");
    this.dateCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.dateCollectionToIntegerPrimArray, "Field 'dateCollectionToIntegerPrimArray'");
    this.dateCollectionToLongPrim =
        Objects.requireNonNull(b.dateCollectionToLongPrim, "Field 'dateCollectionToLongPrim'");
    this.dateCollectionToLongPrimArray = Objects.requireNonNull(b.dateCollectionToLongPrimArray,
        "Field 'dateCollectionToLongPrimArray'");
    this.dateCollectionToNumberBased = Objects.requireNonNull(b.dateCollectionToNumberBased,
        "Field 'dateCollectionToNumberBased'");
    this.dateCollectionToNumberBasedArray = Objects.requireNonNull(
        b.dateCollectionToNumberBasedArray, "Field 'dateCollectionToNumberBasedArray'");
    this.dateCollectionToNumberBasedCollection = Objects.requireNonNull(
        b.dateCollectionToNumberBasedCollection, "Field 'dateCollectionToNumberBasedCollection'");
    this.dateCollectionToObjectId =
        Objects.requireNonNull(b.dateCollectionToObjectId, "Field 'dateCollectionToObjectId'");
    this.dateCollectionToObjectIdArray = Objects.requireNonNull(b.dateCollectionToObjectIdArray,
        "Field 'dateCollectionToObjectIdArray'");
    this.dateCollectionToObjectIdCollection = Objects.requireNonNull(
        b.dateCollectionToObjectIdCollection, "Field 'dateCollectionToObjectIdCollection'");
    this.dateCollectionToShortPrim =
        Objects.requireNonNull(b.dateCollectionToShortPrim, "Field 'dateCollectionToShortPrim'");
    this.dateCollectionToShortPrimArray = Objects.requireNonNull(b.dateCollectionToShortPrimArray,
        "Field 'dateCollectionToShortPrimArray'");
    this.dateCollectionToStringBased = Objects.requireNonNull(b.dateCollectionToStringBased,
        "Field 'dateCollectionToStringBased'");
    this.dateCollectionToStringBasedArray = Objects.requireNonNull(
        b.dateCollectionToStringBasedArray, "Field 'dateCollectionToStringBasedArray'");
    this.dateCollectionToStringBasedCollection = Objects.requireNonNull(
        b.dateCollectionToStringBasedCollection, "Field 'dateCollectionToStringBasedCollection'");
    this.dateToBinary = Objects.requireNonNull(b.dateToBinary, "Field 'dateToBinary'");
    this.dateToBinaryArray =
        Objects.requireNonNull(b.dateToBinaryArray, "Field 'dateToBinaryArray'");
    this.dateToBinaryCollection =
        Objects.requireNonNull(b.dateToBinaryCollection, "Field 'dateToBinaryCollection'");
    this.dateToBoolean = Objects.requireNonNull(b.dateToBoolean, "Field 'dateToBoolean'");
    this.dateToBooleanArray =
        Objects.requireNonNull(b.dateToBooleanArray, "Field 'dateToBooleanArray'");
    this.dateToBooleanCollection =
        Objects.requireNonNull(b.dateToBooleanCollection, "Field 'dateToBooleanCollection'");
    this.dateToBooleanPrim =
        Objects.requireNonNull(b.dateToBooleanPrim, "Field 'dateToBooleanPrim'");
    this.dateToBooleanPrimArray =
        Objects.requireNonNull(b.dateToBooleanPrimArray, "Field 'dateToBooleanPrimArray'");
    this.dateToByte = Objects.requireNonNull(b.dateToByte, "Field 'dateToByte'");
    this.dateToByteArray = Objects.requireNonNull(b.dateToByteArray, "Field 'dateToByteArray'");
    this.dateToByteCollection =
        Objects.requireNonNull(b.dateToByteCollection, "Field 'dateToByteCollection'");
    this.dateToBytePrim = Objects.requireNonNull(b.dateToBytePrim, "Field 'dateToBytePrim'");
    this.dateToBytePrimArray =
        Objects.requireNonNull(b.dateToBytePrimArray, "Field 'dateToBytePrimArray'");
    this.dateToCharacter = Objects.requireNonNull(b.dateToCharacter, "Field 'dateToCharacter'");
    this.dateToCharacterArray =
        Objects.requireNonNull(b.dateToCharacterArray, "Field 'dateToCharacterArray'");
    this.dateToCharacterCollection =
        Objects.requireNonNull(b.dateToCharacterCollection, "Field 'dateToCharacterCollection'");
    this.dateToCharacterPrim =
        Objects.requireNonNull(b.dateToCharacterPrim, "Field 'dateToCharacterPrim'");
    this.dateToCharacterPrimArray =
        Objects.requireNonNull(b.dateToCharacterPrimArray, "Field 'dateToCharacterPrimArray'");
    this.dateToDateBased = Objects.requireNonNull(b.dateToDateBased, "Field 'dateToDateBased'");
    this.dateToDateBasedArray =
        Objects.requireNonNull(b.dateToDateBasedArray, "Field 'dateToDateBasedArray'");
    this.dateToDateBasedCollection =
        Objects.requireNonNull(b.dateToDateBasedCollection, "Field 'dateToDateBasedCollection'");
    this.dateToDoublePrim = Objects.requireNonNull(b.dateToDoublePrim, "Field 'dateToDoublePrim'");
    this.dateToDoublePrimArray =
        Objects.requireNonNull(b.dateToDoublePrimArray, "Field 'dateToDoublePrimArray'");
    this.dateToEnum = Objects.requireNonNull(b.dateToEnum, "Field 'dateToEnum'");
    this.dateToEnumArray = Objects.requireNonNull(b.dateToEnumArray, "Field 'dateToEnumArray'");
    this.dateToEnumCollection =
        Objects.requireNonNull(b.dateToEnumCollection, "Field 'dateToEnumCollection'");
    this.dateToFloatPrim = Objects.requireNonNull(b.dateToFloatPrim, "Field 'dateToFloatPrim'");
    this.dateToFloatPrimArray =
        Objects.requireNonNull(b.dateToFloatPrimArray, "Field 'dateToFloatPrimArray'");
    this.dateToIntegerPrim =
        Objects.requireNonNull(b.dateToIntegerPrim, "Field 'dateToIntegerPrim'");
    this.dateToIntegerPrimArray =
        Objects.requireNonNull(b.dateToIntegerPrimArray, "Field 'dateToIntegerPrimArray'");
    this.dateToLongPrim = Objects.requireNonNull(b.dateToLongPrim, "Field 'dateToLongPrim'");
    this.dateToLongPrimArray =
        Objects.requireNonNull(b.dateToLongPrimArray, "Field 'dateToLongPrimArray'");
    this.dateToNumberBased =
        Objects.requireNonNull(b.dateToNumberBased, "Field 'dateToNumberBased'");
    this.dateToNumberBasedArray =
        Objects.requireNonNull(b.dateToNumberBasedArray, "Field 'dateToNumberBasedArray'");
    this.dateToNumberBasedCollection = Objects.requireNonNull(b.dateToNumberBasedCollection,
        "Field 'dateToNumberBasedCollection'");
    this.dateToObjectId = Objects.requireNonNull(b.dateToObjectId, "Field 'dateToObjectId'");
    this.dateToObjectIdArray =
        Objects.requireNonNull(b.dateToObjectIdArray, "Field 'dateToObjectIdArray'");
    this.dateToObjectIdCollection =
        Objects.requireNonNull(b.dateToObjectIdCollection, "Field 'dateToObjectIdCollection'");
    this.dateToShortPrim = Objects.requireNonNull(b.dateToShortPrim, "Field 'dateToShortPrim'");
    this.dateToShortPrimArray =
        Objects.requireNonNull(b.dateToShortPrimArray, "Field 'dateToShortPrimArray'");
    this.dateToStringBased =
        Objects.requireNonNull(b.dateToStringBased, "Field 'dateToStringBased'");
    this.dateToStringBasedArray =
        Objects.requireNonNull(b.dateToStringBasedArray, "Field 'dateToStringBasedArray'");
    this.dateToStringBasedCollection = Objects.requireNonNull(b.dateToStringBasedCollection,
        "Field 'dateToStringBasedCollection'");
    this.numberCollectionToBinary =
        Objects.requireNonNull(b.numberCollectionToBinary, "Field 'numberCollectionToBinary'");
    this.numberCollectionToBinaryArray = Objects.requireNonNull(b.numberCollectionToBinaryArray,
        "Field 'numberCollectionToBinaryArray'");
    this.numberCollectionToBinaryCollection = Objects.requireNonNull(
        b.numberCollectionToBinaryCollection, "Field 'numberCollectionToBinaryCollection'");
    this.numberCollectionToBoolean =
        Objects.requireNonNull(b.numberCollectionToBoolean, "Field 'numberCollectionToBoolean'");
    this.numberCollectionToBooleanArray = Objects.requireNonNull(b.numberCollectionToBooleanArray,
        "Field 'numberCollectionToBooleanArray'");
    this.numberCollectionToBooleanCollection = Objects.requireNonNull(
        b.numberCollectionToBooleanCollection, "Field 'numberCollectionToBooleanCollection'");
    this.numberCollectionToBooleanPrim = Objects.requireNonNull(b.numberCollectionToBooleanPrim,
        "Field 'numberCollectionToBooleanPrim'");
    this.numberCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.numberCollectionToBooleanPrimArray, "Field 'numberCollectionToBooleanPrimArray'");
    this.numberCollectionToByte =
        Objects.requireNonNull(b.numberCollectionToByte, "Field 'numberCollectionToByte'");
    this.numberCollectionToByteArray = Objects.requireNonNull(b.numberCollectionToByteArray,
        "Field 'numberCollectionToByteArray'");
    this.numberCollectionToByteCollection = Objects.requireNonNull(
        b.numberCollectionToByteCollection, "Field 'numberCollectionToByteCollection'");
    this.numberCollectionToBytePrim =
        Objects.requireNonNull(b.numberCollectionToBytePrim, "Field 'numberCollectionToBytePrim'");
    this.numberCollectionToBytePrimArray = Objects.requireNonNull(b.numberCollectionToBytePrimArray,
        "Field 'numberCollectionToBytePrimArray'");
    this.numberCollectionToCharacter = Objects.requireNonNull(b.numberCollectionToCharacter,
        "Field 'numberCollectionToCharacter'");
    this.numberCollectionToCharacterArray = Objects.requireNonNull(
        b.numberCollectionToCharacterArray, "Field 'numberCollectionToCharacterArray'");
    this.numberCollectionToCharacterCollection = Objects.requireNonNull(
        b.numberCollectionToCharacterCollection, "Field 'numberCollectionToCharacterCollection'");
    this.numberCollectionToCharacterPrim = Objects.requireNonNull(b.numberCollectionToCharacterPrim,
        "Field 'numberCollectionToCharacterPrim'");
    this.numberCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.numberCollectionToCharacterPrimArray, "Field 'numberCollectionToCharacterPrimArray'");
    this.numberCollectionToDateBased = Objects.requireNonNull(b.numberCollectionToDateBased,
        "Field 'numberCollectionToDateBased'");
    this.numberCollectionToDateBasedArray = Objects.requireNonNull(
        b.numberCollectionToDateBasedArray, "Field 'numberCollectionToDateBasedArray'");
    this.numberCollectionToDateBasedCollection = Objects.requireNonNull(
        b.numberCollectionToDateBasedCollection, "Field 'numberCollectionToDateBasedCollection'");
    this.numberCollectionToDoublePrim = Objects.requireNonNull(b.numberCollectionToDoublePrim,
        "Field 'numberCollectionToDoublePrim'");
    this.numberCollectionToDoublePrimArray = Objects.requireNonNull(
        b.numberCollectionToDoublePrimArray, "Field 'numberCollectionToDoublePrimArray'");
    this.numberCollectionToEnum =
        Objects.requireNonNull(b.numberCollectionToEnum, "Field 'numberCollectionToEnum'");
    this.numberCollectionToEnumArray = Objects.requireNonNull(b.numberCollectionToEnumArray,
        "Field 'numberCollectionToEnumArray'");
    this.numberCollectionToEnumCollection = Objects.requireNonNull(
        b.numberCollectionToEnumCollection, "Field 'numberCollectionToEnumCollection'");
    this.numberCollectionToFloatPrim = Objects.requireNonNull(b.numberCollectionToFloatPrim,
        "Field 'numberCollectionToFloatPrim'");
    this.numberCollectionToFloatPrimArray = Objects.requireNonNull(
        b.numberCollectionToFloatPrimArray, "Field 'numberCollectionToFloatPrimArray'");
    this.numberCollectionToIntegerPrim = Objects.requireNonNull(b.numberCollectionToIntegerPrim,
        "Field 'numberCollectionToIntegerPrim'");
    this.numberCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.numberCollectionToIntegerPrimArray, "Field 'numberCollectionToIntegerPrimArray'");
    this.numberCollectionToLongPrim =
        Objects.requireNonNull(b.numberCollectionToLongPrim, "Field 'numberCollectionToLongPrim'");
    this.numberCollectionToLongPrimArray = Objects.requireNonNull(b.numberCollectionToLongPrimArray,
        "Field 'numberCollectionToLongPrimArray'");
    this.numberCollectionToNumberBased = Objects.requireNonNull(b.numberCollectionToNumberBased,
        "Field 'numberCollectionToNumberBased'");
    this.numberCollectionToNumberBasedArray = Objects.requireNonNull(
        b.numberCollectionToNumberBasedArray, "Field 'numberCollectionToNumberBasedArray'");
    this.numberCollectionToNumberBasedCollection =
        Objects.requireNonNull(b.numberCollectionToNumberBasedCollection,
            "Field 'numberCollectionToNumberBasedCollection'");
    this.numberCollectionToObjectId =
        Objects.requireNonNull(b.numberCollectionToObjectId, "Field 'numberCollectionToObjectId'");
    this.numberCollectionToObjectIdArray = Objects.requireNonNull(b.numberCollectionToObjectIdArray,
        "Field 'numberCollectionToObjectIdArray'");
    this.numberCollectionToObjectIdCollection = Objects.requireNonNull(
        b.numberCollectionToObjectIdCollection, "Field 'numberCollectionToObjectIdCollection'");
    this.numberCollectionToShortPrim = Objects.requireNonNull(b.numberCollectionToShortPrim,
        "Field 'numberCollectionToShortPrim'");
    this.numberCollectionToShortPrimArray = Objects.requireNonNull(
        b.numberCollectionToShortPrimArray, "Field 'numberCollectionToShortPrimArray'");
    this.numberCollectionToStringBased = Objects.requireNonNull(b.numberCollectionToStringBased,
        "Field 'numberCollectionToStringBased'");
    this.numberCollectionToStringBasedArray = Objects.requireNonNull(
        b.numberCollectionToStringBasedArray, "Field 'numberCollectionToStringBasedArray'");
    this.numberCollectionToStringBasedCollection =
        Objects.requireNonNull(b.numberCollectionToStringBasedCollection,
            "Field 'numberCollectionToStringBasedCollection'");
    this.numberToBinary = Objects.requireNonNull(b.numberToBinary, "Field 'numberToBinary'");
    this.numberToBinaryArray =
        Objects.requireNonNull(b.numberToBinaryArray, "Field 'numberToBinaryArray'");
    this.numberToBinaryCollection =
        Objects.requireNonNull(b.numberToBinaryCollection, "Field 'numberToBinaryCollection'");
    this.numberToBoolean = Objects.requireNonNull(b.numberToBoolean, "Field 'numberToBoolean'");
    this.numberToBooleanArray =
        Objects.requireNonNull(b.numberToBooleanArray, "Field 'numberToBooleanArray'");
    this.numberToBooleanCollection =
        Objects.requireNonNull(b.numberToBooleanCollection, "Field 'numberToBooleanCollection'");
    this.numberToBooleanPrim =
        Objects.requireNonNull(b.numberToBooleanPrim, "Field 'numberToBooleanPrim'");
    this.numberToBooleanPrimArray =
        Objects.requireNonNull(b.numberToBooleanPrimArray, "Field 'numberToBooleanPrimArray'");
    this.numberToByte = Objects.requireNonNull(b.numberToByte, "Field 'numberToByte'");
    this.numberToByteArray =
        Objects.requireNonNull(b.numberToByteArray, "Field 'numberToByteArray'");
    this.numberToByteCollection =
        Objects.requireNonNull(b.numberToByteCollection, "Field 'numberToByteCollection'");
    this.numberToBytePrim = Objects.requireNonNull(b.numberToBytePrim, "Field 'numberToBytePrim'");
    this.numberToBytePrimArray =
        Objects.requireNonNull(b.numberToBytePrimArray, "Field 'numberToBytePrimArray'");
    this.numberToCharacter =
        Objects.requireNonNull(b.numberToCharacter, "Field 'numberToCharacter'");
    this.numberToCharacterArray =
        Objects.requireNonNull(b.numberToCharacterArray, "Field 'numberToCharacterArray'");
    this.numberToCharacterCollection = Objects.requireNonNull(b.numberToCharacterCollection,
        "Field 'numberToCharacterCollection'");
    this.numberToCharacterPrim =
        Objects.requireNonNull(b.numberToCharacterPrim, "Field 'numberToCharacterPrim'");
    this.numberToCharacterPrimArray =
        Objects.requireNonNull(b.numberToCharacterPrimArray, "Field 'numberToCharacterPrimArray'");
    this.numberToDateBased =
        Objects.requireNonNull(b.numberToDateBased, "Field 'numberToDateBased'");
    this.numberToDateBasedArray =
        Objects.requireNonNull(b.numberToDateBasedArray, "Field 'numberToDateBasedArray'");
    this.numberToDateBasedCollection = Objects.requireNonNull(b.numberToDateBasedCollection,
        "Field 'numberToDateBasedCollection'");
    this.numberToDoublePrim =
        Objects.requireNonNull(b.numberToDoublePrim, "Field 'numberToDoublePrim'");
    this.numberToDoublePrimArray =
        Objects.requireNonNull(b.numberToDoublePrimArray, "Field 'numberToDoublePrimArray'");
    this.numberToEnum = Objects.requireNonNull(b.numberToEnum, "Field 'numberToEnum'");
    this.numberToEnumArray =
        Objects.requireNonNull(b.numberToEnumArray, "Field 'numberToEnumArray'");
    this.numberToEnumCollection =
        Objects.requireNonNull(b.numberToEnumCollection, "Field 'numberToEnumCollection'");
    this.numberToFloatPrim =
        Objects.requireNonNull(b.numberToFloatPrim, "Field 'numberToFloatPrim'");
    this.numberToFloatPrimArray =
        Objects.requireNonNull(b.numberToFloatPrimArray, "Field 'numberToFloatPrimArray'");
    this.numberToIntegerPrim =
        Objects.requireNonNull(b.numberToIntegerPrim, "Field 'numberToIntegerPrim'");
    this.numberToIntegerPrimArray =
        Objects.requireNonNull(b.numberToIntegerPrimArray, "Field 'numberToIntegerPrimArray'");
    this.numberToLongPrim = Objects.requireNonNull(b.numberToLongPrim, "Field 'numberToLongPrim'");
    this.numberToLongPrimArray =
        Objects.requireNonNull(b.numberToLongPrimArray, "Field 'numberToLongPrimArray'");
    this.numberToNumberBased =
        Objects.requireNonNull(b.numberToNumberBased, "Field 'numberToNumberBased'");
    this.numberToNumberBasedArray =
        Objects.requireNonNull(b.numberToNumberBasedArray, "Field 'numberToNumberBasedArray'");
    this.numberToNumberBasedCollection = Objects.requireNonNull(b.numberToNumberBasedCollection,
        "Field 'numberToNumberBasedCollection'");
    this.numberToObjectId = Objects.requireNonNull(b.numberToObjectId, "Field 'numberToObjectId'");
    this.numberToObjectIdArray =
        Objects.requireNonNull(b.numberToObjectIdArray, "Field 'numberToObjectIdArray'");
    this.numberToObjectIdCollection =
        Objects.requireNonNull(b.numberToObjectIdCollection, "Field 'numberToObjectIdCollection'");
    this.numberToShortPrim =
        Objects.requireNonNull(b.numberToShortPrim, "Field 'numberToShortPrim'");
    this.numberToShortPrimArray =
        Objects.requireNonNull(b.numberToShortPrimArray, "Field 'numberToShortPrimArray'");
    this.numberToStringBased =
        Objects.requireNonNull(b.numberToStringBased, "Field 'numberToStringBased'");
    this.numberToStringBasedArray =
        Objects.requireNonNull(b.numberToStringBasedArray, "Field 'numberToStringBasedArray'");
    this.numberToStringBasedCollection = Objects.requireNonNull(b.numberToStringBasedCollection,
        "Field 'numberToStringBasedCollection'");
    this.objectIdCollectionToBinary =
        Objects.requireNonNull(b.objectIdCollectionToBinary, "Field 'objectIdCollectionToBinary'");
    this.objectIdCollectionToBinaryArray = Objects.requireNonNull(b.objectIdCollectionToBinaryArray,
        "Field 'objectIdCollectionToBinaryArray'");
    this.objectIdCollectionToBinaryCollection = Objects.requireNonNull(
        b.objectIdCollectionToBinaryCollection, "Field 'objectIdCollectionToBinaryCollection'");
    this.objectIdCollectionToBoolean = Objects.requireNonNull(b.objectIdCollectionToBoolean,
        "Field 'objectIdCollectionToBoolean'");
    this.objectIdCollectionToBooleanArray = Objects.requireNonNull(
        b.objectIdCollectionToBooleanArray, "Field 'objectIdCollectionToBooleanArray'");
    this.objectIdCollectionToBooleanCollection = Objects.requireNonNull(
        b.objectIdCollectionToBooleanCollection, "Field 'objectIdCollectionToBooleanCollection'");
    this.objectIdCollectionToBooleanPrim = Objects.requireNonNull(b.objectIdCollectionToBooleanPrim,
        "Field 'objectIdCollectionToBooleanPrim'");
    this.objectIdCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToBooleanPrimArray, "Field 'objectIdCollectionToBooleanPrimArray'");
    this.objectIdCollectionToByte =
        Objects.requireNonNull(b.objectIdCollectionToByte, "Field 'objectIdCollectionToByte'");
    this.objectIdCollectionToByteArray = Objects.requireNonNull(b.objectIdCollectionToByteArray,
        "Field 'objectIdCollectionToByteArray'");
    this.objectIdCollectionToByteCollection = Objects.requireNonNull(
        b.objectIdCollectionToByteCollection, "Field 'objectIdCollectionToByteCollection'");
    this.objectIdCollectionToBytePrim = Objects.requireNonNull(b.objectIdCollectionToBytePrim,
        "Field 'objectIdCollectionToBytePrim'");
    this.objectIdCollectionToBytePrimArray = Objects.requireNonNull(
        b.objectIdCollectionToBytePrimArray, "Field 'objectIdCollectionToBytePrimArray'");
    this.objectIdCollectionToCharacter = Objects.requireNonNull(b.objectIdCollectionToCharacter,
        "Field 'objectIdCollectionToCharacter'");
    this.objectIdCollectionToCharacterArray = Objects.requireNonNull(
        b.objectIdCollectionToCharacterArray, "Field 'objectIdCollectionToCharacterArray'");
    this.objectIdCollectionToCharacterCollection =
        Objects.requireNonNull(b.objectIdCollectionToCharacterCollection,
            "Field 'objectIdCollectionToCharacterCollection'");
    this.objectIdCollectionToCharacterPrim = Objects.requireNonNull(
        b.objectIdCollectionToCharacterPrim, "Field 'objectIdCollectionToCharacterPrim'");
    this.objectIdCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToCharacterPrimArray, "Field 'objectIdCollectionToCharacterPrimArray'");
    this.objectIdCollectionToDateBased = Objects.requireNonNull(b.objectIdCollectionToDateBased,
        "Field 'objectIdCollectionToDateBased'");
    this.objectIdCollectionToDateBasedArray = Objects.requireNonNull(
        b.objectIdCollectionToDateBasedArray, "Field 'objectIdCollectionToDateBasedArray'");
    this.objectIdCollectionToDateBasedCollection =
        Objects.requireNonNull(b.objectIdCollectionToDateBasedCollection,
            "Field 'objectIdCollectionToDateBasedCollection'");
    this.objectIdCollectionToDoublePrim = Objects.requireNonNull(b.objectIdCollectionToDoublePrim,
        "Field 'objectIdCollectionToDoublePrim'");
    this.objectIdCollectionToDoublePrimArray = Objects.requireNonNull(
        b.objectIdCollectionToDoublePrimArray, "Field 'objectIdCollectionToDoublePrimArray'");
    this.objectIdCollectionToEnum =
        Objects.requireNonNull(b.objectIdCollectionToEnum, "Field 'objectIdCollectionToEnum'");
    this.objectIdCollectionToEnumArray = Objects.requireNonNull(b.objectIdCollectionToEnumArray,
        "Field 'objectIdCollectionToEnumArray'");
    this.objectIdCollectionToEnumCollection = Objects.requireNonNull(
        b.objectIdCollectionToEnumCollection, "Field 'objectIdCollectionToEnumCollection'");
    this.objectIdCollectionToFloatPrim = Objects.requireNonNull(b.objectIdCollectionToFloatPrim,
        "Field 'objectIdCollectionToFloatPrim'");
    this.objectIdCollectionToFloatPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToFloatPrimArray, "Field 'objectIdCollectionToFloatPrimArray'");
    this.objectIdCollectionToIntegerPrim = Objects.requireNonNull(b.objectIdCollectionToIntegerPrim,
        "Field 'objectIdCollectionToIntegerPrim'");
    this.objectIdCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToIntegerPrimArray, "Field 'objectIdCollectionToIntegerPrimArray'");
    this.objectIdCollectionToLongPrim = Objects.requireNonNull(b.objectIdCollectionToLongPrim,
        "Field 'objectIdCollectionToLongPrim'");
    this.objectIdCollectionToLongPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToLongPrimArray, "Field 'objectIdCollectionToLongPrimArray'");
    this.objectIdCollectionToNumberBased = Objects.requireNonNull(b.objectIdCollectionToNumberBased,
        "Field 'objectIdCollectionToNumberBased'");
    this.objectIdCollectionToNumberBasedArray = Objects.requireNonNull(
        b.objectIdCollectionToNumberBasedArray, "Field 'objectIdCollectionToNumberBasedArray'");
    this.objectIdCollectionToNumberBasedCollection =
        Objects.requireNonNull(b.objectIdCollectionToNumberBasedCollection,
            "Field 'objectIdCollectionToNumberBasedCollection'");
    this.objectIdCollectionToObjectId = Objects.requireNonNull(b.objectIdCollectionToObjectId,
        "Field 'objectIdCollectionToObjectId'");
    this.objectIdCollectionToObjectIdArray = Objects.requireNonNull(
        b.objectIdCollectionToObjectIdArray, "Field 'objectIdCollectionToObjectIdArray'");
    this.objectIdCollectionToObjectIdCollection = Objects.requireNonNull(
        b.objectIdCollectionToObjectIdCollection, "Field 'objectIdCollectionToObjectIdCollection'");
    this.objectIdCollectionToShortPrim = Objects.requireNonNull(b.objectIdCollectionToShortPrim,
        "Field 'objectIdCollectionToShortPrim'");
    this.objectIdCollectionToShortPrimArray = Objects.requireNonNull(
        b.objectIdCollectionToShortPrimArray, "Field 'objectIdCollectionToShortPrimArray'");
    this.objectIdCollectionToStringBased = Objects.requireNonNull(b.objectIdCollectionToStringBased,
        "Field 'objectIdCollectionToStringBased'");
    this.objectIdCollectionToStringBasedArray = Objects.requireNonNull(
        b.objectIdCollectionToStringBasedArray, "Field 'objectIdCollectionToStringBasedArray'");
    this.objectIdCollectionToStringBasedCollection =
        Objects.requireNonNull(b.objectIdCollectionToStringBasedCollection,
            "Field 'objectIdCollectionToStringBasedCollection'");
    this.objectIdToBinary = Objects.requireNonNull(b.objectIdToBinary, "Field 'objectIdToBinary'");
    this.objectIdToBinaryArray =
        Objects.requireNonNull(b.objectIdToBinaryArray, "Field 'objectIdToBinaryArray'");
    this.objectIdToBinaryCollection =
        Objects.requireNonNull(b.objectIdToBinaryCollection, "Field 'objectIdToBinaryCollection'");
    this.objectIdToBoolean =
        Objects.requireNonNull(b.objectIdToBoolean, "Field 'objectIdToBoolean'");
    this.objectIdToBooleanArray =
        Objects.requireNonNull(b.objectIdToBooleanArray, "Field 'objectIdToBooleanArray'");
    this.objectIdToBooleanCollection = Objects.requireNonNull(b.objectIdToBooleanCollection,
        "Field 'objectIdToBooleanCollection'");
    this.objectIdToBooleanPrim =
        Objects.requireNonNull(b.objectIdToBooleanPrim, "Field 'objectIdToBooleanPrim'");
    this.objectIdToBooleanPrimArray =
        Objects.requireNonNull(b.objectIdToBooleanPrimArray, "Field 'objectIdToBooleanPrimArray'");
    this.objectIdToByte = Objects.requireNonNull(b.objectIdToByte, "Field 'objectIdToByte'");
    this.objectIdToByteArray =
        Objects.requireNonNull(b.objectIdToByteArray, "Field 'objectIdToByteArray'");
    this.objectIdToByteCollection =
        Objects.requireNonNull(b.objectIdToByteCollection, "Field 'objectIdToByteCollection'");
    this.objectIdToBytePrim =
        Objects.requireNonNull(b.objectIdToBytePrim, "Field 'objectIdToBytePrim'");
    this.objectIdToBytePrimArray =
        Objects.requireNonNull(b.objectIdToBytePrimArray, "Field 'objectIdToBytePrimArray'");
    this.objectIdToCharacter =
        Objects.requireNonNull(b.objectIdToCharacter, "Field 'objectIdToCharacter'");
    this.objectIdToCharacterArray =
        Objects.requireNonNull(b.objectIdToCharacterArray, "Field 'objectIdToCharacterArray'");
    this.objectIdToCharacterCollection = Objects.requireNonNull(b.objectIdToCharacterCollection,
        "Field 'objectIdToCharacterCollection'");
    this.objectIdToCharacterPrim =
        Objects.requireNonNull(b.objectIdToCharacterPrim, "Field 'objectIdToCharacterPrim'");
    this.objectIdToCharacterPrimArray = Objects.requireNonNull(b.objectIdToCharacterPrimArray,
        "Field 'objectIdToCharacterPrimArray'");
    this.objectIdToDateBased =
        Objects.requireNonNull(b.objectIdToDateBased, "Field 'objectIdToDateBased'");
    this.objectIdToDateBasedArray =
        Objects.requireNonNull(b.objectIdToDateBasedArray, "Field 'objectIdToDateBasedArray'");
    this.objectIdToDateBasedCollection = Objects.requireNonNull(b.objectIdToDateBasedCollection,
        "Field 'objectIdToDateBasedCollection'");
    this.objectIdToDoublePrim =
        Objects.requireNonNull(b.objectIdToDoublePrim, "Field 'objectIdToDoublePrim'");
    this.objectIdToDoublePrimArray =
        Objects.requireNonNull(b.objectIdToDoublePrimArray, "Field 'objectIdToDoublePrimArray'");
    this.objectIdToEnum = Objects.requireNonNull(b.objectIdToEnum, "Field 'objectIdToEnum'");
    this.objectIdToEnumArray =
        Objects.requireNonNull(b.objectIdToEnumArray, "Field 'objectIdToEnumArray'");
    this.objectIdToEnumCollection =
        Objects.requireNonNull(b.objectIdToEnumCollection, "Field 'objectIdToEnumCollection'");
    this.objectIdToFloatPrim =
        Objects.requireNonNull(b.objectIdToFloatPrim, "Field 'objectIdToFloatPrim'");
    this.objectIdToFloatPrimArray =
        Objects.requireNonNull(b.objectIdToFloatPrimArray, "Field 'objectIdToFloatPrimArray'");
    this.objectIdToIntegerPrim =
        Objects.requireNonNull(b.objectIdToIntegerPrim, "Field 'objectIdToIntegerPrim'");
    this.objectIdToIntegerPrimArray =
        Objects.requireNonNull(b.objectIdToIntegerPrimArray, "Field 'objectIdToIntegerPrimArray'");
    this.objectIdToLongPrim =
        Objects.requireNonNull(b.objectIdToLongPrim, "Field 'objectIdToLongPrim'");
    this.objectIdToLongPrimArray =
        Objects.requireNonNull(b.objectIdToLongPrimArray, "Field 'objectIdToLongPrimArray'");
    this.objectIdToNumberBased =
        Objects.requireNonNull(b.objectIdToNumberBased, "Field 'objectIdToNumberBased'");
    this.objectIdToNumberBasedArray =
        Objects.requireNonNull(b.objectIdToNumberBasedArray, "Field 'objectIdToNumberBasedArray'");
    this.objectIdToNumberBasedCollection = Objects.requireNonNull(b.objectIdToNumberBasedCollection,
        "Field 'objectIdToNumberBasedCollection'");
    this.objectIdToObjectId =
        Objects.requireNonNull(b.objectIdToObjectId, "Field 'objectIdToObjectId'");
    this.objectIdToObjectIdArray =
        Objects.requireNonNull(b.objectIdToObjectIdArray, "Field 'objectIdToObjectIdArray'");
    this.objectIdToObjectIdCollection = Objects.requireNonNull(b.objectIdToObjectIdCollection,
        "Field 'objectIdToObjectIdCollection'");
    this.objectIdToShortPrim =
        Objects.requireNonNull(b.objectIdToShortPrim, "Field 'objectIdToShortPrim'");
    this.objectIdToShortPrimArray =
        Objects.requireNonNull(b.objectIdToShortPrimArray, "Field 'objectIdToShortPrimArray'");
    this.objectIdToStringBased =
        Objects.requireNonNull(b.objectIdToStringBased, "Field 'objectIdToStringBased'");
    this.objectIdToStringBasedArray =
        Objects.requireNonNull(b.objectIdToStringBasedArray, "Field 'objectIdToStringBasedArray'");
    this.objectIdToStringBasedCollection = Objects.requireNonNull(b.objectIdToStringBasedCollection,
        "Field 'objectIdToStringBasedCollection'");
    this.stringCollectionToBinary =
        Objects.requireNonNull(b.stringCollectionToBinary, "Field 'stringCollectionToBinary'");
    this.stringCollectionToBinaryArray = Objects.requireNonNull(b.stringCollectionToBinaryArray,
        "Field 'stringCollectionToBinaryArray'");
    this.stringCollectionToBinaryCollection = Objects.requireNonNull(
        b.stringCollectionToBinaryCollection, "Field 'stringCollectionToBinaryCollection'");
    this.stringCollectionToBoolean =
        Objects.requireNonNull(b.stringCollectionToBoolean, "Field 'stringCollectionToBoolean'");
    this.stringCollectionToBooleanArray = Objects.requireNonNull(b.stringCollectionToBooleanArray,
        "Field 'stringCollectionToBooleanArray'");
    this.stringCollectionToBooleanCollection = Objects.requireNonNull(
        b.stringCollectionToBooleanCollection, "Field 'stringCollectionToBooleanCollection'");
    this.stringCollectionToBooleanPrim = Objects.requireNonNull(b.stringCollectionToBooleanPrim,
        "Field 'stringCollectionToBooleanPrim'");
    this.stringCollectionToBooleanPrimArray = Objects.requireNonNull(
        b.stringCollectionToBooleanPrimArray, "Field 'stringCollectionToBooleanPrimArray'");
    this.stringCollectionToByte =
        Objects.requireNonNull(b.stringCollectionToByte, "Field 'stringCollectionToByte'");
    this.stringCollectionToByteArray = Objects.requireNonNull(b.stringCollectionToByteArray,
        "Field 'stringCollectionToByteArray'");
    this.stringCollectionToByteCollection = Objects.requireNonNull(
        b.stringCollectionToByteCollection, "Field 'stringCollectionToByteCollection'");
    this.stringCollectionToBytePrim =
        Objects.requireNonNull(b.stringCollectionToBytePrim, "Field 'stringCollectionToBytePrim'");
    this.stringCollectionToBytePrimArray = Objects.requireNonNull(b.stringCollectionToBytePrimArray,
        "Field 'stringCollectionToBytePrimArray'");
    this.stringCollectionToCharacter = Objects.requireNonNull(b.stringCollectionToCharacter,
        "Field 'stringCollectionToCharacter'");
    this.stringCollectionToCharacterArray = Objects.requireNonNull(
        b.stringCollectionToCharacterArray, "Field 'stringCollectionToCharacterArray'");
    this.stringCollectionToCharacterCollection = Objects.requireNonNull(
        b.stringCollectionToCharacterCollection, "Field 'stringCollectionToCharacterCollection'");
    this.stringCollectionToCharacterPrim = Objects.requireNonNull(b.stringCollectionToCharacterPrim,
        "Field 'stringCollectionToCharacterPrim'");
    this.stringCollectionToCharacterPrimArray = Objects.requireNonNull(
        b.stringCollectionToCharacterPrimArray, "Field 'stringCollectionToCharacterPrimArray'");
    this.stringCollectionToDateBased = Objects.requireNonNull(b.stringCollectionToDateBased,
        "Field 'stringCollectionToDateBased'");
    this.stringCollectionToDateBasedArray = Objects.requireNonNull(
        b.stringCollectionToDateBasedArray, "Field 'stringCollectionToDateBasedArray'");
    this.stringCollectionToDateBasedCollection = Objects.requireNonNull(
        b.stringCollectionToDateBasedCollection, "Field 'stringCollectionToDateBasedCollection'");
    this.stringCollectionToDoublePrim = Objects.requireNonNull(b.stringCollectionToDoublePrim,
        "Field 'stringCollectionToDoublePrim'");
    this.stringCollectionToDoublePrimArray = Objects.requireNonNull(
        b.stringCollectionToDoublePrimArray, "Field 'stringCollectionToDoublePrimArray'");
    this.stringCollectionToEnum =
        Objects.requireNonNull(b.stringCollectionToEnum, "Field 'stringCollectionToEnum'");
    this.stringCollectionToEnumArray = Objects.requireNonNull(b.stringCollectionToEnumArray,
        "Field 'stringCollectionToEnumArray'");
    this.stringCollectionToEnumCollection = Objects.requireNonNull(
        b.stringCollectionToEnumCollection, "Field 'stringCollectionToEnumCollection'");
    this.stringCollectionToFloatPrim = Objects.requireNonNull(b.stringCollectionToFloatPrim,
        "Field 'stringCollectionToFloatPrim'");
    this.stringCollectionToFloatPrimArray = Objects.requireNonNull(
        b.stringCollectionToFloatPrimArray, "Field 'stringCollectionToFloatPrimArray'");
    this.stringCollectionToIntegerPrim = Objects.requireNonNull(b.stringCollectionToIntegerPrim,
        "Field 'stringCollectionToIntegerPrim'");
    this.stringCollectionToIntegerPrimArray = Objects.requireNonNull(
        b.stringCollectionToIntegerPrimArray, "Field 'stringCollectionToIntegerPrimArray'");
    this.stringCollectionToLongPrim =
        Objects.requireNonNull(b.stringCollectionToLongPrim, "Field 'stringCollectionToLongPrim'");
    this.stringCollectionToLongPrimArray = Objects.requireNonNull(b.stringCollectionToLongPrimArray,
        "Field 'stringCollectionToLongPrimArray'");
    this.stringCollectionToNumberBased = Objects.requireNonNull(b.stringCollectionToNumberBased,
        "Field 'stringCollectionToNumberBased'");
    this.stringCollectionToNumberBasedArray = Objects.requireNonNull(
        b.stringCollectionToNumberBasedArray, "Field 'stringCollectionToNumberBasedArray'");
    this.stringCollectionToNumberBasedCollection =
        Objects.requireNonNull(b.stringCollectionToNumberBasedCollection,
            "Field 'stringCollectionToNumberBasedCollection'");
    this.stringCollectionToObjectId =
        Objects.requireNonNull(b.stringCollectionToObjectId, "Field 'stringCollectionToObjectId'");
    this.stringCollectionToObjectIdArray = Objects.requireNonNull(b.stringCollectionToObjectIdArray,
        "Field 'stringCollectionToObjectIdArray'");
    this.stringCollectionToObjectIdCollection = Objects.requireNonNull(
        b.stringCollectionToObjectIdCollection, "Field 'stringCollectionToObjectIdCollection'");
    this.stringCollectionToShortPrim = Objects.requireNonNull(b.stringCollectionToShortPrim,
        "Field 'stringCollectionToShortPrim'");
    this.stringCollectionToShortPrimArray = Objects.requireNonNull(
        b.stringCollectionToShortPrimArray, "Field 'stringCollectionToShortPrimArray'");
    this.stringCollectionToStringBased = Objects.requireNonNull(b.stringCollectionToStringBased,
        "Field 'stringCollectionToStringBased'");
    this.stringCollectionToStringBasedArray = Objects.requireNonNull(
        b.stringCollectionToStringBasedArray, "Field 'stringCollectionToStringBasedArray'");
    this.stringCollectionToStringBasedCollection =
        Objects.requireNonNull(b.stringCollectionToStringBasedCollection,
            "Field 'stringCollectionToStringBasedCollection'");
    this.stringToBinary = Objects.requireNonNull(b.stringToBinary, "Field 'stringToBinary'");
    this.stringToBinaryArray =
        Objects.requireNonNull(b.stringToBinaryArray, "Field 'stringToBinaryArray'");
    this.stringToBinaryCollection =
        Objects.requireNonNull(b.stringToBinaryCollection, "Field 'stringToBinaryCollection'");
    this.stringToBoolean = Objects.requireNonNull(b.stringToBoolean, "Field 'stringToBoolean'");
    this.stringToBooleanArray =
        Objects.requireNonNull(b.stringToBooleanArray, "Field 'stringToBooleanArray'");
    this.stringToBooleanCollection =
        Objects.requireNonNull(b.stringToBooleanCollection, "Field 'stringToBooleanCollection'");
    this.stringToBooleanPrim =
        Objects.requireNonNull(b.stringToBooleanPrim, "Field 'stringToBooleanPrim'");
    this.stringToBooleanPrimArray =
        Objects.requireNonNull(b.stringToBooleanPrimArray, "Field 'stringToBooleanPrimArray'");
    this.stringToByte = Objects.requireNonNull(b.stringToByte, "Field 'stringToByte'");
    this.stringToByteArray =
        Objects.requireNonNull(b.stringToByteArray, "Field 'stringToByteArray'");
    this.stringToByteCollection =
        Objects.requireNonNull(b.stringToByteCollection, "Field 'stringToByteCollection'");
    this.stringToBytePrim = Objects.requireNonNull(b.stringToBytePrim, "Field 'stringToBytePrim'");
    this.stringToBytePrimArray =
        Objects.requireNonNull(b.stringToBytePrimArray, "Field 'stringToBytePrimArray'");
    this.stringToCharacter =
        Objects.requireNonNull(b.stringToCharacter, "Field 'stringToCharacter'");
    this.stringToCharacterArray =
        Objects.requireNonNull(b.stringToCharacterArray, "Field 'stringToCharacterArray'");
    this.stringToCharacterCollection = Objects.requireNonNull(b.stringToCharacterCollection,
        "Field 'stringToCharacterCollection'");
    this.stringToCharacterPrim =
        Objects.requireNonNull(b.stringToCharacterPrim, "Field 'stringToCharacterPrim'");
    this.stringToCharacterPrimArray =
        Objects.requireNonNull(b.stringToCharacterPrimArray, "Field 'stringToCharacterPrimArray'");
    this.stringToDateBased =
        Objects.requireNonNull(b.stringToDateBased, "Field 'stringToDateBased'");
    this.stringToDateBasedArray =
        Objects.requireNonNull(b.stringToDateBasedArray, "Field 'stringToDateBasedArray'");
    this.stringToDateBasedCollection = Objects.requireNonNull(b.stringToDateBasedCollection,
        "Field 'stringToDateBasedCollection'");
    this.stringToDoublePrim =
        Objects.requireNonNull(b.stringToDoublePrim, "Field 'stringToDoublePrim'");
    this.stringToDoublePrimArray =
        Objects.requireNonNull(b.stringToDoublePrimArray, "Field 'stringToDoublePrimArray'");
    this.stringToEnum = Objects.requireNonNull(b.stringToEnum, "Field 'stringToEnum'");
    this.stringToEnumArray =
        Objects.requireNonNull(b.stringToEnumArray, "Field 'stringToEnumArray'");
    this.stringToEnumCollection =
        Objects.requireNonNull(b.stringToEnumCollection, "Field 'stringToEnumCollection'");
    this.stringToFloatPrim =
        Objects.requireNonNull(b.stringToFloatPrim, "Field 'stringToFloatPrim'");
    this.stringToFloatPrimArray =
        Objects.requireNonNull(b.stringToFloatPrimArray, "Field 'stringToFloatPrimArray'");
    this.stringToIntegerPrim =
        Objects.requireNonNull(b.stringToIntegerPrim, "Field 'stringToIntegerPrim'");
    this.stringToIntegerPrimArray =
        Objects.requireNonNull(b.stringToIntegerPrimArray, "Field 'stringToIntegerPrimArray'");
    this.stringToLongPrim = Objects.requireNonNull(b.stringToLongPrim, "Field 'stringToLongPrim'");
    this.stringToLongPrimArray =
        Objects.requireNonNull(b.stringToLongPrimArray, "Field 'stringToLongPrimArray'");
    this.stringToNumberBased =
        Objects.requireNonNull(b.stringToNumberBased, "Field 'stringToNumberBased'");
    this.stringToNumberBasedArray =
        Objects.requireNonNull(b.stringToNumberBasedArray, "Field 'stringToNumberBasedArray'");
    this.stringToNumberBasedCollection = Objects.requireNonNull(b.stringToNumberBasedCollection,
        "Field 'stringToNumberBasedCollection'");
    this.stringToObjectId = Objects.requireNonNull(b.stringToObjectId, "Field 'stringToObjectId'");
    this.stringToObjectIdArray =
        Objects.requireNonNull(b.stringToObjectIdArray, "Field 'stringToObjectIdArray'");
    this.stringToObjectIdCollection =
        Objects.requireNonNull(b.stringToObjectIdCollection, "Field 'stringToObjectIdCollection'");
    this.stringToShortPrim =
        Objects.requireNonNull(b.stringToShortPrim, "Field 'stringToShortPrim'");
    this.stringToShortPrimArray =
        Objects.requireNonNull(b.stringToShortPrimArray, "Field 'stringToShortPrimArray'");
    this.stringToStringBased =
        Objects.requireNonNull(b.stringToStringBased, "Field 'stringToStringBased'");
    this.stringToStringBasedArray =
        Objects.requireNonNull(b.stringToStringBasedArray, "Field 'stringToStringBasedArray'");
    this.stringToStringBasedCollection = Objects.requireNonNull(b.stringToStringBasedCollection,
        "Field 'stringToStringBasedCollection'");
  }

  public BinaryCollectionToBinary getBinaryCollectionToBinary() {
    return this.binaryCollectionToBinary;
  }

  public BinaryCollectionToBinaryArray getBinaryCollectionToBinaryArray() {
    return this.binaryCollectionToBinaryArray;
  }

  public BinaryCollectionToBinaryCollection getBinaryCollectionToBinaryCollection() {
    return this.binaryCollectionToBinaryCollection;
  }

  public BinaryCollectionToBoolean getBinaryCollectionToBoolean() {
    return this.binaryCollectionToBoolean;
  }

  public BinaryCollectionToBooleanArray getBinaryCollectionToBooleanArray() {
    return this.binaryCollectionToBooleanArray;
  }

  public BinaryCollectionToBooleanCollection getBinaryCollectionToBooleanCollection() {
    return this.binaryCollectionToBooleanCollection;
  }

  public BinaryCollectionToBooleanPrim getBinaryCollectionToBooleanPrim() {
    return this.binaryCollectionToBooleanPrim;
  }

  public BinaryCollectionToBooleanPrimArray getBinaryCollectionToBooleanPrimArray() {
    return this.binaryCollectionToBooleanPrimArray;
  }

  public BinaryCollectionToByte getBinaryCollectionToByte() {
    return this.binaryCollectionToByte;
  }

  public BinaryCollectionToByteArray getBinaryCollectionToByteArray() {
    return this.binaryCollectionToByteArray;
  }

  public BinaryCollectionToByteCollection getBinaryCollectionToByteCollection() {
    return this.binaryCollectionToByteCollection;
  }

  public BinaryCollectionToBytePrim getBinaryCollectionToBytePrim() {
    return this.binaryCollectionToBytePrim;
  }

  public BinaryCollectionToBytePrimArray getBinaryCollectionToBytePrimArray() {
    return this.binaryCollectionToBytePrimArray;
  }

  public BinaryCollectionToCharacter getBinaryCollectionToCharacter() {
    return this.binaryCollectionToCharacter;
  }

  public BinaryCollectionToCharacterArray getBinaryCollectionToCharacterArray() {
    return this.binaryCollectionToCharacterArray;
  }

  public BinaryCollectionToCharacterCollection getBinaryCollectionToCharacterCollection() {
    return this.binaryCollectionToCharacterCollection;
  }

  public BinaryCollectionToCharacterPrim getBinaryCollectionToCharacterPrim() {
    return this.binaryCollectionToCharacterPrim;
  }

  public BinaryCollectionToCharacterPrimArray getBinaryCollectionToCharacterPrimArray() {
    return this.binaryCollectionToCharacterPrimArray;
  }

  public BinaryCollectionToDateBased getBinaryCollectionToDateBased() {
    return this.binaryCollectionToDateBased;
  }

  public BinaryCollectionToDateBasedArray getBinaryCollectionToDateBasedArray() {
    return this.binaryCollectionToDateBasedArray;
  }

  public BinaryCollectionToDateBasedCollection getBinaryCollectionToDateBasedCollection() {
    return this.binaryCollectionToDateBasedCollection;
  }

  public BinaryCollectionToDoublePrim getBinaryCollectionToDoublePrim() {
    return this.binaryCollectionToDoublePrim;
  }

  public BinaryCollectionToDoublePrimArray getBinaryCollectionToDoublePrimArray() {
    return this.binaryCollectionToDoublePrimArray;
  }

  public BinaryCollectionToEnum getBinaryCollectionToEnum() {
    return this.binaryCollectionToEnum;
  }

  public BinaryCollectionToEnumArray getBinaryCollectionToEnumArray() {
    return this.binaryCollectionToEnumArray;
  }

  public BinaryCollectionToEnumCollection getBinaryCollectionToEnumCollection() {
    return this.binaryCollectionToEnumCollection;
  }

  public BinaryCollectionToFloatPrim getBinaryCollectionToFloatPrim() {
    return this.binaryCollectionToFloatPrim;
  }

  public BinaryCollectionToFloatPrimArray getBinaryCollectionToFloatPrimArray() {
    return this.binaryCollectionToFloatPrimArray;
  }

  public BinaryCollectionToIntegerPrim getBinaryCollectionToIntegerPrim() {
    return this.binaryCollectionToIntegerPrim;
  }

  public BinaryCollectionToIntegerPrimArray getBinaryCollectionToIntegerPrimArray() {
    return this.binaryCollectionToIntegerPrimArray;
  }

  public BinaryCollectionToLongPrim getBinaryCollectionToLongPrim() {
    return this.binaryCollectionToLongPrim;
  }

  public BinaryCollectionToLongPrimArray getBinaryCollectionToLongPrimArray() {
    return this.binaryCollectionToLongPrimArray;
  }

  public BinaryCollectionToNumberBased getBinaryCollectionToNumberBased() {
    return this.binaryCollectionToNumberBased;
  }

  public BinaryCollectionToNumberBasedArray getBinaryCollectionToNumberBasedArray() {
    return this.binaryCollectionToNumberBasedArray;
  }

  public BinaryCollectionToNumberBasedCollection getBinaryCollectionToNumberBasedCollection() {
    return this.binaryCollectionToNumberBasedCollection;
  }

  public BinaryCollectionToObjectId getBinaryCollectionToObjectId() {
    return this.binaryCollectionToObjectId;
  }

  public BinaryCollectionToObjectIdArray getBinaryCollectionToObjectIdArray() {
    return this.binaryCollectionToObjectIdArray;
  }

  public BinaryCollectionToObjectIdCollection getBinaryCollectionToObjectIdCollection() {
    return this.binaryCollectionToObjectIdCollection;
  }

  public BinaryCollectionToShortPrim getBinaryCollectionToShortPrim() {
    return this.binaryCollectionToShortPrim;
  }

  public BinaryCollectionToShortPrimArray getBinaryCollectionToShortPrimArray() {
    return this.binaryCollectionToShortPrimArray;
  }

  public BinaryCollectionToStringBased getBinaryCollectionToStringBased() {
    return this.binaryCollectionToStringBased;
  }

  public BinaryCollectionToStringBasedArray getBinaryCollectionToStringBasedArray() {
    return this.binaryCollectionToStringBasedArray;
  }

  public BinaryCollectionToStringBasedCollection getBinaryCollectionToStringBasedCollection() {
    return this.binaryCollectionToStringBasedCollection;
  }

  public BinaryToBinary getBinaryToBinary() {
    return this.binaryToBinary;
  }

  public BinaryToBinaryArray getBinaryToBinaryArray() {
    return this.binaryToBinaryArray;
  }

  public BinaryToBinaryCollection getBinaryToBinaryCollection() {
    return this.binaryToBinaryCollection;
  }

  public BinaryToBoolean getBinaryToBoolean() {
    return this.binaryToBoolean;
  }

  public BinaryToBooleanArray getBinaryToBooleanArray() {
    return this.binaryToBooleanArray;
  }

  public BinaryToBooleanCollection getBinaryToBooleanCollection() {
    return this.binaryToBooleanCollection;
  }

  public BinaryToBooleanPrim getBinaryToBooleanPrim() {
    return this.binaryToBooleanPrim;
  }

  public BinaryToBooleanPrimArray getBinaryToBooleanPrimArray() {
    return this.binaryToBooleanPrimArray;
  }

  public BinaryToByte getBinaryToByte() {
    return this.binaryToByte;
  }

  public BinaryToByteArray getBinaryToByteArray() {
    return this.binaryToByteArray;
  }

  public BinaryToByteCollection getBinaryToByteCollection() {
    return this.binaryToByteCollection;
  }

  public BinaryToBytePrim getBinaryToBytePrim() {
    return this.binaryToBytePrim;
  }

  public BinaryToBytePrimArray getBinaryToBytePrimArray() {
    return this.binaryToBytePrimArray;
  }

  public BinaryToCharacter getBinaryToCharacter() {
    return this.binaryToCharacter;
  }

  public BinaryToCharacterArray getBinaryToCharacterArray() {
    return this.binaryToCharacterArray;
  }

  public BinaryToCharacterCollection getBinaryToCharacterCollection() {
    return this.binaryToCharacterCollection;
  }

  public BinaryToCharacterPrim getBinaryToCharacterPrim() {
    return this.binaryToCharacterPrim;
  }

  public BinaryToCharacterPrimArray getBinaryToCharacterPrimArray() {
    return this.binaryToCharacterPrimArray;
  }

  public BinaryToDateBased getBinaryToDateBased() {
    return this.binaryToDateBased;
  }

  public BinaryToDateBasedArray getBinaryToDateBasedArray() {
    return this.binaryToDateBasedArray;
  }

  public BinaryToDateBasedCollection getBinaryToDateBasedCollection() {
    return this.binaryToDateBasedCollection;
  }

  public BinaryToDoublePrim getBinaryToDoublePrim() {
    return this.binaryToDoublePrim;
  }

  public BinaryToDoublePrimArray getBinaryToDoublePrimArray() {
    return this.binaryToDoublePrimArray;
  }

  public BinaryToEnum getBinaryToEnum() {
    return this.binaryToEnum;
  }

  public BinaryToEnumArray getBinaryToEnumArray() {
    return this.binaryToEnumArray;
  }

  public BinaryToEnumCollection getBinaryToEnumCollection() {
    return this.binaryToEnumCollection;
  }

  public BinaryToFloatPrim getBinaryToFloatPrim() {
    return this.binaryToFloatPrim;
  }

  public BinaryToFloatPrimArray getBinaryToFloatPrimArray() {
    return this.binaryToFloatPrimArray;
  }

  public BinaryToIntegerPrim getBinaryToIntegerPrim() {
    return this.binaryToIntegerPrim;
  }

  public BinaryToIntegerPrimArray getBinaryToIntegerPrimArray() {
    return this.binaryToIntegerPrimArray;
  }

  public BinaryToLongPrim getBinaryToLongPrim() {
    return this.binaryToLongPrim;
  }

  public BinaryToLongPrimArray getBinaryToLongPrimArray() {
    return this.binaryToLongPrimArray;
  }

  public BinaryToNumberBased getBinaryToNumberBased() {
    return this.binaryToNumberBased;
  }

  public BinaryToNumberBasedArray getBinaryToNumberBasedArray() {
    return this.binaryToNumberBasedArray;
  }

  public BinaryToNumberBasedCollection getBinaryToNumberBasedCollection() {
    return this.binaryToNumberBasedCollection;
  }

  public BinaryToObjectId getBinaryToObjectId() {
    return this.binaryToObjectId;
  }

  public BinaryToObjectIdArray getBinaryToObjectIdArray() {
    return this.binaryToObjectIdArray;
  }

  public BinaryToObjectIdCollection getBinaryToObjectIdCollection() {
    return this.binaryToObjectIdCollection;
  }

  public BinaryToShortPrim getBinaryToShortPrim() {
    return this.binaryToShortPrim;
  }

  public BinaryToShortPrimArray getBinaryToShortPrimArray() {
    return this.binaryToShortPrimArray;
  }

  public BinaryToStringBased getBinaryToStringBased() {
    return this.binaryToStringBased;
  }

  public BinaryToStringBasedArray getBinaryToStringBasedArray() {
    return this.binaryToStringBasedArray;
  }

  public BinaryToStringBasedCollection getBinaryToStringBasedCollection() {
    return this.binaryToStringBasedCollection;
  }

  public BooleanCollectionToBinary getBooleanCollectionToBinary() {
    return this.booleanCollectionToBinary;
  }

  public BooleanCollectionToBinaryArray getBooleanCollectionToBinaryArray() {
    return this.booleanCollectionToBinaryArray;
  }

  public BooleanCollectionToBinaryCollection getBooleanCollectionToBinaryCollection() {
    return this.booleanCollectionToBinaryCollection;
  }

  public BooleanCollectionToBoolean getBooleanCollectionToBoolean() {
    return this.booleanCollectionToBoolean;
  }

  public BooleanCollectionToBooleanArray getBooleanCollectionToBooleanArray() {
    return this.booleanCollectionToBooleanArray;
  }

  public BooleanCollectionToBooleanCollection getBooleanCollectionToBooleanCollection() {
    return this.booleanCollectionToBooleanCollection;
  }

  public BooleanCollectionToBooleanPrim getBooleanCollectionToBooleanPrim() {
    return this.booleanCollectionToBooleanPrim;
  }

  public BooleanCollectionToBooleanPrimArray getBooleanCollectionToBooleanPrimArray() {
    return this.booleanCollectionToBooleanPrimArray;
  }

  public BooleanCollectionToByte getBooleanCollectionToByte() {
    return this.booleanCollectionToByte;
  }

  public BooleanCollectionToByteArray getBooleanCollectionToByteArray() {
    return this.booleanCollectionToByteArray;
  }

  public BooleanCollectionToByteCollection getBooleanCollectionToByteCollection() {
    return this.booleanCollectionToByteCollection;
  }

  public BooleanCollectionToBytePrim getBooleanCollectionToBytePrim() {
    return this.booleanCollectionToBytePrim;
  }

  public BooleanCollectionToBytePrimArray getBooleanCollectionToBytePrimArray() {
    return this.booleanCollectionToBytePrimArray;
  }

  public BooleanCollectionToCharacter getBooleanCollectionToCharacter() {
    return this.booleanCollectionToCharacter;
  }

  public BooleanCollectionToCharacterArray getBooleanCollectionToCharacterArray() {
    return this.booleanCollectionToCharacterArray;
  }

  public BooleanCollectionToCharacterCollection getBooleanCollectionToCharacterCollection() {
    return this.booleanCollectionToCharacterCollection;
  }

  public BooleanCollectionToCharacterPrim getBooleanCollectionToCharacterPrim() {
    return this.booleanCollectionToCharacterPrim;
  }

  public BooleanCollectionToCharacterPrimArray getBooleanCollectionToCharacterPrimArray() {
    return this.booleanCollectionToCharacterPrimArray;
  }

  public BooleanCollectionToDateBased getBooleanCollectionToDateBased() {
    return this.booleanCollectionToDateBased;
  }

  public BooleanCollectionToDateBasedArray getBooleanCollectionToDateBasedArray() {
    return this.booleanCollectionToDateBasedArray;
  }

  public BooleanCollectionToDateBasedCollection getBooleanCollectionToDateBasedCollection() {
    return this.booleanCollectionToDateBasedCollection;
  }

  public BooleanCollectionToDoublePrim getBooleanCollectionToDoublePrim() {
    return this.booleanCollectionToDoublePrim;
  }

  public BooleanCollectionToDoublePrimArray getBooleanCollectionToDoublePrimArray() {
    return this.booleanCollectionToDoublePrimArray;
  }

  public BooleanCollectionToEnum getBooleanCollectionToEnum() {
    return this.booleanCollectionToEnum;
  }

  public BooleanCollectionToEnumArray getBooleanCollectionToEnumArray() {
    return this.booleanCollectionToEnumArray;
  }

  public BooleanCollectionToEnumCollection getBooleanCollectionToEnumCollection() {
    return this.booleanCollectionToEnumCollection;
  }

  public BooleanCollectionToFloatPrim getBooleanCollectionToFloatPrim() {
    return this.booleanCollectionToFloatPrim;
  }

  public BooleanCollectionToFloatPrimArray getBooleanCollectionToFloatPrimArray() {
    return this.booleanCollectionToFloatPrimArray;
  }

  public BooleanCollectionToIntegerPrim getBooleanCollectionToIntegerPrim() {
    return this.booleanCollectionToIntegerPrim;
  }

  public BooleanCollectionToIntegerPrimArray getBooleanCollectionToIntegerPrimArray() {
    return this.booleanCollectionToIntegerPrimArray;
  }

  public BooleanCollectionToLongPrim getBooleanCollectionToLongPrim() {
    return this.booleanCollectionToLongPrim;
  }

  public BooleanCollectionToLongPrimArray getBooleanCollectionToLongPrimArray() {
    return this.booleanCollectionToLongPrimArray;
  }

  public BooleanCollectionToNumberBased getBooleanCollectionToNumberBased() {
    return this.booleanCollectionToNumberBased;
  }

  public BooleanCollectionToNumberBasedArray getBooleanCollectionToNumberBasedArray() {
    return this.booleanCollectionToNumberBasedArray;
  }

  public BooleanCollectionToNumberBasedCollection getBooleanCollectionToNumberBasedCollection() {
    return this.booleanCollectionToNumberBasedCollection;
  }

  public BooleanCollectionToObjectId getBooleanCollectionToObjectId() {
    return this.booleanCollectionToObjectId;
  }

  public BooleanCollectionToObjectIdArray getBooleanCollectionToObjectIdArray() {
    return this.booleanCollectionToObjectIdArray;
  }

  public BooleanCollectionToObjectIdCollection getBooleanCollectionToObjectIdCollection() {
    return this.booleanCollectionToObjectIdCollection;
  }

  public BooleanCollectionToShortPrim getBooleanCollectionToShortPrim() {
    return this.booleanCollectionToShortPrim;
  }

  public BooleanCollectionToShortPrimArray getBooleanCollectionToShortPrimArray() {
    return this.booleanCollectionToShortPrimArray;
  }

  public BooleanCollectionToStringBased getBooleanCollectionToStringBased() {
    return this.booleanCollectionToStringBased;
  }

  public BooleanCollectionToStringBasedArray getBooleanCollectionToStringBasedArray() {
    return this.booleanCollectionToStringBasedArray;
  }

  public BooleanCollectionToStringBasedCollection getBooleanCollectionToStringBasedCollection() {
    return this.booleanCollectionToStringBasedCollection;
  }

  public BooleanToBinary getBooleanToBinary() {
    return this.booleanToBinary;
  }

  public BooleanToBinaryArray getBooleanToBinaryArray() {
    return this.booleanToBinaryArray;
  }

  public BooleanToBinaryCollection getBooleanToBinaryCollection() {
    return this.booleanToBinaryCollection;
  }

  public BooleanToBoolean getBooleanToBoolean() {
    return this.booleanToBoolean;
  }

  public BooleanToBooleanArray getBooleanToBooleanArray() {
    return this.booleanToBooleanArray;
  }

  public BooleanToBooleanCollection getBooleanToBooleanCollection() {
    return this.booleanToBooleanCollection;
  }

  public BooleanToBooleanPrim getBooleanToBooleanPrim() {
    return this.booleanToBooleanPrim;
  }

  public BooleanToBooleanPrimArray getBooleanToBooleanPrimArray() {
    return this.booleanToBooleanPrimArray;
  }

  public BooleanToByte getBooleanToByte() {
    return this.booleanToByte;
  }

  public BooleanToByteArray getBooleanToByteArray() {
    return this.booleanToByteArray;
  }

  public BooleanToByteCollection getBooleanToByteCollection() {
    return this.booleanToByteCollection;
  }

  public BooleanToBytePrim getBooleanToBytePrim() {
    return this.booleanToBytePrim;
  }

  public BooleanToBytePrimArray getBooleanToBytePrimArray() {
    return this.booleanToBytePrimArray;
  }

  public BooleanToCharacter getBooleanToCharacter() {
    return this.booleanToCharacter;
  }

  public BooleanToCharacterArray getBooleanToCharacterArray() {
    return this.booleanToCharacterArray;
  }

  public BooleanToCharacterCollection getBooleanToCharacterCollection() {
    return this.booleanToCharacterCollection;
  }

  public BooleanToCharacterPrim getBooleanToCharacterPrim() {
    return this.booleanToCharacterPrim;
  }

  public BooleanToCharacterPrimArray getBooleanToCharacterPrimArray() {
    return this.booleanToCharacterPrimArray;
  }

  public BooleanToDateBased getBooleanToDateBased() {
    return this.booleanToDateBased;
  }

  public BooleanToDateBasedArray getBooleanToDateBasedArray() {
    return this.booleanToDateBasedArray;
  }

  public BooleanToDateBasedCollection getBooleanToDateBasedCollection() {
    return this.booleanToDateBasedCollection;
  }

  public BooleanToDoublePrim getBooleanToDoublePrim() {
    return this.booleanToDoublePrim;
  }

  public BooleanToDoublePrimArray getBooleanToDoublePrimArray() {
    return this.booleanToDoublePrimArray;
  }

  public BooleanToEnum getBooleanToEnum() {
    return this.booleanToEnum;
  }

  public BooleanToEnumArray getBooleanToEnumArray() {
    return this.booleanToEnumArray;
  }

  public BooleanToEnumCollection getBooleanToEnumCollection() {
    return this.booleanToEnumCollection;
  }

  public BooleanToFloatPrim getBooleanToFloatPrim() {
    return this.booleanToFloatPrim;
  }

  public BooleanToFloatPrimArray getBooleanToFloatPrimArray() {
    return this.booleanToFloatPrimArray;
  }

  public BooleanToIntegerPrim getBooleanToIntegerPrim() {
    return this.booleanToIntegerPrim;
  }

  public BooleanToIntegerPrimArray getBooleanToIntegerPrimArray() {
    return this.booleanToIntegerPrimArray;
  }

  public BooleanToLongPrim getBooleanToLongPrim() {
    return this.booleanToLongPrim;
  }

  public BooleanToLongPrimArray getBooleanToLongPrimArray() {
    return this.booleanToLongPrimArray;
  }

  public BooleanToNumberBased getBooleanToNumberBased() {
    return this.booleanToNumberBased;
  }

  public BooleanToNumberBasedArray getBooleanToNumberBasedArray() {
    return this.booleanToNumberBasedArray;
  }

  public BooleanToNumberBasedCollection getBooleanToNumberBasedCollection() {
    return this.booleanToNumberBasedCollection;
  }

  public BooleanToObjectId getBooleanToObjectId() {
    return this.booleanToObjectId;
  }

  public BooleanToObjectIdArray getBooleanToObjectIdArray() {
    return this.booleanToObjectIdArray;
  }

  public BooleanToObjectIdCollection getBooleanToObjectIdCollection() {
    return this.booleanToObjectIdCollection;
  }

  public BooleanToShortPrim getBooleanToShortPrim() {
    return this.booleanToShortPrim;
  }

  public BooleanToShortPrimArray getBooleanToShortPrimArray() {
    return this.booleanToShortPrimArray;
  }

  public BooleanToStringBased getBooleanToStringBased() {
    return this.booleanToStringBased;
  }

  public BooleanToStringBasedArray getBooleanToStringBasedArray() {
    return this.booleanToStringBasedArray;
  }

  public BooleanToStringBasedCollection getBooleanToStringBasedCollection() {
    return this.booleanToStringBasedCollection;
  }

  public DateCollectionToBinary getDateCollectionToBinary() {
    return this.dateCollectionToBinary;
  }

  public DateCollectionToBinaryArray getDateCollectionToBinaryArray() {
    return this.dateCollectionToBinaryArray;
  }

  public DateCollectionToBinaryCollection getDateCollectionToBinaryCollection() {
    return this.dateCollectionToBinaryCollection;
  }

  public DateCollectionToBoolean getDateCollectionToBoolean() {
    return this.dateCollectionToBoolean;
  }

  public DateCollectionToBooleanArray getDateCollectionToBooleanArray() {
    return this.dateCollectionToBooleanArray;
  }

  public DateCollectionToBooleanCollection getDateCollectionToBooleanCollection() {
    return this.dateCollectionToBooleanCollection;
  }

  public DateCollectionToBooleanPrim getDateCollectionToBooleanPrim() {
    return this.dateCollectionToBooleanPrim;
  }

  public DateCollectionToBooleanPrimArray getDateCollectionToBooleanPrimArray() {
    return this.dateCollectionToBooleanPrimArray;
  }

  public DateCollectionToByte getDateCollectionToByte() {
    return this.dateCollectionToByte;
  }

  public DateCollectionToByteArray getDateCollectionToByteArray() {
    return this.dateCollectionToByteArray;
  }

  public DateCollectionToByteCollection getDateCollectionToByteCollection() {
    return this.dateCollectionToByteCollection;
  }

  public DateCollectionToBytePrim getDateCollectionToBytePrim() {
    return this.dateCollectionToBytePrim;
  }

  public DateCollectionToBytePrimArray getDateCollectionToBytePrimArray() {
    return this.dateCollectionToBytePrimArray;
  }

  public DateCollectionToCharacter getDateCollectionToCharacter() {
    return this.dateCollectionToCharacter;
  }

  public DateCollectionToCharacterArray getDateCollectionToCharacterArray() {
    return this.dateCollectionToCharacterArray;
  }

  public DateCollectionToCharacterCollection getDateCollectionToCharacterCollection() {
    return this.dateCollectionToCharacterCollection;
  }

  public DateCollectionToCharacterPrim getDateCollectionToCharacterPrim() {
    return this.dateCollectionToCharacterPrim;
  }

  public DateCollectionToCharacterPrimArray getDateCollectionToCharacterPrimArray() {
    return this.dateCollectionToCharacterPrimArray;
  }

  public DateCollectionToDateBased getDateCollectionToDateBased() {
    return this.dateCollectionToDateBased;
  }

  public DateCollectionToDateBasedArray getDateCollectionToDateBasedArray() {
    return this.dateCollectionToDateBasedArray;
  }

  public DateCollectionToDateBasedCollection getDateCollectionToDateBasedCollection() {
    return this.dateCollectionToDateBasedCollection;
  }

  public DateCollectionToDoublePrim getDateCollectionToDoublePrim() {
    return this.dateCollectionToDoublePrim;
  }

  public DateCollectionToDoublePrimArray getDateCollectionToDoublePrimArray() {
    return this.dateCollectionToDoublePrimArray;
  }

  public DateCollectionToEnum getDateCollectionToEnum() {
    return this.dateCollectionToEnum;
  }

  public DateCollectionToEnumArray getDateCollectionToEnumArray() {
    return this.dateCollectionToEnumArray;
  }

  public DateCollectionToEnumCollection getDateCollectionToEnumCollection() {
    return this.dateCollectionToEnumCollection;
  }

  public DateCollectionToFloatPrim getDateCollectionToFloatPrim() {
    return this.dateCollectionToFloatPrim;
  }

  public DateCollectionToFloatPrimArray getDateCollectionToFloatPrimArray() {
    return this.dateCollectionToFloatPrimArray;
  }

  public DateCollectionToIntegerPrim getDateCollectionToIntegerPrim() {
    return this.dateCollectionToIntegerPrim;
  }

  public DateCollectionToIntegerPrimArray getDateCollectionToIntegerPrimArray() {
    return this.dateCollectionToIntegerPrimArray;
  }

  public DateCollectionToLongPrim getDateCollectionToLongPrim() {
    return this.dateCollectionToLongPrim;
  }

  public DateCollectionToLongPrimArray getDateCollectionToLongPrimArray() {
    return this.dateCollectionToLongPrimArray;
  }

  public DateCollectionToNumberBased getDateCollectionToNumberBased() {
    return this.dateCollectionToNumberBased;
  }

  public DateCollectionToNumberBasedArray getDateCollectionToNumberBasedArray() {
    return this.dateCollectionToNumberBasedArray;
  }

  public DateCollectionToNumberBasedCollection getDateCollectionToNumberBasedCollection() {
    return this.dateCollectionToNumberBasedCollection;
  }

  public DateCollectionToObjectId getDateCollectionToObjectId() {
    return this.dateCollectionToObjectId;
  }

  public DateCollectionToObjectIdArray getDateCollectionToObjectIdArray() {
    return this.dateCollectionToObjectIdArray;
  }

  public DateCollectionToObjectIdCollection getDateCollectionToObjectIdCollection() {
    return this.dateCollectionToObjectIdCollection;
  }

  public DateCollectionToShortPrim getDateCollectionToShortPrim() {
    return this.dateCollectionToShortPrim;
  }

  public DateCollectionToShortPrimArray getDateCollectionToShortPrimArray() {
    return this.dateCollectionToShortPrimArray;
  }

  public DateCollectionToStringBased getDateCollectionToStringBased() {
    return this.dateCollectionToStringBased;
  }

  public DateCollectionToStringBasedArray getDateCollectionToStringBasedArray() {
    return this.dateCollectionToStringBasedArray;
  }

  public DateCollectionToStringBasedCollection getDateCollectionToStringBasedCollection() {
    return this.dateCollectionToStringBasedCollection;
  }

  public DateToBinary getDateToBinary() {
    return this.dateToBinary;
  }

  public DateToBinaryArray getDateToBinaryArray() {
    return this.dateToBinaryArray;
  }

  public DateToBinaryCollection getDateToBinaryCollection() {
    return this.dateToBinaryCollection;
  }

  public DateToBoolean getDateToBoolean() {
    return this.dateToBoolean;
  }

  public DateToBooleanArray getDateToBooleanArray() {
    return this.dateToBooleanArray;
  }

  public DateToBooleanCollection getDateToBooleanCollection() {
    return this.dateToBooleanCollection;
  }

  public DateToBooleanPrim getDateToBooleanPrim() {
    return this.dateToBooleanPrim;
  }

  public DateToBooleanPrimArray getDateToBooleanPrimArray() {
    return this.dateToBooleanPrimArray;
  }

  public DateToByte getDateToByte() {
    return this.dateToByte;
  }

  public DateToByteArray getDateToByteArray() {
    return this.dateToByteArray;
  }

  public DateToByteCollection getDateToByteCollection() {
    return this.dateToByteCollection;
  }

  public DateToBytePrim getDateToBytePrim() {
    return this.dateToBytePrim;
  }

  public DateToBytePrimArray getDateToBytePrimArray() {
    return this.dateToBytePrimArray;
  }

  public DateToCharacter getDateToCharacter() {
    return this.dateToCharacter;
  }

  public DateToCharacterArray getDateToCharacterArray() {
    return this.dateToCharacterArray;
  }

  public DateToCharacterCollection getDateToCharacterCollection() {
    return this.dateToCharacterCollection;
  }

  public DateToCharacterPrim getDateToCharacterPrim() {
    return this.dateToCharacterPrim;
  }

  public DateToCharacterPrimArray getDateToCharacterPrimArray() {
    return this.dateToCharacterPrimArray;
  }

  public DateToDateBased getDateToDateBased() {
    return this.dateToDateBased;
  }

  public DateToDateBasedArray getDateToDateBasedArray() {
    return this.dateToDateBasedArray;
  }

  public DateToDateBasedCollection getDateToDateBasedCollection() {
    return this.dateToDateBasedCollection;
  }

  public DateToDoublePrim getDateToDoublePrim() {
    return this.dateToDoublePrim;
  }

  public DateToDoublePrimArray getDateToDoublePrimArray() {
    return this.dateToDoublePrimArray;
  }

  public DateToEnum getDateToEnum() {
    return this.dateToEnum;
  }

  public DateToEnumArray getDateToEnumArray() {
    return this.dateToEnumArray;
  }

  public DateToEnumCollection getDateToEnumCollection() {
    return this.dateToEnumCollection;
  }

  public DateToFloatPrim getDateToFloatPrim() {
    return this.dateToFloatPrim;
  }

  public DateToFloatPrimArray getDateToFloatPrimArray() {
    return this.dateToFloatPrimArray;
  }

  public DateToIntegerPrim getDateToIntegerPrim() {
    return this.dateToIntegerPrim;
  }

  public DateToIntegerPrimArray getDateToIntegerPrimArray() {
    return this.dateToIntegerPrimArray;
  }

  public DateToLongPrim getDateToLongPrim() {
    return this.dateToLongPrim;
  }

  public DateToLongPrimArray getDateToLongPrimArray() {
    return this.dateToLongPrimArray;
  }

  public DateToNumberBased getDateToNumberBased() {
    return this.dateToNumberBased;
  }

  public DateToNumberBasedArray getDateToNumberBasedArray() {
    return this.dateToNumberBasedArray;
  }

  public DateToNumberBasedCollection getDateToNumberBasedCollection() {
    return this.dateToNumberBasedCollection;
  }

  public DateToObjectId getDateToObjectId() {
    return this.dateToObjectId;
  }

  public DateToObjectIdArray getDateToObjectIdArray() {
    return this.dateToObjectIdArray;
  }

  public DateToObjectIdCollection getDateToObjectIdCollection() {
    return this.dateToObjectIdCollection;
  }

  public DateToShortPrim getDateToShortPrim() {
    return this.dateToShortPrim;
  }

  public DateToShortPrimArray getDateToShortPrimArray() {
    return this.dateToShortPrimArray;
  }

  public DateToStringBased getDateToStringBased() {
    return this.dateToStringBased;
  }

  public DateToStringBasedArray getDateToStringBasedArray() {
    return this.dateToStringBasedArray;
  }

  public DateToStringBasedCollection getDateToStringBasedCollection() {
    return this.dateToStringBasedCollection;
  }

  public NumberCollectionToBinary getNumberCollectionToBinary() {
    return this.numberCollectionToBinary;
  }

  public NumberCollectionToBinaryArray getNumberCollectionToBinaryArray() {
    return this.numberCollectionToBinaryArray;
  }

  public NumberCollectionToBinaryCollection getNumberCollectionToBinaryCollection() {
    return this.numberCollectionToBinaryCollection;
  }

  public NumberCollectionToBoolean getNumberCollectionToBoolean() {
    return this.numberCollectionToBoolean;
  }

  public NumberCollectionToBooleanArray getNumberCollectionToBooleanArray() {
    return this.numberCollectionToBooleanArray;
  }

  public NumberCollectionToBooleanCollection getNumberCollectionToBooleanCollection() {
    return this.numberCollectionToBooleanCollection;
  }

  public NumberCollectionToBooleanPrim getNumberCollectionToBooleanPrim() {
    return this.numberCollectionToBooleanPrim;
  }

  public NumberCollectionToBooleanPrimArray getNumberCollectionToBooleanPrimArray() {
    return this.numberCollectionToBooleanPrimArray;
  }

  public NumberCollectionToByte getNumberCollectionToByte() {
    return this.numberCollectionToByte;
  }

  public NumberCollectionToByteArray getNumberCollectionToByteArray() {
    return this.numberCollectionToByteArray;
  }

  public NumberCollectionToByteCollection getNumberCollectionToByteCollection() {
    return this.numberCollectionToByteCollection;
  }

  public NumberCollectionToBytePrim getNumberCollectionToBytePrim() {
    return this.numberCollectionToBytePrim;
  }

  public NumberCollectionToBytePrimArray getNumberCollectionToBytePrimArray() {
    return this.numberCollectionToBytePrimArray;
  }

  public NumberCollectionToCharacter getNumberCollectionToCharacter() {
    return this.numberCollectionToCharacter;
  }

  public NumberCollectionToCharacterArray getNumberCollectionToCharacterArray() {
    return this.numberCollectionToCharacterArray;
  }

  public NumberCollectionToCharacterCollection getNumberCollectionToCharacterCollection() {
    return this.numberCollectionToCharacterCollection;
  }

  public NumberCollectionToCharacterPrim getNumberCollectionToCharacterPrim() {
    return this.numberCollectionToCharacterPrim;
  }

  public NumberCollectionToCharacterPrimArray getNumberCollectionToCharacterPrimArray() {
    return this.numberCollectionToCharacterPrimArray;
  }

  public NumberCollectionToDateBased getNumberCollectionToDateBased() {
    return this.numberCollectionToDateBased;
  }

  public NumberCollectionToDateBasedArray getNumberCollectionToDateBasedArray() {
    return this.numberCollectionToDateBasedArray;
  }

  public NumberCollectionToDateBasedCollection getNumberCollectionToDateBasedCollection() {
    return this.numberCollectionToDateBasedCollection;
  }

  public NumberCollectionToDoublePrim getNumberCollectionToDoublePrim() {
    return this.numberCollectionToDoublePrim;
  }

  public NumberCollectionToDoublePrimArray getNumberCollectionToDoublePrimArray() {
    return this.numberCollectionToDoublePrimArray;
  }

  public NumberCollectionToEnum getNumberCollectionToEnum() {
    return this.numberCollectionToEnum;
  }

  public NumberCollectionToEnumArray getNumberCollectionToEnumArray() {
    return this.numberCollectionToEnumArray;
  }

  public NumberCollectionToEnumCollection getNumberCollectionToEnumCollection() {
    return this.numberCollectionToEnumCollection;
  }

  public NumberCollectionToFloatPrim getNumberCollectionToFloatPrim() {
    return this.numberCollectionToFloatPrim;
  }

  public NumberCollectionToFloatPrimArray getNumberCollectionToFloatPrimArray() {
    return this.numberCollectionToFloatPrimArray;
  }

  public NumberCollectionToIntegerPrim getNumberCollectionToIntegerPrim() {
    return this.numberCollectionToIntegerPrim;
  }

  public NumberCollectionToIntegerPrimArray getNumberCollectionToIntegerPrimArray() {
    return this.numberCollectionToIntegerPrimArray;
  }

  public NumberCollectionToLongPrim getNumberCollectionToLongPrim() {
    return this.numberCollectionToLongPrim;
  }

  public NumberCollectionToLongPrimArray getNumberCollectionToLongPrimArray() {
    return this.numberCollectionToLongPrimArray;
  }

  public NumberCollectionToNumberBased getNumberCollectionToNumberBased() {
    return this.numberCollectionToNumberBased;
  }

  public NumberCollectionToNumberBasedArray getNumberCollectionToNumberBasedArray() {
    return this.numberCollectionToNumberBasedArray;
  }

  public NumberCollectionToNumberBasedCollection getNumberCollectionToNumberBasedCollection() {
    return this.numberCollectionToNumberBasedCollection;
  }

  public NumberCollectionToObjectId getNumberCollectionToObjectId() {
    return this.numberCollectionToObjectId;
  }

  public NumberCollectionToObjectIdArray getNumberCollectionToObjectIdArray() {
    return this.numberCollectionToObjectIdArray;
  }

  public NumberCollectionToObjectIdCollection getNumberCollectionToObjectIdCollection() {
    return this.numberCollectionToObjectIdCollection;
  }

  public NumberCollectionToShortPrim getNumberCollectionToShortPrim() {
    return this.numberCollectionToShortPrim;
  }

  public NumberCollectionToShortPrimArray getNumberCollectionToShortPrimArray() {
    return this.numberCollectionToShortPrimArray;
  }

  public NumberCollectionToStringBased getNumberCollectionToStringBased() {
    return this.numberCollectionToStringBased;
  }

  public NumberCollectionToStringBasedArray getNumberCollectionToStringBasedArray() {
    return this.numberCollectionToStringBasedArray;
  }

  public NumberCollectionToStringBasedCollection getNumberCollectionToStringBasedCollection() {
    return this.numberCollectionToStringBasedCollection;
  }

  public NumberToBinary getNumberToBinary() {
    return this.numberToBinary;
  }

  public NumberToBinaryArray getNumberToBinaryArray() {
    return this.numberToBinaryArray;
  }

  public NumberToBinaryCollection getNumberToBinaryCollection() {
    return this.numberToBinaryCollection;
  }

  public NumberToBoolean getNumberToBoolean() {
    return this.numberToBoolean;
  }

  public NumberToBooleanArray getNumberToBooleanArray() {
    return this.numberToBooleanArray;
  }

  public NumberToBooleanCollection getNumberToBooleanCollection() {
    return this.numberToBooleanCollection;
  }

  public NumberToBooleanPrim getNumberToBooleanPrim() {
    return this.numberToBooleanPrim;
  }

  public NumberToBooleanPrimArray getNumberToBooleanPrimArray() {
    return this.numberToBooleanPrimArray;
  }

  public NumberToByte getNumberToByte() {
    return this.numberToByte;
  }

  public NumberToByteArray getNumberToByteArray() {
    return this.numberToByteArray;
  }

  public NumberToByteCollection getNumberToByteCollection() {
    return this.numberToByteCollection;
  }

  public NumberToBytePrim getNumberToBytePrim() {
    return this.numberToBytePrim;
  }

  public NumberToBytePrimArray getNumberToBytePrimArray() {
    return this.numberToBytePrimArray;
  }

  public NumberToCharacter getNumberToCharacter() {
    return this.numberToCharacter;
  }

  public NumberToCharacterArray getNumberToCharacterArray() {
    return this.numberToCharacterArray;
  }

  public NumberToCharacterCollection getNumberToCharacterCollection() {
    return this.numberToCharacterCollection;
  }

  public NumberToCharacterPrim getNumberToCharacterPrim() {
    return this.numberToCharacterPrim;
  }

  public NumberToCharacterPrimArray getNumberToCharacterPrimArray() {
    return this.numberToCharacterPrimArray;
  }

  public NumberToDateBased getNumberToDateBased() {
    return this.numberToDateBased;
  }

  public NumberToDateBasedArray getNumberToDateBasedArray() {
    return this.numberToDateBasedArray;
  }

  public NumberToDateBasedCollection getNumberToDateBasedCollection() {
    return this.numberToDateBasedCollection;
  }

  public NumberToDoublePrim getNumberToDoublePrim() {
    return this.numberToDoublePrim;
  }

  public NumberToDoublePrimArray getNumberToDoublePrimArray() {
    return this.numberToDoublePrimArray;
  }

  public NumberToEnum getNumberToEnum() {
    return this.numberToEnum;
  }

  public NumberToEnumArray getNumberToEnumArray() {
    return this.numberToEnumArray;
  }

  public NumberToEnumCollection getNumberToEnumCollection() {
    return this.numberToEnumCollection;
  }

  public NumberToFloatPrim getNumberToFloatPrim() {
    return this.numberToFloatPrim;
  }

  public NumberToFloatPrimArray getNumberToFloatPrimArray() {
    return this.numberToFloatPrimArray;
  }

  public NumberToIntegerPrim getNumberToIntegerPrim() {
    return this.numberToIntegerPrim;
  }

  public NumberToIntegerPrimArray getNumberToIntegerPrimArray() {
    return this.numberToIntegerPrimArray;
  }

  public NumberToLongPrim getNumberToLongPrim() {
    return this.numberToLongPrim;
  }

  public NumberToLongPrimArray getNumberToLongPrimArray() {
    return this.numberToLongPrimArray;
  }

  public NumberToNumberBased getNumberToNumberBased() {
    return this.numberToNumberBased;
  }

  public NumberToNumberBasedArray getNumberToNumberBasedArray() {
    return this.numberToNumberBasedArray;
  }

  public NumberToNumberBasedCollection getNumberToNumberBasedCollection() {
    return this.numberToNumberBasedCollection;
  }

  public NumberToObjectId getNumberToObjectId() {
    return this.numberToObjectId;
  }

  public NumberToObjectIdArray getNumberToObjectIdArray() {
    return this.numberToObjectIdArray;
  }

  public NumberToObjectIdCollection getNumberToObjectIdCollection() {
    return this.numberToObjectIdCollection;
  }

  public NumberToShortPrim getNumberToShortPrim() {
    return this.numberToShortPrim;
  }

  public NumberToShortPrimArray getNumberToShortPrimArray() {
    return this.numberToShortPrimArray;
  }

  public NumberToStringBased getNumberToStringBased() {
    return this.numberToStringBased;
  }

  public NumberToStringBasedArray getNumberToStringBasedArray() {
    return this.numberToStringBasedArray;
  }

  public NumberToStringBasedCollection getNumberToStringBasedCollection() {
    return this.numberToStringBasedCollection;
  }

  public ObjectIdCollectionToBinary getObjectIdCollectionToBinary() {
    return this.objectIdCollectionToBinary;
  }

  public ObjectIdCollectionToBinaryArray getObjectIdCollectionToBinaryArray() {
    return this.objectIdCollectionToBinaryArray;
  }

  public ObjectIdCollectionToBinaryCollection getObjectIdCollectionToBinaryCollection() {
    return this.objectIdCollectionToBinaryCollection;
  }

  public ObjectIdCollectionToBoolean getObjectIdCollectionToBoolean() {
    return this.objectIdCollectionToBoolean;
  }

  public ObjectIdCollectionToBooleanArray getObjectIdCollectionToBooleanArray() {
    return this.objectIdCollectionToBooleanArray;
  }

  public ObjectIdCollectionToBooleanCollection getObjectIdCollectionToBooleanCollection() {
    return this.objectIdCollectionToBooleanCollection;
  }

  public ObjectIdCollectionToBooleanPrim getObjectIdCollectionToBooleanPrim() {
    return this.objectIdCollectionToBooleanPrim;
  }

  public ObjectIdCollectionToBooleanPrimArray getObjectIdCollectionToBooleanPrimArray() {
    return this.objectIdCollectionToBooleanPrimArray;
  }

  public ObjectIdCollectionToByte getObjectIdCollectionToByte() {
    return this.objectIdCollectionToByte;
  }

  public ObjectIdCollectionToByteArray getObjectIdCollectionToByteArray() {
    return this.objectIdCollectionToByteArray;
  }

  public ObjectIdCollectionToByteCollection getObjectIdCollectionToByteCollection() {
    return this.objectIdCollectionToByteCollection;
  }

  public ObjectIdCollectionToBytePrim getObjectIdCollectionToBytePrim() {
    return this.objectIdCollectionToBytePrim;
  }

  public ObjectIdCollectionToBytePrimArray getObjectIdCollectionToBytePrimArray() {
    return this.objectIdCollectionToBytePrimArray;
  }

  public ObjectIdCollectionToCharacter getObjectIdCollectionToCharacter() {
    return this.objectIdCollectionToCharacter;
  }

  public ObjectIdCollectionToCharacterArray getObjectIdCollectionToCharacterArray() {
    return this.objectIdCollectionToCharacterArray;
  }

  public ObjectIdCollectionToCharacterCollection getObjectIdCollectionToCharacterCollection() {
    return this.objectIdCollectionToCharacterCollection;
  }

  public ObjectIdCollectionToCharacterPrim getObjectIdCollectionToCharacterPrim() {
    return this.objectIdCollectionToCharacterPrim;
  }

  public ObjectIdCollectionToCharacterPrimArray getObjectIdCollectionToCharacterPrimArray() {
    return this.objectIdCollectionToCharacterPrimArray;
  }

  public ObjectIdCollectionToDateBased getObjectIdCollectionToDateBased() {
    return this.objectIdCollectionToDateBased;
  }

  public ObjectIdCollectionToDateBasedArray getObjectIdCollectionToDateBasedArray() {
    return this.objectIdCollectionToDateBasedArray;
  }

  public ObjectIdCollectionToDateBasedCollection getObjectIdCollectionToDateBasedCollection() {
    return this.objectIdCollectionToDateBasedCollection;
  }

  public ObjectIdCollectionToDoublePrim getObjectIdCollectionToDoublePrim() {
    return this.objectIdCollectionToDoublePrim;
  }

  public ObjectIdCollectionToDoublePrimArray getObjectIdCollectionToDoublePrimArray() {
    return this.objectIdCollectionToDoublePrimArray;
  }

  public ObjectIdCollectionToEnum getObjectIdCollectionToEnum() {
    return this.objectIdCollectionToEnum;
  }

  public ObjectIdCollectionToEnumArray getObjectIdCollectionToEnumArray() {
    return this.objectIdCollectionToEnumArray;
  }

  public ObjectIdCollectionToEnumCollection getObjectIdCollectionToEnumCollection() {
    return this.objectIdCollectionToEnumCollection;
  }

  public ObjectIdCollectionToFloatPrim getObjectIdCollectionToFloatPrim() {
    return this.objectIdCollectionToFloatPrim;
  }

  public ObjectIdCollectionToFloatPrimArray getObjectIdCollectionToFloatPrimArray() {
    return this.objectIdCollectionToFloatPrimArray;
  }

  public ObjectIdCollectionToIntegerPrim getObjectIdCollectionToIntegerPrim() {
    return this.objectIdCollectionToIntegerPrim;
  }

  public ObjectIdCollectionToIntegerPrimArray getObjectIdCollectionToIntegerPrimArray() {
    return this.objectIdCollectionToIntegerPrimArray;
  }

  public ObjectIdCollectionToLongPrim getObjectIdCollectionToLongPrim() {
    return this.objectIdCollectionToLongPrim;
  }

  public ObjectIdCollectionToLongPrimArray getObjectIdCollectionToLongPrimArray() {
    return this.objectIdCollectionToLongPrimArray;
  }

  public ObjectIdCollectionToNumberBased getObjectIdCollectionToNumberBased() {
    return this.objectIdCollectionToNumberBased;
  }

  public ObjectIdCollectionToNumberBasedArray getObjectIdCollectionToNumberBasedArray() {
    return this.objectIdCollectionToNumberBasedArray;
  }

  public ObjectIdCollectionToNumberBasedCollection getObjectIdCollectionToNumberBasedCollection() {
    return this.objectIdCollectionToNumberBasedCollection;
  }

  public ObjectIdCollectionToObjectId getObjectIdCollectionToObjectId() {
    return this.objectIdCollectionToObjectId;
  }

  public ObjectIdCollectionToObjectIdArray getObjectIdCollectionToObjectIdArray() {
    return this.objectIdCollectionToObjectIdArray;
  }

  public ObjectIdCollectionToObjectIdCollection getObjectIdCollectionToObjectIdCollection() {
    return this.objectIdCollectionToObjectIdCollection;
  }

  public ObjectIdCollectionToShortPrim getObjectIdCollectionToShortPrim() {
    return this.objectIdCollectionToShortPrim;
  }

  public ObjectIdCollectionToShortPrimArray getObjectIdCollectionToShortPrimArray() {
    return this.objectIdCollectionToShortPrimArray;
  }

  public ObjectIdCollectionToStringBased getObjectIdCollectionToStringBased() {
    return this.objectIdCollectionToStringBased;
  }

  public ObjectIdCollectionToStringBasedArray getObjectIdCollectionToStringBasedArray() {
    return this.objectIdCollectionToStringBasedArray;
  }

  public ObjectIdCollectionToStringBasedCollection getObjectIdCollectionToStringBasedCollection() {
    return this.objectIdCollectionToStringBasedCollection;
  }

  public ObjectIdToBinary getObjectIdToBinary() {
    return this.objectIdToBinary;
  }

  public ObjectIdToBinaryArray getObjectIdToBinaryArray() {
    return this.objectIdToBinaryArray;
  }

  public ObjectIdToBinaryCollection getObjectIdToBinaryCollection() {
    return this.objectIdToBinaryCollection;
  }

  public ObjectIdToBoolean getObjectIdToBoolean() {
    return this.objectIdToBoolean;
  }

  public ObjectIdToBooleanArray getObjectIdToBooleanArray() {
    return this.objectIdToBooleanArray;
  }

  public ObjectIdToBooleanCollection getObjectIdToBooleanCollection() {
    return this.objectIdToBooleanCollection;
  }

  public ObjectIdToBooleanPrim getObjectIdToBooleanPrim() {
    return this.objectIdToBooleanPrim;
  }

  public ObjectIdToBooleanPrimArray getObjectIdToBooleanPrimArray() {
    return this.objectIdToBooleanPrimArray;
  }

  public ObjectIdToByte getObjectIdToByte() {
    return this.objectIdToByte;
  }

  public ObjectIdToByteArray getObjectIdToByteArray() {
    return this.objectIdToByteArray;
  }

  public ObjectIdToByteCollection getObjectIdToByteCollection() {
    return this.objectIdToByteCollection;
  }

  public ObjectIdToBytePrim getObjectIdToBytePrim() {
    return this.objectIdToBytePrim;
  }

  public ObjectIdToBytePrimArray getObjectIdToBytePrimArray() {
    return this.objectIdToBytePrimArray;
  }

  public ObjectIdToCharacter getObjectIdToCharacter() {
    return this.objectIdToCharacter;
  }

  public ObjectIdToCharacterArray getObjectIdToCharacterArray() {
    return this.objectIdToCharacterArray;
  }

  public ObjectIdToCharacterCollection getObjectIdToCharacterCollection() {
    return this.objectIdToCharacterCollection;
  }

  public ObjectIdToCharacterPrim getObjectIdToCharacterPrim() {
    return this.objectIdToCharacterPrim;
  }

  public ObjectIdToCharacterPrimArray getObjectIdToCharacterPrimArray() {
    return this.objectIdToCharacterPrimArray;
  }

  public ObjectIdToDateBased getObjectIdToDateBased() {
    return this.objectIdToDateBased;
  }

  public ObjectIdToDateBasedArray getObjectIdToDateBasedArray() {
    return this.objectIdToDateBasedArray;
  }

  public ObjectIdToDateBasedCollection getObjectIdToDateBasedCollection() {
    return this.objectIdToDateBasedCollection;
  }

  public ObjectIdToDoublePrim getObjectIdToDoublePrim() {
    return this.objectIdToDoublePrim;
  }

  public ObjectIdToDoublePrimArray getObjectIdToDoublePrimArray() {
    return this.objectIdToDoublePrimArray;
  }

  public ObjectIdToEnum getObjectIdToEnum() {
    return this.objectIdToEnum;
  }

  public ObjectIdToEnumArray getObjectIdToEnumArray() {
    return this.objectIdToEnumArray;
  }

  public ObjectIdToEnumCollection getObjectIdToEnumCollection() {
    return this.objectIdToEnumCollection;
  }

  public ObjectIdToFloatPrim getObjectIdToFloatPrim() {
    return this.objectIdToFloatPrim;
  }

  public ObjectIdToFloatPrimArray getObjectIdToFloatPrimArray() {
    return this.objectIdToFloatPrimArray;
  }

  public ObjectIdToIntegerPrim getObjectIdToIntegerPrim() {
    return this.objectIdToIntegerPrim;
  }

  public ObjectIdToIntegerPrimArray getObjectIdToIntegerPrimArray() {
    return this.objectIdToIntegerPrimArray;
  }

  public ObjectIdToLongPrim getObjectIdToLongPrim() {
    return this.objectIdToLongPrim;
  }

  public ObjectIdToLongPrimArray getObjectIdToLongPrimArray() {
    return this.objectIdToLongPrimArray;
  }

  public ObjectIdToNumberBased getObjectIdToNumberBased() {
    return this.objectIdToNumberBased;
  }

  public ObjectIdToNumberBasedArray getObjectIdToNumberBasedArray() {
    return this.objectIdToNumberBasedArray;
  }

  public ObjectIdToNumberBasedCollection getObjectIdToNumberBasedCollection() {
    return this.objectIdToNumberBasedCollection;
  }

  public ObjectIdToObjectId getObjectIdToObjectId() {
    return this.objectIdToObjectId;
  }

  public ObjectIdToObjectIdArray getObjectIdToObjectIdArray() {
    return this.objectIdToObjectIdArray;
  }

  public ObjectIdToObjectIdCollection getObjectIdToObjectIdCollection() {
    return this.objectIdToObjectIdCollection;
  }

  public ObjectIdToShortPrim getObjectIdToShortPrim() {
    return this.objectIdToShortPrim;
  }

  public ObjectIdToShortPrimArray getObjectIdToShortPrimArray() {
    return this.objectIdToShortPrimArray;
  }

  public ObjectIdToStringBased getObjectIdToStringBased() {
    return this.objectIdToStringBased;
  }

  public ObjectIdToStringBasedArray getObjectIdToStringBasedArray() {
    return this.objectIdToStringBasedArray;
  }

  public ObjectIdToStringBasedCollection getObjectIdToStringBasedCollection() {
    return this.objectIdToStringBasedCollection;
  }

  public StringCollectionToBinary getStringCollectionToBinary() {
    return this.stringCollectionToBinary;
  }

  public StringCollectionToBinaryArray getStringCollectionToBinaryArray() {
    return this.stringCollectionToBinaryArray;
  }

  public StringCollectionToBinaryCollection getStringCollectionToBinaryCollection() {
    return this.stringCollectionToBinaryCollection;
  }

  public StringCollectionToBoolean getStringCollectionToBoolean() {
    return this.stringCollectionToBoolean;
  }

  public StringCollectionToBooleanArray getStringCollectionToBooleanArray() {
    return this.stringCollectionToBooleanArray;
  }

  public StringCollectionToBooleanCollection getStringCollectionToBooleanCollection() {
    return this.stringCollectionToBooleanCollection;
  }

  public StringCollectionToBooleanPrim getStringCollectionToBooleanPrim() {
    return this.stringCollectionToBooleanPrim;
  }

  public StringCollectionToBooleanPrimArray getStringCollectionToBooleanPrimArray() {
    return this.stringCollectionToBooleanPrimArray;
  }

  public StringCollectionToByte getStringCollectionToByte() {
    return this.stringCollectionToByte;
  }

  public StringCollectionToByteArray getStringCollectionToByteArray() {
    return this.stringCollectionToByteArray;
  }

  public StringCollectionToByteCollection getStringCollectionToByteCollection() {
    return this.stringCollectionToByteCollection;
  }

  public StringCollectionToBytePrim getStringCollectionToBytePrim() {
    return this.stringCollectionToBytePrim;
  }

  public StringCollectionToBytePrimArray getStringCollectionToBytePrimArray() {
    return this.stringCollectionToBytePrimArray;
  }

  public StringCollectionToCharacter getStringCollectionToCharacter() {
    return this.stringCollectionToCharacter;
  }

  public StringCollectionToCharacterArray getStringCollectionToCharacterArray() {
    return this.stringCollectionToCharacterArray;
  }

  public StringCollectionToCharacterCollection getStringCollectionToCharacterCollection() {
    return this.stringCollectionToCharacterCollection;
  }

  public StringCollectionToCharacterPrim getStringCollectionToCharacterPrim() {
    return this.stringCollectionToCharacterPrim;
  }

  public StringCollectionToCharacterPrimArray getStringCollectionToCharacterPrimArray() {
    return this.stringCollectionToCharacterPrimArray;
  }

  public StringCollectionToDateBased getStringCollectionToDateBased() {
    return this.stringCollectionToDateBased;
  }

  public StringCollectionToDateBasedArray getStringCollectionToDateBasedArray() {
    return this.stringCollectionToDateBasedArray;
  }

  public StringCollectionToDateBasedCollection getStringCollectionToDateBasedCollection() {
    return this.stringCollectionToDateBasedCollection;
  }

  public StringCollectionToDoublePrim getStringCollectionToDoublePrim() {
    return this.stringCollectionToDoublePrim;
  }

  public StringCollectionToDoublePrimArray getStringCollectionToDoublePrimArray() {
    return this.stringCollectionToDoublePrimArray;
  }

  public StringCollectionToEnum getStringCollectionToEnum() {
    return this.stringCollectionToEnum;
  }

  public StringCollectionToEnumArray getStringCollectionToEnumArray() {
    return this.stringCollectionToEnumArray;
  }

  public StringCollectionToEnumCollection getStringCollectionToEnumCollection() {
    return this.stringCollectionToEnumCollection;
  }

  public StringCollectionToFloatPrim getStringCollectionToFloatPrim() {
    return this.stringCollectionToFloatPrim;
  }

  public StringCollectionToFloatPrimArray getStringCollectionToFloatPrimArray() {
    return this.stringCollectionToFloatPrimArray;
  }

  public StringCollectionToIntegerPrim getStringCollectionToIntegerPrim() {
    return this.stringCollectionToIntegerPrim;
  }

  public StringCollectionToIntegerPrimArray getStringCollectionToIntegerPrimArray() {
    return this.stringCollectionToIntegerPrimArray;
  }

  public StringCollectionToLongPrim getStringCollectionToLongPrim() {
    return this.stringCollectionToLongPrim;
  }

  public StringCollectionToLongPrimArray getStringCollectionToLongPrimArray() {
    return this.stringCollectionToLongPrimArray;
  }

  public StringCollectionToNumberBased getStringCollectionToNumberBased() {
    return this.stringCollectionToNumberBased;
  }

  public StringCollectionToNumberBasedArray getStringCollectionToNumberBasedArray() {
    return this.stringCollectionToNumberBasedArray;
  }

  public StringCollectionToNumberBasedCollection getStringCollectionToNumberBasedCollection() {
    return this.stringCollectionToNumberBasedCollection;
  }

  public StringCollectionToObjectId getStringCollectionToObjectId() {
    return this.stringCollectionToObjectId;
  }

  public StringCollectionToObjectIdArray getStringCollectionToObjectIdArray() {
    return this.stringCollectionToObjectIdArray;
  }

  public StringCollectionToObjectIdCollection getStringCollectionToObjectIdCollection() {
    return this.stringCollectionToObjectIdCollection;
  }

  public StringCollectionToShortPrim getStringCollectionToShortPrim() {
    return this.stringCollectionToShortPrim;
  }

  public StringCollectionToShortPrimArray getStringCollectionToShortPrimArray() {
    return this.stringCollectionToShortPrimArray;
  }

  public StringCollectionToStringBased getStringCollectionToStringBased() {
    return this.stringCollectionToStringBased;
  }

  public StringCollectionToStringBasedArray getStringCollectionToStringBasedArray() {
    return this.stringCollectionToStringBasedArray;
  }

  public StringCollectionToStringBasedCollection getStringCollectionToStringBasedCollection() {
    return this.stringCollectionToStringBasedCollection;
  }

  public StringToBinary getStringToBinary() {
    return this.stringToBinary;
  }

  public StringToBinaryArray getStringToBinaryArray() {
    return this.stringToBinaryArray;
  }

  public StringToBinaryCollection getStringToBinaryCollection() {
    return this.stringToBinaryCollection;
  }

  public StringToBoolean getStringToBoolean() {
    return this.stringToBoolean;
  }

  public StringToBooleanArray getStringToBooleanArray() {
    return this.stringToBooleanArray;
  }

  public StringToBooleanCollection getStringToBooleanCollection() {
    return this.stringToBooleanCollection;
  }

  public StringToBooleanPrim getStringToBooleanPrim() {
    return this.stringToBooleanPrim;
  }

  public StringToBooleanPrimArray getStringToBooleanPrimArray() {
    return this.stringToBooleanPrimArray;
  }

  public StringToByte getStringToByte() {
    return this.stringToByte;
  }

  public StringToByteArray getStringToByteArray() {
    return this.stringToByteArray;
  }

  public StringToByteCollection getStringToByteCollection() {
    return this.stringToByteCollection;
  }

  public StringToBytePrim getStringToBytePrim() {
    return this.stringToBytePrim;
  }

  public StringToBytePrimArray getStringToBytePrimArray() {
    return this.stringToBytePrimArray;
  }

  public StringToCharacter getStringToCharacter() {
    return this.stringToCharacter;
  }

  public StringToCharacterArray getStringToCharacterArray() {
    return this.stringToCharacterArray;
  }

  public StringToCharacterCollection getStringToCharacterCollection() {
    return this.stringToCharacterCollection;
  }

  public StringToCharacterPrim getStringToCharacterPrim() {
    return this.stringToCharacterPrim;
  }

  public StringToCharacterPrimArray getStringToCharacterPrimArray() {
    return this.stringToCharacterPrimArray;
  }

  public StringToDateBased getStringToDateBased() {
    return this.stringToDateBased;
  }

  public StringToDateBasedArray getStringToDateBasedArray() {
    return this.stringToDateBasedArray;
  }

  public StringToDateBasedCollection getStringToDateBasedCollection() {
    return this.stringToDateBasedCollection;
  }

  public StringToDoublePrim getStringToDoublePrim() {
    return this.stringToDoublePrim;
  }

  public StringToDoublePrimArray getStringToDoublePrimArray() {
    return this.stringToDoublePrimArray;
  }

  public StringToEnum getStringToEnum() {
    return this.stringToEnum;
  }

  public StringToEnumArray getStringToEnumArray() {
    return this.stringToEnumArray;
  }

  public StringToEnumCollection getStringToEnumCollection() {
    return this.stringToEnumCollection;
  }

  public StringToFloatPrim getStringToFloatPrim() {
    return this.stringToFloatPrim;
  }

  public StringToFloatPrimArray getStringToFloatPrimArray() {
    return this.stringToFloatPrimArray;
  }

  public StringToIntegerPrim getStringToIntegerPrim() {
    return this.stringToIntegerPrim;
  }

  public StringToIntegerPrimArray getStringToIntegerPrimArray() {
    return this.stringToIntegerPrimArray;
  }

  public StringToLongPrim getStringToLongPrim() {
    return this.stringToLongPrim;
  }

  public StringToLongPrimArray getStringToLongPrimArray() {
    return this.stringToLongPrimArray;
  }

  public StringToNumberBased getStringToNumberBased() {
    return this.stringToNumberBased;
  }

  public StringToNumberBasedArray getStringToNumberBasedArray() {
    return this.stringToNumberBasedArray;
  }

  public StringToNumberBasedCollection getStringToNumberBasedCollection() {
    return this.stringToNumberBasedCollection;
  }

  public StringToObjectId getStringToObjectId() {
    return this.stringToObjectId;
  }

  public StringToObjectIdArray getStringToObjectIdArray() {
    return this.stringToObjectIdArray;
  }

  public StringToObjectIdCollection getStringToObjectIdCollection() {
    return this.stringToObjectIdCollection;
  }

  public StringToShortPrim getStringToShortPrim() {
    return this.stringToShortPrim;
  }

  public StringToShortPrimArray getStringToShortPrimArray() {
    return this.stringToShortPrimArray;
  }

  public StringToStringBased getStringToStringBased() {
    return this.stringToStringBased;
  }

  public StringToStringBasedArray getStringToStringBasedArray() {
    return this.stringToStringBasedArray;
  }

  public StringToStringBasedCollection getStringToStringBasedCollection() {
    return this.stringToStringBasedCollection;
  }
}
