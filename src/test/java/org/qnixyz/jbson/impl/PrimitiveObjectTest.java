package org.qnixyz.jbson.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PrimitiveObjectTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(PrimitiveObjectTest.class);

  @SuppressWarnings("unused")
  private Integer integerObj;

  @SuppressWarnings("unused")
  private Integer[] integerObjArray;

  @SuppressWarnings("unused")
  private int integerPrim;

  @SuppressWarnings("unused")
  private int[] integerPrimArray;

  @Test
  public void test_obj() throws Exception {
    final Field field = PrimitiveObjectTest.class.getDeclaredField("integerObj");
    field.setAccessible(true);
    final PrimitiveObjectTest o = new PrimitiveObjectTest();
    final Object vPre = 1;
    field.set(o, vPre);
    final Object vPost = field.get(o);
    assertEquals(vPre, vPost);
  }

  @Test
  public void test_obj_array() throws Exception {
    final Field field = PrimitiveObjectTest.class.getDeclaredField("integerObjArray");
    field.setAccessible(true);
    final PrimitiveObjectTest o = new PrimitiveObjectTest();
    final Object[] vPre = new Integer[] {1, 2};
    field.set(o, vPre);
    final Object[] vPost = (Object[]) field.get(o);
    assertArrayEquals(vPre, vPost);
  }

  @Test
  public void test_prim() throws Exception {
    final Field field = PrimitiveObjectTest.class.getDeclaredField("integerPrim");
    field.setAccessible(true);
    final PrimitiveObjectTest o = new PrimitiveObjectTest();
    final Object vPre = Integer.valueOf(1);
    field.set(o, vPre);
    final Object vPost = field.get(o);
    assertEquals(vPre, vPost);
  }

  @Test
  public void test_prim_array() throws Exception {
    final Field field = PrimitiveObjectTest.class.getDeclaredField("integerPrimArray");
    field.setAccessible(true);
    final PrimitiveObjectTest o = new PrimitiveObjectTest();
    final int[] vPre = new int[] {1, 2};
    field.set(o, vPre);
    final int[] vPost = (int[]) field.get(o);
    assertArrayEquals(vPre, vPost);
  }
}
