package org.qnixyz.jbson.impl;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.qnixyz.jbson.impl.Utils.toObjectArray;
import static org.qnixyz.jbson.impl.Utils.toPrimitiveArray;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UtilsTest {

  @Test
  public void test_toObjectArray() throws Exception {
    assertNull(toObjectArray((boolean[]) null));
    assertNotNull(toObjectArray(new boolean[] {}));
    assertArrayEquals(new Boolean[] {Boolean.TRUE, Boolean.FALSE},
        toObjectArray(new boolean[] {true, false}));

    assertNull(toObjectArray((byte[]) null));
    assertNotNull(toObjectArray(new byte[] {}));
    assertArrayEquals(new Byte[] {Byte.valueOf((byte) 0x00), Byte.valueOf((byte) 0x01)},
        toObjectArray(new byte[] {0x00, 0x01}));

    assertNotNull(toObjectArray(new char[] {}));
    assertArrayEquals(new Character[] {Character.valueOf('a'), Character.valueOf('b')},
        toObjectArray(new char[] {'a', 'b'}));

    assertNull(toObjectArray((double[]) null));
    assertNotNull(toObjectArray(new double[] {}));
    assertArrayEquals(new Double[] {Double.valueOf(1), Double.valueOf(2)},
        toObjectArray(new double[] {1, 2}));

    assertNotNull(toObjectArray(new int[] {}));
    assertNull(toObjectArray((int[]) null));
    assertArrayEquals(new Integer[] {Integer.valueOf(1), Integer.valueOf(2)},
        toObjectArray(new int[] {1, 2}));

    assertNotNull(toObjectArray(new long[] {}));
    assertNull(toObjectArray((long[]) null));
    assertArrayEquals(new Long[] {Long.valueOf(1), Long.valueOf(2)},
        toObjectArray(new long[] {1, 2}));
  }


  @Test
  public void test_toPrimitiveArray() throws Exception {
    assertNull(toPrimitiveArray((Byte[]) null));
    assertNotNull(toPrimitiveArray(new Byte[] {}));
    assertArrayEquals(new byte[] {0x00, 0x01},
        toPrimitiveArray(new Byte[] {Byte.valueOf((byte) 0x00), Byte.valueOf((byte) 0x01)}));

    assertNull(toPrimitiveArray((Character[]) null));
    assertNotNull(toPrimitiveArray(new Character[] {}));
    assertArrayEquals(new char[] {'a', 'b'},
        toPrimitiveArray(new Character[] {Character.valueOf('a'), Character.valueOf('b')}));
  }
}
