/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok.supersub;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlElementRefListCat extends XmlElementRefListAnimal {

  private String meowSound;

  public XmlElementRefListCat() {}

  public XmlElementRefListCat(final String meowSound) {
    this.meowSound = meowSound;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final XmlElementRefListCat other = (XmlElementRefListCat) obj;
    return Objects.equals(this.meowSound, other.meowSound);
  }

  public String getMeowSound() {
    return this.meowSound;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.meowSound);
  }

  public void setMeowSound(final String meowSound) {
    this.meowSound = meowSound;
  }
}
