/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import org.qnixyz.jbson.annotations.JaxBsonNumberHint;

@XmlAccessorType(XmlAccessType.FIELD)
public class AllSimpleTypes {

  @JaxBsonNumberHint(asString = true)
  private BigDecimal propBigDecimal;

  @JaxBsonNumberHint(asString = true)
  private BigDecimal[] propBigDecimalArray;

  @JaxBsonNumberHint(asString = true)
  private List<BigDecimal> propBigDecimalList;

  @JaxBsonNumberHint(asString = true)
  private Set<BigDecimal> propBigDecimalSet;

  @JaxBsonNumberHint(asString = true)
  private SortedSet<BigDecimal> propBigDecimalSortedSet;

  private BigInteger propBigInteger;

  private BigInteger[] propBigIntegerArray;

  private List<BigInteger> propBigIntegerList;

  private Set<BigInteger> propBigIntegerSet;

  private SortedSet<BigInteger> propBigIntegerSortedSet;

  private Boolean propBoolean;

  private Boolean[] propBooleanArray;

  private List<Boolean> propBooleanList;

  private boolean propBooleanPrim;

  private boolean[] propBooleanPrimArray;

  private Set<Boolean> propBooleanSet;

  private SortedSet<Boolean> propBooleanSortedSet;

  private Byte propByte;

  private Byte[] propByteArray;

  private List<Byte> propByteList;

  private byte propBytePrim;

  private byte[] propBytePrimArray;

  private Set<Byte> propByteSet;

  private SortedSet<Byte> propByteSortedSet;

  private Calendar propCalendar;

  private Calendar[] propCalendarArray;

  private List<Calendar> propCalendarList;

  private Set<Calendar> propCalendarSet;

  private SortedSet<Calendar> propCalendarSortedSet;

  private Character propCharacter;

  private Character[] propCharacterArray;

  private List<Character> propCharacterList;

  private char propCharacterPrim;

  private char[] propCharacterPrimArray;

  private Set<Character> propCharacterSet;

  private SortedSet<Character> propCharacterSortedSet;

  private Date propDate;

  private Date[] propDateArray;

  private List<Date> propDateList;

  private Set<Date> propDateSet;

  private SortedSet<Date> propDateSortedSet;

  private Double propDouble;

  private Double[] propDoubleArray;

  private List<Double> propDoubleList;

  private double propDoublePrim;

  private double[] propDoublePrimArray;

  private Set<Double> propDoubleSet;

  private SortedSet<Double> propDoubleSortedSet;

  private File propFile;

  private File[] propFileArray;

  private List<File> propFileList;

  private Set<File> propFileSet;

  private SortedSet<File> propFileSortedSet;

  private Float propFloat;

  private Float[] propFloatArray;

  private List<Float> propFloatList;

  private float propFloatPrim;

  private float[] propFloatPrimArray;

  private Set<Float> propFloatSet;

  private SortedSet<Float> propFloatSortedSet;

  private Integer propInteger;

  private Integer[] propIntegerArray;

  private List<Integer> propIntegerList;

  private int propIntegerPrim;

  private int[] propIntegerPrimArray;

  private Set<Integer> propIntegerSet;

  private SortedSet<Integer> propIntegerSortedSet;

  private Long propLong;

  private Long[] propLongArray;

  private List<Long> propLongList;

  private long propLongPrim;

  private long[] propLongPrimArray;

  private Set<Long> propLongSet;

  private SortedSet<Long> propLongSortedSet;

  private Short propShort;

  private Short[] propShortArray;

  private List<Short> propShortList;

  private short propShortPrim;

  private short[] propShortPrimArray;

  private Set<Short> propShortSet;

  private SortedSet<Short> propShortSortedSet;

  private String propString;

  private String[] propStringArray;

  private List<String> propStringList;

  private Set<String> propStringSet;

  private SortedSet<String> propStringSortedSet;

  private URI propURI;

  private URI[] propURIArray;

  private List<URI> propURIList;

  private Set<URI> propURISet;

  private SortedSet<URI> propURISortedSet;

  private URL propURL;

  private URL[] propURLArray;

  private List<URL> propURLList;

  private Set<URL> propURLSet;

  private XMLGregorianCalendar propXMLGregorianCalendar;

  private XMLGregorianCalendar[] propXMLGregorianCalendarArray;

  private List<XMLGregorianCalendar> propXMLGregorianCalendarList;

  private Set<XMLGregorianCalendar> propXMLGregorianCalendarSet;

  @XmlJavaTypeAdapter(NonXmlComplexType.AdapterToSimpleType.class)
  private NonXmlComplexType propXmlJavaTypeAdapter;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final AllSimpleTypes other = (AllSimpleTypes) obj;
    return Objects.equals(this.propBigDecimal, other.propBigDecimal)
        && Arrays.equals(this.propBigDecimalArray, other.propBigDecimalArray)
        && Objects.equals(this.propBigDecimalList, other.propBigDecimalList)
        && Objects.equals(this.propBigDecimalSet, other.propBigDecimalSet)
        && Objects.equals(this.propBigDecimalSortedSet, other.propBigDecimalSortedSet)
        && Objects.equals(this.propBigInteger, other.propBigInteger)
        && Arrays.equals(this.propBigIntegerArray, other.propBigIntegerArray)
        && Objects.equals(this.propBigIntegerList, other.propBigIntegerList)
        && Objects.equals(this.propBigIntegerSet, other.propBigIntegerSet)
        && Objects.equals(this.propBigIntegerSortedSet, other.propBigIntegerSortedSet)
        && Objects.equals(this.propBoolean, other.propBoolean)
        && Arrays.equals(this.propBooleanArray, other.propBooleanArray)
        && Objects.equals(this.propBooleanList, other.propBooleanList)
        && (this.propBooleanPrim == other.propBooleanPrim)
        && Arrays.equals(this.propBooleanPrimArray, other.propBooleanPrimArray)
        && Objects.equals(this.propBooleanSet, other.propBooleanSet)
        && Objects.equals(this.propBooleanSortedSet, other.propBooleanSortedSet)
        && Objects.equals(this.propByte, other.propByte)
        && Arrays.equals(this.propByteArray, other.propByteArray)
        && Objects.equals(this.propByteList, other.propByteList)
        && (this.propBytePrim == other.propBytePrim)
        && Arrays.equals(this.propBytePrimArray, other.propBytePrimArray)
        && Objects.equals(this.propByteSet, other.propByteSet)
        && Objects.equals(this.propByteSortedSet, other.propByteSortedSet)
        && Objects.equals(this.propCalendar, other.propCalendar)
        && Arrays.equals(this.propCalendarArray, other.propCalendarArray)
        && Objects.equals(this.propCalendarList, other.propCalendarList)
        && Objects.equals(this.propCalendarSet, other.propCalendarSet)
        && Objects.equals(this.propCalendarSortedSet, other.propCalendarSortedSet)
        && Objects.equals(this.propCharacter, other.propCharacter)
        && Arrays.equals(this.propCharacterArray, other.propCharacterArray)
        && Objects.equals(this.propCharacterList, other.propCharacterList)
        && (this.propCharacterPrim == other.propCharacterPrim)
        && Arrays.equals(this.propCharacterPrimArray, other.propCharacterPrimArray)
        && Objects.equals(this.propCharacterSet, other.propCharacterSet)
        && Objects.equals(this.propCharacterSortedSet, other.propCharacterSortedSet)
        && Objects.equals(this.propDate, other.propDate)
        && Arrays.equals(this.propDateArray, other.propDateArray)
        && Objects.equals(this.propDateList, other.propDateList)
        && Objects.equals(this.propDateSet, other.propDateSet)
        && Objects.equals(this.propDateSortedSet, other.propDateSortedSet)
        && Objects.equals(this.propDouble, other.propDouble)
        && Arrays.equals(this.propDoubleArray, other.propDoubleArray)
        && Objects.equals(this.propDoubleList, other.propDoubleList)
        && (Double.doubleToLongBits(this.propDoublePrim) == Double
            .doubleToLongBits(other.propDoublePrim))
        && Arrays.equals(this.propDoublePrimArray, other.propDoublePrimArray)
        && Objects.equals(this.propDoubleSet, other.propDoubleSet)
        && Objects.equals(this.propDoubleSortedSet, other.propDoubleSortedSet)
        && Objects.equals(this.propFile, other.propFile)
        && Arrays.equals(this.propFileArray, other.propFileArray)
        && Objects.equals(this.propFileList, other.propFileList)
        && Objects.equals(this.propFileSet, other.propFileSet)
        && Objects.equals(this.propFileSortedSet, other.propFileSortedSet)
        && Objects.equals(this.propFloat, other.propFloat)
        && Arrays.equals(this.propFloatArray, other.propFloatArray)
        && Objects.equals(this.propFloatList, other.propFloatList)
        && (Float.floatToIntBits(this.propFloatPrim) == Float.floatToIntBits(other.propFloatPrim))
        && Arrays.equals(this.propFloatPrimArray, other.propFloatPrimArray)
        && Objects.equals(this.propFloatSet, other.propFloatSet)
        && Objects.equals(this.propFloatSortedSet, other.propFloatSortedSet)
        && Objects.equals(this.propInteger, other.propInteger)
        && Arrays.equals(this.propIntegerArray, other.propIntegerArray)
        && Objects.equals(this.propIntegerList, other.propIntegerList)
        && (this.propIntegerPrim == other.propIntegerPrim)
        && Arrays.equals(this.propIntegerPrimArray, other.propIntegerPrimArray)
        && Objects.equals(this.propIntegerSet, other.propIntegerSet)
        && Objects.equals(this.propIntegerSortedSet, other.propIntegerSortedSet)
        && Objects.equals(this.propLong, other.propLong)
        && Arrays.equals(this.propLongArray, other.propLongArray)
        && Objects.equals(this.propLongList, other.propLongList)
        && (this.propLongPrim == other.propLongPrim)
        && Arrays.equals(this.propLongPrimArray, other.propLongPrimArray)
        && Objects.equals(this.propLongSet, other.propLongSet)
        && Objects.equals(this.propLongSortedSet, other.propLongSortedSet)
        && Objects.equals(this.propShort, other.propShort)
        && Arrays.equals(this.propShortArray, other.propShortArray)
        && Objects.equals(this.propShortList, other.propShortList)
        && (this.propShortPrim == other.propShortPrim)
        && Arrays.equals(this.propShortPrimArray, other.propShortPrimArray)
        && Objects.equals(this.propShortSet, other.propShortSet)
        && Objects.equals(this.propShortSortedSet, other.propShortSortedSet)
        && Objects.equals(this.propString, other.propString)
        && Arrays.equals(this.propStringArray, other.propStringArray)
        && Objects.equals(this.propStringList, other.propStringList)
        && Objects.equals(this.propStringSet, other.propStringSet)
        && Objects.equals(this.propStringSortedSet, other.propStringSortedSet)
        && Objects.equals(this.propURI, other.propURI)
        && Arrays.equals(this.propURIArray, other.propURIArray)
        && Objects.equals(this.propURIList, other.propURIList)
        && Objects.equals(this.propURISet, other.propURISet)
        && Objects.equals(this.propURISortedSet, other.propURISortedSet)
        && Objects.equals(this.propURL, other.propURL)
        && Arrays.equals(this.propURLArray, other.propURLArray)
        && Objects.equals(this.propURLList, other.propURLList)
        && Objects.equals(this.propURLSet, other.propURLSet)
        && Objects.equals(this.propXMLGregorianCalendar, other.propXMLGregorianCalendar)
        && Arrays.equals(this.propXMLGregorianCalendarArray, other.propXMLGregorianCalendarArray)
        && Objects.equals(this.propXMLGregorianCalendarList, other.propXMLGregorianCalendarList)
        && Objects.equals(this.propXMLGregorianCalendarSet, other.propXMLGregorianCalendarSet)
        && Objects.equals(this.propXmlJavaTypeAdapter, other.propXmlJavaTypeAdapter);
  }

  public char getAttrCHARACTER() {
    return this.propCharacterPrim;
  }

  public BigDecimal getPropBigDecimal() {
    return this.propBigDecimal;
  }

  public BigDecimal[] getPropBigDecimalArray() {
    return this.propBigDecimalArray;
  }

  public List<BigDecimal> getPropBigDecimalList() {
    return this.propBigDecimalList;
  }

  public Set<BigDecimal> getPropBigDecimalSet() {
    return this.propBigDecimalSet;
  }

  public SortedSet<BigDecimal> getPropBigDecimalSortedSet() {
    return this.propBigDecimalSortedSet;
  }

  public BigInteger getPropBigInteger() {
    return this.propBigInteger;
  }

  public BigInteger[] getPropBigIntegerArray() {
    return this.propBigIntegerArray;
  }

  public List<BigInteger> getPropBigIntegerList() {
    return this.propBigIntegerList;
  }

  public Set<BigInteger> getPropBigIntegerSet() {
    return this.propBigIntegerSet;
  }

  public SortedSet<BigInteger> getPropBigIntegerSortedSet() {
    return this.propBigIntegerSortedSet;
  }

  public Boolean getPropBoolean() {
    return this.propBoolean;
  }

  public Boolean[] getPropBooleanArray() {
    return this.propBooleanArray;
  }

  public List<Boolean> getPropBooleanList() {
    return this.propBooleanList;
  }

  public boolean[] getPropBooleanPrimArray() {
    return this.propBooleanPrimArray;
  }

  public Set<Boolean> getPropBooleanSet() {
    return this.propBooleanSet;
  }

  public SortedSet<Boolean> getPropBooleanSortedSet() {
    return this.propBooleanSortedSet;
  }

  public Byte getPropByte() {
    return this.propByte;
  }

  public Byte[] getPropByteArray() {
    return this.propByteArray;
  }

  public List<Byte> getPropByteList() {
    return this.propByteList;
  }

  public byte getPropBytePrim() {
    return this.propBytePrim;
  }

  public byte[] getPropBytePrimArray() {
    return this.propBytePrimArray;
  }

  public Set<Byte> getPropByteSet() {
    return this.propByteSet;
  }

  public SortedSet<Byte> getPropByteSortedSet() {
    return this.propByteSortedSet;
  }

  public Calendar getPropCalendar() {
    return this.propCalendar;
  }

  public Calendar[] getPropCalendarArray() {
    return this.propCalendarArray;
  }

  public List<Calendar> getPropCalendarList() {
    return this.propCalendarList;
  }

  public Set<Calendar> getPropCalendarSet() {
    return this.propCalendarSet;
  }

  public SortedSet<Calendar> getPropCalendarSortedSet() {
    return this.propCalendarSortedSet;
  }

  public Character getPropCharacter() {
    return this.propCharacter;
  }

  public Character[] getPropCharacterArray() {
    return this.propCharacterArray;
  }

  public List<Character> getPropCharacterList() {
    return this.propCharacterList;
  }

  public char getPropCharacterPrim() {
    return this.propCharacterPrim;
  }

  public char[] getPropCharacterPrimArray() {
    return this.propCharacterPrimArray;
  }

  public Set<Character> getPropCharacterSet() {
    return this.propCharacterSet;
  }

  public SortedSet<Character> getPropCharacterSortedSet() {
    return this.propCharacterSortedSet;
  }

  public Date getPropDate() {
    return this.propDate;
  }

  public Date[] getPropDateArray() {
    return this.propDateArray;
  }

  public List<Date> getPropDateList() {
    return this.propDateList;
  }

  public Set<Date> getPropDateSet() {
    return this.propDateSet;
  }

  public SortedSet<Date> getPropDateSortedSet() {
    return this.propDateSortedSet;
  }

  public Double getPropDouble() {
    return this.propDouble;
  }

  public Double[] getPropDoubleArray() {
    return this.propDoubleArray;
  }

  public List<Double> getPropDoubleList() {
    return this.propDoubleList;
  }

  public double getPropDoublePrim() {
    return this.propDoublePrim;
  }

  public double[] getPropDoublePrimArray() {
    return this.propDoublePrimArray;
  }

  public Set<Double> getPropDoubleSet() {
    return this.propDoubleSet;
  }

  public SortedSet<Double> getPropDoubleSortedSet() {
    return this.propDoubleSortedSet;
  }

  public File getPropFile() {
    return this.propFile;
  }

  public File[] getPropFileArray() {
    return this.propFileArray;
  }

  public List<File> getPropFileList() {
    return this.propFileList;
  }

  public Set<File> getPropFileSet() {
    return this.propFileSet;
  }

  public SortedSet<File> getPropFileSortedSet() {
    return this.propFileSortedSet;
  }

  public Float getPropFloat() {
    return this.propFloat;
  }

  public Float[] getPropFloatArray() {
    return this.propFloatArray;
  }

  public List<Float> getPropFloatList() {
    return this.propFloatList;
  }

  public float getPropFloatPrim() {
    return this.propFloatPrim;
  }

  public float[] getPropFloatPrimArray() {
    return this.propFloatPrimArray;
  }

  public Set<Float> getPropFloatSet() {
    return this.propFloatSet;
  }

  public SortedSet<Float> getPropFloatSortedSet() {
    return this.propFloatSortedSet;
  }

  public Integer getPropInteger() {
    return this.propInteger;
  }

  public Integer[] getPropIntegerArray() {
    return this.propIntegerArray;
  }

  public List<Integer> getPropIntegerList() {
    return this.propIntegerList;
  }

  public int getPropIntegerPrim() {
    return this.propIntegerPrim;
  }

  public int[] getPropIntegerPrimArray() {
    return this.propIntegerPrimArray;
  }

  public Set<Integer> getPropIntegerSet() {
    return this.propIntegerSet;
  }

  public SortedSet<Integer> getPropIntegerSortedSet() {
    return this.propIntegerSortedSet;
  }

  public Long getPropLong() {
    return this.propLong;
  }

  public Long[] getPropLongArray() {
    return this.propLongArray;
  }

  public List<Long> getPropLongList() {
    return this.propLongList;
  }

  public long getPropLongPrim() {
    return this.propLongPrim;
  }

  public long[] getPropLongPrimArray() {
    return this.propLongPrimArray;
  }

  public Set<Long> getPropLongSet() {
    return this.propLongSet;
  }

  public SortedSet<Long> getPropLongSortedSet() {
    return this.propLongSortedSet;
  }

  public Short getPropShort() {
    return this.propShort;
  }

  public Short[] getPropShortArray() {
    return this.propShortArray;
  }

  public List<Short> getPropShortList() {
    return this.propShortList;
  }

  public short getPropShortPrim() {
    return this.propShortPrim;
  }

  public short[] getPropShortPrimArray() {
    return this.propShortPrimArray;
  }

  public Set<Short> getPropShortSet() {
    return this.propShortSet;
  }

  public SortedSet<Short> getPropShortSortedSet() {
    return this.propShortSortedSet;
  }

  public String getPropString() {
    return this.propString;
  }

  public String[] getPropStringArray() {
    return this.propStringArray;
  }

  public List<String> getPropStringList() {
    return this.propStringList;
  }

  public Set<String> getPropStringSet() {
    return this.propStringSet;
  }

  public SortedSet<String> getPropStringSortedSet() {
    return this.propStringSortedSet;
  }

  public URI getPropURI() {
    return this.propURI;
  }

  public URI[] getPropURIArray() {
    return this.propURIArray;
  }

  public List<URI> getPropURIList() {
    return this.propURIList;
  }

  public Set<URI> getPropURISet() {
    return this.propURISet;
  }

  public SortedSet<URI> getPropURISortedSet() {
    return this.propURISortedSet;
  }

  public URL getPropURL() {
    return this.propURL;
  }

  public URL[] getPropURLArray() {
    return this.propURLArray;
  }

  public List<URL> getPropURLList() {
    return this.propURLList;
  }

  public Set<URL> getPropURLSet() {
    return this.propURLSet;
  }

  public XMLGregorianCalendar getPropXMLGregorianCalendar() {
    return this.propXMLGregorianCalendar;
  }

  public XMLGregorianCalendar[] getPropXMLGregorianCalendarArray() {
    return this.propXMLGregorianCalendarArray;
  }

  public List<XMLGregorianCalendar> getPropXMLGregorianCalendarList() {
    return this.propXMLGregorianCalendarList;
  }

  public Set<XMLGregorianCalendar> getPropXMLGregorianCalendarSet() {
    return this.propXMLGregorianCalendarSet;
  }

  public NonXmlComplexType getPropXmlJavaTypeAdapter() {
    return this.propXmlJavaTypeAdapter;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + Arrays.hashCode(this.propBigDecimalArray);
    result = (prime * result) + Arrays.hashCode(this.propBigIntegerArray);
    result = (prime * result) + Arrays.hashCode(this.propBooleanArray);
    result = (prime * result) + Arrays.hashCode(this.propBooleanPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propByteArray);
    result = (prime * result) + Arrays.hashCode(this.propBytePrimArray);
    result = (prime * result) + Arrays.hashCode(this.propCalendarArray);
    result = (prime * result) + Arrays.hashCode(this.propCharacterArray);
    result = (prime * result) + Arrays.hashCode(this.propCharacterPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propDateArray);
    result = (prime * result) + Arrays.hashCode(this.propDoubleArray);
    result = (prime * result) + Arrays.hashCode(this.propDoublePrimArray);
    result = (prime * result) + Arrays.hashCode(this.propFileArray);
    result = (prime * result) + Arrays.hashCode(this.propFloatArray);
    result = (prime * result) + Arrays.hashCode(this.propFloatPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propIntegerArray);
    result = (prime * result) + Arrays.hashCode(this.propIntegerPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propLongArray);
    result = (prime * result) + Arrays.hashCode(this.propLongPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propShortArray);
    result = (prime * result) + Arrays.hashCode(this.propShortPrimArray);
    result = (prime * result) + Arrays.hashCode(this.propStringArray);
    result = (prime * result) + Arrays.hashCode(this.propURIArray);
    result = (prime * result) + Arrays.hashCode(this.propURLArray);
    result = (prime * result) + Arrays.hashCode(this.propXMLGregorianCalendarArray);
    result = (prime * result) + Objects.hash(this.propBigDecimal, this.propBigDecimalList,
        this.propBigDecimalSet, this.propBigDecimalSortedSet, this.propBigInteger,
        this.propBigIntegerList, this.propBigIntegerSet, this.propBigIntegerSortedSet,
        this.propBoolean, this.propBooleanList, this.propBooleanPrim, this.propBooleanSet,
        this.propBooleanSortedSet, this.propByte, this.propByteList, this.propBytePrim,
        this.propByteSet, this.propByteSortedSet, this.propCalendar, this.propCalendarList,
        this.propCalendarSet, this.propCalendarSortedSet, this.propCharacter,
        this.propCharacterList, this.propCharacterPrim, this.propCharacterSet,
        this.propCharacterSortedSet, this.propDate, this.propDateList, this.propDateSet,
        this.propDateSortedSet, this.propDouble, this.propDoubleList, this.propDoublePrim,
        this.propDoubleSet, this.propDoubleSortedSet, this.propFile, this.propFileList,
        this.propFileSet, this.propFileSortedSet, this.propFloat, this.propFloatList,
        this.propFloatPrim, this.propFloatSet, this.propFloatSortedSet, this.propInteger,
        this.propIntegerList, this.propIntegerPrim, this.propIntegerSet, this.propIntegerSortedSet,
        this.propLong, this.propLongList, this.propLongPrim, this.propLongSet,
        this.propLongSortedSet, this.propShort, this.propShortList, this.propShortPrim,
        this.propShortSet, this.propShortSortedSet, this.propString, this.propStringList,
        this.propStringSet, this.propStringSortedSet, this.propURI, this.propURIList,
        this.propURISet, this.propURISortedSet, this.propURL, this.propURLList, this.propURLSet,
        this.propXMLGregorianCalendar, this.propXMLGregorianCalendarList,
        this.propXMLGregorianCalendarSet, this.propXmlJavaTypeAdapter);
    return result;
  }

  public boolean isAttrBOOLEAN() {
    return this.propBooleanPrim;
  }

  public boolean isPropBooleanPrim() {
    return this.propBooleanPrim;
  }

  public void setPropBigDecimal(final BigDecimal propBigDecimal) {
    this.propBigDecimal = propBigDecimal;
  }

  public void setPropBigDecimalArray(final BigDecimal[] propBigDecimalArray) {
    this.propBigDecimalArray = propBigDecimalArray;
  }

  public void setPropBigDecimalList(final List<BigDecimal> propBigDecimalList) {
    this.propBigDecimalList = propBigDecimalList;
  }

  public void setPropBigDecimalSet(final Set<BigDecimal> propBigDecimalSet) {
    this.propBigDecimalSet = propBigDecimalSet;
  }

  public void setPropBigDecimalSortedSet(final SortedSet<BigDecimal> propBigDecimalSortedSet) {
    this.propBigDecimalSortedSet = propBigDecimalSortedSet;
  }

  public void setPropBigInteger(final BigInteger propBigInteger) {
    this.propBigInteger = propBigInteger;
  }

  public void setPropBigIntegerArray(final BigInteger[] propBigIntegerArray) {
    this.propBigIntegerArray = propBigIntegerArray;
  }

  public void setPropBigIntegerList(final List<BigInteger> propBigIntegerList) {
    this.propBigIntegerList = propBigIntegerList;
  }

  public void setPropBigIntegerSet(final Set<BigInteger> propBigIntegerSet) {
    this.propBigIntegerSet = propBigIntegerSet;
  }

  public void setPropBigIntegerSortedSet(final SortedSet<BigInteger> propBigIntegerSortedSet) {
    this.propBigIntegerSortedSet = propBigIntegerSortedSet;
  }

  public void setPropBoolean(final Boolean propBoolean) {
    this.propBoolean = propBoolean;
  }

  public void setPropBooleanArray(final Boolean[] propBooleanArray) {
    this.propBooleanArray = propBooleanArray;
  }

  public void setPropBooleanList(final List<Boolean> propBooleanList) {
    this.propBooleanList = propBooleanList;
  }

  public void setPropBooleanPrim(final boolean propBooleanPrim) {
    this.propBooleanPrim = propBooleanPrim;
  }

  public void setPropBooleanPrimArray(final boolean[] propBooleanPrimArray) {
    this.propBooleanPrimArray = propBooleanPrimArray;
  }

  public void setPropBooleanSet(final Set<Boolean> propBooleanSet) {
    this.propBooleanSet = propBooleanSet;
  }

  public void setPropBooleanSortedSet(final SortedSet<Boolean> propBooleanSortedSet) {
    this.propBooleanSortedSet = propBooleanSortedSet;
  }

  public void setPropByte(final Byte propByte) {
    this.propByte = propByte;
  }

  public void setPropByteArray(final Byte[] propByteArray) {
    this.propByteArray = propByteArray;
  }

  public void setPropByteList(final List<Byte> propByteList) {
    this.propByteList = propByteList;
  }

  public void setPropBytePrim(final byte propBytePrim) {
    this.propBytePrim = propBytePrim;
  }

  public void setPropBytePrimArray(final byte[] propBytePrimArray) {
    this.propBytePrimArray = propBytePrimArray;
  }

  public void setPropByteSet(final Set<Byte> propByteSet) {
    this.propByteSet = propByteSet;
  }

  public void setPropByteSortedSet(final SortedSet<Byte> propByteSortedSet) {
    this.propByteSortedSet = propByteSortedSet;
  }

  public void setPropCalendar(final Calendar propCalendar) {
    this.propCalendar = propCalendar;
  }

  public void setPropCalendarArray(final Calendar[] propCalendarArray) {
    this.propCalendarArray = propCalendarArray;
  }

  public void setPropCalendarList(final List<Calendar> propCalendarList) {
    this.propCalendarList = propCalendarList;
  }

  public void setPropCalendarSet(final Set<Calendar> propCalendarSet) {
    this.propCalendarSet = propCalendarSet;
  }

  public void setPropCalendarSortedSet(final SortedSet<Calendar> propCalendarSortedSet) {
    this.propCalendarSortedSet = propCalendarSortedSet;
  }

  public void setPropCharacter(final Character propCharacter) {
    this.propCharacter = propCharacter;
  }

  public void setPropCharacterArray(final Character[] propCharacterArray) {
    this.propCharacterArray = propCharacterArray;
  }

  public void setPropCharacterList(final List<Character> propCharacterList) {
    this.propCharacterList = propCharacterList;
  }

  public void setPropCharacterPrim(final char propCharacterPrim) {
    this.propCharacterPrim = propCharacterPrim;
  }

  public void setPropCharacterPrimArray(final char[] propCharacterPrimArray) {
    this.propCharacterPrimArray = propCharacterPrimArray;
  }

  public void setPropCharacterSet(final Set<Character> propCharacterSet) {
    this.propCharacterSet = propCharacterSet;
  }

  public void setPropCharacterSortedSet(final SortedSet<Character> propCharacterSortedSet) {
    this.propCharacterSortedSet = propCharacterSortedSet;
  }

  public void setPropDate(final Date propDate) {
    this.propDate = propDate;
  }

  public void setPropDateArray(final Date[] propDateArray) {
    this.propDateArray = propDateArray;
  }

  public void setPropDateList(final List<Date> propDateList) {
    this.propDateList = propDateList;
  }

  public void setPropDateSet(final Set<Date> propDateSet) {
    this.propDateSet = propDateSet;
  }

  public void setPropDateSortedSet(final SortedSet<Date> propDateSortedSet) {
    this.propDateSortedSet = propDateSortedSet;
  }

  public void setPropDouble(final Double propDouble) {
    this.propDouble = propDouble;
  }

  public void setPropDoubleArray(final Double[] propDoubleArray) {
    this.propDoubleArray = propDoubleArray;
  }

  public void setPropDoubleList(final List<Double> propDoubleList) {
    this.propDoubleList = propDoubleList;
  }

  public void setPropDoublePrim(final double propDoublePrim) {
    this.propDoublePrim = propDoublePrim;
  }

  public void setPropDoublePrimArray(final double[] propDoublePrimArray) {
    this.propDoublePrimArray = propDoublePrimArray;
  }

  public void setPropDoubleSet(final Set<Double> propDoubleSet) {
    this.propDoubleSet = propDoubleSet;
  }

  public void setPropDoubleSortedSet(final SortedSet<Double> propDoubleSortedSet) {
    this.propDoubleSortedSet = propDoubleSortedSet;
  }

  public void setPropFile(final File propFile) {
    this.propFile = propFile;
  }

  public void setPropFileArray(final File[] propFileArray) {
    this.propFileArray = propFileArray;
  }

  public void setPropFileList(final List<File> propFileList) {
    this.propFileList = propFileList;
  }

  public void setPropFileSet(final Set<File> propFileSet) {
    this.propFileSet = propFileSet;
  }

  public void setPropFileSortedSet(final SortedSet<File> propFileSortedSet) {
    this.propFileSortedSet = propFileSortedSet;
  }

  public void setPropFloat(final Float propFloat) {
    this.propFloat = propFloat;
  }

  public void setPropFloatArray(final Float[] propFloatArray) {
    this.propFloatArray = propFloatArray;
  }

  public void setPropFloatList(final List<Float> propFloatList) {
    this.propFloatList = propFloatList;
  }

  public void setPropFloatPrim(final float propFloatPrim) {
    this.propFloatPrim = propFloatPrim;
  }

  public void setPropFloatPrimArray(final float[] propFloatPrimArray) {
    this.propFloatPrimArray = propFloatPrimArray;
  }

  public void setPropFloatSet(final Set<Float> propFloatSet) {
    this.propFloatSet = propFloatSet;
  }

  public void setPropFloatSortedSet(final SortedSet<Float> propFloatSortedSet) {
    this.propFloatSortedSet = propFloatSortedSet;
  }

  public void setPropInteger(final Integer propInteger) {
    this.propInteger = propInteger;
  }

  public void setPropIntegerArray(final Integer[] propIntegerArray) {
    this.propIntegerArray = propIntegerArray;
  }

  public void setPropIntegerList(final List<Integer> propIntegerList) {
    this.propIntegerList = propIntegerList;
  }

  public void setPropIntegerPrim(final int propIntegerPrim) {
    this.propIntegerPrim = propIntegerPrim;
  }

  public void setPropIntegerPrimArray(final int[] propIntegerPrimArray) {
    this.propIntegerPrimArray = propIntegerPrimArray;
  }

  public void setPropIntegerSet(final Set<Integer> propIntegerSet) {
    this.propIntegerSet = propIntegerSet;
  }

  public void setPropIntegerSortedSet(final SortedSet<Integer> propIntegerSortedSet) {
    this.propIntegerSortedSet = propIntegerSortedSet;
  }

  public void setPropLong(final Long propLong) {
    this.propLong = propLong;
  }

  public void setPropLongArray(final Long[] propLongArray) {
    this.propLongArray = propLongArray;
  }

  public void setPropLongList(final List<Long> propLongList) {
    this.propLongList = propLongList;
  }

  public void setPropLongPrim(final long propLongPrim) {
    this.propLongPrim = propLongPrim;
  }

  public void setPropLongPrimArray(final long[] propLongPrimArray) {
    this.propLongPrimArray = propLongPrimArray;
  }

  public void setPropLongSet(final Set<Long> propLongSet) {
    this.propLongSet = propLongSet;
  }

  public void setPropLongSortedSet(final SortedSet<Long> propLongSortedSet) {
    this.propLongSortedSet = propLongSortedSet;
  }

  public void setPropShort(final Short propShort) {
    this.propShort = propShort;
  }

  public void setPropShortArray(final Short[] propShortArray) {
    this.propShortArray = propShortArray;
  }

  public void setPropShortList(final List<Short> propShortList) {
    this.propShortList = propShortList;
  }

  public void setPropShortPrim(final short propShortPrim) {
    this.propShortPrim = propShortPrim;
  }

  public void setPropShortPrimArray(final short[] propShortPrimArray) {
    this.propShortPrimArray = propShortPrimArray;
  }

  public void setPropShortSet(final Set<Short> propShortSet) {
    this.propShortSet = propShortSet;
  }

  public void setPropShortSortedSet(final SortedSet<Short> propShortSortedSet) {
    this.propShortSortedSet = propShortSortedSet;
  }

  public void setPropString(final String propString) {
    this.propString = propString;
  }

  public void setPropStringArray(final String[] propStringArray) {
    this.propStringArray = propStringArray;
  }

  public void setPropStringList(final List<String> propStringList) {
    this.propStringList = propStringList;
  }

  public void setPropStringSet(final Set<String> propStringSet) {
    this.propStringSet = propStringSet;
  }

  public void setPropStringSortedSet(final SortedSet<String> propStringSortedSet) {
    this.propStringSortedSet = propStringSortedSet;
  }

  public void setPropURI(final URI propURI) {
    this.propURI = propURI;
  }

  public void setPropURIArray(final URI[] propURIArray) {
    this.propURIArray = propURIArray;
  }

  public void setPropURIList(final List<URI> propURIList) {
    this.propURIList = propURIList;
  }

  public void setPropURISet(final Set<URI> propURISet) {
    this.propURISet = propURISet;
  }

  public void setPropURISortedSet(final SortedSet<URI> propURISortedSet) {
    this.propURISortedSet = propURISortedSet;
  }

  public void setPropURL(final URL propURL) {
    this.propURL = propURL;
  }

  public void setPropURLArray(final URL[] propURLArray) {
    this.propURLArray = propURLArray;
  }

  public void setPropURLList(final List<URL> propURLList) {
    this.propURLList = propURLList;
  }

  public void setPropURLSet(final Set<URL> propURLSet) {
    this.propURLSet = propURLSet;
  }

  public void setPropXMLGregorianCalendar(final XMLGregorianCalendar propXMLGregorianCalendar) {
    this.propXMLGregorianCalendar = propXMLGregorianCalendar;
  }

  public void setPropXMLGregorianCalendarArray(
      final XMLGregorianCalendar[] propXMLGregorianCalendarArray) {
    this.propXMLGregorianCalendarArray = propXMLGregorianCalendarArray;
  }

  public void setPropXMLGregorianCalendarList(
      final List<XMLGregorianCalendar> propXMLGregorianCalendarList) {
    this.propXMLGregorianCalendarList = propXMLGregorianCalendarList;
  }

  public void setPropXMLGregorianCalendarSet(
      final Set<XMLGregorianCalendar> propXMLGregorianCalendarSet) {
    this.propXMLGregorianCalendarSet = propXMLGregorianCalendarSet;
  }

  public void setPropXmlJavaTypeAdapter(final NonXmlComplexType propXmlJavaTypeAdapter) {
    this.propXmlJavaTypeAdapter = propXmlJavaTypeAdapter;
  }
}
