/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok.supersub;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Dog extends Animal {

  private String barkSound;

  public Dog() {}

  public Dog(final String barkSound) {
    this.barkSound = barkSound;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Dog other = (Dog) obj;
    return Objects.equals(this.barkSound, other.barkSound);
  }

  public String getBarkSound() {
    return this.barkSound;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.barkSound);
  }

  public void setBarkSound(final String barkSound) {
    this.barkSound = barkSound;
  }
}
