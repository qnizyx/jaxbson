/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class ComplexTypes {

  @XmlJavaTypeAdapter(NonXmlComplexType.AdapterToComplexType.class)
  private NonXmlComplexType propXmlJavaTypeAdapterAsComplexType;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final ComplexTypes other = (ComplexTypes) obj;
    return Objects.equals(this.propXmlJavaTypeAdapterAsComplexType,
        other.propXmlJavaTypeAdapterAsComplexType);
  }

  public NonXmlComplexType getPropXmlJavaTypeAdapterAsComplexType() {
    return this.propXmlJavaTypeAdapterAsComplexType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.propXmlJavaTypeAdapterAsComplexType);
  }

  public void setPropXmlJavaTypeAdapterAsComplexType(
      final NonXmlComplexType propXmlJavaTypeAdapterAsComplexType) {
    this.propXmlJavaTypeAdapterAsComplexType = propXmlJavaTypeAdapterAsComplexType;
  }
}
