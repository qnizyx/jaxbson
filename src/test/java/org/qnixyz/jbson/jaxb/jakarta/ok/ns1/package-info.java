/* This file is derived from its javax equivalent. Do not modify by hand. */
@XmlSchema(namespace = NS1, elementFormDefault = XmlNsForm.QUALIFIED)
package org.qnixyz.jbson.jaxb.jakarta.ok.ns1;

import static org.qnixyz.jbson.jaxb.jakarta.ok.Constants.NS1;
import jakarta.xml.bind.annotation.XmlNsForm;
import jakarta.xml.bind.annotation.XmlSchema;
