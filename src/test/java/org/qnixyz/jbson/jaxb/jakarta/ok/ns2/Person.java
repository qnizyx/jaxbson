/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok.ns2;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;
import org.qnixyz.jbson.jaxb.jakarta.ok.ns1.CrossElementFromNs1;

@XmlType(name = "personNs2")
@XmlAccessorType(XmlAccessType.FIELD)
public class Person {

  private CrossElementFromNs1 crossElementFromNs1;

  @XmlElementRef
  private CrossElementFromNs1 crossElementFromNs1Ref;

  private String hobby;

  @XmlAttribute
  private String yawp;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Person other = (Person) obj;
    return Objects.equals(this.crossElementFromNs1, other.crossElementFromNs1)
        && Objects.equals(this.crossElementFromNs1Ref, other.crossElementFromNs1Ref)
        && Objects.equals(this.hobby, other.hobby) && Objects.equals(this.yawp, other.yawp);
  }

  public CrossElementFromNs1 getCrossElementFromNs1() {
    return this.crossElementFromNs1;
  }

  public CrossElementFromNs1 getCrossElementFromNs1Ref() {
    return this.crossElementFromNs1Ref;
  }

  public String getHobby() {
    return this.hobby;
  }

  public String getYawp() {
    return this.yawp;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.crossElementFromNs1, this.crossElementFromNs1Ref, this.hobby,
        this.yawp);
  }

  public void setCrossElementFromNs1(final CrossElementFromNs1 crossElementFromNs1) {
    this.crossElementFromNs1 = crossElementFromNs1;
  }

  public void setCrossElementFromNs1Ref(final CrossElementFromNs1 crossElementFromNs1Ref) {
    this.crossElementFromNs1Ref = crossElementFromNs1Ref;
  }

  public void setHobby(final String hobby) {
    this.hobby = hobby;
  }

  public void setYawp(final String yawp) {
    this.yawp = yawp;
  }
}
