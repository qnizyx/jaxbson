/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import org.bson.Document;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonConfiguration;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.jaxb.jakarta.ok.AllSimpleTypes;
import org.qnixyz.jbson.jaxb.jakarta.ok.ComplexTypes;
import org.qnixyz.jbson.jaxb.jakarta.ok.NonXmlComplexType;
import org.qnixyz.jbson.jaxb.jakarta.ok.OkRoot;
import org.qnixyz.jbson.jaxb.jakarta.ok.XmlElementRefTest;
import org.qnixyz.jbson.jaxb.jakarta.ok.ns1.CrossElementFromNs1;
import org.qnixyz.jbson.jaxb.jakarta.ok.ns2.CrossElementFromNs2;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.Animal;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.Cat;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.Dog;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementListAnimal;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementListCat;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementListDog;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementRefListAnimal;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementRefListCat;
import org.qnixyz.jbson.jaxb.jakarta.ok.supersub.XmlElementRefListDog;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OkJaxbJaxbsonJakartaTest {

  public static class CreateOkRootOptions {

    private final boolean doBigDecimal;

    private final boolean doIndeterministic;

    private final boolean setValues;

    public CreateOkRootOptions(final boolean setValues, final boolean doBigDecimal,
        final boolean doIndeterministic) {
      this.setValues = setValues;
      this.doBigDecimal = doBigDecimal;
      this.doIndeterministic = doIndeterministic;
    }
  }

  private static final long MS_PER_DAY = 1000 * 60 * 60 * 24;

  private static AllSimpleTypes createAllSimpleTypes(final CreateOkRootOptions opts) {

    final AllSimpleTypes ret = new AllSimpleTypes();
    if (!opts.setValues) {
      return ret;
    }

    if (opts.doBigDecimal) {
      ret.setPropBigDecimal(new BigDecimal(1949.012345678901234567890123456789));
      ret.setPropBigDecimalArray(new BigDecimal[] {new BigDecimal("2.2"), new BigDecimal("3.3")});
      ret.setPropBigDecimalList(
          Arrays.asList(new BigDecimal[] {new BigDecimal("4.4"), new BigDecimal("5.5")}));
      if (opts.doIndeterministic) {
        ret.setPropBigDecimalSet(new HashSet<>(
            Arrays.asList(new BigDecimal[] {new BigDecimal("6.6"), new BigDecimal("7.7")})));
      }
      ret.setPropBigDecimalSortedSet(new TreeSet<>(
          Arrays.asList(new BigDecimal[] {new BigDecimal("8.8"), new BigDecimal("9.9")})));
    }

    ret.setPropBigInteger(BigInteger.valueOf(1L));
    ret.setPropBigIntegerArray(new BigInteger[] {BigInteger.valueOf(2L), BigInteger.valueOf(3L)});
    ret.setPropBigIntegerList(
        Arrays.asList(new BigInteger[] {BigInteger.valueOf(4L), BigInteger.valueOf(5L)}));
    if (opts.doIndeterministic) {
      ret.setPropBigIntegerSet(new HashSet<>(
          Arrays.asList(new BigInteger[] {BigInteger.valueOf(6L), BigInteger.valueOf(7L)})));
    }
    ret.setPropBigIntegerSortedSet(new TreeSet<>(
        Arrays.asList(new BigInteger[] {BigInteger.valueOf(8L), BigInteger.valueOf(9L)})));

    ret.setPropBoolean(true);
    ret.setPropBooleanPrim(false);
    ret.setPropBooleanArray(new Boolean[] {true, false});
    ret.setPropBooleanPrimArray(new boolean[] {false, true});
    ret.setPropBooleanList(Arrays.asList(new Boolean[] {true, false}));
    if (opts.doIndeterministic) {
      ret.setPropBooleanSet(new HashSet<>(Arrays.asList(new Boolean[] {false, true})));
    }
    ret.setPropBooleanSortedSet(new TreeSet<>(Arrays.asList(new Boolean[] {true, false})));

    ret.setPropByte((byte) 0x01);
    ret.setPropBytePrim((byte) 0x02);
    ret.setPropByteArray(new Byte[] {(byte) 0x3, (byte) 0x4});
    ret.setPropBytePrimArray(new byte[] {(byte) 0x5, (byte) 0x6});
    ret.setPropByteList(Arrays.asList(new Byte[] {(byte) 0x7, (byte) 0x8}));
    if (opts.doIndeterministic) {
      ret.setPropByteSet(new HashSet<>(Arrays.asList(new Byte[] {(byte) 0x9, (byte) 0x10})));
    }
    ret.setPropByteSortedSet(new TreeSet<>(Arrays.asList(new Byte[] {(byte) 0x11, (byte) 0x12})));

    ret.setPropCalendar(newCalendar(0 * MS_PER_DAY));
    ret.setPropCalendarArray(
        new Calendar[] {newCalendar(1 * MS_PER_DAY), newCalendar(2 * MS_PER_DAY)});
    ret.setPropCalendarList(
        Arrays.asList(new Calendar[] {newCalendar(3 * MS_PER_DAY), newCalendar(4 * MS_PER_DAY)}));
    if (opts.doIndeterministic) {
      ret.setPropCalendarSet(new HashSet<>(Arrays
          .asList(new Calendar[] {newCalendar(5 * MS_PER_DAY), newCalendar(6 * MS_PER_DAY)})));
    }
    ret.setPropCalendarSortedSet(new TreeSet<>(
        Arrays.asList(new Calendar[] {newCalendar(7 * MS_PER_DAY), newCalendar(8 * MS_PER_DAY)})));

    ret.setPropCharacter('a');
    ret.setPropCharacterPrim('b');
    ret.setPropCharacterArray(new Character[] {'c', 'd'});
    ret.setPropCharacterPrimArray(new char[] {'e', 'f'});
    ret.setPropCharacterList(Arrays.asList(new Character[] {'g', 'h'}));
    if (opts.doIndeterministic) {
      ret.setPropCharacterSet(new HashSet<>(Arrays.asList(new Character[] {'i', 'j'})));
    }
    ret.setPropCharacterSortedSet(new TreeSet<>(Arrays.asList(new Character[] {'k', 'l'})));

    ret.setPropDate(new Date(0 * MS_PER_DAY));
    ret.setPropDateArray(new Date[] {new Date(1 * MS_PER_DAY), new Date(2 * MS_PER_DAY)});
    ret.setPropDateList(
        Arrays.asList(new Date[] {new Date(3 * MS_PER_DAY), new Date(4 * MS_PER_DAY)}));
    if (opts.doIndeterministic) {
      ret.setPropDateSet(new HashSet<>(
          Arrays.asList(new Date[] {new Date(5 * MS_PER_DAY), new Date(6 * MS_PER_DAY)})));
    }
    ret.setPropDateSortedSet(new TreeSet<>(
        Arrays.asList(new Date[] {new Date(7 * MS_PER_DAY), new Date(8 * MS_PER_DAY)})));

    ret.setPropDouble(1.1D);
    ret.setPropDoublePrim(2.2F);
    ret.setPropDoubleArray(new Double[] {3.3D, 4.4D});
    ret.setPropDoublePrimArray(new double[] {5.5D, 6.6D});
    ret.setPropDoubleList(Arrays.asList(new Double[] {7.7D, 8.8D}));
    if (opts.doIndeterministic) {
      ret.setPropDoubleSet(new HashSet<>(Arrays.asList(new Double[] {9.9D, 10.1D})));
    }
    ret.setPropDoubleSortedSet(new TreeSet<>(Arrays.asList(new Double[] {11.11D, 12.12D})));

    ret.setPropFile(new File("file/path/1"));
    ret.setPropFileArray(new File[] {new File("file/path/2"), new File("file/path/3")});
    ret.setPropFileList(
        Arrays.asList(new File[] {new File("file/path/4"), new File("file/path/5")}));
    if (opts.doIndeterministic) {
      ret.setPropFileSet(new HashSet<>(
          Arrays.asList(new File[] {new File("file/path/6"), new File("file/path/7")})));
    }
    ret.setPropFileSortedSet(new TreeSet<>(
        Arrays.asList(new File[] {new File("file/path/8"), new File("file/path/8")})));

    ret.setPropFloat(1.1F);
    ret.setPropFloatPrim(2.2F);
    ret.setPropFloatArray(new Float[] {3.3F, 4.4F});
    ret.setPropFloatPrimArray(new float[] {5.5F, 6.6F});
    ret.setPropFloatList(Arrays.asList(new Float[] {7.7F, 8.8F}));
    ret.setPropFloatSet(new HashSet<>(Arrays.asList(new Float[] {9.9F, 10.1F})));
    ret.setPropFloatSortedSet(new TreeSet<>(Arrays.asList(new Float[] {11.11F, 12.12F})));

    ret.setPropInteger(0x01);
    ret.setPropIntegerPrim(0x02);
    ret.setPropIntegerArray(new Integer[] {0x3, null, 0x4});
    ret.setPropIntegerPrimArray(new int[] {0x5, 0x6});
    ret.setPropIntegerList(Arrays.asList(new Integer[] {0x7, null, 0x8}));
    if (opts.doIndeterministic) {
      ret.setPropIntegerSet(new HashSet<>(Arrays.asList(new Integer[] {0x9, 0x10})));
    }
    ret.setPropIntegerSortedSet(new TreeSet<>(Arrays.asList(new Integer[] {0x11, 0x12})));

    ret.setPropLong(0x01L);
    ret.setPropLongPrim(0x02L);
    ret.setPropLongArray(new Long[] {0x3L, 0x4L});
    ret.setPropLongPrimArray(new long[] {0x5L, 0x6L});
    ret.setPropLongList(Arrays.asList(new Long[] {0x7L, 0x8L}));
    if (opts.doIndeterministic) {
      ret.setPropLongSet(new HashSet<>(Arrays.asList(new Long[] {0x9L, 0x10L})));
    }
    ret.setPropLongSortedSet(new TreeSet<>(Arrays.asList(new Long[] {0x11L, 0x12L})));

    ret.setPropShort((short) 0x01);
    ret.setPropShortPrim((short) 0x02);
    ret.setPropShortArray(new Short[] {(short) 0x3, (short) 0x4});
    ret.setPropShortPrimArray(new short[] {(short) 0x5, (short) 0x6});
    ret.setPropShortList(Arrays.asList(new Short[] {(short) 0x7, (short) 0x8}));
    if (opts.doIndeterministic) {
      ret.setPropShortSet(new HashSet<>(Arrays.asList(new Short[] {(short) 0x9, (short) 0x10})));
    }
    ret.setPropShortSortedSet(
        new TreeSet<>(Arrays.asList(new Short[] {(short) 0x11, (short) 0x12})));

    ret.setPropString("1");
    ret.setPropStringArray(new String[] {"2", "3"});
    ret.setPropStringList(Arrays.asList(new String[] {"4", "5"}));
    if (opts.doIndeterministic) {
      ret.setPropStringSet(new HashSet<>(Arrays.asList(new String[] {"6", "7"})));
    }
    ret.setPropStringSortedSet(new TreeSet<>(Arrays.asList(new String[] {"8", "9"})));

    ret.setPropURI(newURI(1));
    ret.setPropURIArray(new URI[] {newURI(2), null, newURI(3)});
    ret.setPropURIList(Arrays.asList(new URI[] {newURI(4), null, newURI(5)}));
    if (opts.doIndeterministic) {
      ret.setPropURISet(new HashSet<>(Arrays.asList(new URI[] {newURI(6), newURI(7)})));
    }
    ret.setPropURISortedSet(new TreeSet<>(Arrays.asList(new URI[] {newURI(8), newURI(9)})));

    ret.setPropURL(newURL(1));
    ret.setPropURLArray(new URL[] {newURL(2), newURL(3)});
    ret.setPropURLList(Arrays.asList(new URL[] {newURL(4), newURL(5)}));
    if (opts.doIndeterministic) {
      ret.setPropURLSet(new HashSet<>(Arrays.asList(new URL[] {newURL(6), newURL(7)})));
    }

    ret.setPropXMLGregorianCalendar(newXMLGregorianCalendar(0 * MS_PER_DAY));
    ret.setPropXMLGregorianCalendarArray(new XMLGregorianCalendar[] {
        newXMLGregorianCalendar(1 * MS_PER_DAY), null, newXMLGregorianCalendar(2 * MS_PER_DAY)});
    ret.setPropXMLGregorianCalendarList(Arrays.asList(new XMLGregorianCalendar[] {
        newXMLGregorianCalendar(3 * MS_PER_DAY), null, newXMLGregorianCalendar(4 * MS_PER_DAY)}));
    if (opts.doIndeterministic) {
      ret.setPropXMLGregorianCalendarSet(new HashSet<>(Arrays.asList(new XMLGregorianCalendar[] {
          newXMLGregorianCalendar(5 * MS_PER_DAY), newXMLGregorianCalendar(6 * MS_PER_DAY)})));
    }

    ret.setPropXmlJavaTypeAdapter(new NonXmlComplexType("bar", "foo"));

    return ret;
  }

  private static Animal[] createAnimalElementArray(final CreateOkRootOptions opts) {
    final ArrayList<Animal> ret = new ArrayList<>();
    ret.add(new Dog("Woof!"));
    ret.add(new Cat("Meauw..."));
    return ret.toArray(new Animal[] {});
  }

  private static ArrayList<Animal> createAnimalElementList(final CreateOkRootOptions opts) {
    final ArrayList<Animal> ret = new ArrayList<>();
    ret.add(new Dog("Woof!"));
    ret.add(new Cat("Meauw..."));
    return ret;
  }

  private static Map<QName, String> createAnyAttribute(final CreateOkRootOptions opts) {
    final Map<QName, String> ret = new HashMap<>();
    ret.put(new QName("urn:other", "anyAttribute1"), "anyAttributeV1");
    ret.put(new QName("urn:other2", "anyAttribute2"), "anyAttributeV2");
    return ret;
  }

  private static ComplexTypes createComplexTypes(final CreateOkRootOptions opts) {
    final ComplexTypes ret = new ComplexTypes();
    if (opts.setValues) {
      ret.setPropXmlJavaTypeAdapterAsComplexType(
          new NonXmlComplexType("myBarNonXml", "myFooNonXml"));
    }
    return ret;
  }

  public static OkRoot createOkRoot(final CreateOkRootOptions opts) {
    final OkRoot ret = new OkRoot(null);
    ret.setAnyAttribute(createAnyAttribute(opts));
    ret.setAllSimpleTypes(createAllSimpleTypes(opts));
    if (opts.setValues) {
      ret.setAnimalElement(new Dog("Woof!"));
      ret.setAnimalElementRef(new Cat("Meauw..."));
      ret.setAnimalElementList(createAnimalElementList(opts));
      ret.setXmlElementListAnimal(createXmlElementListAnimal(opts));
      ret.setXmlElementRefListAnimal(createXmlElementRefListAnimal(opts));
      ret.setAnimalElementArray(createAnimalElementArray(opts));
      ret.setPersonNs1(createPersonNs1());
      ret.setPersonNs2(createPersonNs2());
      ret.setBsonClashAttribute1("bsonClashAttribute1-value");
      ret.setBsonClashAttribute2("bsonClashAttribute2-value");
      ret.setBsonClashElement1("bsonClashElement1-value");
      ret.setBsonClashElement2("bsonClashElement2-value");
      ret.setXmlElementRefTest(XmlElementRefTest());
    }
    ret.setComplexTypes(createComplexTypes(opts));
    return ret;
  }

  private static org.qnixyz.jbson.jaxb.jakarta.ok.ns1.Person createPersonNs1() {
    final org.qnixyz.jbson.jaxb.jakarta.ok.ns1.Person ret =
        new org.qnixyz.jbson.jaxb.jakarta.ok.ns1.Person();
    ret.setPetName("Banzai");
    ret.setYap("Yap yap yap");
    ret.setCrossElementFromNs2(new CrossElementFromNs2("cross2", "cross2-attr"));
    ret.setCrossElementFromNs2Ref(new CrossElementFromNs2("cross2-ref", "cross2-attr-ref"));
    return ret;
  }

  private static org.qnixyz.jbson.jaxb.jakarta.ok.ns2.Person createPersonNs2() {
    final org.qnixyz.jbson.jaxb.jakarta.ok.ns2.Person ret =
        new org.qnixyz.jbson.jaxb.jakarta.ok.ns2.Person();
    ret.setHobby("Watch tree grow");
    ret.setYawp("I say!");
    ret.setCrossElementFromNs1(new CrossElementFromNs1("cross1", "cross1-attr"));
    ret.setCrossElementFromNs1Ref(new CrossElementFromNs1("cross1-ref", "cross1-attr-ref"));
    return ret;
  }

  private static List<XmlElementListAnimal> createXmlElementListAnimal(
      final CreateOkRootOptions opts) {
    final ArrayList<XmlElementListAnimal> ret = new ArrayList<>();
    ret.add(new XmlElementListDog("Woof!"));
    ret.add(new XmlElementListCat("Meauw..."));
    return ret;
  }

  private static List<XmlElementRefListAnimal> createXmlElementRefListAnimal(
      final CreateOkRootOptions opts) {
    final ArrayList<XmlElementRefListAnimal> ret = new ArrayList<>();
    ret.add(new XmlElementRefListDog("Woof!"));
    ret.add(new XmlElementRefListCat("Meauw..."));
    return ret;
  }

  private static Calendar newCalendar(final long millis) {
    final Calendar ret = Calendar.getInstance();
    ret.setTimeInMillis(millis);
    return ret;
  }

  private static URI newURI(final int i) {
    try {
      return new URI("urn:" + i);
    } catch (final URISyntaxException e) {
      throw new IllegalStateException("Unexpected exception: " + e.getMessage(), e);
    }
  }

  private static URL newURL(final int i) {
    try {
      return new URL("http://host/path/" + i);
    } catch (final MalformedURLException e) {
      throw new IllegalStateException("Unexpected exception: " + e.getMessage(), e);
    }
  }

  private static XMLGregorianCalendar newXMLGregorianCalendar(final long millis) {
    final GregorianCalendar c = new GregorianCalendar();
    c.setTimeInMillis(millis);
    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    } catch (final DatatypeConfigurationException e) {
      throw new IllegalStateException("Unexpected exception: " + e.getMessage(), e);
    }
  }

  private static XmlElementRefTest XmlElementRefTest() {
    final XmlElementRefTest ret = new XmlElementRefTest();
    ret.setAnimalElementRef(new Dog("Woof! Ref!"));
    return ret;
  }

  @Test
  public void test() throws Exception {
    final OkRoot objOrig =
        OkJaxbJaxbsonJakartaTest.createOkRoot(new CreateOkRootOptions(true, true, false));
    objOrig.setId("qnixyz");
    final JaxBsonConfiguration cfg =
        new JaxBsonConfiguration.Builder().allowSmartTypeField(false).build();
    final JaxBsonContext ctx = JaxBsonContext.newInstance(cfg, OkRoot.class);
    final Document bsonOrig = ctx.toBson(objOrig, true);
    final OkRoot objBack = (OkRoot) ctx.toObject(bsonOrig);
    assertEquals(objOrig, objBack);
  }
}
