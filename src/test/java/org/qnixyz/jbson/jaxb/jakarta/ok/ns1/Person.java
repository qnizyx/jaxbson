/* This file is derived from its javax equivalent. Do not modify by hand. */
package org.qnixyz.jbson.jaxb.jakarta.ok.ns1;

import java.util.Objects;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;
import org.qnixyz.jbson.jaxb.jakarta.ok.Constants;
import org.qnixyz.jbson.jaxb.jakarta.ok.ns2.CrossElementFromNs2;

@XmlType(name = "personNs1")
@XmlAccessorType(XmlAccessType.FIELD)
public class Person {

  @XmlElement
  private CrossElementFromNs2 crossElementFromNs2;

  @XmlElementRef
  private CrossElementFromNs2 crossElementFromNs2Ref;

  private String petName;

  @XmlAttribute(namespace = Constants.NS1)
  private String yap;

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Person other = (Person) obj;
    return Objects.equals(this.crossElementFromNs2, other.crossElementFromNs2)
        && Objects.equals(this.crossElementFromNs2Ref, other.crossElementFromNs2Ref)
        && Objects.equals(this.petName, other.petName) && Objects.equals(this.yap, other.yap);
  }

  public CrossElementFromNs2 getCrossElementFromNs2() {
    return this.crossElementFromNs2;
  }

  public CrossElementFromNs2 getCrossElementFromNs2Ref() {
    return this.crossElementFromNs2Ref;
  }

  public String getPetName() {
    return this.petName;
  }

  public String getYap() {
    return this.yap;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.crossElementFromNs2, this.crossElementFromNs2Ref, this.petName,
        this.yap);
  }

  public void setCrossElementFromNs2(final CrossElementFromNs2 crossElementFromNs2) {
    this.crossElementFromNs2 = crossElementFromNs2;
  }

  public void setCrossElementFromNs2Ref(final CrossElementFromNs2 crossElementFromNs2Ref) {
    this.crossElementFromNs2Ref = crossElementFromNs2Ref;
  }

  public void setPetName(final String petName) {
    this.petName = petName;
  }

  public void setYap(final String yap) {
    this.yap = yap;
  }
}
