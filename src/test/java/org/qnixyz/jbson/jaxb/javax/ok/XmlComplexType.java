package org.qnixyz.jbson.jaxb.javax.ok;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class XmlComplexType {

  private String barXml;

  private String fooXml;

  @SuppressWarnings("unused")
  private XmlComplexType() {}

  public XmlComplexType(final String barXml, final String fooXml) {
    this.barXml = barXml;
    this.fooXml = fooXml;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final XmlComplexType other = (XmlComplexType) obj;
    return Objects.equals(this.barXml, other.barXml) && Objects.equals(this.fooXml, other.fooXml);
  }

  public String getBarXml() {
    return this.barXml;
  }

  public String getFooXml() {
    return this.fooXml;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.barXml, this.fooXml);
  }
}
