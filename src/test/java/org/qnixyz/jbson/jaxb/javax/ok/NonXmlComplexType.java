package org.qnixyz.jbson.jaxb.javax.ok;

import java.util.Objects;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class NonXmlComplexType {

  public static class AdapterToComplexType extends XmlAdapter<XmlComplexType, NonXmlComplexType> {

    private AdapterToComplexType() {}

    @Override
    public XmlComplexType marshal(final NonXmlComplexType v) throws Exception {
      if (v == null) {
        return null;
      }
      return new XmlComplexType(v.barNonXml, v.fooNonXml);
    }

    @Override
    public NonXmlComplexType unmarshal(final XmlComplexType v) throws Exception {
      if (v == null) {
        return null;
      }
      return new NonXmlComplexType(v.getBarXml(), v.getFooXml());
    }
  }

  public static class AdapterToSimpleType extends XmlAdapter<String, NonXmlComplexType> {

    private AdapterToSimpleType() {}

    @Override
    public String marshal(final NonXmlComplexType v) throws Exception {
      if (v == null) {
        return null;
      }
      final StringBuilder sb = new StringBuilder();
      if (v.barNonXml != null) {
        sb.append(v.barNonXml);
      }
      sb.append(":");
      if (v.fooNonXml != null) {
        sb.append(v.fooNonXml);
      }
      return sb.toString();
    }

    @Override
    public NonXmlComplexType unmarshal(final String v) throws Exception {
      if (v == null) {
        return null;
      }
      final NonXmlComplexType ret = new NonXmlComplexType();
      final String[] split = v.split(":", 2);
      if (split.length > 0) {
        ret.barNonXml = split[0];
      }
      if (split.length > 1) {
        ret.fooNonXml = split[1];
      }
      return ret;
    }
  }

  private String barNonXml;

  private String fooNonXml;

  private NonXmlComplexType() {}

  public NonXmlComplexType(final String barNonXml, final String fooNonXml) {
    this.barNonXml = barNonXml;
    this.fooNonXml = fooNonXml;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final NonXmlComplexType other = (NonXmlComplexType) obj;
    return Objects.equals(this.barNonXml, other.barNonXml)
        && Objects.equals(this.fooNonXml, other.fooNonXml);
  }

  public String getBarNonXml() {
    return this.barNonXml;
  }

  public String getFooNonXml() {
    return this.fooNonXml;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.barNonXml, this.fooNonXml);
  }
}
