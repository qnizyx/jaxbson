package org.qnixyz.jbson.jaxb.javax.ok;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.namespace.QName;
import org.bson.types.Decimal128;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.annotations.JaxBsonName;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPost;
import org.qnixyz.jbson.annotations.JaxBsonToBsonPre;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPost;
import org.qnixyz.jbson.annotations.JaxBsonToObjectPre;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMapping;
import org.qnixyz.jbson.annotations.JaxBsonXmlAnyAttributeMappings;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.Animal;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.Cat;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.Dog;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementListAnimal;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementListCat;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementListDog;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementRefListAnimal;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementRefListCat;
import org.qnixyz.jbson.jaxb.javax.ok.supersub.XmlElementRefListDog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@XmlRootElement
@JaxBsonName(name = "OkRoot")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(value = {Cat.class, Dog.class})
public class OkRoot {

  private static final Logger LOG = LoggerFactory.getLogger(OkRoot.class);

  private AllSimpleTypes allSimpleTypes;

  @XmlElements(value = { //
      @XmlElement(name = "catXmlElement", type = Cat.class), //
      @XmlElement(name = "dogXmlElement", type = Dog.class), //
  })
  private Animal animalElement;

  @XmlElementWrapper
  @XmlElements(value = { //
      @XmlElement(name = "catXmlElementArray", type = Cat.class), //
      @XmlElement(name = "dogXmlElementArray", type = Dog.class), //
  })
  private Animal[] animalElementArray;

  @XmlElementWrapper
  @XmlElements(value = { //
      @XmlElement(name = "catXmlElementList", type = Cat.class), //
      @XmlElement(name = "dogXmlElementList", type = Dog.class), //
  })
  private ArrayList<Animal> animalElementList;

  @XmlElementRefs(value = { //
      @XmlElementRef(type = Cat.class), //
      @XmlElementRef(type = Dog.class), //
  })
  private Animal animalElementRef;

  @XmlAnyAttribute
  @JaxBsonXmlAnyAttributeMappings(mappings = { //
      @JaxBsonXmlAnyAttributeMapping(bsonPrefix = "", namespaceURI = "urn:other"), //
      @JaxBsonXmlAnyAttributeMapping(bsonPrefix = "o2:", namespaceURI = "urn:other2"), //
  })
  private Map<QName, String> anyAttribute;

  @JaxBsonName(name = "bsonClashAttribute1")
  @XmlAttribute(name = "bsonClash")
  private String bsonClashAttribute1;

  @JaxBsonName(name = "bsonClashAttribute2")
  @XmlAttribute(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashAttribute2;

  @JaxBsonName(name = "bsonClashElement1")
  @XmlElement(name = "bsonClash")
  private String bsonClashElement1;

  @JaxBsonName(name = "bsonClashElement2")
  @XmlElement(name = "bsonClash", namespace = "http://www.qnixyz.org/ns2")
  private String bsonClashElement2;

  private ComplexTypes complexTypes;

  @XmlElementWrapper(name = "emptyBigIntegerListElements")
  private final List<BigInteger> emptyBigIntegerList = new ArrayList<>();

  @XmlElementWrapper(name = "emptyComplexTypeListElements")
  private final List<Dog> emptyComplexTypeList = new ArrayList<>();

  @XmlElementWrapper(name = "emptyDecimal128ListElements")
  private final List<Decimal128> emptyDecimal128List = new ArrayList<>();

  @XmlElementWrapper(name = "emptyIntegerListElements")
  private final List<Integer> emptyIntegerList = new ArrayList<>();

  @XmlElementWrapper(name = "emptyStringListElements")
  private final List<String> emptyStringList = new ArrayList<>();

  @JaxBsonName(name = "_id") // Bson and XML name are different
  @XmlAttribute
  private String id;

  private org.qnixyz.jbson.jaxb.javax.ok.ns1.Person personNs1;

  private org.qnixyz.jbson.jaxb.javax.ok.ns2.Person personNs2;

  @XmlElementWrapper
  @XmlElements(value = { //
      @XmlElement(name = "xmlElementListCat", type = XmlElementListCat.class), //
      @XmlElement(name = "xmlElementListDog", type = XmlElementListDog.class), //
  })
  private List<XmlElementListAnimal> xmlElementListAnimal;

  @XmlElementWrapper
  @XmlElementRefs(value = { //
      @XmlElementRef(type = XmlElementRefListCat.class), //
      @XmlElementRef(type = XmlElementRefListDog.class), //
  })
  private List<XmlElementRefListAnimal> xmlElementRefListAnimal;

  private XmlElementRefTest xmlElementRefTest;

  @SuppressWarnings("unused")
  private OkRoot() {}

  public OkRoot(final String bogus) {}

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final OkRoot other = (OkRoot) obj;
    return Objects.equals(this.allSimpleTypes, other.allSimpleTypes)
        && Objects.equals(this.animalElement, other.animalElement)
        && Arrays.equals(this.animalElementArray, other.animalElementArray)
        && Objects.equals(this.animalElementList, other.animalElementList)
        && Objects.equals(this.animalElementRef, other.animalElementRef)
        && Objects.equals(this.anyAttribute, other.anyAttribute)
        && Objects.equals(this.bsonClashAttribute1, other.bsonClashAttribute1)
        && Objects.equals(this.bsonClashAttribute2, other.bsonClashAttribute2)
        && Objects.equals(this.bsonClashElement1, other.bsonClashElement1)
        && Objects.equals(this.bsonClashElement2, other.bsonClashElement2)
        && Objects.equals(this.complexTypes, other.complexTypes)
        && Objects.equals(this.emptyBigIntegerList, other.emptyBigIntegerList)
        && Objects.equals(this.emptyComplexTypeList, other.emptyComplexTypeList)
        && Objects.equals(this.emptyDecimal128List, other.emptyDecimal128List)
        && Objects.equals(this.emptyIntegerList, other.emptyIntegerList)
        && Objects.equals(this.emptyStringList, other.emptyStringList)
        && Objects.equals(this.id, other.id) && Objects.equals(this.personNs1, other.personNs1)
        && Objects.equals(this.personNs2, other.personNs2)
        && Objects.equals(this.xmlElementListAnimal, other.xmlElementListAnimal)
        && Objects.equals(this.xmlElementRefListAnimal, other.xmlElementRefListAnimal)
        && Objects.equals(this.xmlElementRefTest, other.xmlElementRefTest);
  }

  public AllSimpleTypes getAllSimpleTypes() {
    return this.allSimpleTypes;
  }

  public Animal getAnimalElement() {
    return this.animalElement;
  }

  public Animal[] getAnimalElementArray() {
    return this.animalElementArray;
  }

  public List<Animal> getAnimalElementList() {
    return this.animalElementList;
  }

  public Animal getAnimalElementRef() {
    return this.animalElementRef;
  }

  public Map<QName, String> getAnyAttribute() {
    return this.anyAttribute;
  }

  public String getBsonClashAttribute1() {
    return this.bsonClashAttribute1;
  }

  public String getBsonClashAttribute2() {
    return this.bsonClashAttribute2;
  }

  public String getBsonClashElement1() {
    return this.bsonClashElement1;
  }

  public String getBsonClashElement2() {
    return this.bsonClashElement2;
  }

  public ComplexTypes getComplexTypes() {
    return this.complexTypes;
  }

  public String getId() {
    return this.id;
  }

  public org.qnixyz.jbson.jaxb.javax.ok.ns1.Person getPersonNs1() {
    return this.personNs1;
  }

  public org.qnixyz.jbson.jaxb.javax.ok.ns2.Person getPersonNs2() {
    return this.personNs2;
  }

  public XmlElementRefTest getXmlElementRefTest() {
    return this.xmlElementRefTest;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + Arrays.hashCode(this.animalElementArray);
    result = (prime * result) + Objects.hash(this.allSimpleTypes, this.animalElement,
        this.animalElementList, this.animalElementRef, this.anyAttribute, this.bsonClashAttribute1,
        this.bsonClashAttribute2, this.bsonClashElement1, this.bsonClashElement2, this.complexTypes,
        this.emptyBigIntegerList, this.emptyComplexTypeList, this.emptyDecimal128List,
        this.emptyIntegerList, this.emptyStringList, this.id, this.personNs1, this.personNs2,
        this.xmlElementListAnimal, this.xmlElementRefListAnimal, this.xmlElementRefTest);
    return result;
  }

  public void setAllSimpleTypes(final AllSimpleTypes allSimpleTypes) {
    this.allSimpleTypes = allSimpleTypes;
  }

  public void setAnimalElement(final Animal animal) {
    this.animalElement = animal;
  }

  public void setAnimalElementArray(final Animal[] animalElementArray) {
    this.animalElementArray = animalElementArray;
  }

  public void setAnimalElementList(final ArrayList<Animal> animalElementList) {
    this.animalElementList = animalElementList;
  }

  public void setAnimalElementRef(final Animal animalElementRef) {
    this.animalElementRef = animalElementRef;
  }

  public void setAnyAttribute(final Map<QName, String> anyAttribute) {
    this.anyAttribute = anyAttribute;
  }

  public void setBsonClashAttribute1(final String bsonClashAttribute1) {
    this.bsonClashAttribute1 = bsonClashAttribute1;
  }

  public void setBsonClashAttribute2(final String bsonClashAttribute2) {
    this.bsonClashAttribute2 = bsonClashAttribute2;
  }

  public void setBsonClashElement1(final String bsonClashElement1) {
    this.bsonClashElement1 = bsonClashElement1;
  }

  public void setBsonClashElement2(final String bsonClashElement2) {
    this.bsonClashElement2 = bsonClashElement2;
  }

  public void setComplexTypes(final ComplexTypes complexTypes) {
    this.complexTypes = complexTypes;
  }

  public void setId(final String id) {
    this.id = id;
  }

  public void setPersonNs1(final org.qnixyz.jbson.jaxb.javax.ok.ns1.Person personNs1) {
    this.personNs1 = personNs1;
  }

  public void setPersonNs2(final org.qnixyz.jbson.jaxb.javax.ok.ns2.Person personNs2) {
    this.personNs2 = personNs2;
  }

  public void setXmlElementListAnimal(final List<XmlElementListAnimal> xmlElementListAnimal) {
    this.xmlElementListAnimal = xmlElementListAnimal;
  }

  public void setXmlElementRefListAnimal(
      final List<XmlElementRefListAnimal> xmlElementRefListAnimal) {
    this.xmlElementRefListAnimal = xmlElementRefListAnimal;
  }

  public void setXmlElementRefTest(final XmlElementRefTest xmlElementRefTest) {
    this.xmlElementRefTest = xmlElementRefTest;
  }

  @JaxBsonToBsonPost
  private void toBsonPost() {
    LOG.debug("Called without parameter");
  }

  @JaxBsonToBsonPost
  private void toBsonPost(final JaxBsonContext ctx) {
    LOG.debug("Called with parameter: " + ctx);
  }

  @JaxBsonToBsonPre
  private void toBsonPre() {
    LOG.debug("Called without parameter");
  }

  @JaxBsonToBsonPre
  private void toBsonPre(final JaxBsonContext ctx) {
    LOG.debug("Called with parameter: " + ctx);
  }

  @JaxBsonToObjectPost
  private void toObjectPost() {
    LOG.debug("Called without parameter");
  }

  @JaxBsonToObjectPost
  private void toObjectPost(final JaxBsonContext ctx) {
    LOG.debug("Called with parameter: " + ctx);
  }

  @JaxBsonToObjectPre
  private void toObjectPre() {
    LOG.debug("Called without parameter");
  }

  @JaxBsonToObjectPre
  private void toObjectPre(final JaxBsonContext ctx) {
    LOG.debug("Called with parameter: " + ctx);
  }

  @Override
  public String toString() {
    final Gson gson =
        new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
    return gson.toJson(this);
  }
}
