package org.qnixyz.jbson.jaxb.javax.ok;

public class Constants {

  public static final String NS1 = "http://www.qnixyz.org/ns1";

  public static final String NS2 = "http://www.qnixyz.org/ns2";
}
