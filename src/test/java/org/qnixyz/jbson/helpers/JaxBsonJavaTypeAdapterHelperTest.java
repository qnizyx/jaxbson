package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonJavaTypeAdapter;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonJavaTypeAdapterHelperTest {

  private static class JakartaXmlAdapter
      extends jakarta.xml.bind.annotation.adapters.XmlAdapter<String, String> {

    @Override
    public String marshal(final String v) throws Exception {
      return "JakartaXmlAdapter.marshal." + v;
    }

    @Override
    public String unmarshal(final String v) throws Exception {
      return "JakartaXmlAdapter.unmarshal." + v;
    }
  }

  private static class JavaxXmlAdapter
      extends javax.xml.bind.annotation.adapters.XmlAdapter<String, String> {

    @Override
    public String marshal(final String v) throws Exception {
      return "JavaxXmlAdapter.marshal." + v;
    }

    @Override
    public String unmarshal(final String v) throws Exception {
      return "JavaxXmlAdapter.unmarshal." + v;
    }
  }

  private jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter createJakarta() {
    return new jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter.class;
      }

      @Override
      public Class<?> type() {
        return null;
      }

      @SuppressWarnings("rawtypes")
      @Override
      public Class<? extends jakarta.xml.bind.annotation.adapters.XmlAdapter> value() {
        return JakartaXmlAdapter.class;
      }
    };
  }

  private javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter createJavax() {
    return new javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.class;
      }

      @Override
      public Class<?> type() {
        return null;
      }

      @SuppressWarnings("rawtypes")
      @Override
      public Class<? extends javax.xml.bind.annotation.adapters.XmlAdapter> value() {
        return JavaxXmlAdapter.class;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof JaxBsonJavaTypeAdapter));
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof JaxBsonJavaTypeAdapter));
  }
}
