package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlAnyAttributeHelperTest {

  private jakarta.xml.bind.annotation.XmlAnyAttribute createJakarta() {
    return new jakarta.xml.bind.annotation.XmlAnyAttribute() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlAnyAttribute.class;
      }
    };
  }

  private javax.xml.bind.annotation.XmlAnyAttribute createJavax() {
    return new javax.xml.bind.annotation.XmlAnyAttribute() {
      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlAnyAttribute.class;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlAnyAttributeHelperJakarta));
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlAnyAttributeHelperJavax));
  }
}
