package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.JaxBsonName;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonNameHelperTest {

  @jakarta.xml.bind.annotation.XmlType(name = JAKARTA_XML_TYPE_CLASS)
  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbAttributeJakarta {
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    private final String field = null;
  }

  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbAttributeJavax {
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    private final String field = null;
  }

  @jakarta.xml.bind.annotation.XmlType(name = JAKARTA_XML_TYPE_CLASS)
  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbElementJakarta {
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @jakarta.xml.bind.annotation.XmlElement(name = JAKARTA_XML_ELEMENT_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlElement(name = JAVAX_XML_ELEMENT_FIELD)
    private final String field = null;
  }

  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbElementJavax {
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlElement(name = JAVAX_XML_ELEMENT_FIELD)
    private final String field = null;
  }

  @jakarta.xml.bind.annotation.XmlType(name = JAKARTA_XML_TYPE_CLASS)
  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbElementWrapperJakarta {
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @jakarta.xml.bind.annotation.XmlElement(name = JAKARTA_XML_ELEMENT_FIELD)
    @jakarta.xml.bind.annotation.XmlElementWrapper(name = JAKARTA_XML_ELEMENT_WRAPPER_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlElement(name = JAVAX_XML_ELEMENT_FIELD)
    @javax.xml.bind.annotation.XmlElementWrapper(name = JAVAX_XML_ELEMENT_WRAPPER_FIELD)
    private final String field = null;
  }

  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxbElementWrapperJavax {
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @jakarta.xml.bind.annotation.XmlElement(name = JAKARTA_XML_ELEMENT_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlElement(name = JAVAX_XML_ELEMENT_FIELD)
    @javax.xml.bind.annotation.XmlElementWrapper(name = JAVAX_XML_ELEMENT_WRAPPER_FIELD)
    private final String field = null;
  }

  @JaxBsonName(name = JAX_BSON_NAME_CLASS)
  @jakarta.xml.bind.annotation.XmlType(name = JAKARTA_XML_TYPE_CLASS)
  @javax.xml.bind.annotation.XmlType(name = JAVAX_XML_TYPE_CLASS)
  private static final class NameJaxBson {
    @JaxBsonName(name = JAX_BSON_NAME_FIELD)
    @jakarta.xml.bind.annotation.XmlAttribute(name = JAKARTA_XML_ATTRIBUTE_FIELD)
    @jakarta.xml.bind.annotation.XmlElement(name = JAKARTA_XML_ELEMENT_FIELD)
    @jakarta.xml.bind.annotation.XmlElementWrapper(name = JAKARTA_XML_ELEMENT_WRAPPER_FIELD)
    @javax.xml.bind.annotation.XmlAttribute(name = JAVAX_XML_ATTRIBUTE_FIELD)
    @javax.xml.bind.annotation.XmlElement(name = JAVAX_XML_ELEMENT_FIELD)
    @javax.xml.bind.annotation.XmlElementWrapper(name = JAVAX_XML_ELEMENT_WRAPPER_FIELD)
    private final String field = null;
  }

  private static final class NoName {
    @SuppressWarnings("unused")
    private final String field = null;
  }

  private static final String FIELD = "field";
  private static final String JAKARTA_XML_ATTRIBUTE_FIELD = "JakartaXmlAttributeField";
  private static final String JAKARTA_XML_ELEMENT_FIELD = "JakartaXmlElementField";
  private static final String JAKARTA_XML_ELEMENT_WRAPPER_FIELD = "JakartaXmlElementWrapperField";
  private static final String JAKARTA_XML_TYPE_CLASS = "JakartaXmlTypeClass";
  private static final String JAVAX_XML_ATTRIBUTE_FIELD = "JavaxXmlAttributeField";
  private static final String JAVAX_XML_ELEMENT_FIELD = "JavaxXmlElementField";
  private static final String JAVAX_XML_ELEMENT_WRAPPER_FIELD = "JavaxXmlElementWrapperField";
  private static final String JAVAX_XML_TYPE_CLASS = "JavaxXmlTypeClass";
  private static final String JAX_BSON_NAME_CLASS = "JaxBsonNameClass";
  private static final String JAX_BSON_NAME_FIELD = "JaxBsonNameField";

  @Test
  public void testNameJaxbAttributeJakarta() throws Exception {
    assertEquals(JAKARTA_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbAttributeJakarta.class).name());
    assertEquals(JAKARTA_XML_ATTRIBUTE_FIELD,
        JaxBsonNameHelper.instance(NameJaxbAttributeJakarta.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxbAttributeJavax() throws Exception {
    assertEquals(JAVAX_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbAttributeJavax.class).name());
    assertEquals(JAVAX_XML_ATTRIBUTE_FIELD,
        JaxBsonNameHelper.instance(NameJaxbAttributeJavax.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxbElementJakarta() throws Exception {
    assertEquals(JAKARTA_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbElementJakarta.class).name());
    assertEquals(JAKARTA_XML_ELEMENT_FIELD,
        JaxBsonNameHelper.instance(NameJaxbElementJakarta.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxbElementJavax() throws Exception {
    assertEquals(JAVAX_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbElementJavax.class).name());
    assertEquals(JAVAX_XML_ELEMENT_FIELD,
        JaxBsonNameHelper.instance(NameJaxbElementJavax.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxbElementWrapperJakarta() throws Exception {
    assertEquals(JAKARTA_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbElementWrapperJakarta.class).name());
    assertEquals(JAKARTA_XML_ELEMENT_WRAPPER_FIELD, JaxBsonNameHelper
        .instance(NameJaxbElementWrapperJakarta.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxbElementWrapperJavax() throws Exception {
    assertEquals(JAVAX_XML_TYPE_CLASS,
        JaxBsonNameHelper.instance(NameJaxbElementWrapperJavax.class).name());
    assertEquals(JAVAX_XML_ELEMENT_WRAPPER_FIELD, JaxBsonNameHelper
        .instance(NameJaxbElementWrapperJavax.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNameJaxBson() throws Exception {
    assertEquals(JAX_BSON_NAME_CLASS, JaxBsonNameHelper.instance(NameJaxBson.class).name());
    assertEquals(JAX_BSON_NAME_FIELD,
        JaxBsonNameHelper.instance(NameJaxBson.class.getDeclaredField(FIELD)).name());
  }

  @Test
  public void testNoName() throws Exception {
    assertNull(JaxBsonNameHelper.instance(NoName.class));
    assertNull(JaxBsonNameHelper.instance(NoName.class.getDeclaredField(FIELD)));
  }
}
