package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlElementWrapperHelperTest {

  private jakarta.xml.bind.annotation.XmlElementWrapper createJakarta() {
    return new jakarta.xml.bind.annotation.XmlElementWrapper() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlElementWrapper.class;
      }

      @Override
      public String name() {
        return "nameJakarta";
      }

      @Override
      public String namespace() {
        return "namespaceJakarta";
      }

      @Override
      public boolean nillable() {
        return false;
      }

      @Override
      public boolean required() {
        return false;
      }
    };
  }

  private javax.xml.bind.annotation.XmlElementWrapper createJavax() {
    return new javax.xml.bind.annotation.XmlElementWrapper() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlElementWrapper.class;
      }

      @Override
      public String name() {
        return "nameJavax";
      }

      @Override
      public String namespace() {
        return "namespaceJavax";
      }

      @Override
      public boolean nillable() {
        return false;
      }

      @Override
      public boolean required() {
        return false;
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementWrapperHelperJakarta));
    final XmlElementWrapperHelperJakarta cast = (XmlElementWrapperHelperJakarta) inst;
    assertEquals("nameJakarta", cast.name());
    assertEquals("namespaceJakarta", cast.namespace());
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlElementWrapperHelperJavax));
    final XmlElementWrapperHelperJavax cast = (XmlElementWrapperHelperJavax) inst;
    assertEquals("nameJavax", cast.name());
    assertEquals("namespaceJavax", cast.namespace());
  }
}
