package org.qnixyz.jbson.helpers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.lang.annotation.Annotation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class XmlSeeAlsoHelperTest {

  private jakarta.xml.bind.annotation.XmlSeeAlso createJakarta() {
    return new jakarta.xml.bind.annotation.XmlSeeAlso() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return jakarta.xml.bind.annotation.XmlSeeAlso.class;
      }

      @Override
      public Class<?>[] value() {
        return new Class<?>[] {String.class};
      }
    };
  }

  private javax.xml.bind.annotation.XmlSeeAlso createJavax() {
    return new javax.xml.bind.annotation.XmlSeeAlso() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return javax.xml.bind.annotation.XmlSeeAlso.class;
      }

      @Override
      public Class<?>[] value() {
        return new Class<?>[] {Integer.class};
      }
    };
  }

  @Test
  public void testJakarta() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJakarta());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlSeeAlsoHelperJakarta));
    final XmlSeeAlsoHelperJakarta cast = (XmlSeeAlsoHelperJakarta) inst;
    assertNotNull(cast.value());
    assertEquals(1, cast.value().length);
    assertEquals(String.class, cast.value()[0]);
  }

  @Test
  public void testJavax() throws Exception {
    final Object inst = JaxBsonJaxb.instance(createJavax());
    assertNotNull(inst);
    assertTrue((inst instanceof XmlSeeAlsoHelperJavax));
    final XmlSeeAlsoHelperJavax cast = (XmlSeeAlsoHelperJavax) inst;
    assertNotNull(cast.value());
    assertEquals(1, cast.value().length);
    assertEquals(Integer.class, cast.value()[0]);
  }
}
