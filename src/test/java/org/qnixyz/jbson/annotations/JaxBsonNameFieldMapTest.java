package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNameImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonNameFieldMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonNameFieldMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonNameFieldMap.Builder b = new JaxBsonNameFieldMap.Builder();

    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    b.put(testField1, new JaxBsonNameImpl("new-name"));
    JaxBsonName annotation1 = b.build().get(testField1);
    assertNotNull(annotation1);

    b.remove(testField1);
    annotation1 = b.build().get(testField1);
    assertNull(annotation1);
  }
}
