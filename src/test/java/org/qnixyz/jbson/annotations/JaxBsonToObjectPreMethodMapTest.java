package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.lang.reflect.Method;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonToObjectPreMethodMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonToObjectPreMethodMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonToObjectPreMethodMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonToObjectPreMethodMap.Builder b = new JaxBsonToObjectPreMethodMap.Builder();

    final Method testMethod1 = ReflectiionTestClass.TEST_CLASS
        .getDeclaredMethod(ReflectiionTestClass.METHOD_NAME_1, JaxBsonContext.class);
    b.put(testMethod1, new JaxBsonToObjectPreImpl());
    JaxBsonToObjectPre annotation1 = b.build().get(testMethod1);
    assertNotNull(annotation1);

    b.remove(testMethod1);
    annotation1 = b.build().get(testMethod1);
    assertNull(annotation1);
  }
}
