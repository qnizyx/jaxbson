package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingImpl;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonXmlAnyAttributeMappingsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonXmlAnyAttributeMappingsFieldMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG =
      LoggerFactory.getLogger(JaxBsonXmlAnyAttributeMappingsFieldMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonXmlAnyAttributeMappingsFieldMap.Builder b =
        new JaxBsonXmlAnyAttributeMappingsFieldMap.Builder();

    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    b.put(testField1,
        new JaxBsonXmlAnyAttributeMappingsImpl(new JaxBsonXmlAnyAttributeMappingImpl[] { //
            new JaxBsonXmlAnyAttributeMappingImpl("bsonPrefix", "namespaceURI"), //
        }));
    JaxBsonXmlAnyAttributeMappings annotation1 = b.build().get(testField1);
    assertNotNull(annotation1);

    b.remove(testField1);
    annotation1 = b.build().get(testField1);
    assertNull(annotation1);
  }
}
