package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintFieldMap;
import org.qnixyz.jbson.annotations.cfg.JaxBsonNumberHintImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonNumberHintFieldMapTest {

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonNumberHintFieldMapTest.class);

  @Test
  public void test() throws Exception {
    final JaxBsonNumberHintFieldMap.Builder b = new JaxBsonNumberHintFieldMap.Builder();

    final Field testField1 =
        ReflectiionTestClass.TEST_CLASS.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    b.put(testField1, JaxBsonNumberHintImpl.DEFAULT);
    JaxBsonNumberHint annotation1 = b.build().get(testField1);
    assertNotNull(annotation1);

    b.remove(testField1);
    annotation1 = b.build().get(testField1);
    assertNull(annotation1);
  }
}
