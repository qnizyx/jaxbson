package org.qnixyz.jbson.annotations;

import org.qnixyz.jbson.JaxBsonContext;

public class ReflectiionTestClass {

  private static class TestClass {

    @SuppressWarnings("unused")
    private String testField1;

    @SuppressWarnings("unused")
    private String testField2;

    @SuppressWarnings("unused")
    private void testMethod1(final JaxBsonContext ctx) {}

    @SuppressWarnings("unused")
    private void testMethod2(final JaxBsonContext ctx) {}
  }

  public static final String FIELD_NAME_1 = "testField1";

  public static final String FIELD_NAME_2 = "testField2";

  public static final String METHOD_NAME_1 = "testMethod1";

  public static final String METHOD_NAME_2 = "testMethod2";

  public static final Class<?> TEST_CLASS = TestClass.class;

  public static final String TEST_CLASS_NAME = TestClass.class.getName();
}
