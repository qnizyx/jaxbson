package org.qnixyz.jbson.annotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.lang.reflect.Field;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ReflectiionTest {

  @Test
  public void test() throws Exception {
    final Class<?> testClass = Class.forName(ReflectiionTestClass.TEST_CLASS_NAME);
    assertNotNull(testClass);
    final Field testField1 = testClass.getDeclaredField(ReflectiionTestClass.FIELD_NAME_1);
    assertNotNull(testField1);
    assertEquals(testField1.getName(), ReflectiionTestClass.FIELD_NAME_1);
    final Class<?> cls = testField1.getDeclaringClass();
    assertNotNull(cls);
    assertEquals(cls.getName(), ReflectiionTestClass.TEST_CLASS_NAME);
    final Class<?> cls2 = Class.forName(cls.getName());
    assertNotNull(cls2);
    assertEquals(cls2, testClass);
  }
}
