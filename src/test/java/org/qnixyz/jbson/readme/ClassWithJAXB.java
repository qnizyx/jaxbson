package org.qnixyz.jbson.readme;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import org.qnixyz.jbson.annotations.JaxBsonName;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ClassWithJAXB {

  Date date;

  String foo;

  @JaxBsonName(name = "_id") // NOTE: This is a JaxBson annotation to overwrite the JAXB name
  @XmlAttribute
  String id;
}
