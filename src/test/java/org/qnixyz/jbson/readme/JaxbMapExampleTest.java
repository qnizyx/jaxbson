package org.qnixyz.jbson.readme;

import static org.qnixyz.jbson.impl.Utils.isBlank;
import static org.junit.Assume.assumeTrue;
import static org.qnixyz.jbson.readme.ClassWithJAXBTest.DO_TEST_PROP;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.bson.Document;
import org.bson.json.JsonWriterSettings;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonContext;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxbMapExampleTest {

  @BeforeClass
  public static void beforeMethod() {
    assumeTrue(!isBlank(System.getenv(DO_TEST_PROP))
        && System.getenv(DO_TEST_PROP).equalsIgnoreCase("true") || //
        !isBlank(System.getProperty(DO_TEST_PROP))
            && System.getProperty(DO_TEST_PROP).equalsIgnoreCase("true"));
  }

  @Test
  public void test() throws Exception {

    final JAXBContext jaxbContext = JAXBContext.newInstance(JaxbMapExample.class);
    final Marshaller m = jaxbContext.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    final JaxbMapExample o = new JaxbMapExample();
    o.map.put("key1", "value1");
    o.map.put("key2", "value2");
    m.marshal(o, System.out);

    final JaxBsonContext jaxBsonContext = JaxBsonContext.newInstance(JaxbMapExample.class);
    final Document bson = jaxBsonContext.toBson(o);
    final JsonWriterSettings ws = JsonWriterSettings.builder().indent(true).build();
    System.out.println(bson.toJson(ws));

    final JaxbMapExample oBack = (JaxbMapExample) jaxBsonContext.toObject(bson);
    m.marshal(oBack, System.out);
  }
}
