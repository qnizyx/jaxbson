package org.qnixyz.jbson.readme;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.JaxBsonConfiguration;
import org.qnixyz.jbson.JaxBsonContext;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MarshallJaxBsonConfiguration {

  @Test
  public void test_bxon() throws Exception {
    final JaxBsonContext ctx = JaxBsonContext.newInstance(JaxBsonConfiguration.class);
    ctx.toObject(ctx.toBson(JaxBsonConfiguration.DEFAULT));
  }
}
