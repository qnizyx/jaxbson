package org.qnixyz.jbson;

import static org.hamcrest.MatcherAssert.assertThat;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.bind.Marshaller;
import org.bson.Document;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest;
import org.qnixyz.jbson.jaxb.javax.OkJaxbJaxbsonJavaxTest.CreateOkRootOptions;
import org.qnixyz.jbson.jaxb.javax.OkJaxbTest;
import org.qnixyz.jbson.jaxb.javax.ok.OkRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xmlunit.matchers.CompareMatcher;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JaxBsonContextTest {

  private static final File _DIR = new File("target/test-classes");

  @SuppressWarnings("unused")
  private static final Logger LOG = LoggerFactory.getLogger(JaxBsonContextTest.class);

  private static File T01_OUT_JSON_ACTUAL =
      new File(_DIR, JaxBsonContextTest.class.getSimpleName() + "-1-actual.json");

  private static File T01_OUT_XML_ACTUAL =
      new File(_DIR, JaxBsonContextTest.class.getSimpleName() + "-1-actual.xml");

  private static File T01_OUT_XML_EXPECTED =
      new File(_DIR, JaxBsonContextTest.class.getSimpleName() + "-1-expect.xml");


  @Test
  public void test() throws Exception {

    final Marshaller m = OkJaxbTest.createMarshaller();
    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    final OkRoot objOrig =
        OkJaxbJaxbsonJavaxTest.createOkRoot(new CreateOkRootOptions(true, true, false));
    objOrig.setId("qnixyz");
    m.marshal(objOrig, T01_OUT_XML_EXPECTED);

    final JaxBsonConfiguration cfg =
        new JaxBsonConfiguration.Builder().allowSmartTypeField(false).build();
    final JaxBsonContext ctx = JaxBsonContext.newInstance(cfg, OkRoot.class);
    final Document bson = ctx.toBson(objOrig, true);
    try (OutputStream os = new BufferedOutputStream(new FileOutputStream(T01_OUT_JSON_ACTUAL))) {
      os.write(bson.toJson().getBytes("UTF-8"));
    }

    final OkRoot objBack = (OkRoot) ctx.toObject(bson);
    m.marshal(objBack, T01_OUT_XML_ACTUAL);
    assertThat(T01_OUT_XML_EXPECTED,
        CompareMatcher.isSimilarTo(T01_OUT_XML_ACTUAL).ignoreWhitespace());
  }
}
